#!/usr/bin/env python
"""
extract relations between entities

output ../hex/hex.proto graph pbtxt
the description is the short abstract of the entity
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
import networkx as nx
this_dir = osp.dirname(__file__)
import bz2

sys.path.append(osp.join(this_dir, '../../models/hex/build/caffe/proto/'))
import hex_pb2
import itertools

sys.path.append(osp.join(this_dir, '../label/'))
from label_imagenet import load_vocab, \
    load_list, write_list, list2dict

import random
from collections import defaultdict
from collections import Counter

import re
url_ptn = re.compile(r'.*/([^/]+)\/?>')
def strip_url(uri, line='skip'):
  m = url_ptn.search(uri)
  if m:
    return m.group(1)
  else:
    raise Exception("Invalid uri: %s %s" % (uri, line))

def load_graph(reader, leaves):
  G = nx.DiGraph()

  for idx, line in enumerate(reader):
    try:
      line = line.strip()
      if '^^' in line or '@' in line:
        continue
      fields = line.split()
      fields = [strip_url(f, line) for f in fields[:3]]
      parent = fields[2]
      cls = fields[1]
      child = fields[0]
      G.add_edge(child, parent, rtype=cls)
    except Exception as ioe:
      pass

    if (idx+1) % 1000000 == 0:
      print(idx, G.number_of_edges())

  return G

def load_redirects(reader):
  print('loading redirects')
  res = {}
  for line in reader:
    fields = line.strip().split()
    try:
      res[strip_url(fields[0])] = strip_url(fields[2])
    except Exception as ioe:
      print(ioe)
  return res

import csv
def load_abstract(meta_dir):
  abstract_path = osp.join(meta_dir, 'short_abstracts_en.tql.bz2')
  reader_ = bz2.BZ2File(abstract_path, 'r')
  print("Loading abstract")
  res = {}

  def _cvt_escape():
    for line in reader_:
      yield line.replace(r'\"', '""')

  reader = csv.reader(_cvt_escape(), delimiter=' ')
  for idx, fields in enumerate(reader):
    try:
      entity = strip_url(fields[0])
      res[entity] = fields[2]
    except Exception as ioe:
      print(ioe)

  print("#entities with abstract:", len(res))
  return res

def filter_nodes(G):
  print('remove nodes with too many children')
  good_nodes = []
  for node in G:
    if len(G.predecessors(node)) < 10:
      # too much neighboors create intractable clique in the hex graph
      good_nodes.append(node)
  return G.subgraph(good_nodes)

def remove_circles(G):
  print("remove circles")
  G_c = G.copy()

  def _prune():
    roots = [1]
    leaves = []
    while len(roots) > 0 or len(leaves) > 0:
      roots, leaves = roots_leaves(G_c)
      G_c.remove_nodes_from(roots)
      G_c.remove_nodes_from(leaves)

  _prune()

  print(" start:", G.number_of_edges())

  has_circle = True
  while has_circle:
    has_circle = False
    for circle in nx.simple_cycles(G_c):
      circle.append(circle[0])
      i = random.choice(range(len(circle)-1))
      G.remove_edge(circle[i], circle[i+1])
      G_c.remove_edge(circle[i], circle[i+1])
      _prune()
      has_circle = True
      break

  # in the end G_c becomes empty
  print(" end:", G.number_of_edges())

  return G

def filter_edges(G, threshold=.95):
  print('keep the edge types that account for 95% of all edges')
  rtype_stats = Counter()
  for u, v, d in G.edges_iter(data=True):
    rtype_stats[d['rtype']] += 1

  accum = 0
  total = G.number_of_edges()

  blacklist = ['rdf-schema#seeAlso', 'owl#differentFrom']
  blacklist = set(blacklist)

  rtypes = []
  for rtype, cnt in rtype_stats.most_common():
    if rtype in blacklist:
      total -= cnt
      continue
    rtypes.append(rtype)
    accum += cnt
    if accum / total >= threshold:
      break

  rtypes = set(rtypes)
  print('len(rtypes):', len(rtypes))
  G_new = nx.DiGraph()
  for u, v, d in G.edges_iter(data=True):
    if d['rtype'] in rtypes and u != v:
      G_new.add_edge(u, v, **d)
  return G_new, rtypes

def filter_by_leaf(G, leaves, cutoff=1):
  print('filter by reachablity to leaves')
  ancestors = defaultdict(int)
  for idx, en in enumerate(leaves):
    if en not in G:
      continue
    for node in G.successors(en):
      ancestors[node] += 1

  entities = leaves[:]
  for node, cnt in ancestors.items():
    if cnt >= cutoff:
      entities.append(node)
  return G.subgraph(entities)

def roots_leaves(G):
  roots = []
  leaves = []
  for node, odgree in G.out_degree_iter():
    if odgree == 0:
      leaves.append(node)

  for node, idgree in G.in_degree_iter():
    if idgree == 0:
      roots.append(node)

  return roots, leaves

def normalize_graph(G):
  G0 = G
  G = G.copy()
  print('remove redundant edges')

  roots, leaves = roots_leaves(G)
  for root, leave in itertools.product(roots, leaves):
    flag = True
    while flag:
      try:
        PATH = nx.shortest_path(G, root, leave)
      except:
        break
      invalid_edges = []
      for i in range(len(PATH)-1):
        edge = (PATH[i], PATH[i+1])
        for path in nx.all_simple_paths(G, *edge):
          if len(path) > 2:
            invalid_edges.append(edge)
            break
      G.remove_edges_from(invalid_edges)
      flag = len(invalid_edges) > 0
  print("#edges:", G0.number_of_edges(), G.number_of_edges())
  return G

def load(entities_path, meta_dir = '../link/data/meta-data/'):
  leaves = load_list(entities_path)
  relation_path = osp.join(meta_dir, 'mappingbased_objects_en.tql.bz2')
  #relation_path = osp.join(meta_dir, 'infobox_properties_en.ttl.bz2')
  #redirect_path = osp.join(meta_dir, 'transitive_redirects_en.ttl.bz2')

  resolve_redict = False
  if resolve_redict:
    with bz2.BZ2File(redirect_path, 'r') as reader:
      redirects = load_redirects(reader)

    leaves_ = []
    for leaf in leaves:
      leaves_.append(redirects.get(leaf, leaf))

    leaves_uniq = list(set(leaves_))
    w2i = {w:i for i, w in enumerate(leaves_uniq)}
    with open(entities_path + '.resolve', 'w') as writer:
      for l0, l1 in zip(leaves, leaves_):
        print(l0, l1, w2i[l1], file=writer)
  else:
    leaves_uniq = leaves

  abstract = load_abstract(meta_dir)

  print('loading graph')
  with bz2.BZ2File(relation_path, 'r') as reader:
    G = load_graph(reader, set(leaves))

  return G, leaves_uniq, abstract

def main():
  G, leaves, abstract = load(sys.argv[1], sys.argv[2])
  process(G, leaves, abstract, sys.argv[1]+'.graph')

def process(G, leaves, abstract, save_path):
  G = filter_by_leaf(G, leaves)

  # make sure the index is the same as the leaves
  # for the leaves
  extras = set(G.nodes()) - set(leaves)
  entities = leaves + list(extras)
  vocab = list2dict(entities)
  print("len(extras):", len(extras))

  graph = hex_pb2.Graph()
  graph.num_labels = len(entities)
  graph.num_leaves = len(leaves)
  for entity, idx in vocab.items():
    info = graph.label_infos.add()
    info.index = idx
    info.str_id = entity.decode('utf-8')
    if entity not in abstract:
      abstract[entity] = "No abstract"
    info.description = abstract[entity].decode('utf-8')

  print('output graph')
  for edge in G.edges_iter(data=True):
    _edge = graph.subset_relations.add()
    _edge.parent = vocab[edge[1]]
    _edge.child = vocab[edge[0]]
    _edge.rtype = edge[2]['rtype'].decode('utf-8')

  with open(save_path, 'w') as writer:
    print(str(graph), file=writer)

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
