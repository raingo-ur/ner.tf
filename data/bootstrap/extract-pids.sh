#!/bin/bash
# vim ft=sh

entities=$1
link_dir=$2
link_code=$3

cat $link_dir/yfcc100m_dataset.entities | python $link_code/filter-in.py $entities -2 $'\t' | awk -F'\t' '{print $1,$3}' > $entities.pids
cat $entities.pids | python group_and_sample.py 1000 > $entities.pids.1000
cat $entities.pids | python group_and_sample.py 10 > $entities.pids.10
awk '{print $2}' $entities.pids | sort | uniq -c | sort -nr > $entities.pids.stat
