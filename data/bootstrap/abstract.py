#!/usr/bin/env python
"""
extract abstract given a list of entities
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp

from extract_relations import load_abstract, load_list
import bz2

def main():
  meta_dir = '../link/data/meta-data/'
  abstract = load_abstract(meta_dir)
  writer=open(sys.argv[1]+'.names', 'w')
  for entity in load_list(sys.argv[1]):
    entity = entity.strip()
    if entity in abstract:
      print(entity, abstract[entity], sep='\t', file=writer)
  pass

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
