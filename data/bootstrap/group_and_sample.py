#!/usr/bin/env python
"""
python group_and_sample.py [num, e.g., 500]
given a list of (item key) pairs from stdin
sample based on the key, output at most `num` per key to stdout
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp

from collections import defaultdict
import random
def main():
  max_per_group = int(sys.argv[1])
  key_idx = 1
  try:
    key_idx = int(sys.argv[2])
  except Exception as ioe:
    print(ioe, file=sys.stderr)

  groups = defaultdict(list)
  for line in sys.stdin:
    fields = line.strip().split()
    groups[fields[key_idx]].append(line.strip())

  for key, items in groups.items():
    random.shuffle(items)
    for item in items[:max_per_group]:
      print(item)
  pass

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
