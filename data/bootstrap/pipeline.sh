#!/bin/bash
# vim ft=sh


#base_dir='./data/V1'
entities=$1
if [ ! -f "$entities" ]
then
  echo $0 path-to-entities
  exit
fi
link_dir=$2
if [ -z "$link_dir" ]
then
  link_dir='../link/data/with-thing/'
fi
link_code='../link'
meta_dir=$link_code/data/meta-data/

#./extract-pids.sh $entities $link_dir $link_code
python extract_relations.py $entities $meta_dir
