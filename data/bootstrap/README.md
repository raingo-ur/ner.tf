

## Bootstrap label relations

1. `./pipeline.sh` the entry scripts

`./pipeline.sh $PATH_TO_ENTITY` will generate `$PATH_TO_ENTITY.pids`, consumed by pig scripts to download images from grid, and `$PATH_TO_ENTITY.graph`, consumed by inference algorithm

The `link_dir` in `./pipeline.sh` should be adapted according to the linking result

3. `./extract-pids.sh` sample photos for each valid entities
4. `./extract-relations.sh` extract relations for the valid entities
