package com.yahoo.yfcc100m;

import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.util.HashMap;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.apache.pig.impl.logicalLayer.schema.Schema;
import org.apache.pig.impl.logicalLayer.schema.Schema.FieldSchema;
import org.apache.pig.impl.util.Utils;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.yahoo.util.fs.FileReader;

public class VideoURL extends EvalFunc<String> {

	// xml parser
	private DocumentBuilderFactory docBuilderFactory;
	private DocumentBuilder docBuilder;
	// random number generator
	private Random random;

	public VideoURL() {
		// prepare xml parser
		try {
			this.docBuilderFactory = DocumentBuilderFactory.newInstance();
			this.docBuilder = docBuilderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			System.err.println("ERROR: could not prepare xml parser");
			System.err.println(e.getMessage()); e.printStackTrace();
			throw new RuntimeException("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: could not create xml parser");
		}
		// prepare random number generator
		this.random = new Random(System.currentTimeMillis()+Thread.currentThread().getId());
	}

	@Override
	public Schema outputSchema(Schema input) {
		try {
			// check the schema this udf expects
			if (input == null || input.size() != 2 || input.getField(0).type != DataType.LONG || input.getField(1).type != DataType.CHARARRAY) {
				StringBuilder sb = new StringBuilder();
				sb.append("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: expected input (long, chararray) but received (");
				StringBuilder sb2 = new StringBuilder();
				for (FieldSchema fs : input.getFields()) {
					if (sb2.length() > 0)
						sb2.append(", ");
					sb2.append(DataType.findTypeName(fs.type));
				}
				sb.append(sb2);
				sb.append(")");
				throw new RuntimeException(sb.toString());
			}
			// construct the schema this udf will output
			return Utils.getSchemaFromString("s:chararray");
		} catch (Exception e) { throw new RuntimeException(e); }
	}

	public String exec(Tuple input) throws IOException {
		// validate input
		if (input == null || input.size() != 2) {
			System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: received invalid record " + input);
			return null;
		}
		// extract url to optimal resolution of video
		try {
			Long p = (Long)input.get(0);
			String s = (String)input.get(1);
			if (p == null || s == null || s.isEmpty()) {
				System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: received invalid data (p=" + p + ", s=" + s + ")");
				return null;
			}
			// parse xml
			Document xdoc;
			try {
				xdoc = docBuilder.parse(new InputSource(new StringReader(s)));
				xdoc.getDocumentElement().normalize();
			} catch (Exception e) {
				System.err.println("WARNING: could not parse xml (p=" + p + ", s=" + s + ")");
				System.err.println(e.getMessage()); e.printStackTrace();
				return null;
			}
			// extract relevant attributes
			XPath xpath = XPathFactory.newInstance().newXPath();
			NodeList xstrs;
			try {
				xstrs = (NodeList)xpath.evaluate("//stream", xdoc, XPathConstants.NODESET);
			} catch (Exception e) {
				System.err.println("ERROR: could not run xpath (p=" + p + ", s=" + s + ")");
				System.err.println(e.getMessage()); e.printStackTrace();
				return null;
			}
			if (xstrs == null || xstrs.getLength() == 0)
				return null;
			// parse the different formats
			HashMap<String, String> formats = new HashMap<String, String>();
			for (int i = 0; i < xstrs.getLength(); i++) {
				// extract the video format resolution and the one-time atlas url
				Node xstr = xstrs.item(i);
				String format = null;
				String url = null;
				NamedNodeMap xattrs = xstr.getAttributes();
				if (xattrs != null) {
					Node xattr = xattrs.getNamedItem("type");
					if (xattr != null)
						format = xattr.getNodeValue();
					url = xstr.getTextContent();
				}
				if (format == null || format.isEmpty() || url == null || url.isEmpty())
					continue;
				// modify the url to point to the servers that have been taken out of rotation
				// note: https://ne1ing09.atlas.cdn.ne1.yimg.com and https://ne1ing10.atlas.cdn.ne1.yimg.com
				URI uri;
				try { uri = new URI(url); }
				catch (Exception e) {
					System.err.println("ERROR: could not parse url (p=" + p + ", f=" + format + ", u=" + url + ")");
					continue;
				}
				String host = this.random.nextDouble() >= 0.5 ? "https://ne1ing09.atlas.cdn.ne1.yimg.com" : "https://ne1ing10.atlas.cdn.ne1.yimg.com";
				url = host + uri.getPath();
				formats.put(format, url);
			}
			if (formats.isEmpty())
				return null;
			// pick the best video resolution for our use
			// note: we aim to trade off resolution vs storage
			String[] resolution = new String[] { "360p", "700", "288p", "300", "100" };
			for (String r : resolution) {
				String url = formats.get(r);
				if (url != null) {
					System.out.println("selected: " + r + "\t" + url);
					return url;
				}
			}
			return null;
		} catch(Exception e){
			System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: could not obtain string from record " + input);
			System.err.println(e.getMessage()); e.printStackTrace();
			return null;
		}
	}
	
	public static void main(String[] args) throws Exception {
		String xml = FileReader.readString("/Users/bthomee/Desktop/video.xml", "UTF-8");
		VideoURL vu = new VideoURL();
		Tuple tuple = TupleFactory.getInstance().newTuple();
		tuple.append(0L);
		tuple.append(xml);
		String s = vu.exec(tuple);
		System.out.println(s);
		System.out.println("done!");
	}

}