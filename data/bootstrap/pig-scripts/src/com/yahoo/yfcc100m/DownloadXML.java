package com.yahoo.yfcc100m;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLPeerUnverifiedException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.apache.pig.EvalFunc;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.apache.pig.impl.logicalLayer.schema.Schema;
import org.apache.pig.impl.logicalLayer.schema.Schema.FieldSchema;
import org.apache.pig.impl.util.Utils;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

public class DownloadXML extends EvalFunc<String> {

	private static final int attempts = 5;
	// xml parser
	private DocumentBuilderFactory docBuilderFactory;
	private DocumentBuilder docBuilder;

	public DownloadXML() {
		// prepare xml parser
		try {
			this.docBuilderFactory = DocumentBuilderFactory.newInstance();
			this.docBuilder = docBuilderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			System.err.println("ERROR: could not prepare xml parser");
			System.err.println(e.getMessage()); e.printStackTrace();
			throw new RuntimeException("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: could not create xml parser");
		}
	}

	@Override
	public Schema outputSchema(Schema input) {
		try {
			// check the schema this udf expects
			if (input == null || input.size() != 7 || input.getField(0).type != DataType.LONG || input.getField(1).type != DataType.CHARARRAY ||
					input.getField(2).type != DataType.CHARARRAY || input.getField(3).type != DataType.CHARARRAY ||
					input.getField(4).type != DataType.CHARARRAY || input.getField(5).type != DataType.CHARARRAY ||
					input.getField(6).type != DataType.INTEGER) {
				StringBuilder sb = new StringBuilder();
				sb.append("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: expected input (long, chararray, chararray, chararray, chararray, chararray, int) but received (");
				StringBuilder sb2 = new StringBuilder();
				for (FieldSchema fs : input.getFields()) {
					if (sb2.length() > 0)
						sb2.append(", ");
					sb2.append(DataType.findTypeName(fs.type));
				}
				sb.append(sb2);
				sb.append(")");
				throw new RuntimeException(sb.toString());
			}
			// construct the schema this udf will output
			return Utils.getSchemaFromString("s:chararray");
		} catch (Exception e) { throw new RuntimeException(e); }
	}

	public String exec(Tuple input) throws IOException {
		// validate input
		if (input == null || input.size() != 7) {
			System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: received invalid record " + input);
			return null;
		}
		// obtain the data for given url
		try {
			Long p = (Long)input.get(0);
			String u = (String)input.get(1);
			String r = (String)input.get(2);
			Boolean f = Boolean.valueOf((String)input.get(3));
			String c1 = (String)input.get(4);
			String c2 = (String)input.get(5);
			Integer s = (Integer)input.get(6);
			if (p == null || u == null || u.isEmpty() || s == null) {
				System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: received invalid data (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ")");
				return null;
			}
			// for now rate limit the querying by sleeping until we figure out
			// how this can be achieve using other means
			// note: if the sleep is negative, randomly sleep for a while between
			//       0 and the indicated (absolute) value
			try {
				if (s < 0)
					s = (int)(-1 * Math.random() * s);
				if (s > 0)
					Thread.sleep(s);
			} 
			catch (InterruptedException e) {
				// restore interrupted status before throwing an error
				Thread.currentThread().interrupt();
				throw new RuntimeException("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: unexpected interrupt", e);
			}
			System.out.println("downloading data (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ")");
			// determine protocol
			String url = u;
			boolean https;
			if (url.startsWith("http://")) {
				url = url.substring(7);
				https = false;
			}
			else if (url.startsWith("https://")) {
				url = url.substring(8);
				https = true;
			}
			else {
				System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: no valid protocol provided (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ")");
				return null;
			}
			// create target
			int pos = url.indexOf('/');
			if (pos == -1) {
				System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: no valid target url provided (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ")");
				return null;
			}
			String h = url.substring(0, pos);
			url = url.substring(pos);
			String[] split = h.split(":");
			if (split.length > 2) {
				System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: no valid target host provided (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ")");
				return null;
			}
			// set host
			String host = split[0];
			if (host.isEmpty()) {
				System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: no valid host provided (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ")");
				return null;
			}
			// set port
			int port;
			try {
				port = split.length == 1 ? (https ? 443 : 80) : Integer.parseInt(split[1]);
				if (port <= 0 || port > 65535)
					throw new Exception();
			} catch (Exception e) {
				System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: no valid host port provided (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ")");
				return null;
			}
			// set host
			HttpHost target = new HttpHost(host, port, https ? "https" : "http");
			// create request
			HttpGet request = new HttpGet(url);
			// set options
			// note: disable redirection to ensure we don't download any 'file-not-found' images
			// HTTPCLIENT 4.3.x
			/*RequestConfig config = RequestConfig.custom()
					.setSocketTimeout(5000)
					.setConnectTimeout(5000)
					.setConnectionRequestTimeout(5000)
					.setStaleConnectionCheckEnabled(true)
					.setRedirectsEnabled(f)
					.build();
			request.setConfig(config);*/
			// HTTPCLIENT 4.2.x
			HttpParams params = new BasicHttpParams();
			HttpConnectionParams.setSoTimeout(params, 5000);
			HttpConnectionParams.setConnectionTimeout(params, 5000);
			HttpConnectionParams.setStaleCheckingEnabled(params, true);
			HttpClientParams.setRedirecting(params, f);
			// use proxy, if supplied
			if (r != null && r.length() > 0) {
				// parse the proxy
				split = r.split(":");
				if (split.length != 2) {
					System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: no valid proxy provided (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ")");
					return null;
				}
				// set host
				host = split[0];
				if (host.isEmpty()) {
					System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: no valid proxy host provided (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ")");
					return null;
				}
				// set port
				try {
					port = Integer.parseInt(split[1]);
					if (port <= 0 || port > 65535)
						throw new Exception();
				} catch (Exception e) {
					System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: no valid proxy port provided (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ")");
					return null;
				}
				// set proxy
				// HTTPCLIENT 4.3.x
				/*HttpHost httpproxy = new HttpHost(host, port, "http");
				config = RequestConfig.copy(request.getConfig())
						.setProxy(httpproxy)
						.build();
				request.setConfig(config);*/
				// HTTPCLIENT 4.2.x
				ConnRouteParams.setDefaultProxy(params, new HttpHost(host, port, "http"));
			}
			// set user agent to prevent our requests from triggering the long-term caching of the photo
			// note: an unexpected consequence of setting the user agent to a non-browser-like value may result in
			//       the photo being returned in a different size than requested. this happens in the cases when
			//       the photo is not already present in the cache and the photo is not available in the requested
			//       (valid) size. while for regular users the backend will create the image in the requested size,
			//       for bots the backend will simply return a different sized image from the filers. 
			request.setHeader("User-Agent", "FlickrJobBot-yfcc100m");
			// perform the request
			// note: repeat a few times in case read timeouts are encountered
			for (int attempt = 0; attempt < attempts; attempt++) {
				// retry if necessary
				if (attempt > 0)
					System.out.println("retrying...");
				// create http client
				// note: disable certificate checking for SSL due to internal host mismatches
				// HTTPCLIENT 4.3.x
				// note: has not been tested
				/*CloseableHttpClient httpclient;
				CloseableHttpResponse response = null;
				if (https) {
					SSLContextBuilder builder = new SSLContextBuilder();
					builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
					SSLConnectionSocketFactory ssf = new SSLConnectionSocketFactory(builder.build());
					httpclient = HttpClients.custom().setSSLSocketFactory(ssf).build();
				}
				else
					httpclient = HttpClients.createDefault();*/
				// HTTPCLIENT 4.2.x
				DefaultHttpClient httpclient = new DefaultHttpClient(params);
				if (https) {
					// disable certificate checking
					TrustStrategy trustStrategy = new TrustStrategy() {
						@Override
						public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
							return true;
						}
					};
					SSLSocketFactory sf = new SSLSocketFactory(trustStrategy, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
					ClientConnectionManager ccm = httpclient.getConnectionManager();
					SchemeRegistry sr = ccm.getSchemeRegistry();
					sr.register(new Scheme("https", 443, sf)); 
				}
				// set username/password, if provided
				// note: password may be empty
				if (c1 != null && !c1.isEmpty()) {
					// HTTPCLIENT 4.3.x
					// note: has not been tested
					/*CredentialsProvider credsProvider = new BasicCredentialsProvider();
			        credsProvider.setCredentials(
			                new AuthScope("localhost", 443),
			                new UsernamePasswordCredentials(c1, c2 == null ? "" : c2));
			        CloseableHttpClient httpclient = HttpClients.custom()
			                .setDefaultCredentialsProvider(credsProvider)
			                .build();*/
					// HTTPCLIENT 4.2.x
					UsernamePasswordCredentials upc = new UsernamePasswordCredentials(c1, c2 == null ? "" : c2);
					httpclient.getCredentialsProvider().setCredentials(new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT), upc);
				}
				// execute the query
				HttpResponse response;
				try {
					response = httpclient.execute(target, request);
					// parse the response
					int code = response.getStatusLine().getStatusCode();
					// check return code
					if (code != HttpStatus.SC_OK) {
						System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: could not query url (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ", a=" + attempt + ", c=" + code + ")");
						return null;
					}
					// get response entity
					HttpEntity httpentity = response.getEntity();
					// read response
					String xml = null;
					try {
						// get string data
						xml = IOUtils.toString(httpentity.getContent());
						if (xml == null)
							throw new Exception();
						// clean data
						xml = xml.trim();
						if (xml.isEmpty())
							throw new Exception();
					} catch (OutOfMemoryError e) {
						System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: too large response received (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ", a=" + attempt + ")");
						return null;
					} catch (Exception e) {
						System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " WARNING: could not parse response (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ", x=" + xml + ", a=" + attempt + ")");
						return null;
					} finally {
						// release response entity
						EntityUtils.consumeQuietly(httpentity);
					}
					// validate response
					try {
						Document xdoc = docBuilder.parse(new InputSource(new StringReader(xml)));
						xdoc.getDocumentElement().normalize();
					} catch (Exception e) {
						System.err.println("WARNING: could not validate response (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ", x=" + xml + ", a=" + attempt + ")");
						// try again after a small pause
						try { Thread.sleep(2000); }
						catch (InterruptedException e2) {
							// restore interrupted status before throwing an error
							Thread.currentThread().interrupt();
							throw new RuntimeException("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: unexpected interrupt", e2);
						}
						continue;
					}
					return xml;
				} catch (SocketTimeoutException e) {
					System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " WARNING: socket timeout (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ", a=" + attempt + ")");
					// try again after a small pause
					try { Thread.sleep(2000); }
					catch (InterruptedException e2) {
						// restore interrupted status before throwing an error
						Thread.currentThread().interrupt();
						throw new RuntimeException("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: unexpected interrupt", e2);
					}
					continue;
				} catch (SSLPeerUnverifiedException e) {
					System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " WARNING: ssl peer error (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ", a=" + attempt + ")");
					// try again after a small pause
					try { Thread.sleep(2000); }
					catch (InterruptedException e2) {
						// restore interrupted status before throwing an error
						Thread.currentThread().interrupt();
						throw new RuntimeException("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: unexpected interrupt", e2);
					}
					continue;
				} catch (Exception e) {
					System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: could not execute query (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ", a=" + attempt + ")");
					System.err.println(e.getMessage()); e.printStackTrace();
					return null;
					// HTTPCLIENT 4.3.x
				}/* finally {
					if (response != null)
						response.close();
					httpclient.close();
				}*/
			}
			// the request timed out several times
			return null;
		} catch (Exception e) {
			System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: could not obtain data from record " + input);
			System.err.println(e.getMessage()); e.printStackTrace();
			return null;
		}
	}

	public static void main(String[] args) throws Exception {
		String proxy = ""; //"httpproxy-res.tan.ygrid.yahoo.com:4080";
		DownloadXML dd = new DownloadXML();
		Tuple tuple = TupleFactory.getInstance().newTuple();
		tuple.append(0L);
		tuple.append("https://api.flickr.com/services/rest/?method=flickr.video.getStreamInfo&api_key=712d491d24929fbdbb447436c008ec27&photo_id=7843781152&secret=cca6aacbea");
		tuple.append(proxy);
		tuple.append("true");
		tuple.append(0);
		String x = dd.exec(tuple);
		FileOutputStream output = new FileOutputStream(new File("/Users/bthomee/Desktop/video.xml"));
		IOUtils.write(x, output);
		System.out.println("done!");
	}

}