package com.yahoo.yfcc100m;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.Imaging;
import org.apache.pig.EvalFunc;
import org.apache.pig.data.DataByteArray;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.apache.pig.impl.logicalLayer.schema.Schema;
import org.apache.pig.impl.logicalLayer.schema.Schema.FieldSchema;
import org.apache.pig.impl.util.Utils;

public class PhotoResolution extends EvalFunc<Tuple> {

	@Override
	public Schema outputSchema(Schema input) {
		try {
			// check the schema this udf expects
			if (input == null || input.size() != 2 || input.getField(0).type != DataType.LONG || input.getField(1).type != DataType.BYTEARRAY) {
				StringBuilder sb = new StringBuilder();
				sb.append("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: expected input (long, bytearray) but received (");
				StringBuilder sb2 = new StringBuilder();
				for (FieldSchema fs : input.getFields()) {
					if (sb2.length() > 0)
						sb2.append(", ");
					sb2.append(DataType.findTypeName(fs.type));
				}
				sb.append(sb2);
				sb.append(")");
				throw new RuntimeException(sb.toString());
			}
			// construct the schema this udf will output
			return Utils.getSchemaFromString("t:tuple(x:int,y:int)");
		} catch (Exception e) { throw new RuntimeException(e); }
	}

	public Tuple exec(Tuple input) throws IOException {
		// validate input
		if (input == null || input.size() != 2) {
			System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: received invalid record " + input);
			return null;
		}
		// extract resolution of image
		try {
			Long p = (Long)input.get(0);
			DataByteArray d = (DataByteArray)input.get(1);
			if (p == null || d == null || d.size() == 0) {
				System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: received invalid data (p=" + p + ", d=" + d + ")");
				return null;
			}
			// read image
			BufferedImage image = null;
			try {
				image = Imaging.getBufferedImage(new ByteArrayInputStream(d.get()));
				if (image == null) {
					System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: could not read image from bytes (p=" + p + ")");
					return null;
				}
			} catch (ImageReadException e) {
				try {
					image = ImageIO.read(new ByteArrayInputStream(d.get()));
				} catch (Exception e2) {
					System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: could not read image from bytes (p=" + p + ")");
					System.err.println(e2.getMessage()); e2.printStackTrace();
					return null;
				}
			} catch (Exception e) {
				System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: could not read image from bytes (p=" + p + ")");
				System.err.println(e.getMessage()); e.printStackTrace();
				return null;
			}
			// get image dimensions
			int xdim = image.getWidth();
			int ydim = image.getHeight();
			System.out.println("image dimensions (p=" + p + ", x=" + xdim + ", y=" + ydim + ")");
			Tuple tuple = TupleFactory.getInstance().newTuple();
			tuple.append(xdim);
			tuple.append(ydim);
			return tuple;
		} catch(Exception e){
			System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: could not obtain string from record " + input);
			System.err.println(e.getMessage()); e.printStackTrace();
			return null;
		}
	}

}