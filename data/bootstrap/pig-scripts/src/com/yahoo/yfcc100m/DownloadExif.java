package com.yahoo.yfcc100m;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.LinkedList;

import javax.net.ssl.SSLPeerUnverifiedException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.apache.pig.EvalFunc;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.apache.pig.impl.logicalLayer.schema.Schema;
import org.apache.pig.impl.logicalLayer.schema.Schema.FieldSchema;
import org.apache.pig.impl.util.Utils;
import org.json.JSONObject;

import com.drew.imaging.jpeg.JpegSegmentType;
import com.drew.lang.StreamReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.drew.metadata.exif.ExifReader;
import com.yahoo.util.text.TextCleaner;

public class DownloadExif extends EvalFunc<String> {

	private static final int attempts = 5;

	@Override
	public Schema outputSchema(Schema input) {
		try {
			// check the schema this udf expects
			if (input == null || input.size() != 7 || input.getField(0).type != DataType.LONG || input.getField(1).type != DataType.CHARARRAY ||
					input.getField(2).type != DataType.CHARARRAY || input.getField(3).type != DataType.CHARARRAY ||
					input.getField(4).type != DataType.CHARARRAY || input.getField(5).type != DataType.CHARARRAY ||
					input.getField(6).type != DataType.INTEGER) {
				StringBuilder sb = new StringBuilder();
				sb.append("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: expected input (long, chararray, chararray, chararray, chararray, chararray, int) but received (");
				StringBuilder sb2 = new StringBuilder();
				for (FieldSchema fs : input.getFields()) {
					if (sb2.length() > 0)
						sb2.append(", ");
					sb2.append(DataType.findTypeName(fs.type));
				}
				sb.append(sb2);
				sb.append(")");
				throw new RuntimeException(sb.toString());
			}
			// construct the schema this udf will output
			return Utils.getSchemaFromString("s:chararray");
		} catch (Exception e) { throw new RuntimeException(e); }
	}

	public String exec(Tuple input) throws IOException {
		// validate input
		if (input == null || input.size() != 7) {
			System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: received invalid record " + input);
			return null;
		}
		// obtain the data for given url
		try {
			Long p = (Long)input.get(0);
			String u = (String)input.get(1);
			String r = (String)input.get(2);
			Boolean f = Boolean.valueOf((String)input.get(3));
			String c1 = (String)input.get(4);
			String c2 = (String)input.get(5);
			Integer s = (Integer)input.get(6);
			if (p == null || u == null || u.isEmpty() || s == null) {
				System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: received invalid data (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ")");
				return null;
			}
			// for now rate limit the querying by sleeping until we figure out
			// how this can be achieve using other means
			// note: if the sleep is negative, randomly sleep for a while between
			//       0 and the indicated (absolute) value
			try {
				if (s < 0)
					s = (int)(-1 * Math.random() * s);
				if (s > 0)
					Thread.sleep(s);
			} catch (InterruptedException e) {
				// restore interrupted status before throwing an error
				Thread.currentThread().interrupt();
				throw new RuntimeException("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: unexpected interrupt", e);
			}
			System.out.println("downloading data (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ")");
			// determine protocol
			String url = u;
			boolean https;
			if (url.startsWith("http://")) {
				url = url.substring(7);
				https = false;
			}
			else if (url.startsWith("https://")) {
				url = url.substring(8);
				https = true;
			}
			else {
				System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: no valid protocol provided (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ")");
				return null;
			}
			// create target
			int pos = url.indexOf('/');
			if (pos == -1) {
				System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: no valid target url provided (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ")");
				return null;
			}
			String h = url.substring(0, pos);
			url = url.substring(pos);
			String[] split = h.split(":");
			if (split.length > 2) {
				System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: no valid target host provided (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ")");
				return null;
			}
			// set host
			String host = split[0];
			if (host.isEmpty()) {
				System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: no valid host provided (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ")");
				return null;
			}
			// set port
			int port;
			try {
				port = split.length == 1 ? (https ? 443 : 80) : Integer.parseInt(split[1]);
				if (port <= 0 || port > 65535)
					throw new Exception();
			} catch (Exception e) {
				System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: no valid host port provided (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ")");
				return null;
			}
			// set host
			HttpHost target = new HttpHost(host, port, https ? "https" : "http");
			// create request
			HttpGet request = new HttpGet(url);
			// set options
			// note: disable redirection to ensure we don't download any 'file-not-found' images
			// HTTPCLIENT 4.3.x
			/*RequestConfig config = RequestConfig.custom()
								.setSocketTimeout(5000)
								.setConnectTimeout(5000)
								.setConnectionRequestTimeout(5000)
								.setStaleConnectionCheckEnabled(true)
								.setRedirectsEnabled(f)
								.build();
						request.setConfig(config);*/
			// HTTPCLIENT 4.2.x
			HttpParams params = new BasicHttpParams();
			HttpConnectionParams.setSoTimeout(params, 5000);
			HttpConnectionParams.setConnectionTimeout(params, 5000);
			HttpConnectionParams.setStaleCheckingEnabled(params, true);
			HttpClientParams.setRedirecting(params, f);
			// use proxy, if supplied
			if (r != null && r.length() > 0) {
				// parse the proxy
				split = r.split(":");
				if (split.length != 2) {
					System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: no valid proxy provided (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ")");
					return null;
				}
				// set host
				host = split[0];
				if (host.isEmpty()) {
					System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: no valid proxy host provided (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ")");
					return null;
				}
				// set port
				try {
					port = Integer.parseInt(split[1]);
					if (port <= 0 || port > 65535)
						throw new Exception();
				} catch (Exception e) {
					System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: no valid proxy port provided (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ")");
					return null;
				}
				// set proxy
				// HTTPCLIENT 4.3.x
				/*HttpHost httpproxy = new HttpHost(host, port, "http");
				config = RequestConfig.copy(request.getConfig())
						.setProxy(httpproxy)
						.build();
				request.setConfig(config);*/
				// HTTPCLIENT 4.2.x
				ConnRouteParams.setDefaultProxy(params, new HttpHost(host, port, "http"));
			}
			// set user agent to prevent our requests from triggering the long-term caching of the photo
			// note: an unexpected consequence of setting the user agent to a non-browser-like value may result in
			//       the photo being returned in a different size than requested. this happens in the cases when
			//       the photo is not already present in the cache and the photo is not available in the requested
			//       (valid) size. while for regular users the backend will create the image in the requested size,
			//       for bots the backend will simply return a different sized image from the filers. 
			request.setHeader("User-Agent", "FlickrJobBot-yfcc100m");
			// perform the request
			// note: repeat a few times in case read timeouts are encountered
			for (int attempt = 0; attempt < attempts; attempt++) {
				// retry if necessary
				if (attempt > 0)
					System.out.println("retrying...");
				// create http client
				// note: disable certificate checking for SSL due to internal host mismatches
				// HTTPCLIENT 4.3.x
				// note: has not been tested
				/*CloseableHttpClient httpclient;
				CloseableHttpResponse response = null;
				if (https) {
					SSLContextBuilder builder = new SSLContextBuilder();
					builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
					SSLConnectionSocketFactory ssf = new SSLConnectionSocketFactory(builder.build());
					httpclient = HttpClients.custom().setSSLSocketFactory(ssf).build();
				}
				else
					httpclient = HttpClients.createDefault();*/
				// HTTPCLIENT 4.2.x
				DefaultHttpClient httpclient = new DefaultHttpClient(params);
				if (https) {
					// disable certificate checking
					TrustStrategy trustStrategy = new TrustStrategy() {
						@Override
						public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
							return true;
						}
					};
					SSLSocketFactory sf = new SSLSocketFactory(trustStrategy, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
					ClientConnectionManager ccm = httpclient.getConnectionManager();
					SchemeRegistry sr = ccm.getSchemeRegistry();
					sr.register(new Scheme("https", 443, sf)); 
				}
				// set username/password, if provided
				// note: password may be empty
				if (c1 != null && !c1.isEmpty()) {
					// HTTPCLIENT 4.3.x
					// note: has not been tested
					/*CredentialsProvider credsProvider = new BasicCredentialsProvider();
			        credsProvider.setCredentials(
			                new AuthScope("localhost", 443),
			                new UsernamePasswordCredentials(c1, c2 == null ? "" : c2));
			        CloseableHttpClient httpclient = HttpClients.custom()
			                .setDefaultCredentialsProvider(credsProvider)
			                .build();*/
					// HTTPCLIENT 4.2.x
					UsernamePasswordCredentials upc = new UsernamePasswordCredentials(c1, c2 == null ? "" : c2);
					httpclient.getCredentialsProvider().setCredentials(new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT), upc);
				}
				// execute the query
				HttpResponse response;
				try {
					response = httpclient.execute(target, request);
					// parse the response
					int code = response.getStatusLine().getStatusCode();
					// check return code
					if (code != HttpStatus.SC_OK && code != HttpStatus.SC_PARTIAL_CONTENT) {
						System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: could not query url (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ", a=" + attempt + ", c=" + code + ")");
						return null;
					}
					// get response entity
					HttpEntity httpentity = response.getEntity();
					// attempt to read only the exif data
					try {
						/*LinkedList<JpegSegmentMetadataReader> readers = new LinkedList<JpegSegmentMetadataReader>();
						readers.add(new ExifReader());
						Metadata md = JpegMetadataReader.readMetadata(httpentity.getContent(), readers);
						return md.toString();
						StreamReader sr = new StreamReader(new FileInputStream(new File("/Users/bthomee/Desktop/IMG_0700.jpg")));*/
						// prepare stream reader
						StreamReader sr = new StreamReader(httpentity.getContent());
						// must be big-endian
						if (!sr.isMotorolaByteOrder()) {
							System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: unsupported jpeg (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ", a=" + attempt + ", b=" + "motorola" + ")");
							return null;
						}
						// first two bytes should be JPEG magic number
						final int magicNumber = sr.getUInt16();
						if (magicNumber != 0xFFD8) {
							System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: not a valid jpeg (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ", a=" + attempt + ", x=" + Integer.toHexString(magicNumber) + ")");
							return null;
						}
						// walk through the data
						ExifReader er = new ExifReader();
						LinkedList<Metadata> mds = new LinkedList<Metadata>();
						do {
							// next byte is the segment identifier: 0xFF
							final short id = sr.getUInt8();
							if (id != 0xFF) {
								System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: not a valid jpeg (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ", a=" + attempt + ", s=" + id + ")");
								return null;
							}
							// next byte is the segment type
							byte type = sr.getInt8();
							if (type == (byte)0xDA || type == (byte)0xD9) {
								// at this point we've readed the start of image data (0xDA) or the end of image data (0xD9),
								// after which there will not be any exif data in the image, so we might as well return
								break;
							}
							// next 2-bytes are <segment-size>: [high-byte] [low-byte]
							int length = sr.getUInt16();
							// segment length includes size bytes, so subtract two
							length -= 2;
							if (length < 0) {
								System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: not a valid jpeg (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ", a=" + attempt + ", l=" + length + ")");
								return null;
							}
							// check whether we are interested in this segment
							if (type == (byte)0xE1) {
								// get the data
								byte[] bytes = sr.getBytes(length);
								if (length != bytes.length) {
									System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: not a valid jpeg (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ", a=" + attempt + ", l1=" + length + ", l2=" + bytes.length + ")");
									return null;
								}
								// process the data
								if (er.canProcess(bytes, JpegSegmentType.APP1)) {
									Metadata md = new Metadata();
									er.extract(bytes, md, JpegSegmentType.APP1);
									mds.add(md);
								}
								break;
							}
							// not the segment in which the exif lives, so read and skip this section
							if (!sr.trySkip(length)) {
								System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: not a valid jpeg (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ", a=" + attempt + ", t=" + "truncated" + ")");
								return null;
							}
						} while (true);
						if (mds.isEmpty()) {
							System.out.println("no metadata present");
							return null;
						}
						// convert the metadata to json
						// note: sanitize the values
						JSONObject json = new JSONObject();
						for (Metadata md : mds) {
							for (Directory d : md.getDirectories()) {
								// get the directory name
								String n = d.getName();
								n = TextCleaner.condense(n);
								n = TextCleaner.trim(n);
								if (n.isEmpty())
									continue;
								// process all tags in the directory
								JSONObject jobj = new JSONObject();
								for (Tag t : d.getTags()) {
									// get key
									String k = t.getTagName();
									n = TextCleaner.condense(n);
									n = TextCleaner.trim(n);
									if (k.isEmpty() || k.toLowerCase().contains("unknown"))
										continue;
									// get value
									String v = t.getDescription();
									if (v == null)
										v = d.getString(t.getTagType());
									v = TextCleaner.condense(v);
									v = TextCleaner.trim(v);
									if (v.isEmpty() || (v.startsWith("[") && v.endsWith("bytes]")))
										continue;
									jobj.put(k, v);
								}
								// add to json
								if (jobj.length() > 0)
									json.append(n, jobj);
							}
						}
						// check if any metadata was found
						if (json.length() == 0) {
							System.out.println("no exif data present");
							return null;
						}
						return json.toString();
					} catch (Exception e) {
						System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: could not read image data (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ", a=" + attempt + ")");
						System.err.println(e.getMessage()); e.printStackTrace();
						return null;
					} finally {
						// release response entity
						EntityUtils.consumeQuietly(httpentity);
					}
				} catch (SocketTimeoutException e) {
					System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " WARNING: socket timeout (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ", a=" + attempt + ")");
					// try again after a small pause
					try { Thread.sleep(2000); }
					catch (InterruptedException e2) {
						// restore interrupted status before throwing an error
						Thread.currentThread().interrupt();
						throw new RuntimeException("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: unexpected interrupt", e2);
					}
					continue;
				} catch (SSLPeerUnverifiedException e) {
					System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " WARNING: ssl peer error (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ", a=" + attempt + ")");
					// try again after a small pause
					try { Thread.sleep(2000); }
					catch (InterruptedException e2) {
						// restore interrupted status before throwing an error
						Thread.currentThread().interrupt();
						throw new RuntimeException("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: unexpected interrupt", e2);
					}
					continue;
				} catch (Exception e) {
					System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: could not execute query (p=" + p + ", u=" + u + ", r=" + r + ", f=" + f + ", s=" + s + ", a=" + attempt + ")");
					System.err.println(e.getMessage()); e.printStackTrace();
					return null;
					// HTTPCLIENT 4.3.x
				}/* finally {
					if (response != null)
						response.close();
					httpclient.close();
				}*/
			}
			// the request timed out several times
			return null;
		} catch (Exception e) {
			System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: could not obtain data from record " + input);
			System.err.println(e.getMessage()); e.printStackTrace();
			return null;
		}
	}

	public static void main(String[] args) throws Exception {
		String proxy = ""; //"httpproxy-res.tan.ygrid.yahoo.com:4080";
		DownloadExif dd = new DownloadExif();
		Tuple tuple = TupleFactory.getInstance().newTuple();
		tuple.append(0L);
		tuple.append("http://img4.wikia.nocookie.net/__cb20111202180816/happyfeet/images/1/1f/1x1_pixel.JPG");
		//tuple.append("https://farm6.staticflickr.com/5465/10201275523_f400826feb_o.jpg");
		tuple.append(proxy);
		tuple.append("false");
		tuple.append(0);
		String s = dd.exec(tuple);
		System.out.println(s);
		System.out.println("done!");
	}

	/*public static void main(String[] args) throws Exception {
		String proxy = ""; //"httpproxy-res.tan.ygrid.yahoo.com:4080";
		DownloadBytes dd = new DownloadBytes();
		Tuple tuple = TupleFactory.getInstance().newTuple();
		tuple.append(0L);
		tuple.append("https://www.flickr.com/videos/41533706@N03/7843781152/play/orig/c257ebee2e");
		tuple.append(proxy);
		tuple.append("true");
		tuple.append(0);
		DataByteArray d = dd.exec(tuple);
		FileOutputStream output = new FileOutputStream(new File("/Users/bthomee/Desktop/video.mp4"));
		IOUtils.write(d.get(), output);
		System.out.println("done!");
	}*/

}