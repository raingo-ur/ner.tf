package com.yahoo.yfcc100m;

import java.util.Iterator;
import java.util.LinkedList;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.BagFactory;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;
import org.apache.pig.impl.logicalLayer.schema.Schema;
import org.apache.pig.impl.logicalLayer.schema.Schema.FieldSchema;
import org.apache.pig.impl.util.Utils;
import org.json.JSONException;
import org.json.JSONObject;

import com.yahoo.util.fs.FileReader;

public class AutotagParser extends EvalFunc<DataBag> {

	@Override
	public Schema outputSchema(Schema input) {
		try {
			// check the schema this udf expects
			if (input == null || input.size() != 2 || input.getField(0).type != DataType.LONG || input.getField(1).type != DataType.CHARARRAY) {
				StringBuilder sb = new StringBuilder();
				sb.append("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: expected input (long, chararray) but received (");
				StringBuilder sb2 = new StringBuilder();
				for (FieldSchema fs : input.getFields()) {
					if (sb2.length() > 0)
						sb2.append(", ");
					sb2.append(DataType.findTypeName(fs.type));
				}
				sb.append(sb2);
				sb.append(")");
				throw new RuntimeException(sb.toString());
			}
			// construct the schema this udf will output
			return Utils.getSchemaFromString("b:bag{t:tuple(version:chararray,autotag:chararray,score:double)}");
		} catch (Exception e) { throw new RuntimeException(e); }
	}
	
	public DataBag exec(Tuple input) {
		// validate input
		if (input == null || input.size() != 2) {
			System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: received invalid record " + input);
			return null;
		}
		// parse the geotag
		try {
			Long id = (Long)input.get(0);
			String json = (String)input.get(1);
			if (id == null || json == null || json.isEmpty()) {
				System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: received invalid data (p= " + id + ", j=" + json + ")");
				return null;
			}
			System.out.println("processing " + id);
			// parse the json
			DataBag bag = BagFactory.getInstance().newDefaultBag();
			try {
				JSONObject jobj = new JSONObject(json);
				// unpack and rewrite the json if its created by a more recent autotag version
				if (jobj.has("modules")) {
					jobj = jobj.getJSONObject("modules").getJSONObject("autotags");
					JSONObject j = jobj.getJSONObject("data");
					jobj.remove("data");
					jobj.put("tags", j);
				}
				// get the version and convert it to numeric
				String version = jobj.getString("version");
				if (version.startsWith("v"))
					version = version.substring(1);
				// get the autotags
				jobj = jobj.getJSONObject("tags");
				if (jobj == null || jobj.length() == 0)
					return null;
				String[] keys = JSONObject.getNames(jobj);
				if (keys == null || keys.length == 0)
					return null;
				// get the score of each autotag
				// note: no need to store an autotag if its score is zero
				for (String key : keys) {
					double val = jobj.getDouble(key);
					if (val == 0.0)
						continue;
					// add to bag
					Tuple tuple = TupleFactory.getInstance().newTuple();
					tuple.append(version);
					tuple.append(key);
					tuple.append(val);
					bag.add(tuple);
				}
				return bag.size() > 0 ? bag : null;
			} catch (JSONException e) {
				System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: could not parse json (p= " + id + ", j=" + json + ")");
				System.err.println(e.getMessage()); e.printStackTrace();
				return null;
			}
		} catch (Exception e) {
			System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: could not obtain data from record " + input);
			System.err.println(e.getMessage()); e.printStackTrace();
			return null;
		}
	}

	public static void main(String[] args) throws Exception {
		AutotagParser at = new AutotagParser();
		LinkedList<String> lines = FileReader.readLines("/Users/bthomee/Desktop/json.txt", "UTF-8");
		for (String line : lines) {
			String[] split = line.split("\t");
			if (split.length != 2) {
				System.err.println("ERROR: invalid data in " + line);
				return;
			}
			Tuple tuple = TupleFactory.getInstance().newTuple();
			tuple.append(Long.parseLong(split[0]));
			tuple.append(split[1]);
			DataBag bag = at.exec(tuple);
			if (bag == null)
				return;
			for (Iterator<Tuple> it = bag.iterator(); it.hasNext(); )
				System.out.println(it.next());
		}
	}
}