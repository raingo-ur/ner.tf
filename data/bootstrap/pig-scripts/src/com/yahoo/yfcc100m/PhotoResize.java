package com.yahoo.yfcc100m;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.Imaging;
import org.apache.pig.EvalFunc;
import org.apache.pig.data.DataByteArray;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.impl.logicalLayer.schema.Schema;
import org.apache.pig.impl.logicalLayer.schema.Schema.FieldSchema;
import org.apache.pig.impl.util.Utils;

public class PhotoResize extends EvalFunc<DataByteArray> {
	
	private int mindim;
	
	public PhotoResize(int mindim) {
		this.mindim = mindim;
	}
	
	public PhotoResize(String mindim) {
		this.mindim = Integer.parseInt(mindim);
	}

	@Override
	public Schema outputSchema(Schema input) {
		try {
			// check the schema this udf expects
			if (input == null || input.size() != 2 || input.getField(0).type != DataType.LONG || input.getField(1).type != DataType.BYTEARRAY) {
				StringBuilder sb = new StringBuilder();
				sb.append("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: expected input (long, bytearray) but received (");
				StringBuilder sb2 = new StringBuilder();
				for (FieldSchema fs : input.getFields()) {
					if (sb2.length() > 0)
						sb2.append(", ");
					sb2.append(DataType.findTypeName(fs.type));
				}
				sb.append(sb2);
				sb.append(")");
				throw new RuntimeException(sb.toString());
			}
			// construct the schema this udf will output
			return Utils.getSchemaFromString("b:bytearray");
		} catch (Exception e) { throw new RuntimeException(e); }
	}

	public DataByteArray exec(Tuple input) throws IOException {
		// validate input
		if (input == null || input.size() != 2) {
			System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: received invalid record " + input);
			return null;
		}
		// extract resolution of image
		try {
			Long p = (Long)input.get(0);
			DataByteArray d = (DataByteArray)input.get(1);
			if (p == null || d == null || d.size() == 0) {
				System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: received invalid data (p=" + p + ", d=" + d + ")");
				return null;
			}
			// read image
			BufferedImage image = null;
			try {
				image = Imaging.getBufferedImage(new ByteArrayInputStream(d.get()));
				if (image == null) {
					System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: could not read image from bytes (p=" + p + ")");
					return null;
				}
			} catch (ImageReadException e) {
				try {
					image = ImageIO.read(new ByteArrayInputStream(d.get()));
				} catch (Exception e2) {
					System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: could not read image from bytes (p=" + p + ")");
					System.err.println(e2.getMessage()); e2.printStackTrace();
					return null;
				}
			} catch (Exception e) {
				System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: could not read image from bytes (p=" + p + ")");
				System.err.println(e.getMessage()); e.printStackTrace();
				return null;
			}
			// resize image using interpolation
			int xcurdim = image.getWidth();
			int ycurdim = image.getHeight();
			double aspect = (double)xcurdim / (double)ycurdim;
			int xnewdim = xcurdim <= ycurdim ? this.mindim : (int)(this.mindim * aspect);
			int ynewdim = ycurdim <= xcurdim ? this.mindim : (int)(this.mindim / aspect);
			System.out.println("resizing image (p=" + p + ", xo=" + xcurdim + ", yo=" + ycurdim + ", xn=" + xnewdim + ", yn=" + ynewdim + ")");
			BufferedImage transformed = new BufferedImage(xnewdim, ynewdim, BufferedImage.TYPE_INT_RGB);
			Graphics2D g = transformed.createGraphics();
			g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			g.drawImage(image, 0, 0, xnewdim, ynewdim, null);
			g.dispose();
			// convert image to bytearray
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(transformed, "jpg", baos);
			byte[] b = baos.toByteArray();
			return new DataByteArray(b);
		} catch(Exception e){
			System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: could not obtain string from record " + input);
			System.err.println(e.getMessage()); e.printStackTrace();
			return null;
		}
	}
	
	/*public static void main(String[] args) throws Exception {
		String proxy = ""; //"httpproxy-res.tan.ygrid.yahoo.com:4080";
		DownloadBytes dd = new DownloadBytes();
		Tuple tuple = TupleFactory.getInstance().newTuple();
		tuple.append(0L);
		tuple.append("https://farm4.staticflickr.com/3099/3174965401_2742004421.jpg");
		tuple.append(proxy);
		tuple.append("true");
		tuple.append("");
		tuple.append("");
		tuple.append(0);
		DataByteArray d = dd.exec(tuple);
		InputStream in = new ByteArrayInputStream(d.get());
		BufferedImage image = Imaging.getBufferedImage(in);
		ImageIO.write(image, "jpg", new File("/Users/bthomee/Desktop/image1.jpg"));
		tuple = TupleFactory.getInstance().newTuple();
		tuple.append(0L);
		tuple.append(d);
		PhotoResize pr = new PhotoResize(256);
		d = pr.exec(tuple);
		in = new ByteArrayInputStream(d.get());
		image = Imaging.getBufferedImage(in);
		ImageIO.write(image, "jpg", new File("/Users/bthomee/Desktop/image2.jpg"));
		System.out.println("done!");
	}*/

}