package com.yahoo.yfcc100m;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.Imaging;
import org.apache.pig.FilterFunc;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.impl.logicalLayer.schema.Schema;
import org.apache.pig.impl.logicalLayer.schema.Schema.FieldSchema;
import org.apache.pig.impl.util.Utils;

public class PhotoFilter extends FilterFunc {

	private final static String signature = 
			"R0lGODdh9AF2AfQfAFVVVf8LiQFk3CkpKf///6vM9AYGBs/i+P+Uy4Gy7r7X9uvz/P++3//3+9zq+vT5/v/6/fH2/fn7/v/u9/"
					+ "79/szMzERERLu7u4iIiN3d3WZmZpmZme7u7qqqqnd3d1aY6CH/C1hNUCBEYXRhWE1QRD94cGFja2U8L3JkZjpEZXNjcmlwdGlv"
					+ "bj4gPC9yZGY6UkRGPiA8Lzp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+ACwAAAAA9AF2AQAF/2AljmRpnmiqrmzrvnAsz3"
					+ "Rt33iu73zv/yWCcEgsGo/IpHLJbDqf0Kh0Sq1ar9isdsvter9CEXhMLpvP6LR6zW67ieK3fE6v2+/4vD5c2fv/gIGCg4RecYWI"
					+ "iYqLjI1kh46RkpOUlYKQlpmam5ydV5ieoaKjpJOgpaipqqtzp6yvsLGyVa6ztre4srW5vL2+m7u/wsPEhMHFyMnKb8fLzs/QXM"
					+ "3R1NXWS9PX2tvU2dzf4MTe4eTltuPm6eqo6Ovu75rt8PP0jPL1+PmA9/r9/nT8/gkciCYgwYMIpfVJyLDhGoMOI0pMAnGiRYsV"
					+ "L2psmHGjR4IdP4rsF3KkSXolT/+qXJdypUtyLV/K3BZzps1uC2/q/Fdzp89iPW9tsEC0KAEPOYVYWLJUT1MiT59EfZiBwIYNbZ"
					+ "ZW8IBk6tZ8QW1luHBhAAayBCxcKLJWSds8b4cYoBAl7hq1BDBgaLP2wlS5dIf4BZt0HV6lGzRY2JtXSAeiHYrszXDVggcOiiMT"
					+ "qKAYwEIMFjR0WMsBtIaqREqHrpqhgwYAqCtozls6TGYDQi4AsICVgOrTkh1DLsLBQ2gOjYVsyND6dXPYGRRjQE7WNIeyp/XKFk"
					+ "KZCFoC3TFU2K2ZswXPtMNE7rBbA/K9gwmwP07BwGMNfeLr5v0uLK7DadnXgX0EzJWBgAagJsT/XBcYcNYAA2wwYAYcOHgBBgNQ"
					+ "gOGFDlIAAAAcIjfEeSE2iEFv4GWYYn0UVIhBBwPMVYEBG5S1F4kYGCDighQc2MGEUGlwgWIFBqaWiTU6uAEHZg25FIYPCjlAdh"
					+ "hkoKIHjAmhV25LGQBijhRaiCEFGvRWZoOjAaBBkYOhqduaBgzQQY7XaUWjje74dwuA/OWFQX2byQkeEYCWRYCGXKV1AQeR9TjX"
					+ "AFWRWeUAQ5QpRI+UCnFmpkQI6oEHgCbGHW6WpmhlpSgCWoGgCmI6xAaAKkVWpoZateahkGJwq18U4LWlnBQMkNSWBPCa4KFqMX"
					+ "rogfJlWCEHYylnQaFLRWvV/7THEnDmUqWeylJh6vDZ1pZzHWWAAe4RweC0fsqalwVxzlWunw2ee+6fXAamV3xE6EqAsCwSWyS8"
					+ "9hrAq5ZZFmkuujseDJis+jWVY8EXMjaYr3vpuqoE/VrcpZFrgRYvXQluAECLGsQ4ALYUDIaZyiwjfHHB86ajpy3iIhxrVTBmSS"
					+ "3Csm4wwAUUSICbjkJguSoFTCsYaKVn/VWhoYBiwFWLcwHQAdM9rgr1EDs3m6XXjlU4IllNDYYo11URe3HIkzmY6BBuY3u20EQb"
					+ "nTQGAGClAX4t2z3Y3xUEXp+IWPKqNdt5gptOzu3OJTRyACS8LtCK8u2bBnP9vZmD/042dP9goRebIL9EAGAWyZgSzTkFQre9lF"
					+ "mmOy35AJQnTHuDFa5ncMSj9qVj3WlFRiyEdoHGAQdEKZw5AJuXuypuBFSe4seDWW9llxq0DOlgsb/b+DyQ6xWrB+feeilu8RGb"
					+ "LMHoUwhAnJVTUAHBCdsPr6Cov3osoBSAkQHQp6VzWaBw94vTbA5FPfShi1AJFJTQBniktD1FgIKqm4ZoRKwNUC818zOL3WTFvH"
					+ "PFTwhTOtT0BuCBDLFvWitsYX3Qxz+JGbAPAoOJ4/TBtEv1kFCkc0IHOMC0Ui2Ba0/oIdMkgESwATGIhwpME4vQxCkmgWtQBGK/"
					+ "1EfFH/4QCljE4hPDaEX/QvVnhzKBEud29IsMgOkntECjTOY0HWScBY5xxKMez7jHPtpMjn4MZDRuJshChoKQhkxkPACpyEb6Ap"
					+ "GOjGQjICnJSiKCkpbMZCAwqclO6oGTngxlHUApylK6gZSmTGUaUKnKVj6Cka6M5SJYKctaZoGWtswlFXCpy14+gZe+DKYSgCnM"
					+ "YhaBmMZMJjKTWcxlMjOYznxmL6NpSTayUZpHoOY6ONCbiunljn6q0ga+eSK69YWcKALnhYxAlnaSpTClGs8QruPOekKLnPisEn"
					+ "jyqRcUSVKb6xANeOqJGrII6UjvNCe90KbQYiUMV/lsCj3LMhoLjGZRbiSng75JoXq2/3No1BHShUTaP0cCVB0cqFxRVppOv7B0"
					+ "MxaA0GLe8hR1PjQtUGxKRvmpz4454SnrdOiIMnlSd3hzX0mLaUmRlTENfLOmK1tp8yCKz6cMhShcwUoF/Pm50RhBqkq5VFmIIl"
					+ "OjWLKo6xiLO1GzFKZJtTcVwt2+GKoluxRhaPWsaVuWspSghoGFQ+LqU5oCVHxdyEhEhSUch4KbqxalN3gtiz3z0rfK0bShXfkq"
					+ "3aLKV6EKx41rcSpUhpoWwVgsS39pJFrN0Vd8jgtDKYOS1eR3ljk5VS9P2U1RRqcuqU41L3st7TqjY4H3jGtoYU2uVaNa1t/+U7"
					+ "F4fNJZqpMwvmqIM/9YOcsd58ovTmnILn+BqlFa65ssgXMzC1lpovSaG9QKZ4GGXC05mORGztJuqEthEmOMC1zSZgB6mE3uaOsa"
					+ "1vzc9LxEmBtpq1dQ9/6XwaqFrk8eo5vpuhOufHUN3hSqq6cKoW/9sosBfKtX5OTXsyFegoja6i2Hrhg80INNhB25KMfkM7QWVd"
					+ "OSwGNRAnO3Wlzsb4KNMLcNyIY/GSAKuIy8BJf2pgOw4s4F7tenx8C3kPJlCIWweYcsc1mUXv6yJ8MsZk2SucxnlTCa1zyFM7PZ"
					+ "pGp+s5yZ4OY5xzfOds7zMfGs5z7Xuc99/DOg9SjoQcOx0Ib2CaITrZNFM9omjn7/tEwiLWmXULrSKrk0pk2i6U2LpNOe9gioQ6"
					+ "2RUZMaI3w+9TNN/YaVCilpqU6CVBVMhCtr9gi2ZkNfjFCBqlwlCl8xwq/h4DQpGGdRFeAAGpGCEFa74XcX8GBkku0FaE8ZCTVD"
					+ "gl15RIfOFgEvzImCso+QQ0VVQbLKRgqtobJtksQaGbH6q1Wq8hvUPKbH4JEOcjBzHEJ9cJ6myQDsQFfv4MgHMgN/6GO0hpWCb4"
					+ "Y8FttNb8RDnuREWzk8aw9yxqmWKal1c4tZTr/G0+PuyKfiVgPNZbCjoA4spDbEreMF5hNbkydn4VBOC9+0Zpvz5Ic9QTaHs9uQ"
					+ "bWHh5UMh8tGE//TrJG0JyXPqyumXdOTBP+EIaYBResk6JBj7eHApSKeTiwRovzvRzkscslIfOLeg69jnTQUSjdColBYhzY9ocp"
					+ "n6WPqKIA49yAMsf9WtEsN0IuXItk16CoO8PuIASchgLrpQhg6f63AMfQ3xlpVaWlymVUXG17cK1n89QCE21qdgrtIUrAzEKSOq"
					+ "ilUMLEKphsL5q9yKWd1yYaTUYr6VVUA3A5XW4ZAFN29JALnr2z1DS3UBN+5qWgDCGnLkJCpcufFqvIoPoGbP194QvlHMsto8Lr"
					+ "+GovuFLDT7kwPdMzF7TVll8DUAE7nmMHIZDnPcXj9ysn2Y86dfYHNBMPbCIv+ysipJhiVYsjkwQ4Dmti/soiikMy8VZG504zEU"
					+ "OAR9E340UzFcUiyK1ytt8TYdKDIxoiE39UfqkHnMohZL0zTcITZWwziskSP+dkyt9ye4QTba4jPUwzOr82/VoxmP0YI9khdXUy"
					+ "EesjVNE294YRYvojpVQTiG80EYo4PIF3sQA3aaMRbEAyDCoStr44LEo324wXMHVzyO0Tejozflpg7kpwbzsio3sha0YyXRhjvV"
					+ "UyUGYzpMolVAmG2l0yBVgTS7ky08Mjl5WCBE9Cq4wySzIzrNZwBrdzuy4zx40UKLAiEfJjofoxTGsxcRYhV7+DCKAj6QklLf1I"
					+ "H41luFcyD/wlMaFjgjVYE+sNOIK5MWuGMlcwI9mDEXbYiC6XB653JfaqE/95I06eMY8Gc/KhMXmadC+9Mo85MBEbRA55OMHmKI"
					+ "G4QuMIQ/yhEjBJQXN8SERMM7pcOMcSJD1LNB47QX06NUYEOFdDUx7Ocx7Ch7nIJBn9iB2+hBh0KPfOVAe1FCA5QgxOOG7wZvYt"
					+ "QEUyRGS5RFX/REYCNFERlFPkQ6X9R8yhF0A9U0A8AxTmSREtlFZDSRr3IpkKJFSpBFJjmSUVSRZuRDGmkrl9JsCalIJoI+21Yh"
					+ "WHJfaHAeoJFacCE3o5gQb6gNFaAXxUYEGYV3acBN5TQISdlTDHGUqgYS/zd5lb5klVrJE1nZlbnElWCpD2I5lvhQlmaJEl+Zlq"
					+ "6ElmzJR2/JZm4ZlwhJl2s2l3ZZDniZl5a3lnw5Zn75l2YWmIKZZoWJTXt5mNeQmIpZDYzZmINEmJCpSI85mc5QmZapDJiZmciw"
					+ "mVVgHCvlOL2mYrtxTb7RbmMwmqNSOEjwFULZZlklWGnAbG1wHYTxOBZWY1+FmloCIuR2gmQAIDmXBG2yBeOWQ88YnLxJBr84Pq"
					+ "xlVyQHHx73LovhHUSBFTEHLUSxFtnJlBLic7khcewkng9XnYFHc9fEHip1c8NBGdYRnn1CcSWXXR4gHdzWnUPgnv1WLOT5nQmz"
					+ "H73hhf81ohhHli7X4puUMRSXkRncoW/FciHHkWR4AaBwWQ5eCFFlUSMetyEwgiJOFnm7IaGLYiG74R0DNCSU0iAS4pOfo6F/0i"
					+ "QRwnJuoiYmCmUjBihaNxZoRyQq2jP1oXe8kiMPgoMt0iRE0nUgwqM00jO4+B0tiideCBkYkqRrgiE2in4PEiFLZ6TTAiUst509"
					+ "yqJCJ5l7QjMksyTK1zKoIVoVSJMolFFu+i8Clxuc0jm9YSWkk3v1gabE1yNtMRREwH1F4nmD0iBDACl6miB0MYFguCKS0z1vGh"
					+ "iG+qZ6uoqqxx0ZEqW9oSOBI6cbWSvipyiJERi5AqmDsSV6+i3mAIL/CzlB6IEX46EyCbMlxAKCtMoYICgYTQEohxFvvUIzVhEj"
					+ "r0oaKRMnUdF/I6R/2qcWvSovZ3OquCov2+h+unopzNoWvOqMAnguF2husWIsuto+jMF7G0g8W9KszmmhdiVw4wE9TVglEpBDtD"
					+ "o3wjKvh8qaHggYZsgsGKiEPfKv7WpuhOOBpGOGj6EwPDNTnCIs+3o0IHOqiWI2vggqSygYCzseW4gbF7o4SxilbVEug4F1JiOu"
					+ "shKDS2iue9Gw6UoOF3ogOOSukSGJpjOrcUM0GGKE4GEwGsIp+Rp7sYMZXBQ+oOGylIWG2nOLQwAjFLIyoYI7HnIWNJIXlPKzf0"
					+ "OO/6cqiZgBKrjhinzYddk1tafoORcqtHwVF3jxrdziHlZSIxIzrpH4iuaaVWHLkX35OOkniiPWNjTiQXECdZgjQAfUMg8EuPja"
					+ "swpDj9c0MQeEtxagt0amMjLULybUievnPfCyuOLIMJbIUHwTI+lSLvroHSszYp+RPiZmV4rbBx7LbS7DOaBDsuYWumP4QIjbLm"
					+ "MajGGEBDB5RQs5kWXUklagRLl7BHTxRT/EKyzpQ7xLkRcJRcg7BjCZvMT7uwLhmeSwVGOAvapkveFgc2fgvbLEvZz5C+I7vr1Q"
					+ "vuabC+ibvrewvuw7C+77vrEQv/L7CvRbv6twv/ibCvq7v6XQv//+OwoAHMCHRKYEPBMDfMCckMAKvEgNDGYG/MCZFsESzGkUXM"
					+ "GfdsEYLGoavMGl1sEejGohXEkMPML2AMIm7BAlPB7ngh5IIIRT8JrztJxHIMOpQcNGUHlEprpgoMNWgBTDlgXM5hXrBozfsLZM"
					+ "QydHEB+A2ARNLBnAqQRPbATNSZw2zG7OwwXaewWbt5S0sH9BtMV1Cw5sqiWfp29kEiFVJy2+SQQ1UnH1oRtmSKEiyp99IhgQ6h"
					+ "69Ap9wpRhqu52pAZpCSB5pnF2vUhUUCoHUk8gDekD3ISJD0cbyuTUp409vjG/FMRzv4sLzZByg0U4gly6XrBkFVx39thz1cR//"
					+ "+dEUkYzDOBEOhjhPXKotatwhVjogZqulexgnJbKkZiGiYTqrMLomVTEjLup0Q3IagDwiQuJBOvojBlPJDMiCZjeHg1rNaWFRU5"
					+ "rMUgvNa4F2YCLNI6LLaxEa0VYyQzMmQaIbBoOqT7cmMfV45Tx1pTHM5qZAdHKqcoLLtwsOQLiRKLl3TIMbogfQI8IYo/o/apF7"
					+ "7cLQdPN8qIKp1UN6y2O7J9kiC91NAk2ODF2MpHKnlMIfw8crKWkr9aF8DqMUCL0mWIHRENqKYGN86ZyyFG1iK117/pKvHr2mbF"
					+ "vQcTrG34B1kksxTEwyBfOAFBiyIAMgvmjN3IY5/LKtMgJ//xYtHO7nhUWteWaKFwG41R9LFyF71NNMV2fTgY/hfikVJ/iSLyXr"
					+ "jlSNrPWXijr9zfoSNQUi1nqJwqQAYq+CgIyjNtTDqS3CRvjGHuSosluism3as0kog/uEGznkIxGgKPvKUIftr1Xh0Y3tgme7yF"
					+ "1CRINttVNR2LBhH5OdLMvjQTlhhRw4gxo7yDp4Wxbo0Yhj14Jd0XkdDmXBmjPytnxYK5z6N8tjPeMMLYIigXdIIZ4jflTLRQcZ"
					+ "rJUYijOCs0OwKu9hMD9rWZlCILhMtvdci5V4GCCbtsO9F8hdK8Wti9YtjtzMJElBOx7EgdKtsbkoKIUIp3Mdd96zd05X3v/9DA"
					+ "5fBy9PRtVMkiHT2IuaOyICiYUNaLqmU6UOvtiok7roiFwNoj4U4EDiibgFHqwUFDI3pMgFRLre6tnRwzDk2OEKfowZ3sK8QZAP"
					+ "lUBKZT4rVM4LbowZNNtEU5A1dOLpcqFIqdeoQBaT7bsUWbxH/m12sbuHckVSQJFMtJLRe5EvaQRQFJEQmeXM65JATr3T20VR3q"
					+ "2+e0S9O+b/LRFA/g1p3gVrLsIWIXLmAOdkIOcfUcIpnAh2fueFkOd6LpVC3ucDweeAvg9/Puj+IOiGvgeInuh5sOiM3mWF/uj1"
					+ "4OiSDhCRXunwQOmYzgyXvumq6umK1umgfuajjsCiXur/QI3qk3bqqs4Nmt7qZvDqsP5Ks/4Ssl7rX3DrWECbS1DEbpwUjPJNcg"
					+ "TKSZMEkcFk2MBVU6kd7DSTSakEGeDrSsCRdK4E0m4VrA4U2f4LsLgjaw5gAuZYRnFe91MjMwcA0i7XpYUwHPUk2/bs7RWookEW"
					+ "rhFkSIViR/AhaLQqMeVexLE6TSDDCCYRuu4ERbqiK7ZtwTJgWExgDr8+R9CO5LTuD4pXzCNkbKE+fkXxC+ZbNQKcmLEkd3xXbH"
					+ "VRU5FSrvFQQD5YLzXwEVHwThCD8uG2R8CvAjYqa03uTXLukJpNGknWKDRULm8V4L7xunJRHZCAfa0X1HFTnNHGUgsA/9fEKeR0"
					+ "QSlzXIDnLt+2YAFG8NveC3yi9ZJxhaM9hA/vG3QkHknQNyOFW6khs6U19KojIhuPKXqBh9WdKHNiuYVhmkiQOOFD8dfGFgl/ax"
					+ "x/9l6P5pcl5r4xNA84Yg0HYuUuZCT2WEbAKRNINzSazbxlnW7jexOFV5GFHEmWJpFxWGzhUe0ELhWziEfRXjxF81t/81qCtBMB"
					+ "803QLQB2oUyyNUrPXkJQGqXxFhO1+kjQjh7Qjk/BJMxTzt8FQSkpLJ5V/B7VMLppLR3zTU7YT8KmKUkjIlxYTy+yV9w5UoBK+x"
					+ "h/+1/PC2XxHgEaF2uLgVgRFeGWYsEv/kKJ26TFPP990PzuLrogUFFENmQXRqgEhwGWBWDcSmwwPqQ1T1hjb2W5XCwqA01IXA4u"
					+ "Kw/Mg7nQjCodBgPdTIPeLzgsHpPL5jMaXKmk2+43PC5fYQYWj5KXCVpVWd7FnkpGVqFGH+DOyoZKhZNKFUGXCgUHB9AKygqFTh"
					+ "JBx0DNyRKKohciX5bRxQCjUGFWU5gVBSmb5OOc7i5vL9qab7Dw8PDQGOLfVxGZJpnRJFjz0S1kKJ0GaeDstg+HhTc1qjEYKs/k"
					+ "MnG6+voXMPs7fLxPLnnNDQ4OXqNM4YtXBT4crnqwgabMVAUAdmIAoEahTsByPCTWgHLHA5sKiizAqkOPDxiD8kb/kozjriTKlC"
					+ "pXvqGASR21Rp5Y0qwp56TNnDp38uzp8ycxnECHEi1q9ChSdkKTMm3q9ClUokujUq1q9SpWXVOzcu3q9SvVrWDHki1rdqXYs2rX"
					+ "sm1rMqbbuHLn0q2Rti7evHqt3t3r9y/gn30DEy5seOTgw4oXM9YKtzHkyJLTJJ5s+TLkypg3cwasuTPo0HI/iy5tmizp06pX83"
					+ "3M+jXsrKlj067dc7bt3LpV4t7t+7dS18CHE0csvDjy5MN6K2/uPAzz59KnE4jOGWOZG6ZWdFAHQMOYCoK6O7spKDweipTxbBio"
					+ "AmMFfdKtbx43xkCH8zUMvAzGAbwYxqBzX39p/9gnxjIfvcFBQdtVwMGAz9GH2YHVHdLQJhgYgEEGLVigQQYUbLAhCxh8qJ8kCV"
					+ "lAXgaHzEAAESZqMBOML7inQgcvzMgKiBq0ksFAO+T44SUGkNDeHf+tOIiLNMRIJI97XGBjDznK8AyOMHQHpIxJTGmBKxtU4OKR"
					+ "knhApg0nWPHljcNNeNmBHGyIwgCYaMhhDCggMSIGFOSp4UwGAKBnh3P6U8cUAwDYiAEbsGLKBfhNiY2iLbYyIH+RdjApAZnOOU"
					+ "ArHRjQoQ4XHCJJqYpGWUGjj9ag6YhG8JcBfqKeIKipqzQKSgoWDNCBqI4+82mvRBjBqqM6JPemZXF2RwGtNf9QMKs1BGiwwbTQ"
					+ "VnvtJqMS4OcFGwDICSEARsgtCdWS8MgN4NbyAzrTrqtCu0ay8q0WKgwhLiUmYGDuD8agW0IN6NZb3a8kwGjktyYMHAqYfkzxDI"
					+ "AED7HMw8seV1uFJhowAH/7vQvEH5n+ILHI+paiRDIRWmAAzDBj8h/IdoAL4yp98Efzxz+YPEIyQ2gYswEr4zyPD0SHrLIKy4Ts"
					+ "Acw79jGEffyNk8UyyXQK7jIvE12gb8xOduAGTVAgAcOUGFnBtn2u3Xa3LOvDiUY7DEgBAB24BG3BGojQtRMJ0mKkBn6PbK8VQa"
					+ "Pgwd57tGzEEHjr7ZJ+eWcpr8K83ntFQiz/Gnn1xGZC8rmxkjeuMXEHusCCBksfMYKyke6BBAGxe7vC0kPQ6kQdEjxujwmSICJD"
					+ "upB3dy+re0CNdwolyPouyvPsvnALdhtvQ/Am1gAKqc+XTYMMkTJSB/YdFo60JKFr+KAG6RlbvvCoA+cn0TN4DfV58lIA0MfPAj"
					+ "Aq/xIWN6aBwgAWaFDTUDG0A26CbR/zQJ00tAEOgOohMBvR/moGQXmhQ3GfqFl3HmfB8cGMgZsYkQEOgTmopRBGdjDgLYY2I/Rh"
					+ "7RkKSWEV3ieJEiLQTRurzd5cEgYhUoKI3wLC3nhgxE0sUQwFCqIQo5jEIh4RimBT4hSDsMQnYrGII1gG/9iuiEUxUkI5YqOOYi"
					+ "KERses0TZAaiMvzgjHOcJGjnS842nsiMc9gkaPfPzjZfwIyEFm5oeEPGQdDYnIReZRkYx8ZB8dCclJBlKSlLxkITGpydAIcpOe"
					+ "LEsnPylKr4RylKa8SilPqUqopHKVrkxKK18pS6lYcpa2/Eosb6nLnORyl75ESy1/KUyn9HKYxoRHMY+pzHQkc5nO9EUznynNOU"
					+ "STLBbBgWvEEwYOvIBGLFDQGIrQAfWYgTwq0AAAwAAf+cSBViIQQ3t+AqGwBRMyHCGFN9H3BReAU2tm8MAGiAcHzJ0zn0pgkC46"
					+ "0CYv+HMnDa1NNcdSoeq8IAVREh5HXv8FA0a0iCMcyAAMnNDRF60ASOPE0Cd0RAMPgYgHLB2PlmywoUpYxJyQgIKJAuEKE2HoAh"
					+ "3AKSRktAeTxoANb+zALVpAgial71UoIFKJTsQDRx2iAuOcoQ3ydKQbeEBJLGLqk2YEUgFRKTYRBUuFOJGoZIHII6AYSBHAJKcp"
					+ "vGCsVDAUIiLlAVOFQlOc+hPtVgCAQQGKVsAaFZ98gI0R6edDUyraMspGpx0mqleERYJe+Xo0ftlgRqU6ldYekirwAHYmWqrDoM"
					+ "5XB4VC9lOhGhUFp6BCREUppOLjlVnr2RivxWwEjfrotyKnDWuZ4g+crR0hUnBcEwBhc1s7Ab0sUAL/IKArXdQVU8IEETJXUDAX"
					+ "073C+0xk1IekM13ftVaycAc9OV3iV+ICgr+Kq4EvSrdO53RPxDp1iWUEr7Oby9c83nuFctG3qQ8jYyRX464plk1QbDBGQmpW3B"
					+ "QkA1x/qLB94mUkntlhQBWOVP0IwMIZYW6cHytaJmhxMSNw82MU3sG0PBy6byHuWwDYQLSGFjOjRa8IMq6BMTBXBIL6OHE7EJrS"
					+ "jJa1Yqktt6yZaIgSkk5j6CADvpuw6Dh3YfkM4J1He13h2FAEtq1AA4ogM+VGsIdxug1a+LmEfcgM3mVYggMj0og+5CRn4mIKep"
					+ "/gCHi00LiHYBlnezYzkB8RMqcl/yGgHTzy4gb9uz9YjgRpY81Zv1Ihd0piyt0xgJhJRIfmFU20otudqVPcZOI5r3YWvd0V+lSC"
					+ "cA2AAxKQwbQYVGsL0kNZI3rf+SioEVD/Bw+2U1OTMdVlSwcOCf783bETTeMEWkuslzKyyqYXqepRO19lM9+iMK1bxvAWZsotoe"
					+ "MahUJFhTsZBTxgLVr4bi9/eWsOHMAGA2jT6nhtByMewf8ywEIbYYJ/vnqfN8zdaZDNUN/1NlmZq1XAhD3bevzu30QUXeD/KPzR"
					+ "BARhU48WqUBHjQYPFU2mvWJFLzQRDFAcY8upuPIqarEHRpSiGMNYgyxsseUxl1YWxxD0LiqxDP9BlNC4p2mGkysdDimfJRGaHs"
					+ "ekS73qyKS61bPOTKxrvevQ5LrXw05NsIu97G54utmljva0K33tbJem29/uzLjLXZl0r7sx7453Yep9777su991CfjA23LwhJel"
					+ "4Q/vysQrXpWMb7wpHw95UUp+8p6svOU1ifnMr4UCB0jABz6QgAQ4gAAPcADqUb+AOUgg9apH8NQ5v8YDfEAAtr99AgiggNvbPv"
					+ "cEAH3oQ3+AMziA9wL4wNVlT50DGP/2BaBAAYyfgBHUnvcKOAPzeY/8d2xe+WCRQPWbf/3o89734bf99c2Q/dtvPzjef8762S98"
					+ "6EtfAgQ4vwAUAPsaxP/4yX//v3LQn/ZFgARIwAiQH+6ZXgEEn/Bhn/G13zp0HwByRQJI30sgYO/ZnwG+Qf9BIEyQ3QRyhgDeXv"
					+ "qtAAYKgO8ZXQQsAAHy3wO+wQRMQAOMAAXIIHSAYAhaBgM+YPAdwAhmIAEsIAM6wEtIwAI+YAKsXgfWAPAFXwFIAAQgAAIEgBQy"
					+ "QAMwQABkYQAwAAQwgBTeYA4OR/ONYQEEofSZHv6VoO6NIQkSwBJ+y+4Z3/BNgBZmIQJgYR0iQANUIRiG4W+wofEVQAScYALYXx"
					+ "quQAUCogAM3xsuQPM9IQU0QB1S4SRuIQF4IQL0oR/uhiImICGqwCH+XicKQOm9If59ACZU/2IlMsAITIAYSOAmOsXogR4Sjl7p"
					+ "EaIhGt/1FV/zfUABKIAQEuESnuAHRMAKSKIq1uEEQAAZwGIsPkUitmENfOL96aIoBmINSMAC2N8SNl/p1YAq3uExloEzPmNTRK"
					+ "PtleE01l81Wl874t4DgE0HoqMAlCEmIOMkQoAr/gIOmmNj0KM6muAZviP6PYA3fkH/NV8KguMkMsDZ9aM/LgZA8gAuEmT+RUDz"
					+ "RcAVJWQgPgAP4KMWTsD+BUE5RuRRTOQ6lh8o6qJBGt/qeUEHHiQPVGIDPKRJ5gZKCmT55aI7ih9CPmAcsp8xHmMlvkFJ3iRR5K"
					+ "QKUOMh4p8AvGQPdCD4Df/kClQiM7bBUSIlUCilGe6kRY5fL+qfCkjAAQjjC57gIgIBSGahUUKkVgYGVzKlNS6AUx5f6KWjG77g"
					+ "VBqfR6qAVdrkW8JGXLJjKHJkIJLlC+alBRLAWgZAWwamYGJjSibgV64A7Sli7i0h9fkkAfwlVrolZO7FYHplKKpABNAj7z3fGx"
					+ "IAL/IlBBQlYIamahxAAdRmbULltyyAbdbm8HnebhYAbo5lATTh6CmAMQ7ibg7fCgCjbQLj6nkhdDpkbMombPTcHIykzencdFKn"
					+ "12Uld9LTd4qdd4ZnbowneUIUaJ4n5aWnel4ee7an5r0nfF6Sec7natSnfTZSfjYdfu4zJ8qtAYAGqIAOKIEWqIEeKIImqIIuKI"
					+ "M2qIM+KIRGqIROKIVWqIVeKIZmqIZuKIcKaAgAADs=";

	@Override
	public Schema outputSchema(Schema input) {
		try {
			// check the schema this udf expects
			if (input == null || input.size() != 2 || input.getField(0).type != DataType.LONG || input.getField(1).type != DataType.CHARARRAY) {
				StringBuilder sb = new StringBuilder();
				sb.append("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: expected input (long, chararray) but received (");
				StringBuilder sb2 = new StringBuilder();
				for (FieldSchema fs : input.getFields()) {
					if (sb2.length() > 0)
						sb2.append(", ");
					sb2.append(DataType.findTypeName(fs.type));
				}
				sb.append(sb2);
				sb.append(")");
				throw new RuntimeException(sb.toString());
			}
			// construct the schema this udf will output
			return Utils.getSchemaFromString("b:boolean");
		} catch (Exception e) { throw new RuntimeException(e); }
	}

	public Boolean exec(Tuple input) throws IOException {
		// validate input
		if (input == null || input.size() != 2) {
			System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: received invalid record " + input);
			return false;
		}
		// check if image is valid
		try {
			Long p = (Long)input.get(0);
			String s = (String)input.get(1);
			if (p == null || s == null || s.length() == 0) {
				System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: received invalid data (p=" + p + ", s=" + s + ")");
				return false;
			}
			// match against the 'photo is currently unavailable' signature
			if (s.equals(signature))
				return false;
			// decode the image
			byte[] b;
			try { b = Base64.decodeBase64(s.getBytes()); }
			catch (Exception e) {
				System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: could not decode bytes (p=" + p + ")");
				System.err.println(e.getMessage()); e.printStackTrace();
				return null;
			}
			// read image
			BufferedImage image = null;
			try {
				image = Imaging.getBufferedImage(new ByteArrayInputStream(b));
				if (image == null) {
					System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: could not read image from bytes (p=" + p + ")");
					return false;
				}
			} catch (ImageReadException e) {
				try {
					image = ImageIO.read(new ByteArrayInputStream(b));
				} catch (Exception e2) {
					System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: could not read image from bytes (p=" + p + ")");
					System.err.println(e2.getMessage()); e2.printStackTrace();
					return false;
				}
			} catch (Exception e) {
				System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: could not read image from bytes (p=" + p + ")");
				System.err.println(e.getMessage()); e.printStackTrace();
				return false;
			}
			return true;
		} catch(Exception e){
			System.err.println("[" + this.getClass() + "@" + Thread.currentThread().getStackTrace()[1].getLineNumber() + "]" + " ERROR: could not obtain string from record " + input);
			System.err.println(e.getMessage()); e.printStackTrace();
			return false;
		}
	}

	/*public static void main(String[] args) throws Exception {
		byte[] i = IOUtils.toByteArray(new FileInputStream(new File("/Users/bthomee/Desktop/photo_unavailable.gif")));
		byte[] b;
		try { b = Base64.encodeBase64(i); }
		catch (Exception e) {
			System.err.println("ERROR: could not serialize data");
			System.err.println(e.getMessage()); e.printStackTrace();
			return;
		}
		PixelsFilter pf = new PixelsFilter();
		Tuple tuple = TupleFactory.getInstance().newTuple();
		tuple.append(0L);
		tuple.append(new String(b));
		Boolean f = pf.exec(tuple);
		System.out.println(f);
		System.out.println("done!");
	}*/

}