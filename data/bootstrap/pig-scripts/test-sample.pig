
a = LOAD 'test-sample.txt' USING PigStorage(',') AS (a:chararray,b:chararray); 
a_r = foreach a generate *, RANDOM() AS r;
b = group a_r by a;
c = foreach b 
{
    a_g_sort = ORDER a_r BY r;
    a_g_top = LIMIT a_g_sort 2;
    GENERATE FLATTEN(a_g_top);
}
dump c;
