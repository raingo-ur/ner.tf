#!/bin/bash
# vim ft=sh


#name=subsets/yfcc100m_mscoco_intersection.txt
input=/projects/flickr/flabs/yfcc100m/yfcc100m_dataset.bz2
autotags=/projects/flickr/flabs/yfcc100m/yfcc100m_autotags.bz2
output=yfcc100m_all_text.bz2
./scripts2/tokenize.sh $input $autotags $output
