-- load YFCC-100M data from flat file in HDFS.
-- note: the userhandle, camera, name, description, tags, and machinetags are all URL-encoded; the photopage
--       and photopixels URLs do not contain any tabs and therefore have not been encoded.
-- note: camera, name, description, tags, machinetags, longitude, latitude and altitude can be empty.

DEFINE loadYFCC100M(inputFiles) RETURNS outputFiles {
	$outputFiles = LOAD '$inputFiles' USING PigStorage('\t') AS 
	          (photoid:long, usernsid:chararray, userhandle:chararray, date_taken:chararray, date_imported:long,
               camera:chararray, name:chararray, description:chararray, tags:chararray, machinetags:chararray,
               longitude:double, latitude:double, accuracy:int,
               photopage:chararray, photopixels:chararray, licensename:chararray, licenseurl:chararray, 
               server:int, farm:int, secret:chararray, secreto:chararray, extension:chararray,
               isvideo:int);
};
