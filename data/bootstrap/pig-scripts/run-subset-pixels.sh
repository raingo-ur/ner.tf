#!/bin/bash
# vim ft=sh

#name=subsets/yfcc100m_mscoco_intersection.txt
#name=subsets/yfcc100m_mscoco_intersection_2014.txt
#name=subsets/test-keywords.txt.subset-v2.bz2
#name=subsets/lda-samples.txt
#subset=tags-v2.2.txt
subset=synsets.pids
name=subsets/$subset
hadoop fs -rm $name
hadoop fs -copyFromLocal $HOME/$subset subsets/
#input=/projects/flickr/flabs/yfcc100m/yfcc100m_dataset.bz2
input='/projects/flickr/flabs/yfcc100m/photos/data/'
ds=/projects/flickr/flabs/yfcc100m/yfcc100m_dataset.bz2
./scripts2/subset-pixels.sh $input $name $name.pixels.bz2
./scripts2/subset-photos.sh $ds $name $name.ds.bz2

python email_notify.py "subseting done $name"
