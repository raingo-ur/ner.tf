-- set name of job
SET job.name '$name';

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
IMPORT 'macros/load_yfcc100m.pig';
register 'udfs/utils.py' using jython as utils

-- load dataset
Y1 = loadYFCC100M('$input');
Y2 = FILTER Y1 BY (isvideo == 0 AND photoid IS NOT NULL AND (tags IS NOT NULL OR description IS NOT NULL OR name IS NOT NULL));
Y3 = FOREACH Y2 GENERATE photoid as pid, name, description, tags;
-- Y3 = LOAD '$input' USING PigStorage('\t') AS (pid:long, name:chararray, description:chararray, tags:chararray);

Y_name = FOREACH Y3 GENERATE pid, FLATTEN(utils.genBiGram(name)) as bigram;
Y_description = FOREACH Y3 GENERATE pid, FLATTEN(utils.genBiGram(description)) as bigram;
Y_tags = FOREACH Y3 GENERATE pid, FLATTEN(utils.genBiGram(tags)) as bigram;

Y4 = UNION Y_name, Y_description, Y_tags;

-- load target list
L1 = LOAD '$keywords' USING PigStorage AS (keywords:chararray);
L2 = FILTER L1 BY (keywords IS NOT NULL);
L3 = FOREACH L2 GENERATE keywords as bigram;
L3_g = GROUP L3 ALL;
L3_cnt = FOREACH L3_g GENERATE COUNT(L3) AS cnt;

--  subsetting
S1 = JOIN L3 by bigram, Y4 by bigram;
S2 = FILTER S1 BY (L3::bigram IS NOT NULL AND Y4::pid IS NOT NULL AND Y4::bigram IS NOT NULL);

S5 = FOREACH S2 GENERATE Y4::pid AS pid, Y4::bigram AS bigram;
S6 = DISTINCT S5;

-- positive pids
S3 = FOREACH S6 GENERATE pid AS pid;
S4 = DISTINCT S3;

-- negative pids
NP1 = COGROUP S4 by pid, Y3 by pid;
NP2 = FILTER NP1 By IsEmpty(S4);
NP3 = FOREACH NP2 GENERATE group AS pid, 'NONE' AS bigram;
NP3_G =  GROUP NP3 ALL;
NP3_Cnt = FOREACH NP3_G GENERATE COUNT(NP3) AS cnt;
NP4 = SAMPLE NP3 $max_per_class * L3_cnt.cnt / (double)NP3_Cnt.cnt;

-- get pids without co-occurs
S7 = GROUP S6 BY pid;
S8 = FOREACH S7 GENERATE group AS pid, COUNT(S6) AS occurs;
S9 = FILTER S8 BY (occurs == 1);
S10 = FOREACH S9 GENERATE pid;

-- join backwards to get bigram mapping
BM1 = JOIN S10 BY pid,  S6 BY pid;
BM2 = FILTER BM1 BY (S10::pid IS NOT NULL AND S6::pid IS NOT NULL);
BM3 = FOREACH BM2 GENERATE S10::pid AS pid, S6::bigram AS bigram;

-- sample each bigram
BS = FOREACH BM3 GENERATE *, RANDOM() AS r;
BS_G = GROUP BS BY bigram;
BS_S = FOREACH BS_G {
    BS_sort = ORDER BS BY r;
    BS_sample = LIMIT BS_sort $max_per_class;
    GENERATE FLATTEN(BS_sample);
}
BS_R = FOREACH BS_S GENERATE pid AS pid, bigram AS bigram;

-- union positive and negative
R1 = UNION BS_R, NP4;
R2 = ORDER R1 BY pid PARALLEL 1;

STORE R2 INTO '$output' USING PigStorage('\t');

