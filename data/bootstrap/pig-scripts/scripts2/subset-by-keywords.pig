-- set name of job
SET job.name '$name';

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
IMPORT 'macros/load_yfcc100m.pig';
register 'udfs/utils.py' using jython as utils

-- load dataset
-- Y1 = loadYFCC100M('$input');
-- Y2 = FILTER Y1 BY (isvideo == 0 AND photoid IS NOT NULL AND (tags IS NOT NULL OR description IS NOT NULL OR name IS NOT NULL));
-- Y3 = FOREACH Y2 GENERATE photoid as pid, name, description, tags;
Y3 = LOAD '$input' USING PigStorage('\t') AS (pid:long, name:chararray, description:chararray, tags:chararray);

Y_name = FOREACH Y3 GENERATE pid, FLATTEN(utils.genBiGram(name)) as bigram;
Y_description = FOREACH Y3 GENERATE pid, FLATTEN(utils.genBiGram(description)) as bigram;
Y_tags = FOREACH Y3 GENERATE pid, FLATTEN(utils.genBiGram(tags)) as bigram;

Y4 = UNION Y_name, Y_description, Y_tags;

-- load target list
L1 = LOAD '$keywords' USING PigStorage AS (keywords:chararray);
L2 = FILTER L1 BY (keywords IS NOT NULL);
L3 = FOREACH L2 GENERATE keywords as bigram;

--  subsetting
S1 = JOIN L3 by bigram, Y4 by bigram;
S2 = FILTER S1 BY (L3::bigram IS NOT NULL AND Y4::pid IS NOT NULL AND Y4::bigram IS NOT NULL);


S5 = FOREACH S2 GENERATE Y4::pid AS pid, Y4::bigram AS bigram;
S6 = DISTINCT S5;

S3 = FOREACH S6 GENERATE pid AS pid;
S4 = DISTINCT S3;

S7 = GROUP S6 BY pid;
S8 = FOREACH S7 GENERATE group, COUNT(S6) AS occurs;
S9 = ORDER S8 BY occurs PARALLEL 1;
-- describe S9;

STORE S9 INTO '$output_stat' USING PigStorage('\t');

-- obtaining data for subset data
R1 = JOIN S4 by pid, Y3 by pid;
R2 = FILTER R1 BY (S4::pid IS NOT NULL AND Y3::pid IS NOT NULL);
R3 = FOREACH R2 GENERATE $1..;
R4 = ORDER R3 by Y3::pid PARALLEL 10;
-- describe R4;
 
-- save output
STORE R4 INTO '$output' USING PigStorage('\t');
