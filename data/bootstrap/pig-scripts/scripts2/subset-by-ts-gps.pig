-- set name of job
-- mostly copy from yfcc100m_stats_geopoi.pig
SET job.name '$name';

-- set optimizations
-- SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
IMPORT 'macros/load_yfcc100m.pig';
REGISTER yfcc100m.jar;
DEFINE Date2Timestamp com.yahoo.util.pig.Date2Timestamp();
DEFINE Timestamp2Date com.yahoo.util.pig.Timestamp2Date('yyyy-MM-dd HH:mm:ss');
DEFINE URLDecode com.yahoo.util.pig.URLDecode();
DEFINE StringNormalize com.yahoo.util.pig.StringNormalize();
DEFINE StringAlphaOnly com.yahoo.util.pig.StringAlphaOnly();
DEFINE StringStrip com.yahoo.util.pig.StringStrip();
DEFINE ConvertPoint2BBox com.yahoo.util.pig.ConvertPoint2BBox();
DEFINE DistancePoint2Point com.yahoo.util.pig.DistancePoint2Point();

-- load dataset
Y1 = loadYFCC100M('$input');
Y2 = FILTER Y1 BY (photopixels IS NOT NULL AND date_taken IS NOT NULL AND SIZE(date_taken) > 0L AND date_imported IS NOT NULL AND longitude IS NOT NULL AND latitude IS NOT NULL AND tags IS NOT NULL AND SIZE(tags) > 0L);
Y3 = FOREACH Y2 GENERATE photopixels AS murl, Date2Timestamp(date_taken) AS dt, date_imported AS mt, longitude AS x, latitude AS y, tags AS t;

-- process the tags
-- note: tags are already lowercased
Y4 = FOREACH Y3 GENERATE murl, dt, mt, x, y, FLATTEN(TOKENIZE(t, ',')) AS t:chararray;
Y5 = FOREACH Y4 GENERATE murl, dt, mt, x, y, URLDecode(t) AS t:chararray;
Y6 = FOREACH Y4 GENERATE murl, dt, mt, x, y, StringStrip(StringAlphaOnly(StringNormalize(t))) AS t:chararray;
Y7 = FILTER Y6 BY (t IS NOT NULL AND SIZE(t) > 0L);
Y8 = DISTINCT Y7;
 
-- load and process event meta data
E1 = LOAD '$events' USING PigStorage('\t') AS (eventid:long, name:chararray, names:chararray, longitude:double, latitude:double, ts1:chararray, ts2:chararray);
E2 = FILTER E1 BY (eventid IS NOT NULL AND 
                name IS NOT NULL AND SIZE(name) > 0L AND 
                names IS NOT NULL AND SIZE(names) > 0L AND  
                longitude IS NOT NULL AND longitude > -180.0 AND longitude < 180.0 AND longitude != 0.0 AND
                latitude IS NOT NULL AND latitude > -90.0 AND latitude < 90.0 AND latitude != 0.0 AND
                ts1 IS NOT NULL AND SIZE(ts1) > 0L AND 
                ts2 IS NOT NULL AND SIZE(ts2) > 0L);

E3 = FOREACH E2 GENERATE eventid AS eid, name AS n, names AS nn, longitude AS x, latitude AS y, Date2Timestamp(ts1) AS ts1, Date2Timestamp(ts2) AS ts2;
E4 = FILTER E3 BY ts1 IS NOT NULL AND ts2 IS NOT NULL AND ts1 <= ts2;

-- separate the alternate names
N1 = FOREACH E4 GENERATE eid, n, n AS nn, x, y, ts1, ts2;
N2 = FOREACH E4 GENERATE eid, n, FLATTEN(TOKENIZE(nn, ',')) AS nn:chararray, x, y, ts1, ts2;
N3 = FILTER N2 BY (nn IS NOT NULL AND SIZE(nn) > 0L);
N5 = UNION N1, N3;
-- normalize the names
N6 = FOREACH N5 GENERATE eid, n, StringStrip(StringAlphaOnly(StringNormalize(LOWER(nn)))) AS nn:chararray, x, y, ts1, ts2;
N7 = FILTER N6 BY (nn IS NOT NULL AND SIZE(nn) > 0L);
-- deduplicate the data
N8 = DISTINCT N7;

-- convert place to enlarged bounding box surrounding it
B1 = FOREACH N8 GENERATE eid, n, nn, x AS xctr, y AS yctr, ConvertPoint2BBox(x, y, 2.5) AS box:tuple(xmin:double, ymin:double, xmax:double, ymax:double), ts1, ts2;
B2 = FILTER B1 BY (box IS NOT NULL);
B3 = FOREACH B2 GENERATE eid, n, nn, xctr, yctr, FLATTEN(box) AS (xmin, ymin, xmax, ymax), ts1, ts2;

-- JOIN the tow
F1 = JOIN B3 BY nn, Y8 BY t;
F2 = FOREACH F1 GENERATE B3::eid AS eid, B3::n AS n,
                         B3::xctr AS xctr, B3::yctr AS yctr, B3::xmin AS xmin, B3::ymin AS ymin, B3::xmax AS xmax, B3::ymax AS ymax, B3::ts1 AS ts1, B3::ts2 AS ts2,
                         Y8::murl AS murl, Y8::dt AS dt, Y8::mt AS mt, Y8::x AS x, Y8::y AS y;
-- filter out all photos not falling within the bounding box of a poi
F3 = FILTER F2 BY (x >= xmin AND x <= xmax AND y >= ymin AND y <= ymax AND dt >= ts1 AND dt <= ts2);
F4 = FOREACH F3 GENERATE eid, n, murl, Timestamp2Date(dt) AS dt, Timestamp2Date(mt) AS mt;
-- deduplicate the data, in case a photo's tags matched more than one alternative spelling of the poi
F5 = DISTINCT F4;
F6 = ORDER F5 BY eid PARALLEL 1;

-- describe F6;
-- Store
STORE F6 INTO '$output' USING PigStorage('\t');
