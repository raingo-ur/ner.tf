-- set name of job
SET job.name '$name';

-- set optimizations
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;
SET pig.exec.mapPartAgg true;
SET pig.maxCombinedSplitSize 2000000000;
SET mapreduce.input.fileinputformat.split.minsize 2000000000;
SET mapreduce.input.fileinputformat.split.maxsize 2000000000;

-- import macros
IMPORT 'macros/load_yfcc100m.pig';

-- load dataset
P1 = LOAD '$input' USING PigStorage('\t') AS (pid:long, pixel:chararray);
P2 = FILTER P1 BY (pid IS NOT NULL AND pixel IS NOT NULL);

-- load target list
L1 = LOAD '$idlist' USING PigStorage AS pid:long;
L2 = FILTER L1 BY (pid IS NOT NULL);

--  subsetting
S1 = JOIN L2 by pid, P2 by pid;
S2 = FILTER S1 BY (L2::pid IS NOT NULL AND P2::pid IS NOT NULL);
S3 = FOREACH S2 GENERATE $1..;
S4 = ORDER S3 by P2::pid PARALLEL 100;
 
-- save output
STORE S4 INTO '$output' USING PigStorage('\t');
