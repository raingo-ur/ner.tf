-- set name of job
SET job.name '$name';

-- set optimizations
SET default_parallel 100;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
IMPORT 'macros/load_yfcc100m.pig';
register 'udfs/utils.py' using jython as utils

-- load dataset
Y1 = loadYFCC100M('$input');
Y2 = FILTER Y1 BY (isvideo == 0 AND tags IS NOT NULL AND name IS NOT NULL AND description IS NOT NULL);
Y3 = FOREACH Y2 GENERATE photoid as pid, utils.tokenize(name) AS title, utils.tokenize(description) AS description, utils.tokenize(tags) AS tags;

-- load autotags
A = LOAD '$autotags' USING PigStorage('\t') AS (photoid:long, tags:chararray);
A1 = FILTER A BY (tags IS NOT NULL AND SIZE(tags) > 0L);
A2 = FOREACH A1 GENERATE photoid as pid, FLATTEN(TOKENIZE(tags, '@')) AS tag;
A3 = FOREACH A2 GENERATE pid, FLATTEN(STRSPLIT(tag, ':')) AS (ver:chararray, tag:chararray, conf:chararray);
A4 = FOREACH A3 GENERATE pid, tag, (double) conf;
A5 = FILTER A4 BY conf > 0.5;
A6 = GROUP A5 BY pid;
-- describe A6;
A7 = FOREACH A6 GENERATE group as pid, utils.convertBagToStr(A5) AS atags;

-- join texts
J1 = JOIN A7 by pid, Y3 by pid;
J2 = FILTER J1 BY (A7::pid IS NOT NULL and Y3::pid IS NOT NULL);
J3 = FOREACH J2 GENERATE A7::pid as pid, 'dummy' AS dummy, A7::atags AS atags, Y3::title AS title, Y3::description AS description, Y3::tags AS tags;
J4 = ORDER J3 BY pid parallel 10;

-- describe J4;
STORE J4 INTO '$output' USING PigStorage('\t');

-- Testing
T1 = LIMIT Y1 100;
T11 = FILTER T1 BY name IS NOT NULL;
T2 = FOREACH T11 GENERATE photoid AS pid, FLATTEN(TOKENIZE(utils.tokenize(name))) AS title;
T3 = GROUP T2 BY pid;
T4 = FOREACH T3 GENERATE group AS pid, utils.convertBagToStr(T2) AS title;
T5 = ORDER T4 BY pid parallel 10;
-- STORE T5 INTO '$output' USING PigStorage('\t');
