-- set name of job
SET job.name '$name';

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
IMPORT 'macros/load_yfcc100m.pig';

-- load dataset
Y1 = loadYFCC100M('$input');

-- load target list
L1 = LOAD '$users' USING PigStorage AS (user:chararray);
L2 = FILTER L1 BY (user IS NOT NULL);

--  subsetting
S1 = JOIN L2 by user, Y1 by usernsid;
S2 = FILTER S1 BY (L2::user IS NOT NULL AND Y1::usernsid IS NOT NULL);
S4 = ORDER S2 BY Y1::usernsid PARALLEL 1;

-- describe S4;

STORE S4 INTO '$output' USING PigStorage('\t');
