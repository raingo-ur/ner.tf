#! /usr/bin/env bash

# yfcc100m_stats_users.sh
# arguments: [yfcc100m dataset] [output data]
#            e.g. /projects/example/input /projects/example/output

# parse arguments
NARGS=3;
if [ $# -ne $NARGS ]; then echo "ERROR: this script requires $NARGS arguments, please refer to script itself for usage information"; exit 1; fi
INPUT=$1
IDLIST=$2
OUTPUT=$3

echo "INPUT:      $INPUT"
echo "IDLIST:      $IDLIST"
echo "OUTPUT:     $OUTPUT"

# determine which queue on the grid to use
if [[ `hostname` =~ ^gwta3[0-9]+\.tan\.ygrid\.yahoo\.com$ ]]; then
	kinit flickgr@YGRID.YAHOO.COM -k -t ~/flickgr.prod.headless.keytab; QUEUE=flickr_dev; PROXY=httpproxy-res.tan.ygrid.yahoo.com:4080; # tiberium
elif [[ `hostname` =~ ^gwta7[0-9]+\.tan\.ygrid\.yahoo\.com$ ]]; then
	QUEUE=p5; PROXY=httpproxy-prod.tan.ygrid.yahoo.com:4080; # bassnium
elif [[ `hostname` =~ ^gwbl6[0-9]+\.blue\.ygrid\.yahoo\.com$ ]]; then
	if [ $RANDOM -lt 21500 ]; then QUEUE=search_fast_lane; elif [ $RANDOM -lt 10700 ]; then QUEUE=search_general; else QUEUE=default; fi; PROXY=httpproxy-res.blue.ygrid.yahoo.com:4080; # nitro
else
	QUEUE=default; PROXY=; # default
fi

# set maximum input metadata size
MIMMEM=100000000 # add to pig command line: -Dmapreduce.job.split.metainfo.maxsize=$MIMMEM
# set pig memory options
export PIG_OPTS="-Xmx3096M -XX:NewRatio=8"

echo "subsetting yfcc100m"
NAME=subset-pixels
hadoop fs -rm -r $OUTPUT
pig -w -f scripts2/subset-pixels.pig -param name=$NAME -param input=$INPUT -param idlist=$IDLIST -param output=$OUTPUT -Dmapreduce.job.queuename=$QUEUE -Dmapreduce.job.acl-view-job=* -Dmapreduce.job.acl-modify-job=* -Dpig.import.search.path=$PWD,$HOME -Dmapreduce.job.split.metainfo.maxsize=$MIMMEM
RET=$?; echo "exit code is $RET"; if [ $RET -ne 0 ]; then exit $RET; fi

echo "postprocessing data"
PARENT=`dirname "$OUTPUT"`; if ! `hadoop fs -test -d $PARENT`; then hadoop fs -mkdir -p $PARENT; hadoop fs -chmod -R 700 $PARENT; fi
hadoop fs -chmod 700 $OUTPUT
hadoop fs -chmod 600 $OUTPUT/*
echo "done!"
