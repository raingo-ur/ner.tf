-- set name of job
SET job.name '$name';

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
IMPORT 'macros/load_yfcc100m.pig';

-- load dataset
Y1 = loadYFCC100M('$input');
Y2 = FILTER Y1 BY (isvideo == 0 AND tags IS NOT NULL);
Y3 = FOREACH Y2 GENERATE photoid as pid, name, description, tags, photopixels, photopage;

-- load target list
L1 = LOAD '$idlist' USING PigStorage AS (pid:long);
L2 = FILTER L1 BY (pid IS NOT NULL);

--  subsetting
S1 = JOIN L2 by pid, Y3 by pid;
S2 = FILTER S1 BY (L2::pid IS NOT NULL AND Y3::pid IS NOT NULL);
S3 = FOREACH S2 GENERATE $1..;
S4 = ORDER S3 by Y3::pid PARALLEL 1;
-- describe S3;
 
-- save output
STORE S4 INTO '$output' USING PigStorage('\t');
