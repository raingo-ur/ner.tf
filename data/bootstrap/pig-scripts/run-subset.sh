#!/bin/bash
# vim ft=sh


#name=subsets/yfcc100m_mscoco_intersection.txt
name=subsets/yfcc100m_mscoco_intersection_2014.txt
#input=/projects/flickr/flabs/yfcc100m/yfcc100m_dataset.bz2
input=/projects/flickr/flabs/yfcc100m/yfcc100m_autotags.bz2
./scripts2/subset-photos.sh $input $name $name.autotags.bz2
