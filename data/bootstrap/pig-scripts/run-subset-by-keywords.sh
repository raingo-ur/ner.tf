#!/bin/bash
# vim ft=sh


name=subsets/coco-categories-augment.txt
#input=/projects/flickr/flabs/yfcc100m/yfcc100m_dataset.bz2
#input=subsets/yfcc100m_mscoco_intersection.txt.subset.bz2
input=subsets/yfcc100m_mscoco_intersection_2014.txt.subset.bz2
./scripts2/subset-by-keywords.sh $input $name $name.subset4.bz2 $name.subset4.stat.bz2
