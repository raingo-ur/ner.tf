#! /usr/bin/env bash

# yfcc100m_gadm.sh
# arguments: [yfcc100m data] [gadm indexed data + symlink] [output data]
#            e.g. /projects/example/yfcc100m /projects/example/gadm.tar.gz#gadm.tar.gz /projects/example/output
# note: the gadm index is unarchived on each worker node and a symlink is created to the unarchived directory
#       in the local directory of the mapper/reducer. unfortunately, as of present, the name of the symlink
#       is not actually the name that is passed after the '#', but rather the name of the archive. for ease
#       of use, therefore, ensure the symlink name is the same as the archive name.

# parse arguments
NARGS=3;
if [ $# -ne $NARGS ]; then echo "ERROR: this script requires $NARGS arguments, please refer to script itself for usage information"; exit 1; fi
INPUT=$1
GADM=$2
OUTPUT=$3

echo "INPUT:      $INPUT"
echo "GADM:       $GADM"
echo "OUTPUT:     $OUTPUT"

# determine which queue on the grid to use
if [[ `hostname` =~ ^gwta3[0-9]+\.tan\.ygrid\.yahoo\.com$ ]]; then
	kinit flickgr@YGRID.YAHOO.COM -k -t ~/flickgr.prod.headless.keytab; QUEUE=flickr; PROXY=httpproxy-res.tan.ygrid.yahoo.com:4080; # tiberium
elif [[ `hostname` =~ ^gwta7[0-9]+\.tan\.ygrid\.yahoo\.com$ ]]; then
	QUEUE=p5; PROXY=httpproxy-prod.tan.ygrid.yahoo.com:4080; # bassnium
elif [[ `hostname` =~ ^gwbl6[0-9]+\.blue\.ygrid\.yahoo\.com$ ]]; then
	if [ $RANDOM -lt 21500 ]; then QUEUE=search_fast_lane; elif [ $RANDOM -lt 10700 ]; then QUEUE=search_general; else QUEUE=default; fi; PROXY=httpproxy-res.blue.ygrid.yahoo.com:4080; # nitro
else
	QUEUE=default; PROXY=; # default
fi

# set maximum input metadata size
MIMMEM=100000000 # add to pig command line: -Dmapreduce.job.split.metainfo.maxsize=$MIMMEM

# set maximum mapper memory
# note: the maximum jvm memory should be set about 500MB less than the maximum memory
#MAPMEM=2000
#MAPJVM="-Xmx1500M" 
# add to pig command line: -Dmapreduce.map.memory.mb=$MAPMEM -Dmapreduce.map.java.opts=$MAPJVM

# set maximum reducer memory
REDMEM=2000
REDJVM="-Xmx1500M" 
# add to pig command line: -Dmapreduce.reduce.memory.mb=$REDMEM -Dmapreduce.reduce.java.opts=$REDJVM

echo "performing gadm boundary parsing"
NAME=yfcc100m_gadm-data
TEMP1=/tmp/$NAME-$RANDOM
while hadoop fs -test -d $TEMP1; do TEMP1=/tmp/$NAME-$RANDOM; done
pig -w -f yfcc100m_gadm.pig -param name=$NAME -param input=$INPUT -param gadm=$GADM -param output=$TEMP1 -Dmapreduce.job.queuename=$QUEUE -Dmapreduce.job.acl-view-job=* -Dmapreduce.job.acl-modify-job=* -Dpig.import.search.path=$PWD,$HOME -Dmapreduce.job.split.metainfo.maxsize=$MIMMEM -Dmapreduce.reduce.memory.mb=$REDMEM -Dmapreduce.reduce.java.opts=$REDJVM
RET=$?; echo "exit code is $RET"; if [ $RET -ne 0 ]; then exit $RET; fi

echo "postprocessing data"
PARENT=`dirname "$OUTPUT"`; if ! `hadoop fs -test -d $PARENT`; then hadoop fs -mkdir -p $PARENT; hadoop fs -chmod -R 700 $PARENT; fi
hadoop fs -rm $OUTPUT
hadoop fs -mv $TEMP1/part-*-00000.bz2 $OUTPUT
hadoop fs -chmod 600 $OUTPUT
hadoop fs -rm -r -skipTrash $TEMP1
echo "done!"