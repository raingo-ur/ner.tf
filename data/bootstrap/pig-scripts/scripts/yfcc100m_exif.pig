-- yfcc100m_exif.pig
-- arguments: [job name] AS $name, [photos data] AS $photos, [tags data] AS $tags, [accounts data] AS $accounts,
--            [exif data] AS $exif, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_exif.pig -param name=example -param photos=/projects/example/photos
--            -param tags=/projects/example/tags -param accounts=/projects/example/accounts
--            -param exif=/projects/example/exif -param output=/projects/example/output -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- register the jar containing udfs
REGISTER yfcc100m.jar;
DEFINE URLEncode com.yahoo.util.pig.URLEncode();

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
REGISTER '/homes/bthomee/macros/string.jar';
IMPORT 'macros/inc_read_photos_exif.pig';
IMPORT 'macros/load_yfcc100m.pig';

-- load dataset
Y1 = loadYFCC100M('$input');
Y2 = FILTER Y1 BY (photoid IS NOT NULL);
Y3 = FOREACH Y2 GENERATE photoid AS p;

-- load exif
E1 = readPhotosExif('$exif');
E2 = FOREACH E1 GENERATE photo_id AS p, all_tags AS e;
E3 = FILTER E2 BY (p IS NOT NULL AND e IS NOT NULL AND SIZE(e) > 0L);
-- encode the exif
E4 = FOREACH E3 GENERATE p, URLEncode(e) AS e:chararray;
E5 = FILTER E4 BY (e IS NOT NULL AND SIZE(e) > 0L);

-- associate the exif with the photos
J1 = JOIN Y3 BY p, E5 BY p;
J2 = FOREACH J1 GENERATE E5::p AS p, E5::e AS e;
                         
-- save output
STORE J2 INTO '$output' USING PigStorage('\t');