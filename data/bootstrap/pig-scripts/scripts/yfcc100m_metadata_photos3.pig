-- yfcc100m_metadata_photos3.pig
-- arguments: [job name] AS $name, [input data] AS $input, [accounts data] AS $accounts, [geotagged data] AS $geotagged, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_metadata_photos3.pig -param name=example -param input=/projects/example/input -param accounts=/projects/example/accounts
--            -param geotagged=/projects/example/geotagged -param output=/projects/example/output -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- register the jar containing udfs
REGISTER yfcc100m.jar;
DEFINE BitMask com.yahoo.util.pig.BitMask();
DEFINE Base64EncodeBytes com.yahoo.util.pig.Base64EncodeBytes();

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
REGISTER '/homes/bthomee/macros/string.jar';
IMPORT 'macros/inc_read_geo_tiberium.pig';
IMPORT 'macros/inc_read_accounts.pig';

-- load input
M1 = LOAD '$input' USING PigStorage('\t') AS (photoid:long, usernsid:chararray, metadata:map[]);
M2 = FILTER M1 BY (photoid IS NOT NULL AND usernsid IS NOT NULL AND SIZE(usernsid) > 0L AND metadata IS NOT NULL AND SIZE(metadata) > 0L);
M3 = FOREACH M2 GENERATE photoid AS p, usernsid AS u, metadata AS m;

-- load user accounts
A1 = readAccounts('$accounts');
A2 = FOREACH A1 GENERATE nsid AS u, (int)prefs_main_1 AS i:int;
A3 = FILTER A2 BY (u IS NOT NULL AND SIZE(u) > 0L AND i IS NOT NULL);
-- remove all users for which the metadata should not be extracted
A4 = FOREACH A3 GENERATE u, BitMask(i, 1) AS i:int;
A5 = FILTER A4 BY (i IS NOT NULL AND i == 0);
A6 = FOREACH A5 GENERATE u;
-- deduplicate users (just in case)
A7 = DISTINCT A6;

-- join the two
J1 = JOIN M3 BY u, A7 BY u;
J2 = FOREACH J1 GENERATE M3::p AS p, M3::m AS m;

-- load geotagged
G1 = readGeoTagged('$geotagged');
G2 = FOREACH G1 GENERATE object_id AS p, perm_viewgeo AS i;
G3 = FILTER G2 BY (p IS NOT NULL AND i IS NOT NULL);

-- strip out all gps fields from the metadata in case the user had set the geo-permissions to private
S1 = -- TODO

-- save output
STORE S1 INTO '$output' USING PigStorage('\t');