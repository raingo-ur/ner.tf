-- yfcc100m_lines.pig
-- arguments: [job name] AS $name, [yfcc100m lines data], [hash data] AS $hash, [pixel data] AS $pixels, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_lines.pig -param name=example -param lines=/projects/example/lines -param hashes=/projects/example/hash
--            -param pixels=/projects/example/pixels -param output=/projects/example/output -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- register the jar containing udfs
REGISTER yfcc100m.jar;
DEFINE PhotoResize com.yahoo.yfcc100m.PhotoResize('256');
DEFINE Base64DecodeBytes com.yahoo.util.pig.Base64DecodeBytes();
DEFINE Base64EncodeString com.yahoo.util.pig.Base64EncodeString();

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;
SET pig.maxCombinedSplitSize 2000000000;
SET mapreduce.input.fileinputformat.split.minsize 2000000000;
SET mapreduce.input.fileinputformat.split.maxsize 2000000000;

-- load lines data
L1 = LOAD '$lines' USING PigStorage('\t') AS (line:long, photoid:long);
L2 = FILTER L1 BY (line IS NOT NULL AND photoid IS NOT NULL);
L3 = FOREACH L2 GENERATE line AS l, photoid AS p;

-- load hash data
H1 = LOAD '$hash' USING PigStorage('\t') AS (photoid:long, hash:chararray);
H2 = FILTER H1 BY (photoid IS NOT NULL AND hash IS NOT NULL AND SIZE(hash) > 0L);
H3 = FOREACH H2 GENERATE photoid AS p, hash AS h;

-- join the two
J1 = JOIN L3 BY p, H3 BY p;
J2 = FOREACH J1 GENERATE L3::l AS l, H3::p AS p, H3::h AS h;

-- load pixel data
P1 = LOAD '$pixels' USING PigStorage('\t') AS (photoid:long, image:chararray);
P2 = FILTER P1 BY (photoid IS NOT NULL AND image IS NOT NULL AND SIZE(image) > 0L);
P3 = FOREACH P2 GENERATE photoid AS p, image AS i;

-- resize the photos
R1 = FOREACH P3 GENERATE p, Base64DecodeBytes(i) AS b:bytearray;
R2 = FILTER R1 BY (b IS NOT NULL AND SIZE(b) > 0L);
R3 = FOREACH R2 GENERATE p, PhotoResize(p, b) AS b:bytearray;
R4 = FILTER R3 BY (b IS NOT NULL AND SIZE(b) > 0L);
R5 = FOREACH R4 GENERATE p, Base64EncodeString(b) AS i:chararray;
R6 = FILTER R5 BY (i IS NOT NULL AND SIZE(i) > 0L); 

-- join the two
-- note: if pixel data does not exist, the line will still be created but with empty data  
J3 = JOIN J2 BY p LEFT, R6 BY p;
J4 = FOREACH J3 GENERATE J2::l AS l, J2::h AS h, R6::i AS i;

-- order data
O1 = ORDER J4 BY l PARALLEL 1000;
-- save only the desired data
O2 = FOREACH O1 GENERATE h, i;

-- save output
STORE O2 INTO '$output' USING PigStorage('\t');