-- yfcc100m_stats_cameradob.pig
-- arguments: [job name] AS $name, [input data] AS $input, [account data] AS $accounts, [profile data] AS $profiles, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_stats_cameradob.pig -param name=example -param input=/projects/example/input
--            -param accounts=/projects/example/accounts -param profiles=/projects/example/profiles -param output=/projects/example/output
--            -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
REGISTER '/homes/bthomee/macros/string.jar';
IMPORT 'macros/inc_read_accounts.pig';
IMPORT 'macros/inc_read_accounts_profiles.pig';
IMPORT 'macros/load_yfcc100m.pig';

-- load dataset
Y1 = loadYFCC100M('$input');
Y2 = FILTER Y1 BY (usernsid IS NOT NULL AND SIZE(usernsid) > 0L AND camera IS NOT NULL AND SIZE(camera) > 0L);
Y3 = FOREACH Y2 GENERATE usernsid AS u, LOWER(camera) AS z:chararray;

-- load user accounts
A1 = readAccounts('$accounts');
A2 = FOREACH A1 GENERATE id AS u1, nsid AS u2;
A3 = FILTER A2 BY (u1 IS NOT NULL AND u2 IS NOT NULL AND SIZE(u2) > 0L);

-- load user profiles
P1 = readAccountsProfiles('$profiles');
P2 = FOREACH P1 GENERATE user_id AS u, dob_d AS d1, dob_m AS d2, dob_y AS d3;
P3 = FILTER P2 BY (u IS NOT NULL AND d1 IS NOT NULL AND d1 >= 1 AND d1 <= 31 AND d2 IS NOT NULL AND d2 >= 1 AND d2 <= 12 AND d3 IS NOT NULL AND d3 >= 1900 AND d3 <= 2020);

-- join the three
J1 = JOIN A3 BY u1, P3 BY u;
J2 = FOREACH J1 GENERATE A3::u2 AS u, P3::d1 AS d1, P3::d2 AS d2, P3::d3 AS d3;
J3 = JOIN Y3 BY u, J2 BY u;
J4 = FOREACH J3 GENERATE Y3::u AS u, Y3::z AS z, J2::d1 AS d1, J2::d2 AS d2, J2::d3 AS d3;

-- count unique user-camera date-of-births
-- note: only focus on the year
S1 = GROUP J4 BY (u, z);
S2 = FOREACH S1 {
       L1 = LIMIT J4 1L;
       GENERATE FLATTEN(L1.(z, d3)) AS (z, d3);
};
S3 = GROUP S2 BY (z, d3);
S4 = FOREACH S3 GENERATE FLATTEN($0) AS (z, d3), COUNT($1) AS c:long;
-- order the data
S5 = ORDER S4 BY z ASC, d3 ASC PARALLEL 1;
 
-- save output
STORE S5 INTO '$output' USING PigStorage('\t');