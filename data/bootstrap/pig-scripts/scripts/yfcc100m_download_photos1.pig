-- yfcc100m_download_photos1.pig
-- arguments: [job name] AS $name, [input data] AS $input, [downloaded data] AS $download, [output data] AS $output, [proxy] AS $proxy
-- e.g.:      pig -w -f yfcc100m_download_photos1.pig -param name=example -param input=/projects/example/input -param download=/projects/example/download
--            -param output=/projects/example/output -param proxy=httpproxy-res.tan.ygrid.yahoo.com:4080 -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- register the jar containing udfs
REGISTER yfcc100m.jar;
DEFINE Download com.yahoo.yfcc100m.DownloadBytes();
DEFINE Base64EncodeString com.yahoo.util.pig.Base64EncodeString();
DEFINE PhotoFilter com.yahoo.yfcc100m.PhotoFilter();

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
IMPORT 'macros/load_yfcc100m.pig';

-- load dataset
Y1 = loadYFCC100M('$input');
Y2 = FILTER Y1 BY (photoid IS NOT NULL AND isvideo == 0);
Y3 = FOREACH Y2 GENERATE photoid AS p, photopixels AS u;

-- load already downloaded data
E1 = LOAD '$download' USING PigStorage('\t');
E2 = FILTER E1 BY ($0 IS NOT NULL);
E3 = FOREACH E2 GENERATE (long)$0 AS p:long, 1 AS i:int;

-- join the two
J1 = JOIN Y3 BY p LEFT, E3 BY p;
J2 = FOREACH J1 GENERATE Y3::p AS p, Y3::u AS u, E3::i AS i;
-- filter out all those that have already been downloaded
J3 = FILTER J2 BY (i IS NULL);

-- download the data
-- note: force the downloading to take place during the reduce stage
-- note: we must not use redirection for photos, because in the case the photo
--       does not exist a placeholder image will be returned. in the pixels
--       validation step below, though, such placeholders will get filtered
--       out in case they do appear somehow.
-- note: pause some time before every download to reduce the load on the servers
D1 = FOREACH J3 GENERATE p, u, RANDOM() AS r:double;
D2 = FOREACH (GROUP D1 BY r PARALLEL 1000) GENERATE FLATTEN($1.(p, u)) AS (p, u);
D3 = FOREACH D2 GENERATE p, Download(p, u, '$proxy', 'false', '', '', 300) AS i:bytearray;
D4 = FILTER D3 BY (i IS NOT NULL AND SIZE(i) > 0L);
-- base64 encode image
D5 = FOREACH D4 GENERATE p, Base64EncodeString(i) AS s:chararray;
D6 = FILTER D5 BY (s IS NOT NULL AND SIZE(s) > 0L);
-- validate images
D7 = FILTER D6 BY (PhotoFilter(p, s));

-- save output
STORE D7 INTO '$output' USING PigStorage('\t');