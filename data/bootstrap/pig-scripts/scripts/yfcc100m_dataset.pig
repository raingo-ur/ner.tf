-- yfcc100m_photos.pig
-- arguments: [job name] AS $name, [geo photos data] AS $photosgeo, [non-geo photos data] AS $photosngeo,
--            [geo videos data] AS $videosgeo, [non-geo videos data] AS $videosngeo, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_photos.pig -param name=example -param photosgeo=/projects/example/photosgeo
--            -param photosngeo=/projects/example/photosngeo -param videosgeo=/projects/example/videosgeo
--            -param videosngeo=/projects/example/videosngeo -param output=/projects/example/output
--            -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
REGISTER '/homes/bthomee/macros/string.jar';

-- load geo photos
P1 = LOAD '$photosgeo' USING PigStorage('\t') AS (photoid:long, userid:int, usernsid:chararray, userhandle:chararray, date_taken:chararray, date_imported:long,
                                                  camera:chararray, name:chararray, description:chararray, tags:chararray, machinetags:chararray,
                                                  longitude:double, latitude:double, accuracy:int,
                                                  photopage:chararray, photopixels:chararray, licensename:chararray, licenseurl:chararray,
                                                  server:int, farm:int, secret:chararray, secreto:chararray, extension:chararray);
P2 = FOREACH P1 GENERATE 0 AS v:int, photoid AS p, userid AS u, usernsid AS i, userhandle AS h, date_taken AS s,
                         date_imported AS m, camera AS c, name AS n, description AS d, tags AS t1,
                         machinetags AS t2, longitude AS x, latitude AS y, accuracy AS a, photopage AS z1,
                         photopixels AS z2, licensename AS l1, licenseurl AS l2, server AS s1, farm AS s2,
                         secret AS s3, secreto AS s4, extension AS s5, RANDOM() AS r:double;
-- remove internal user identifier
P3 = FOREACH P2 GENERATE p, i, h, s, m, c, n, d, t1, t2, x, y, a, z1, z2, l1, l2, s1, s2, s3, s4, s5, v, r;

-- load non-geo photos
R1 = LOAD '$photosngeo' USING PigStorage('\t') AS (photoid:long, userid:int, usernsid:chararray, userhandle:chararray, date_taken:chararray, date_imported:long,
                                                  camera:chararray, name:chararray, description:chararray, tags:chararray, machinetags:chararray,
                                                  longitude:double, latitude:double, accuracy:int,
                                                  photopage:chararray, photopixels:chararray, licensename:chararray, licenseurl:chararray,
                                                  server:int, farm:int, secret:chararray, secreto:chararray, extension:chararray);
R2 = FOREACH R1 GENERATE 0 AS v:int, photoid AS p, userid AS u, usernsid AS i, userhandle AS h, date_taken AS s,
                         date_imported AS m, camera AS c, name AS n, description AS d, tags AS t1,
                         machinetags AS t2, longitude AS x, latitude AS y, accuracy AS a, photopage AS z1,
                         photopixels AS z2, licensename AS l1, licenseurl AS l2, server AS s1, farm AS s2,
                         secret AS s3, secreto AS s4, extension AS s5, RANDOM() AS r:double;
R3 = ORDER R2 BY r ASC;
R4 = LIMIT R3 50840241L;
R5 = FOREACH R4 GENERATE p, i, h, s, m, c, n, d, t1, t2, x, y, a, z1, z2, l1, l2, s1, s2, s3, s4, s5, v, r;

-- load geo videos
V1 = LOAD '$videosgeo' USING PigStorage('\t') AS (photoid:long, userid:int, usernsid:chararray, userhandle:chararray, date_taken:chararray, date_imported:long,
                                                  camera:chararray, name:chararray, description:chararray, tags:chararray, machinetags:chararray,
                                                  longitude:double, latitude:double, accuracy:int,
                                                  photopage:chararray, photopixels:chararray, licensename:chararray, licenseurl:chararray,
                                                  server:int, farm:int, secret:chararray, secreto:chararray, extension:chararray);
V2 = FOREACH V1 GENERATE 1 AS v:int, photoid AS p, userid AS u, usernsid AS i, userhandle AS h, date_taken AS s,
                         date_imported AS m, camera AS c, name AS n, description AS d, tags AS t1,
                         machinetags AS t2, longitude AS x, latitude AS y, accuracy AS a, photopage AS z1,
                         photopixels AS z2, licensename AS l1, licenseurl AS l2, server AS s1, farm AS s2,
                         secret AS s3, secreto AS s4, extension AS s5, RANDOM() AS r:double;
V3 = FOREACH V2 GENERATE p, i, h, s, m, c, n, d, t1, t2, x, y, a, z1, z2, l1, l2, s1, s2, s3, s4, s5, v, r;

-- load non-geo videos
W1 = LOAD '$videosngeo' USING PigStorage('\t') AS (photoid:long, userid:int, usernsid:chararray, userhandle:chararray, date_taken:chararray, date_imported:long,
                                                  camera:chararray, name:chararray, description:chararray, tags:chararray, machinetags:chararray,
                                                  longitude:double, latitude:double, accuracy:int,
                                                  photopage:chararray, photopixels:chararray, licensename:chararray, licenseurl:chararray,
                                                  server:int, farm:int, secret:chararray, secreto:chararray, extension:chararray);
W2 = FOREACH W1 GENERATE 1 AS v:int, photoid AS p, userid AS u, usernsid AS i, userhandle AS h, date_taken AS s,
                         date_imported AS m, camera AS c, name AS n, description AS d, tags AS t1,
                         machinetags AS t2, longitude AS x, latitude AS y, accuracy AS a, photopage AS z1,
                         photopixels AS z2, licensename AS l1, licenseurl AS l2, server AS s1, farm AS s2,
                         secret AS s3, secreto AS s4, extension AS s5, RANDOM() AS r:double;
W3 = FOREACH W2 GENERATE p, i, h, s, m, c, n, d, t1, t2, x, y, a, z1, z2, l1, l2, s1, s2, s3, s4, s5, v, r;

-- merge them all
U1 = UNION P3, R5, V3, W3;
-- randomize them
U2 = ORDER U1 BY r ASC PARALLEL 1;
U3 = FOREACH U2 GENERATE p, i, h, s, m, c, n, d, t1, t2, x, y, a, z1, z2, l1, l2, s1, s2, s3, s4, s5, v;

-- save output
STORE U3 INTO '$output' USING PigStorage('\t');