-- yfcc100m_missing_photos.pig
-- arguments: [job name] AS $name, [dataset data] AS $dataset, [pixel data] AS $pixels, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_missing_photos.pig -param name=example -param dataset=/projects/example/dataset -param pixels=/projects/example/pixels
--            -param output=/projects/example/output -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- register the jar containing udfs
REGISTER yfcc100m.jar;

-- set optimizations
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;
SET pig.exec.mapPartAgg true;
SET pig.maxCombinedSplitSize 2000000000;
SET mapreduce.input.fileinputformat.split.minsize 2000000000;
SET mapreduce.input.fileinputformat.split.maxsize 2000000000;

-- import macros
IMPORT 'macros/load_yfcc100m.pig';

-- load dataset
Y1 = loadYFCC100M('$dataset');
Y2 = FILTER Y1 BY (photoid IS NOT NULL AND isvideo IS NOT NULL AND isvideo == 0);
Y3 = FOREACH Y2 GENERATE photoid AS p;

-- load pixel data
P1 = LOAD '$pixels' USING PigStorage('\t') AS (photoid:long, image:chararray);
P2 = FILTER P1 BY (photoid IS NOT NULL);
P3 = FOREACH P2 GENERATE photoid AS p;

-- join the two
J1 = JOIN Y3 BY p LEFT, P3 BY p;
J2 = FOREACH J1 GENERATE Y3::p AS p, P3::p AS p2;
J3 = FILTER J2 BY (p2 IS NULL);
J4 = FOREACH J3 GENERATE p;
J5 = ORDER J4 BY p ASC PARALLEL 1;

-- save output
STORE J5 INTO '$output' USING PigStorage('\t');