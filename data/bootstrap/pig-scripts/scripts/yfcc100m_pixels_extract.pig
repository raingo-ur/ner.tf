-- yfcc100m_pixels_extract.pig
-- arguments: [job name] AS $name, [dataset data] AS $dataset, [pixel data] AS $pixels, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_pixels_extract.pig -param name=example -param dataset=/projects/example/dataset -param pixels=/projects/example/pixels
--            -param output=/projects/example/output -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- register the jar containing udfs
REGISTER yfcc100m.jar;
DEFINE SequenceFileLoader com.yahoo.util.pig.SequenceFileLoader('-c com.twitter.elephantbird.pig.util.BytesWritableConverter', '-c com.twitter.elephantbird.pig.util.BytesWritableConverter');
DEFINE Bytes2String com.yahoo.util.pig.Bytes2String();
DEFINE Base64EncodeString com.yahoo.util.pig.Base64EncodeString();

-- set optimizations
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;
SET pig.maxCombinedSplitSize 200000000;
SET mapreduce.input.fileinputformat.split.minsize 200000000;
SET mapreduce.input.fileinputformat.split.maxsize 200000000;

-- import macros
IMPORT 'macros/load_yfcc100m.pig';

-- load dataset
Y1 = loadYFCC100M('$dataset');
Y2 = FILTER Y1 BY (photoid IS NOT NULL);
Y3 = FOREACH Y2 GENERATE photoid AS p;

-- load pixels
P1 = LOAD '$pixels' USING SequenceFileLoader() AS (key:bytearray, image:bytearray);
P2 = FILTER P1 BY (key IS NOT NULL AND SIZE(key) > 0L AND image IS NOT NULL AND SIZE(image) > 0L);
-- pig cannot easily convert from bytearray to chararray, so we use a UDF for that
P3 = FOREACH P2 GENERATE Bytes2String(key) AS key:chararray, image AS i;
P4 = FILTER P3 BY (key IS NOT NULL AND SIZE(key) > 0L);
-- split the key into userid and photoid
P5 = FOREACH P4 GENERATE REGEX_EXTRACT(key, '.*,\\s*(.*)', 1) AS p:chararray, i;
P6 = FILTER P5 BY (p IS NOT NULL AND SIZE(p) > 0L);
P7 = FOREACH P6 GENERATE (long)p AS p:long, i;
P8 = FILTER P7 BY (p IS NOT NULL);

-- join the two
J1 = JOIN Y3 BY p, P8 BY p;
J2 = FOREACH J1 GENERATE P8::p AS p, P8::i AS i;

-- base64 encode image
I1 = FOREACH J2 GENERATE p, Base64EncodeString(i) AS s:chararray;
I2 = FILTER I1 BY (s IS NOT NULL AND SIZE(s) > 0L);

-- save output
STORE I2 INTO '$output' USING PigStorage('\t');