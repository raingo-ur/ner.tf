-- yfcc100m_dataset_hash.pig
-- arguments: [job name] AS $name, [input data] AS $input, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_dataset_hash.pig -param name=example -param input=/projects/example/input
--            -param output=/projects/example/output -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- register the jar containing udfs
REGISTER yfcc100m.jar;
DEFINE MD5Hash com.yahoo.util.pig.MD5Hash();

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
IMPORT 'macros/load_yfcc100m.pig';

-- load dataset
Y1 = loadYFCC100M('$input');
Y2 = FILTER Y1 BY (photoid IS NOT NULL);
Y3 = FOREACH Y2 GENERATE photoid AS p, photopixels AS u;

-- convert url to hash
H1 = FOREACH Y3 GENERATE p, MD5Hash(u) AS h:chararray;
H2 = FILTER H1 BY (h IS NOT NULL);
-- order results
H3 = ORDER H2 BY p ASC PARALLEL 1;
 
-- save output
STORE H3 INTO '$output' USING PigStorage('\t');