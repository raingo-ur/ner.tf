-- yfcc100m_download_videos2.pig
-- arguments: [job name] AS $name, [input data] AS $input, [downloaded data] AS $download, [output data] AS $output, [proxy] AS $proxy
-- e.g.:      pig -w -f yfcc100m_download_videos2.pig -param name=example -param input=/projects/example/input -param download=/projects/example/download
--            -param output=/projects/example/output -param proxy=httpproxy-res.tan.ygrid.yahoo.com:4080 -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- register the jar containing udfs
REGISTER yfcc100m.jar;
DEFINE VideoURL com.yahoo.yfcc100m.VideoURL();
DEFINE Download com.yahoo.yfcc100m.DownloadBytes();
DEFINE Base64DecodeString com.yahoo.util.pig.Base64DecodeString();
DEFINE Base64EncodeString com.yahoo.util.pig.Base64EncodeString();

-- set optimizations
--SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- load video stream info data
Y1 = LOAD '$input' USING PigStorage('\t') AS (photoid:long, xml:chararray);
Y2 = FILTER Y1 BY (photoid IS NOT NULL AND xml IS NOT NULL AND SIZE(xml) > 0L);
Y3 = FOREACH Y2 GENERATE photoid AS p, xml AS x;

-- create the urls for the videos
U1 = FOREACH Y3 GENERATE p, Base64DecodeString(x) AS s:chararray;
U2 = FILTER U1 BY (s IS NOT NULL AND SIZE(s) > 0L);
U3 = FOREACH U2 GENERATE p, VideoURL(p, s) AS u:chararray;
U4 = FILTER U3 BY (u IS NOT NULL AND SIZE(u) > 0L);

-- load already downloaded data
E1 = LOAD '$download' USING PigStorage('\t');
E2 = FILTER E1 BY ($0 IS NOT NULL);
E3 = FOREACH E2 GENERATE (long)$0 AS p:long, 1 AS v:int;

-- join the two
J1 = JOIN U4 BY p LEFT, E3 BY p;
J2 = FOREACH J1 GENERATE U4::p AS p, U4::u AS u, E3::v AS v;
-- filter out all those that have already been downloaded
J3 = FILTER J2 BY (v IS NULL);

-- download the data
-- note: force the downloading to take place during the reduce stage
-- note: pause some time before every download to reduce the load on the servers
D1 = FOREACH J3 GENERATE p, u, RANDOM() AS r:double;
D2 = FOREACH (GROUP D1 BY r PARALLEL 50) GENERATE FLATTEN($1.(p, u)) AS (p, u);
D3 = FOREACH D2 GENERATE p, Download(p, u, '$proxy', 'false', 'flickrdownload', 'TS39gwbB', 1000) AS v:bytearray;
D4 = FILTER D3 BY (v IS NOT NULL AND SIZE(v) > 0L);
-- base64 encode video
D5 = FOREACH D4 GENERATE p, Base64EncodeString(v) AS s:chararray;
D6 = FILTER D5 BY (s IS NOT NULL AND SIZE(s) > 0L);

-- spread out over multiple output files
S1 = GROUP D6 BY p PARALLEL 1;
S2 = FOREACH S1 GENERATE FLATTEN($1) AS (p, v);

-- save output
STORE S2 INTO '$output' USING PigStorage('\t');