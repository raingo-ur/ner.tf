-- yfcc100m_download_videos1.pig
-- arguments: [job name] AS $name, [input data] AS $input, [api key] AS $apikey, [output data] AS $output, [proxy] AS $proxy
-- e.g.:      pig -w -f yfcc100m_download_videos1.pig -param name=example -param input=/projects/example/input -param apikey=abcdef012345
--            -param output=/projects/example/output -param proxy=httpproxy-res.tan.ygrid.yahoo.com:4080 -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- register the jar containing udfs
REGISTER yfcc100m.jar;
DEFINE Download com.yahoo.yfcc100m.DownloadXML();
DEFINE Base64EncodeString com.yahoo.util.pig.Base64EncodeString();

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
IMPORT 'macros/load_yfcc100m.pig';

-- load dataset
Y1 = loadYFCC100M('$input');
Y2 = FILTER Y1 BY (photoid IS NOT NULL AND secret IS NOT NULL AND SIZE(secret) > 0L AND isvideo == 1);
Y3 = FOREACH Y2 GENERATE photoid AS p, secret AS s;

-- generate api call
A1 = FOREACH Y3 GENERATE p,
                         CONCAT('https://api.flickr.com/services/rest/?method=flickr.video.getStreamInfo',
                         CONCAT('&extras=duration',
                         CONCAT('&api_key=',
                         CONCAT('$apikey',
                         CONCAT('&photo_id=',
                         CONCAT((chararray)p,
                         CONCAT('&secret=', s))))))) AS u:chararray;

-- download the data
-- note: force the downloading to take place during the reduce stage
D1 = FOREACH A1 GENERATE p, u, RANDOM() AS r:double;
D2 = FOREACH (GROUP D1 BY r PARALLEL 1000) GENERATE FLATTEN($1.(p, u)) AS (p, u);
D3 = FOREACH D2 GENERATE p, Download(p, u, '$proxy', 'false', '', '', 0) AS x:chararray;
D4 = FILTER D3 BY (x IS NOT NULL AND SIZE(x) > 0L);
-- base64 encode xml
D5 = FOREACH D4 GENERATE p, Base64EncodeString(x) AS s:chararray;
D6 = FILTER D5 BY (s IS NOT NULL AND SIZE(s) > 0L);
-- order data
D7 = ORDER D6 BY p ASC PARALLEL 1;

-- save output
STORE D7 INTO '$output' USING PigStorage('\t');