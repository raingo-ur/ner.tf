-- yfcc100m_stats_cameratimestamps.pig
-- arguments: [job name] AS $name, [input data] AS $input, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_stats_cameratimestamps.pig -param name=example -param input=/projects/example/input
--            -param output=/projects/example/output -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- register the jar containing udfs
REGISTER yfcc100m.jar;
DEFINE Date2Timestamp com.yahoo.util.pig.Date2Timestamp();
DEFINE Timestamp2Date com.yahoo.util.pig.Timestamp2Date('yyyy-MM-dd HH:mm:ss');

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
IMPORT 'macros/load_yfcc100m.pig';

-- load dataset
Y1 = loadYFCC100M('$input');
Y2 = FILTER Y1 BY (photoid IS NOT NULL AND date_taken IS NOT NULL AND SIZE(date_taken) > 0L AND date_imported IS NOT NULL AND camera IS NOT NULL AND SIZE(camera) > 0L);
Y3 = FOREACH Y2 GENERATE photoid AS p, date_taken AS ds, date_imported AS mt, LOWER(camera) AS z:chararray;

-- convert timestamps
-- note: skip all camera timestamps outside our range of interest, i.e. 01/01/1970 00:00:00 - 01/01/2020 00:00:00
S1 = FOREACH Y3 GENERATE p, Date2Timestamp(ds) AS dt:long, ds, mt, Timestamp2Date(mt) AS ms:chararray, z;
S2 = FILTER S1 BY (dt IS NOT NULL AND dt > 0 AND dt < 1577858400L AND mt > 0 AND mt < 1577858400L AND ms IS NOT NULL AND SIZE(ms) > 0L);
-- determine the delta
S3 = FOREACH S2 GENERATE p, dt, ds, mt, ms, (mt - dt) AS de:long, z;
-- order the data
S4 = ORDER S3 BY dt ASC PARALLEL 1;
 
-- save output
STORE S4 INTO '$output' USING PigStorage('\t');