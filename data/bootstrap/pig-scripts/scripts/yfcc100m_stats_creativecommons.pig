-- yfcc100m_stats_creativecommons.pig
-- arguments: [job name] AS $name, [input data] AS $input, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_stats_creativecommons.pig -param name=example -param input=/projects/example/input
--            -param output=/projects/example/output -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
IMPORT 'macros/load_yfcc100m.pig';

-- load dataset
Y1 = loadYFCC100M('$input');
Y2 = FILTER Y1 BY (licensename IS NOT NULL AND SIZE(licensename) > 0L AND isvideo IS NOT NULL);
Y3 = FOREACH Y2 GENERATE isvideo AS i, licensename AS l;

-- count licenses per media type
S1 = GROUP Y3 BY (i, l);
S2 = FOREACH S1 GENERATE FLATTEN($0) AS (i, l), COUNT($1) AS c:long;
-- order the data
S3 = ORDER S2 BY i ASC, l ASC PARALLEL 1;
 
-- save output
STORE S3 INTO '$output' USING PigStorage('\t');