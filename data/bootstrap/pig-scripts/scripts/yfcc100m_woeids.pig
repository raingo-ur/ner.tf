-- yfcc100m_woeids.pig
-- arguments: [job name] AS $name, [input data] AS $input, [woeid data] AS $woeid, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_woeids.pig -param name=example -param input=/projects/example/input -param woeid=/projects/example/woeid
--            -param output=/projects/example/output -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- register the jar containing udfs
REGISTER yfcc100m.jar;
DEFINE NumberFormatter com.yahoo.util.pig.NumberFormatter('0.00000');

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
IMPORT 'macros/load_yfcc100m.pig';

-- load dataset
Y1 = loadYFCC100M('$input');
Y2 = FOREACH Y1 GENERATE photoid AS p, longitude AS x, latitude AS y;
Y3 = FILTER Y2 BY (p IS NOT NULL AND x IS NOT NULL AND x > -180.0 AND x < 180.0 AND x != 0.0 AND y IS NOT NULL AND y > -90.0 AND y < 90.0 AND y != 0.0);
-- reduce geographic resolution to about 10m
Y4 = FOREACH Y3 GENERATE p, NumberFormatter(x) AS nx:chararray, NumberFormatter(y) AS ny:chararray;

-- load woeid data
W1 = LOAD '$woeid' USING PigStorage('\t') AS (longitude:chararray, latitude:chararray,
                                              index:int, code:int,
                                              admin6:chararray, woeid6:long,
                                              admin5:chararray, woeid5:long,
                                              admin4:chararray, woeid4:long,
                                              admin3:chararray, woeid3:long,
                                              admin2:chararray, woeid2:long,
                                              admin1:chararray, woeid1:long,
                                              admin0:chararray, woeid0:long,
                                              xctr:double, yctr:double, xmin:double, ymin:double, xmax:double, ymax:double);
W2 = FOREACH W1 GENERATE longitude AS nx, latitude AS ny,
                         index AS i, code AS c,
                         admin6 AS a6, woeid6 AS w6,
                         admin5 AS a5, woeid5 AS w5,
                         admin4 AS a4, woeid4 AS w4,
                         admin3 AS a3, woeid3 AS w3,
                         admin2 AS a2, woeid2 AS w2,
                         admin1 AS a1, woeid1 AS w1,
                         admin0 AS a0, woeid0 AS w0,
                         xctr, yctr, xmin, ymin, xmax, ymax;
-- skip non-localities
W3 = FILTER W2 BY (c == 7 OR c == 8 OR c == 9 OR c == 10 OR c == 12 OR c == 13 OR c == 22);
-- only retain a single woeid with the same code
-- note: retain the one with the highest index to focus on the largest place.
W4 = GROUP W3 BY (nx, ny, c);
W5 = FOREACH W4 {
       O1 = ORDER W3 BY i DESC;
       O2 = LIMIT O1 1L;
       GENERATE FLATTEN(O2) AS (nx, ny,
                                i, c,
                                a6, w6, a5, w5, a4, w4, a3, w3, a2, w2, a1, w1, a0, w0, xctr, yctr, xmin, ymin, xmax, ymax);
};
-- next, only retain the node with the lowest index
W6 = GROUP W5 BY (nx, ny);
W7 = FOREACH W6 {
       O3 = ORDER W5 BY i ASC;
       O4 = LIMIT O3 1L;
       GENERATE FLATTEN(O4) AS (nx, ny,
                                i, c,
                                a6, w6, a5, w5, a4, w4, a3, w3, a2, w2, a1, w1, a0, w0, xctr, yctr, xmin, ymin, xmax, ymax);
};

-- join the two
J1 = JOIN W7 BY (nx, ny), Y4 BY (nx, ny);
J2 = FOREACH J1 GENERATE Y4::p AS p,
                         W7::a6 AS a6, W7::w6 AS w6,
                         W7::a5 AS a5, W7::w5 AS w5,
                         W7::a4 AS a4, W7::w4 AS w4,
                         W7::a3 AS a3, W7::w3 AS w3,
                         W7::a2 AS a2, W7::w2 AS w2,
                         W7::a1 AS a1, W7::w1 AS w1,
                         W7::a0 AS a0, W7::w0 AS w0,
                         W7::xctr, W7::yctr, W7::xmin, W7::ymin, W7::xmax, W7::ymax;
-- order the data
J3 = ORDER J2 BY p ASC PARALLEL 1;

-- save output
STORE J3 INTO '$output' USING PigStorage('\t');