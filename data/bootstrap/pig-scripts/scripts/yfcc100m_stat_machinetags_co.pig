-- yfcc100m_stats_machinetags3.pig
-- arguments: [job name] AS $name, [input data] AS $input, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_stats_machinetags3.pig -param name=example -param input=/projects/example/input
--            -param output=/projects/example/output -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- register the jar containing udfs
REGISTER lib/yfcc100m.jar;
DEFINE URLDecode com.yahoo.util.pig.URLDecode();

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
IMPORT 'macros/load_yfcc100m.pig';
register 'udfs/utils.py' using jython as utils

-- load dataset
Y1 = loadYFCC100M('$input');
Y2 = FILTER Y1 BY (machinetags IS NOT NULL AND SIZE(machinetags) > 0L);
Y3 = FOREACH Y2 GENERATE FLATTEN(utils.genCoWords(TOKENIZE(machinetags, ','))) AS (t:chararray, t2:chararray);
-- decode the tag

-- count machinetags
S1 = GROUP Y3 BY (t, t2);
S2 = FOREACH S1 GENERATE group.t AS t, group.t2 AS t2, COUNT($1) AS c:long;
-- only retain the top ones
S3 = ORDER S2 BY c DESC PARALLEL 1;
-- S4 = LIMIT S3 1000L;
 
-- save output
STORE S3 INTO '$output' USING PigStorage('\t');
