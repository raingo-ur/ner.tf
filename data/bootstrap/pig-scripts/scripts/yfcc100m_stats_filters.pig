-- yfcc100m_stats_filters.pig
-- arguments: [job name] AS $name, [input data] AS $input, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_stats_filters.pig -param name=example -param input=/projects/example/input
--            -param output=/projects/example/output -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- register the jar containing udfs
REGISTER yfcc100m.jar;
DEFINE URLDecode com.yahoo.util.pig.URLDecode();

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
IMPORT 'macros/load_yfcc100m.pig';

-- load dataset
Y1 = loadYFCC100M('$input');
Y2 = FILTER Y1 BY (photoid IS NOT NULL AND isvideo IS NOT NULL);
Y3 = FOREACH Y2 GENERATE photoid AS p, tags AS t1, machinetags AS t2;

-- select only the instagram filter tags
T1 = FILTER Y3 BY (t1 IS NOT NULL AND SIZE(t1) > 0L);
T2 = FOREACH T1 GENERATE p, FLATTEN(TOKENIZE(t1, ',')) AS t:chararray;
T3 = FOREACH T2 GENERATE p, URLDecode(t) AS t:chararray;
T4 = FILTER T3 BY (t IS NOT NULL AND SIZE(t) > 0L);
-- note: merge 'lord kelvin' and 'kelvin' filters
T5 = FOREACH T4 GENERATE p, (t == 'lord kelvin' ? 'kelvin' : t) AS t:chararray;
T6 = FILTER T5 BY (t == 'amaro'     OR t == 'mayfair'       OR t == 'rise'          OR t == 'valencia'  OR
                   t == 'x-pro ii'  OR t == 'lo-fi'         OR t == 'willow'        OR t == 'sierra'    OR
                   t == 'earlybird' OR t == 'sutro'         OR t == 'toaster'       OR t == 'brannan'   OR
                   t == 'inkwell'   OR t == 'walden'        OR t == 'hefe'          OR t == 'nashville' OR
                   t == '1977'      OR t == 'gotham'        OR t == 'poprocket'     OR t == 'rise'      OR
                   t == 'apollo'    OR t == 'ashby'         OR t == 'stinson'       OR t == 'vesper'    OR
                   t == 'clarendon' OR t == 'skyline'       OR t == 'hudson'        OR t == 'lomo-fi'   OR
                   t == 'ginza'     OR t == 'moon'          OR t == 'gingham'       OR t == 'maven'     OR
                   t == 'dogpatch'  OR t == 'brooklyn'      OR t == 'kelvin'        OR t == 'charmes');
-- check photos for instagram filters
-- note: check the photos for having all multiple instagram markers
I1 = FILTER T4 BY (t == 'square' OR t == 'square format' OR t == 'instagram app');
I2 = FOREACH I1 GENERATE p;
I3 = GROUP I2 BY p;
I4 = FOREACH I3 GENERATE $0 AS p, COUNT($1) AS c:long;
I5 = FILTER I4 BY (c >= 3);
I6 = FOREACH I5 GENERATE p;
I7 = JOIN I6 BY p, T6 BY p;
I8 = FOREACH I7 GENERATE T6::t AS t;
I9 = FOREACH I8 GENERATE 'instagram' AS f:chararray, t;

-- check photos for flickr filters
-- extract machine tags per photo
F1 = FILTER Y3 BY (t2 IS NOT NULL AND SIZE(t2) > 0L);
F2 = FOREACH F1 GENERATE p, FLATTEN(TOKENIZE(t2, ',')) AS t:chararray;
F3 = FOREACH F2 GENERATE p, URLDecode(t) AS t:chararray;
F4 = FILTER F3 BY (t IS NOT NULL AND SIZE(t) > 0L AND STARTSWITH(t, 'flickriosapp:filter='));
F5 = FOREACH F4 GENERATE 'flickr' AS f:chararray, SUBSTRING(t, 20, (int)SIZE(t)) AS t:chararray;
F6 = FILTER F5 BY (t IS NOT NULL AND SIZE(t) > 0L);

-- count filters
S1 = UNION I9, F6;
S2 = GROUP S1 BY (f, t);
S3 = FOREACH S2 GENERATE FLATTEN($0) AS (f, t), COUNT($1) AS c:long;
-- order the data
S4 = ORDER S3 BY c DESC PARALLEL 1;
 
-- save output
STORE S4 INTO '$output' USING PigStorage('\t');