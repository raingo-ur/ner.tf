-- yfcc100m_dataset_videos.pig
-- arguments: [job name] AS $name, [videos data] AS $photos, [privacy bug data] AS $privacy, [geotagged data] AS $geotagged,
--            [tags data] AS $tags, [accounts data] AS $accounts, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_dataset_videos.pig -param name=example -param videos=/projects/example/videos
--            -param privacy=/projects/example/privacy -param geotagged=/projects/example/geotagged
--            -param tags=/projects/example/tags -param accounts=/projects/example/accounts
--            -param output=/projects/example/output -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- register the jar containing udfs
REGISTER yfcc100m.jar;
DEFINE URLEncode com.yahoo.util.pig.URLEncode();
DEFINE Bag2SortedString com.yahoo.util.pig.Bag2SortedString();
DEFINE StringUnescapeHTML com.yahoo.util.pig.StringUnescapeHTML();
DEFINE StringTrim com.yahoo.util.pig.StringTrim();

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
REGISTER '/homes/bthomee/macros/string.jar';
IMPORT 'macros/inc_read_accounts.pig';
IMPORT 'macros/inc_read_geo_tiberium.pig';
IMPORT 'macros/inc_read_photo_tiberium.pig';
IMPORT 'macros/inc_read_tags.pig';

-- load videos
P1 = readPhotos('$photos');
P2 = FOREACH P1 GENERATE id AS p, owner_id AS u, perms AS i, adultness AS a, license AS l, date_taken AS s, date_imported AS m, camera AS c, name AS n, description AS d, content_type AS t, is_video AS v,
                         server AS s1, (server / 1000 + 1) AS s2:int, secret AS s3, secret_o AS s4, o_ext AS s5;
-- retain only public videos (not screenshots, illustrations, etc.), that are not adult, and that
-- have a suitable creative commons licenses
-- note: camera, title and description may be empty
P3 = FILTER P2 BY (p IS NOT NULL AND u IS NOT NULL AND i IS NOT NULL AND i == 0 AND a IS NOT NULL AND a == 0 AND
                   l IS NOT NULL AND (l == 1 OR l == 2 OR l== 3 OR l == 4 OR l == 5 OR l == 6) AND
                   s IS NOT NULL AND SIZE(s) > 0L AND m IS NOT NULL AND t IS NOT NULL AND t == 0 AND
                   v IS NOT NULL AND v == 1 AND s1 IS NOT NULL AND s2 IS NOT NULL AND s3 IS NOT NULL AND
                   SIZE(s3) > 0L AND s4 IS NOT NULL AND SIZE(s4) > 0L AND s5 IS NOT NULL AND SIZE(s5) > 0L);
-- unescape, trim and reencode the camera, title and description, if exists
-- note: camera, title and description have already been urldecoded by the macro
P4 = FOREACH P3 GENERATE p, u, l, s, m,
                         StringTrim(StringUnescapeHTML(c)) AS c:chararray,
                         StringTrim(StringUnescapeHTML(n)) AS n:chararray,
                         StringTrim(StringUnescapeHTML(d)) AS d:chararray,
                         s1, s2, s3, s4, s5;
P5 = FOREACH P4 GENERATE p, u, l, s, m,
                         URLEncode(c) AS c:chararray,
                         URLEncode(n) AS n:chararray,
                         URLEncode(d) AS d:chararray,
                         s1, s2, s3, s4, s5;

-- load privacy bug videos
B1 = LOAD '$privacy' USING PigStorage('\t') AS (videoid:long);
B2 = FOREACH B1 GENERATE videoid AS p;
B3 = FILTER B2 BY (p IS NOT NULL);

-- remove all videos that have been affected by the privacy bug
Y1 = JOIN P5 BY p LEFT, B3 BY p;
Y2 = FOREACH Y1 GENERATE P5::p AS p, P5::u AS u, P5::l AS l, P5::s AS s, P5::m AS m, P5::c AS c, P5::n AS n, P5::d AS d,
                         P5::s1 AS s1, P5::s2 AS s2, P5::s3 AS s3, P5::s4 AS s4, P5::s5 AS s5,
                         (B3::p IS NULL ? 1 : 0) AS b:int;
Y3 = FILTER Y2 BY (b == 1);
Y4 = FOREACH Y3 GENERATE p, u, l, s, m, c, n, d, s1, s2, s3, s4, s5;

-- load geotagged
G1 = readGeoTagged('$geotagged');
G2 = FOREACH G1 GENERATE object_id AS p, perm_viewgeo AS i, longitude AS x, latitude AS y, accuracy AS a;
G3 = FILTER G2 BY (p IS NOT NULL AND i IS NOT NULL AND i == 0 AND x IS NOT NULL AND x > -180.0 AND x < 180.0 AND x != 0.0 AND
                   y IS NOT NULL AND y > -90.0 AND y < 90.0 AND y != 0.0 AND a IS NOT NULL AND a >= 0 AND a <= 16);                   

-- associate the geolocation with the videos, if available
Z1 = JOIN Y4 BY p LEFT, G3 BY p;
Z2 = FOREACH Z1 GENERATE Y4::p AS p, Y4::u AS u, Y4::l AS l, Y4::s AS s, Y4::m AS m, Y4::c AS c, Y4::n AS n, Y4::d AS d,
                         G3::x AS x, G3::y AS y, G3::a AS a,
                         Y4::s1 AS s1, Y4::s2 AS s2, Y4::s3 AS s3, Y4::s4 AS s4, Y4::s5 AS s5;

-- load tags
T1 = readPhotosTags('$tags');
T2 = FOREACH T1 GENERATE photo_id AS p, tag_raw AS t;
T3 = FILTER T2 BY (p IS NOT NULL AND t IS NOT NULL AND SIZE(t) > 0L);
-- lowercase the tag, and ensure there are no duplicate tags associated with each photo
-- note: tags have already been urldecoded by the macro
-- note: tags may contain non-alphanumeric characters, such as underscores, dashes and ampersands
T4 = FOREACH T3 GENERATE p, LOWER(t) AS t:chararray;
T5 = DISTINCT T4;
-- separate out the machine tags from the regular tags
T6 = FOREACH T5 GENERATE p, (t MATCHES '.+:.+=.+' ? '' : t) AS t1:chararray, (t MATCHES '.+:.+=.+' ? t : '') AS t2:chararray;
-- filter out any privacy sensitive and useless (machine) tags
-- e.g. copyright can contain someones full name (and since it is creative commons there is no real copyright anyway),
--      facebook:user= and similar give out private details that the user probably didn't want to release to the world,
--      checksum and similar are used for duplicate upload checking by the user and not relevant for our purposes.
-- note: do not release the vision:=* tags to the public
T7 = FILTER T6 BY (NOT (t1 MATCHES '.*geotagged.*')         AND NOT (t2 MATCHES '.*geotagged.*')         AND
                   NOT (t1 MATCHES '.*celltagged.*')        AND NOT (t2 MATCHES '.*celltagged.*')        AND
                   NOT (t1 MATCHES '.*copyright.*')         AND NOT (t2 MATCHES '.*copyright.*')         AND
                   NOT (t1 MATCHES '.*allrightsreserved.*') AND NOT (t2 MATCHES '.*allrightsreserved.*') AND
                   NOT (t1 MATCHES '.*creativecommons.*')   AND NOT (t2 MATCHES '.*creativecommons.*')   AND
                   NOT (t2 MATCHES '.*permission.*')        AND NOT (t2 MATCHES '.*license.*')           AND
                   NOT (t2 MATCHES '.*checksum.*')          AND NOT (t2 MATCHES '.*hash:.*')             AND
                   NOT (t2 MATCHES '.*sha1.*')              AND NOT (t2 MATCHES '.*md5:.*')              AND
                   NOT (t2 MATCHES '.*:long=.*')            AND NOT (t2 MATCHES '.*:lon=.*')             AND NOT (t2 MATCHES '.*:lat=.*')          AND
                   NOT (t2 MATCHES '.*geo:street=.*')       AND NOT (t2 MATCHES '.*geo:room=.*')         AND NOT (t2 MATCHES '.*geo:tag=.*')       AND
                   NOT (t2 MATCHES '.*address:street=.*')   AND NOT (t2 MATCHES '.*address:room=.*')     AND NOT (t2 MATCHES '.*address:tag=.*')   AND
                   NOT (t2 MATCHES '.*location:street=.*')  AND NOT (t2 MATCHES '.*location:room=.*')    AND NOT (t2 MATCHES '.*location:tag=.*')  AND
                   NOT (t2 MATCHES '.*location_google.*')   AND NOT (t2 MATCHES '.*observation:.*')      AND
                   NOT (t2 MATCHES '.*:user=.*')            AND NOT (t2 MATCHES '.*:userid=.*')          AND NOT (t2 MATCHES '.*:username=.*')     AND
                   NOT (t2 MATCHES '.*:takenby=.*')         AND NOT (t2 MATCHES '.*:photographer=.*')    AND NOT (t2 MATCHES '.*name:.*')          AND
                   NOT (t2 MATCHES '.*person:.*')           AND NOT (t2 MATCHES '.*people.*')            AND       
                   NOT (t2 MATCHES '.*javascript:.*')       AND NOT (t2 MATCHES '.*file:.*')             AND
                   NOT (t2 MATCHES '.*vision:.*'));
-- reencode
T8 = FOREACH T7 GENERATE p, URLEncode(t1) AS t1:chararray, URLEncode(t2) AS t2:chararray; 
-- group the tags into a single string
-- note: sort them for our legibility
T9 = GROUP T8 BY p;
T10 = FOREACH T9 GENERATE $0 AS p, Bag2SortedString($1.t1, ',') AS t1:chararray, Bag2SortedString($1.t2, ',') AS t2:chararray;

-- associate all tags with the photos
-- note: some photos may not have tags
X1 = JOIN Z2 BY p LEFT, T10 BY p;
X2 = FOREACH X1 GENERATE Z2::p AS p, Z2::u AS u, Z2::l AS l, Z2::s AS s, Z2::m AS m,
                         Z2::c AS c, Z2::n AS n, Z2::d AS d, T10::t1 AS t1, T10::t2 AS t2,
                         Z2::x AS x, Z2::y AS y, Z2::a AS a,
                         Z2::s1 AS s1, Z2::s2 AS s2, Z2::s3 AS s3, Z2::s4 AS s4, Z2::s5 AS s5;
                                                 
-- load user accounts
-- note: character_name is already url-encoded
A1 = readAccounts('$accounts');
A2 = FOREACH A1 GENERATE id AS u, nsid AS i, character_name AS h;
A3 = FILTER A2 BY (u IS NOT NULL AND i IS NOT NULL AND SIZE(i) > 0L AND h IS NOT NULL AND SIZE(h) > 0L);

-- associate all user information with the videos
W1 = JOIN X2 BY u, A3 BY u;
W2 = FOREACH W1 GENERATE X2::p AS p, X2::u AS u, A3::i AS i, A3::h AS h, X2::l AS l, X2::s AS s, X2::m AS m,
                         X2::c AS c, X2::n AS n, X2::d AS d, X2::t1 AS t1, X2::t2 AS t2,
                         X2::x AS x, X2::y AS y, X2::a AS a,
                         X2::s1 AS s1, X2::s2 AS s2, X2::s3 AS s3, X2::s4 AS s4, X2::s5 AS s5;

-- remove all duplicate videos
-- note: we observed there are sometimes duplicate video ids in the tables;
--       to this end we simply remove all such instances.
D1 = GROUP W2 BY p;
D2 = FOREACH D1 GENERATE FLATTEN($1) AS (p, u, i, h, l, s, m, c, n, d, t1, t2, x, y, a, s1, s2, s3, s4, s5), COUNT($1) AS z:long;
D3 = FILTER D2 BY (z == 1L);
D4 = FOREACH D3 GENERATE p, u, i, h, l, s, m, c, n, d, t1, t2, x, y, a, s1, s2, s3, s4, s5;

-- form the url that points to the video page
-- note: https://www.flickr.com/videos/{i}/{p}/
U1 = FOREACH D4 GENERATE p, u, i, h, l, s, m, c, n, d, t1, t2, x, y, a,
                         CONCAT('https://www.flickr.com/videos/',
                         CONCAT(i,
                         CONCAT('/',
                         CONCAT((chararray)p, '/')))) AS z1:chararray,
                         s1, s2, s3, s4, s5;
U2 = FILTER U1 BY (z1 IS NOT NULL AND SIZE(z1) > 0L);
-- form the url that points to the video pixels
-- note: https://www.flickr.com/videos/{i}/{p}/play/orig/{s4}
U3 = FOREACH U2 GENERATE p, u, i, h, l, s, m, c, n, d, t1, t2, x, y, a, z1, 
                         CONCAT('https://www.flickr.com/videos/',
                         CONCAT(i,
                         CONCAT('/',
                         CONCAT((chararray)p,
                         CONCAT('/play/orig/', s4))))) AS z2:chararray,
                         s1, s2, s3, s4, s5;
U4 = FILTER U3 BY (z2 IS NOT NULL AND SIZE(z2) > 0L);
-- form the url that points to the creative commons license and add its name
-- note: license id="0" name="All Rights Reserved" url=""
--       license id="1" name="Attribution-NonCommercial-ShareAlike License" url="http://creativecommons.org/licenses/by-nc-sa/2.0/"
--       license id="2" name="Attribution-NonCommercial License" url="http://creativecommons.org/licenses/by-nc/2.0/"
--       license id="3" name="Attribution-NonCommercial-NoDerivs License" url="http://creativecommons.org/licenses/by-nc-nd/2.0/"
--       license id="4" name="Attribution License" url="http://creativecommons.org/licenses/by/2.0/"
--       license id="5" name="Attribution-ShareAlike License" url="http://creativecommons.org/licenses/by-sa/2.0/"
--       license id="6" name="Attribution-NoDerivs License" url="http://creativecommons.org/licenses/by-nd/2.0/"
--       license id="7" name="No known copyright restrictions" url="http://flickr.com/commons/usage/"
-- note: we only use licenses 1, 2, 3, 4, 5, 6, and 8
U5 = FOREACH U4 GENERATE p, u, i, h, l, s, m, c, n, d, t1, t2, x, y, a, z1, z2,
                         (l == 1 ? 'Attribution-NonCommercial-ShareAlike License' :
                         (l == 2 ? 'Attribution-NonCommercial License' :
                         (l == 3 ? 'Attribution-NonCommercial-NoDerivs License' :
                         (l == 4 ? 'Attribution License' :
                         (l == 5 ? 'Attribution-ShareAlike License' :
                         (l == 6 ? 'Attribution-NoDerivs License' : '')))))) AS l1:chararray,
                         s1, s2, s3, s4, s5;
U6 = FILTER U5 BY (l1 IS NOT NULL AND SIZE(l1) > 0L);
U7 = FOREACH U6 GENERATE p, u, i, h, s, m, c, n, d, t1, t2, x, y, a, z1, z2, l1,
                         (l == 1 ? 'http://creativecommons.org/licenses/by-nc-sa/2.0/' :
                         (l == 2 ? 'http://creativecommons.org/licenses/by-nc/2.0/' :
                         (l == 3 ? 'http://creativecommons.org/licenses/by-nc-nd/2.0/' :
                         (l == 4 ? 'http://creativecommons.org/licenses/by/2.0/' :
                         (l == 5 ? 'http://creativecommons.org/licenses/by-sa/2.0/' :
                         (l == 6 ? 'http://creativecommons.org/licenses/by-nd/2.0/' : '')))))) AS l2:chararray,
                         s1, s2, s3, s4, s5;
U8 = FILTER U7 BY (l2 IS NOT NULL AND SIZE(l2) > 0L);

-- save output
STORE U8 INTO '$output' USING PigStorage('\t');