-- yfcc100m_stats_useriso.pig
-- arguments: [job name] AS $name, [input data] AS $input, [account data] AS $accounts, [extra data] AS $extra, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_stats_useriso.pig -param name=example -param input=/projects/example/input
--            -param accounts=/projects/example/accounts -param extra=/projects/example/extra -param output=/projects/example/output
--            -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
REGISTER '/homes/bthomee/macros/string.jar';
IMPORT 'macros/inc_read_accounts.pig';
IMPORT 'macros/inc_read_accounts_extra.pig';
IMPORT 'macros/load_yfcc100m.pig';

-- load dataset
Y1 = loadYFCC100M('$input');
Y2 = FILTER Y1 BY (isvideo IS NOT NULL AND usernsid IS NOT NULL AND SIZE(usernsid) > 0L);
Y3 = FOREACH Y2 GENERATE isvideo AS i, usernsid AS u;

-- load user accounts
A1 = readAccounts('$accounts');
A2 = FOREACH A1 GENERATE id AS u1, nsid AS u2;
A3 = FILTER A2 BY (u1 IS NOT NULL AND u2 IS NOT NULL AND SIZE(u2) > 0L);

-- load user extra
-- note: macro is broken
E1 = LOAD '$extra' USING PigStorage(',');
E2 = FOREACH E1 GENERATE (int)$0 AS u:int, (chararray)$19 AS l:chararray;
E3 = FILTER E2 BY (u IS NOT NULL AND l IS NOT NULL AND l MATCHES '[a-zA-Z]{2}');
E4 = FOREACH E3 GENERATE u, LOWER(l) AS l:chararray;

-- join the three
J1 = JOIN A3 BY u1, E4 BY u;
J2 = FOREACH J1 GENERATE A3::u2 AS u, E4::l AS l;
J3 = JOIN Y3 BY u, J2 BY u;
J4 = FOREACH J3 GENERATE Y3::i AS i, J2::u AS u, J2::l AS l;

-- count countries
S1 = GROUP J4 BY (i, u);
S2 = FOREACH S1 {
       L1 = LIMIT J4 1L;
       GENERATE FLATTEN(L1.(i, l)) AS (i, l);
};
S3 = GROUP S2 BY (i, l);
S4 = FOREACH S3 GENERATE FLATTEN($0) AS (i, l), COUNT($1) AS c:long;
-- order the data
S5 = ORDER S4 BY i ASC, c DESC PARALLEL 1;
 
-- save output
STORE S5 INTO '$output' USING PigStorage('\t');