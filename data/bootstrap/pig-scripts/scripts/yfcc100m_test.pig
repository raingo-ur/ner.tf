-- yfcc100m_test.pig
-- arguments: [job name] AS $name, [input data] AS $input, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_test.pig -param name=example -param input=/projects/example/input
--            -param output=/projects/example/output -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- register the jar containing udfs
REGISTER yfcc100m.jar;
DEFINE SequenceFileLoader com.yahoo.util.pig.SequenceFileLoader('-c com.twitter.elephantbird.pig.util.BytesWritableConverter', '-c com.twitter.elephantbird.pig.util.BytesWritableConverter');
DEFINE Bytes2String com.yahoo.util.pig.Bytes2String();
DEFINE String2Bytes com.yahoo.util.pig.String2Bytes();
DEFINE Base64EncodeBytes com.yahoo.util.pig.Base64EncodeBytes();
DEFINE Base64DecodeBytes com.yahoo.util.pig.Base64DecodeBytes();
DEFINE PhotoResolution com.yahoo.yfcc100m.PhotoResolution();

-- set optimizations
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;
SET pig.maxCombinedSplitSize 2000000000;
SET mapreduce.input.fileinputformat.split.minsize 2000000000;
SET mapreduce.input.fileinputformat.split.maxsize 2000000000;

-- import macros
IMPORT 'macros/load_yfcc100m.pig';

/*-- load input data
P1 = LOAD '$input' USING PigStorage('\t') AS (photoid:long, image:chararray);
P2 = FILTER P1 BY (photoid IS NOT NULL AND image IS NOT NULL AND SIZE(image) > 0L);
-- count number of records
P3 = FOREACH P2 GENERATE photoid AS p;
P4 = FOREACH (GROUP P3 ALL) GENERATE COUNT($1) AS c:long;
-- order data to create single output file
P5 = ORDER P4 BY c ASC PARALLEL 1;
-- save output
STORE P5 INTO '$output' USING PigStorage('\t');*/


/*-- load dataset
Y1 = loadYFCC100M('$input');
Y2 = FILTER Y1 BY (photoid IS NOT NULL AND isvideo IS NOT NULL AND isvideo == 0);
Y3 = FOREACH Y2 GENERATE photoid AS p;
Y4 = FOREACH (GROUP Y3 ALL) GENERATE COUNT($1) AS c:long;
-- order data to create single output file
Y5 = ORDER Y4 BY c ASC PARALLEL 1;
-- save output
STORE Y5 INTO '$output' USING PigStorage('\t');
-- (produces 99206564 records)*/


/*-- load dataset
Y1 = loadYFCC100M('$input');
Y2 = FILTER Y1 BY (photoid IS NOT NULL AND photopixels IS NOT NULL AND SIZE(photopixels) > 0L AND isvideo IS NOT NULL AND isvideo == 1);
Y3 = FOREACH Y2 GENERATE photoid AS p, photopixels AS u;
-- create wget command
Y4 = FOREACH Y3 GENERATE CONCAT('wget ', CONCAT(u, CONCAT(' -O ', CONCAT((chararray)p, '.dat')))) AS w:chararray;
-- order data to create single output file
Y5 = ORDER Y4 BY w ASC PARALLEL 1;
-- save output
STORE Y5 INTO '$output' USING PigStorage('\t');*/


/*-- load pixel data
P1 = LOAD '$input' USING PigStorage('\t') AS (photoid:long, image:chararray);
P2 = FILTER P1 BY (photoid IS NOT NULL AND image IS NOT NULL AND SIZE(image) > 0L);
P3 = FOREACH P2 GENERATE photoid AS p, image AS i;
-- base64 decode image
I1 = FOREACH P3 GENERATE p, Base64DecodeBytes(i) AS i:bytearray;
I2 = FILTER I1 BY (i IS NOT NULL AND SIZE(i) > 0L);
-- get image resolution
I3 = FOREACH I2 GENERATE p, PhotoResolution(p, i) AS t:tuple(xdim:int, ydim:int);
I4 = FILTER I3 BY (t IS NOT NULL);
I5 = FOREACH I4 GENERATE p, FLATTEN(t) AS (xdim, ydim);
I6 = FILTER I5 BY (xdim IS NOT NULL AND xdim > 0 AND ydim IS NOT NULL AND ydim > 0);
-- aggregate the data
I7 = FOREACH I6 GENERATE (xdim == 240 OR ydim == 240 ? '240' : (xdim == 500 OR ydim == 500 ? '500' : (xdim == 640 OR ydim == 640 ? '640' : 'other'))) AS a:chararray;
I8 = FOREACH (GROUP I7 BY a) GENERATE $0 AS a, COUNT($1) AS c:long;
I9 = ORDER I8 BY c DESC PARALLEL 1;
-- save output
STORE I9 INTO '$output' USING PigStorage('\t');*/


/*-- load dataset
Y1 = loadYFCC100M('/user/bthomee/yfcc100m/yfcc100m_dataset.bz2');
Y2 = FILTER Y1 BY (photoid IS NOT NULL AND isvideo IS NOT NULL AND isvideo == 0);
Y3 = FOREACH Y2 GENERATE photoid AS p;
-- load pixel data
P1 = LOAD '$input' USING PigStorage('\t') AS (photoid:long, image:chararray);
P2 = FILTER P1 BY (photoid IS NOT NULL);
P3 = FOREACH P2 GENERATE photoid AS p;
-- join the two
J1 = JOIN Y3 BY p LEFT, P3 BY p;
J2 = FOREACH J1 GENERATE Y3::p AS p, P3::p AS p2;
J3 = FILTER J2 BY (p2 IS NULL);
J4 = FOREACH J3 GENERATE p;
J5 = ORDER J4 BY p ASC PARALLEL 1;
-- save output
STORE J5 INTO '$output' USING PigStorage('\t');*/


/*-- load missing photos
Y1 = LOAD 'webhdfs://tiberiumtan-nn1.tan.ygrid.yahoo.com/projects/flickr/flabs/yfcc100m/yfcc100m_photos_missing' USING PigStorage('\t') AS (photoid:long);
Y2 = FILTER Y1 BY (photoid IS NOT NULL);
Y3 = FOREACH Y2 GENERATE photoid AS p;
-- load pixels
P1 = LOAD '$input' USING SequenceFileLoader() AS (key:bytearray, image:bytearray);
P2 = FILTER P1 BY (key IS NOT NULL AND SIZE(key) > 0L AND image IS NOT NULL AND SIZE(image) > 0L);
-- pig cannot easily convert from bytearray to chararray, so we use a UDF for that
P3 = FOREACH P2 GENERATE Bytes2String(key) AS key:chararray, image AS i;
P4 = FILTER P3 BY (key IS NOT NULL AND SIZE(key) > 0L);
-- split the key into userid and photoid
P5 = FOREACH P4 GENERATE REGEX_EXTRACT(key, '.*,\\s*(.*)', 1) AS p:chararray, i;
P6 = FILTER P5 BY (p IS NOT NULL AND SIZE(p) > 0L);
P7 = FOREACH P6 GENERATE (long)p AS p:long, i;
P8 = FILTER P7 BY (p IS NOT NULL);
-- join the two
J1 = JOIN Y3 BY p, P8 BY p;
J2 = FOREACH J1 GENERATE P8::p AS p, P8::i AS i;
-- base64 encode image
I1 = FOREACH J2 GENERATE p, Base64EncodeBytes(i) AS i:chararray;
I2 = FILTER I1 BY (i IS NOT NULL AND SIZE(i) > 0L);
-- save output
STORE I2 INTO '$output' USING PigStorage('\t');*/


/*-- load pixel data
P1 = LOAD '$input' USING PigStorage('\t') AS (photoid:long, image:chararray);
P2 = FILTER P1 BY (photoid IS NOT NULL AND image IS NOT NULL AND SIZE(image) > 0L);
P3 = FOREACH P2 GENERATE photoid AS p, image AS i;
-- force reduce phase
P4 = ORDER P3 BY p PARALLEL 1000;
-- base64 decode image
I1 = FOREACH P4 GENERATE p, Base64DecodeBytes(i) AS i:bytearray;
I2 = FILTER I1 BY (i IS NOT NULL AND SIZE(i) > 0L);
-- base64 encode image
I3 = FOREACH I2 GENERATE p, Base64EncodeBytes(i) AS i:chararray; 
I4 = FILTER I3 BY (i IS NOT NULL AND SIZE(i) > 0L);
-- save output
STORE I4 INTO '$output' USING PigStorage('\t');*/


/*-- load datset
Y1 = loadYFCC100M('/projects/flickr/flabs/yfcc100m/yfcc100m_dataset.bz2');
Y2 = FILTER Y1 BY (photoid IS NOT NULL AND isvideo IS NOT NULL AND isvideo == 0 AND longitude IS NOT NULL AND latitude IS NOT NULL);
Y3 = FOREACH Y2 GENERATE photoid AS p, longitude AS x, latitude AS y;
-- filter out all photos outside of bounding box
-- note: Pebble Beach, CA, USA:                    -121.941162, 36.565892 => (-121.99701847198052,36.52083441374012,-121.88530552801949,36.61094924495302)
-- note: Copacabana Beach, Rio de Janeiro, Brazil:  -43.178765,-22.968825 => (-43.227523573290824,-23.013974218155255,-43.13000642670917,-22.92367552486317)
-- note: Sanya Wuzhizhou Island, Hainan, China:     109.762362, 18.311527 => (109.7150661247569,18.266353233340794,109.80965787524309,18.356700553167116)
--F1 = FILTER Y3 BY (x >= -121.99701847198052 AND x <= -121.88530552801949 AND y >= 36.52083441374012 AND y <= 36.61094924495302);
--F1 = FILTER Y3 BY (x >= -43.227523573290824 AND x <= -43.13000642670917 AND y >= -23.013974218155255 AND y <= -22.92367552486317);
F1 = FILTER Y3 BY (x >= 109.7150661247569 AND x <= 109.80965787524309 AND y >= 18.266353233340794 AND y <= 18.356700553167116);
F2 = GROUP F1 BY p PARALLEL 1;
F3 = FOREACH F2 GENERATE FLATTEN($1) AS (p, x, y);
-- save output
STORE F3 INTO '$output' USING PigStorage('\t');*/


/*-- load input data
Q1 = LOAD '$input' USING PigStorage('\t') AS (p:long, i:chararray);
Q2 = FILTER Q1 BY (p IS NOT NULL AND i IS NOT NULL);
-- group everything
Q3 = GROUP Q2 BY p PARALLEL 1000;
Q4 = FOREACH Q3 GENERATE FLATTEN($1) AS (p, i), COUNT($1) AS c:long;
Q5 = FILTER Q4 BY (c == 1L);
Q6 = FOREACH Q5 GENERATE p, i;
-- save output
STORE Q6 INTO '$output' USING PigStorage('\t');*/

-- load dataset
Y1 = loadYFCC100M('$input');
T1 = FILTER Y1 BY (tags IS NOT NULL AND SIZE(tags) > 0L AND isvideo == 0);
T2 = FOREACH (GROUP T1 ALL) GENERATE 'tags-photo' AS n:chararray, COUNT($1) AS c:long;
T3 = FILTER Y1 BY (tags IS NOT NULL AND SIZE(tags) > 0L AND isvideo == 1);
T4 = FOREACH (GROUP T3 ALL) GENERATE 'tags-video' AS n:chararray, COUNT($1) AS c:long;
M1 = FILTER Y1 BY (machinetags IS NOT NULL AND SIZE(machinetags) > 0L AND isvideo == 0);
M2 = FOREACH (GROUP M1 ALL) GENERATE 'machinetags-photo' AS n:chararray, COUNT($1) AS c:long;
M3 = FILTER Y1 BY (machinetags IS NOT NULL AND SIZE(machinetags) > 0L AND isvideo == 1);
M4 = FOREACH (GROUP M3 ALL) GENERATE 'machinetags-video' AS n:chararray, COUNT($1) AS c:long;
U1 = UNION T2, T4, M2, M4 PARALLEL 1;
 
-- save output
STORE U1 INTO '$output' USING PigStorage('\t');