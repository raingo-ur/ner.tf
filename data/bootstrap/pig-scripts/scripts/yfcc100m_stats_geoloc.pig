-- yfcc100m_stats_geoloc.pig
-- arguments: [job name] AS $name, [input data] AS $input, [gadm data] AS $gadm, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_stats_geoloc.pig -param name=example -param input=/projects/example/input
--            -param gadm=/projects/example/gadm -param output=/projects/example/output -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
IMPORT 'macros/load_yfcc100m.pig';

-- load dataset
Y1 = loadYFCC100M('$input');
Y2 = FILTER Y1 BY (photoid IS NOT NULL AND usernsid IS NOT NULL AND SIZE(usernsid) > 0L);
Y3 = FOREACH Y2 GENERATE photoid AS p, usernsid AS u;

-- load gadm
G1 = LOAD '$gadm' USING PigStorage('\t') AS (photoid:long, longitude:double, latitude:double, rid:int, pid:int, name0:chararray, type0:chararray, name1:chararray, type1:chararray, name2:chararray, type2:chararray, name3:chararray, type3:chararray, name4:chararray, type4:chararray, name5:chararray, type5:chararray, xctr:double, yctr:double, xmin:double, ymin:double, xmax:double, ymax:double);
G2 = FILTER G1 BY (photoid IS NOT NULL AND rid IS NOT NULL);
G3 = FOREACH G2 GENERATE photoid AS p, rid AS i, name0 AS a0, type0 AS t0, name1 AS a1, type1 AS t1, name2 AS a2, type2 AS t2, name3 AS a3, type3 AS t3, name4 AS a4, type4 AS t4, name5 AS a5, type5 AS t5, xctr, yctr, xmin, ymin, xmax, ymax;

-- join the two
J1 = JOIN G3 BY p, Y3 BY p;
J2 = FOREACH J1 GENERATE Y3::u AS u,
                         G3::i AS i,
                         G3::a0 AS a0, G3::t0 AS t0,
                         G3::a1 AS a1, G3::t1 AS t1,
                         G3::a2 AS a2, G3::t2 AS t2,
                         G3::a3 AS a3, G3::t3 AS t3,
                         G3::a4 AS a4, G3::t4 AS t4,
                         G3::a5 AS a5, G3::t5 AS t5,
                         G3::xctr AS xctr, G3::yctr AS yctr, G3::xmin AS xmin, G3::ymin AS ymin, G3::xmax AS xmax, G3::ymax AS ymax;

-- count most popular places by number of unique users
S1 = GROUP J2 BY (u, i);
S2 = FOREACH S1 {
       O1 = LIMIT J2 1L;
       GENERATE FLATTEN(O1.(i, a0, t0, a1, t1, a2, t2, a3, t3, a4, t4, a5, t5, xctr, yctr, xmin, ymin, xmax, ymax)) AS (i, a0, t0, a1, t1, a2, t2, a3, t3, a4, t4, a5, t5, xctr, yctr, xmin, ymin, xmax, ymax);
};
S3 = GROUP S2 BY (i, a0, t0, a1, t1, a2, t2, a3, t3, a4, t4, a5, t5, xctr, yctr, xmin, ymin, xmax, ymax);
S4 = FOREACH S3 GENERATE FLATTEN($0) AS (i, a0, t0, a1, t1, a2, t2, a3, t3, a4, t4, a5, t5, xctr, yctr, xmin, ymin, xmax, ymax), COUNT($1) AS c:long;
-- order the data
S5 = ORDER S4 BY c DESC PARALLEL 1;
 
-- save output
STORE S5 INTO '$output' USING PigStorage('\t');