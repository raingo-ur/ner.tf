-- yfcc100m_internetlocality.pig
-- arguments: [job name] AS $name, [input data] AS $input, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_internetlocality.pig -param name=example -param input=/projects/example/input
--            -param output=/projects/example/output -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- register the jar containing udfs
-- note: see for more information http://twiki.corp.yahoo.com/view/Grid/GeoOnGridTransition#InternetLocality_Lookup
REGISTER hdfs:///shared/internetlocality_gridudf/$INTERNETLOCALITY_GRIDUDFVERSION/files/internetlocality.jar
REGISTER hdfs:///shared/internetlocality_gridudf/$INTERNETLOCALITY_GRIDUDFVERSION/files/internetlocality_gridudf-$INTERNETLOCALITY_GRIDUDFVERSION.jar
DEFINE Coordinates2LocationRecord com.yahoo.glt.internetlocality.pig.udf.Coordinates2LocationRecord('default');

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
IMPORT 'macros/load_yfcc100m.pig';

-- load dataset
Y1 = loadYFCC100M('$input');
Y2 = FILTER Y1 BY (photoid IS NOT NULL AND longitude IS NOT NULL AND latitude IS NOT NULL);
Y3 = FOREACH Y2 GENERATE photoid AS p, longitude AS x, latitude AS y;

-- force the next steps to take place during reduce phase
-- note: group by a random number since pig doesn't always make even splits
R1 = FOREACH Y3 GENERATE p, x, y, RANDOM() AS r:double;
R2 = GROUP R1 BY r PARALLEL 1000;
R3 = FOREACH R2 GENERATE FLATTEN($1.(p, x, y)) AS (p, x, y);

-- query internet locality
I1 = FOREACH R3 GENERATE p, 'poi' AS n:chararray, Coordinates2LocationRecord(x, y, 'POI') AS 
                                                                        t:tuple(name:chararray, town:chararray, posttown:chararray, county:chararray, state:chararray, stateshortcode:chararray, zip:chararray,
                                                                        country:chararray, iso2countrycode:chararray, placetype:chararray, placetypename:chararray, latitude:double, longitude:double,
                                                                        box_necorner_latitude:double, box_necorner_longitude:double, box_swcorner_latitude:double, box_swcorner_longitude:double,
                                                                        area:double, placedetails:int, specificprobability:chararray, defaultlanguage:chararray, streetname:chararray, crossstreetname:chararray,
                                                                        buildingnumber:chararray, woeid:chararray, timezone:chararray, dma:chararray, mma:chararray);
I2 = FOREACH R3 GENERATE p, 'neighborhood' AS n:chararray, Coordinates2LocationRecord(x, y, 'Suburb') AS 
                                                                        t:tuple(name:chararray, town:chararray, posttown:chararray, county:chararray, state:chararray, stateshortcode:chararray, zip:chararray,
                                                                        country:chararray, iso2countrycode:chararray, placetype:chararray, placetypename:chararray, latitude:double, longitude:double,
                                                                        box_necorner_latitude:double, box_necorner_longitude:double, box_swcorner_latitude:double, box_swcorner_longitude:double,
                                                                        area:double, placedetails:int, specificprobability:chararray, defaultlanguage:chararray, streetname:chararray, crossstreetname:chararray,
                                                                        buildingnumber:chararray, woeid:chararray, timezone:chararray, dma:chararray, mma:chararray);
I3 = FOREACH R3 GENERATE p, 'city' AS n:chararray, Coordinates2LocationRecord(x, y, 'Town') AS 
                                                                        t:tuple(name:chararray, town:chararray, posttown:chararray, county:chararray, state:chararray, stateshortcode:chararray, zip:chararray,
                                                                        country:chararray, iso2countrycode:chararray, placetype:chararray, placetypename:chararray, latitude:double, longitude:double,
                                                                        box_necorner_latitude:double, box_necorner_longitude:double, box_swcorner_latitude:double, box_swcorner_longitude:double,
                                                                        area:double, placedetails:int, specificprobability:chararray, defaultlanguage:chararray, streetname:chararray, crossstreetname:chararray,
                                                                        buildingnumber:chararray, woeid:chararray, timezone:chararray, dma:chararray, mma:chararray);
I4 = FOREACH R3 GENERATE p, 'county' AS n:chararray, Coordinates2LocationRecord(x, y, 'County') AS
                                                                        t:tuple(name:chararray, town:chararray, posttown:chararray, county:chararray, state:chararray, stateshortcode:chararray, zip:chararray,
                                                                        country:chararray, iso2countrycode:chararray, placetype:chararray, placetypename:chararray, latitude:double, longitude:double,
                                                                        box_necorner_latitude:double, box_necorner_longitude:double, box_swcorner_latitude:double, box_swcorner_longitude:double,
                                                                        area:double, placedetails:int, specificprobability:chararray, defaultlanguage:chararray, streetname:chararray, crossstreetname:chararray,
                                                                        buildingnumber:chararray, woeid:chararray, timezone:chararray, dma:chararray, mma:chararray);
I5 = FOREACH R3 GENERATE p, 'state' AS n:chararray, Coordinates2LocationRecord(x, y, 'State') AS
                                                                        t:tuple(name:chararray, town:chararray, posttown:chararray, county:chararray, state:chararray, stateshortcode:chararray, zip:chararray,
                                                                        country:chararray, iso2countrycode:chararray, placetype:chararray, placetypename:chararray, latitude:double, longitude:double,
                                                                        box_necorner_latitude:double, box_necorner_longitude:double, box_swcorner_latitude:double, box_swcorner_longitude:double,
                                                                        area:double, placedetails:int, specificprobability:chararray, defaultlanguage:chararray, streetname:chararray, crossstreetname:chararray,
                                                                        buildingnumber:chararray, woeid:chararray, timezone:chararray, dma:chararray, mma:chararray);
I6 = FOREACH R3 GENERATE p, 'country' AS n:chararray, Coordinates2LocationRecord(x, y, 'Country') AS
                                                                        t:tuple(name:chararray, town:chararray, posttown:chararray, county:chararray, state:chararray, stateshortcode:chararray, zip:chararray,
                                                                        country:chararray, iso2countrycode:chararray, placetype:chararray, placetypename:chararray, latitude:double, longitude:double,
                                                                        box_necorner_latitude:double, box_necorner_longitude:double, box_swcorner_latitude:double, box_swcorner_longitude:double,
                                                                        area:double, placedetails:int, specificprobability:chararray, defaultlanguage:chararray, streetname:chararray, crossstreetname:chararray,
                                                                        buildingnumber:chararray, woeid:chararray, timezone:chararray, dma:chararray, mma:chararray);
I7 = FOREACH R3 GENERATE p, 'continent' AS n:chararray, Coordinates2LocationRecord(x, y, 'Continent') AS 
                                                                        t:tuple(name:chararray, town:chararray, posttown:chararray, county:chararray, state:chararray, stateshortcode:chararray, zip:chararray,
                                                                        country:chararray, iso2countrycode:chararray, placetype:chararray, placetypename:chararray, latitude:double, longitude:double,
                                                                        box_necorner_latitude:double, box_necorner_longitude:double, box_swcorner_latitude:double, box_swcorner_longitude:double,
                                                                        area:double, placedetails:int, specificprobability:chararray, defaultlanguage:chararray, streetname:chararray, crossstreetname:chararray,
                                                                        buildingnumber:chararray, woeid:chararray, timezone:chararray, dma:chararray, mma:chararray);
I8 = UNION I1, I2, I3, I4, I5, I6, I7;
I9 = FILTER I8 BY (t IS NOT NULL);

-- extract the woeid
W1 = FOREACH I9 GENERATE p, n, FLATTEN(t) AS (name, town, posttown, county, state, stateshortcode, zip,
                                              country, iso2countrycode, placetype, placetypename, latitude, longitude,
                                              box_necorner_latitude, box_necorner_longitude, box_swcorner_latitude, box_swcorner_longitude,
                                              area, placedetails, specificprobability, defaultlanguage, streetname, crossstreetname,
                                              buildingnumber, woeid, timezone, dma, mma);
W2 = FOREACH W1 GENERATE p, n, name, woeid;
W3 = ORDER W2 BY p ASC;

-- save data
STORE W3 INTO '$output' USING PigStorage('\t');