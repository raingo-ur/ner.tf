-- yfcc100m_stats_camerascores.pig
-- arguments: [job name] AS $name, [input data] AS $input, [photos data] AS $photos, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_stats_camerascores.pig -param name=example -param input=/projects/example/input
--            -param photos=/projects/example/photos -param output=/projects/example/output -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- register the jar containing udfs
REGISTER yfcc100m.jar;
DEFINE NumberFormatter com.yahoo.util.pig.NumberFormatter('0.00');

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
REGISTER '/homes/bthomee/macros/string.jar';
IMPORT 'macros/inc_read_photo_tiberium.pig';
IMPORT 'macros/load_yfcc100m.pig';

-- load dataset
Y1 = loadYFCC100M('$input');
Y2 = FILTER Y1 BY (photoid IS NOT NULL AND camera IS NOT NULL AND SIZE(camera) > 0L);
Y3 = FOREACH Y2 GENERATE photoid AS p, LOWER(camera) AS z:chararray;

-- load photos
-- note: retain only positive scores
P1 = readPhotos('$photos');
P2 = FOREACH P1 GENERATE id AS p, count_views AS c1, count_comments AS c2, count_faves AS c3, (int)(ROUND(score + score_offset/100.0) + score_offset_auto) AS c4:int;
P3 = FILTER P2 BY (c1 >= 0 AND c2 >= 0 AND c3 >= 0 AND c4 >= 0);

-- join the two
J1 = JOIN Y3 BY p, P3 BY p;
J2 = FOREACH J1 GENERATE Y3::z AS z, P3::c1 AS c1, P3::c2 AS c2, P3::c3 AS c3, P3::c4 AS c4;

-- count camera scores
S1 = FOREACH J2 GENERATE z, (double)c1 AS c1:double, (double)c2 AS c2:double, (double)c3 AS c3:double, (double)c4 AS c4:double; 
S2 = GROUP S1 BY z;
S3 = FOREACH S2 GENERATE $0 AS z, COUNT($1) AS c:long, AVG($1.c1) AS c1:double, AVG($1.c2) AS c2:double, AVG($1.c3) AS c3:double, AVG($1.c4) AS c4:double;
-- format the data
S4 = FOREACH S3 GENERATE z, c, NumberFormatter(c1) AS c1:chararray, NumberFormatter(c2) AS c2:chararray, NumberFormatter(c3) AS c3:chararray, NumberFormatter(c4) AS c4:chararray;
-- order the data
S5 = ORDER S4 BY c DESC PARALLEL 1;
 
-- save output
STORE S5 INTO '$output' USING PigStorage('\t');