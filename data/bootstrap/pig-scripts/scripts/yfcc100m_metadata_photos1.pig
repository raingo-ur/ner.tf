-- yfcc100m_metadata_photos1.pig
-- arguments: [job name] AS $name, [input data] AS $input, [downloaded data] AS $download, [output data] AS $output, [proxy] AS $proxy
-- e.g.:      pig -w -f yfcc100m_metadata_photos1.pig -param name=example -param input=/projects/example/input -param download=/projects/example/download
--           -param output=/projects/example/output -param proxy=httpproxy-res.tan.ygrid.yahoo.com:4080 -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- register the jar containing udfs
REGISTER yfcc100m.jar;
DEFINE Download com.yahoo.yfcc100m.DownloadMetadata();
DEFINE Base64EncodeString com.yahoo.util.pig.Base64EncodeString();

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
IMPORT 'macros/load_yfcc100m.pig';

-- load dataset
Y1 = loadYFCC100M('$input');
Y2 = FILTER Y1 BY (photoid IS NOT NULL AND usernsid IS NOT NULL AND SIZE(usernsid) > 0L AND isvideo == 0 AND extension == 'jpg');
Y3 = FOREACH Y2 GENERATE photoid AS p, usernsid AS u, server AS s1, farm AS s2, secreto AS s3, extension AS s4;
-- create the URL
-- note: https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{o-secret}_o.(jpg|gif|png)
Y4 = FOREACH Y3 GENERATE p, u,
                         CONCAT('https://farm',
                         CONCAT((chararray)s2,
                         CONCAT('.staticflickr.com/',
                         CONCAT((chararray)s1,
                         CONCAT('/',
                         CONCAT((chararray)p,
                         CONCAT('_',
                         CONCAT(s3,
                         CONCAT('_o',
                         CONCAT('.', s4)))))))))) AS z:chararray;
                         
/*-- load already downloaded metadata data
M1 = LOAD '$download' USING PigStorage('\t');
M2 = FILTER M1 BY ($0 IS NOT NULL);
M3 = FOREACH M2 GENERATE $0 AS p, 1 AS m:int;

-- join the two
J1 = JOIN Y4 BY p LEFT, M3 BY p;
J2 = FOREACH J1 GENERATE Y4::p AS p, Y4::u AS u, Y4::z AS z, M3::m AS m;
-- filter out all those that have already been downloaded
J3 = FILTER J2 BY (m IS NULL);
J4 = FOREACH J3 GENERATE p, u, z;*/

-- download the metadata
-- note: force the downloading to take place during the reduce stage
-- note: we must not use redirection for photos, because in the case the photo
--       does not exist a placeholder image will be returned. in the pixels
--       validation step below, though, such placeholders will get filtered
--       out in case they do appear somehow.
-- note: pause some time before every download to reduce the load on the servers
-- note: the output will be an empty (1x1 px) image with the original metadata
--       intact.
D1 = FOREACH Y4 GENERATE p, u, z, RANDOM() AS r:double;
D2 = FOREACH (GROUP D1 BY r PARALLEL 100) GENERATE FLATTEN($1.(p, u, z)) AS (p, u, z);
D3 = FOREACH D2 GENERATE p, u, z, Download(p, z, '$proxy', 'false', '', '', 300) AS m:bytearray;
D4 = FILTER D3 BY (m IS NOT NULL AND SIZE(m) > 0L);
-- base64 encode metadata
D5 = FOREACH D4 GENERATE p, u, z, Base64EncodeString(m) AS s:chararray;
D6 = FILTER D5 BY (s IS NOT NULL AND SIZE(s) > 0L);

-- save output
STORE D6 INTO '$output' USING PigStorage('\t');