-- yfcc100m_dataset_nongeo.pig
-- arguments: [job name] AS $name, [input data] AS $input, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_dataset_nongeo.pig -param name=example -param input=/projects/example/input -param output=/projects/example/output
--            -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- load input data
I1 = LOAD '$input' USING PigStorage('\t') AS (photoid:long, userid:int, usernsid:chararray, userhandle:chararray, date_taken:chararray, date_imported:long,
                                              camera:chararray, name:chararray, description:chararray, tags:chararray, machinetags:chararray,
                                              longitude:double, latitude:double, accuracy:int,
                                              photopage:chararray, photopixels:chararray, licensename:chararray, licenseurl:chararray,
                                              server:int, farm:int, secret:chararray, secreto:chararray, extension:chararray);
-- remove all geo photos/videos
I2 = FILTER I1 BY (longitude IS NULL OR latitude IS NULL OR accuracy IS NULL);

-- save output
STORE I2 INTO '$output' USING PigStorage('\t');