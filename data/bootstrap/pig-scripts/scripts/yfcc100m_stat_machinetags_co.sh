#! /usr/bin/env bash

# yfcc100m_stats_machinetags3.sh
# arguments: [yfcc100m dataset] [output data]
#            e.g. /projects/example/input /projects/example/output

# parse arguments
NARGS=2;
if [ $# -ne $NARGS ]; then echo "ERROR: this script requires $NARGS arguments, please refer to script itself for usage information"; exit 1; fi
INPUT=$1
OUTPUT=$2

echo "INPUT:      $INPUT"
echo "OUTPUT:     $OUTPUT"

# determine which queue on the grid to use
if [[ `hostname` =~ ^gwta3[0-9]+\.tan\.ygrid\.yahoo\.com$ ]]; then
	kinit flickgr@YGRID.YAHOO.COM -k -t ~/flickgr.prod.headless.keytab; QUEUE=flickr; PROXY=httpproxy-res.tan.ygrid.yahoo.com:4080; # tiberium
elif [[ `hostname` =~ ^gwta7[0-9]+\.tan\.ygrid\.yahoo\.com$ ]]; then
	QUEUE=p5; PROXY=httpproxy-prod.tan.ygrid.yahoo.com:4080; # bassnium
elif [[ `hostname` =~ ^gwbl6[0-9]+\.blue\.ygrid\.yahoo\.com$ ]]; then
	if [ $RANDOM -lt 21500 ]; then QUEUE=search_fast_lane; elif [ $RANDOM -lt 10700 ]; then QUEUE=search_general; else QUEUE=default; fi; PROXY=httpproxy-res.blue.ygrid.yahoo.com:4080; # nitro
else
	QUEUE=default; PROXY=; # default
fi

echo "selecting machinetags3 $QUEUE"
NAME=yfcc100m_stats-data
TEMP1=/tmp/$NAME-$RANDOM
while hadoop fs -test -d $TEMP1; do TEMP1=/tmp/$NAME-$RANDOM; done
pig -w -f scripts/yfcc100m_stat_machinetags_co.pig -param name=$NAME -param input=$INPUT -param output=$TEMP1 -Dmapreduce.job.queuename=$QUEUE -Dmapreduce.job.acl-view-job=* -Dmapreduce.job.acl-modify-job=* -Dpig.import.search.path=$PWD,$HOME
RET=$?; echo "exit code is $RET"; if [ $RET -ne 0 ]; then exit $RET; fi

echo "postprocessing data"
PARENT=`dirname "$OUTPUT"`; if ! `hadoop fs -test -d $PARENT`; then hadoop fs -mkdir -p $PARENT; hadoop fs -chmod -R 700 $PARENT; fi
hadoop fs -rm $OUTPUT
hadoop fs -mv $TEMP1/part-*-00000.bz2 $OUTPUT
hadoop fs -chmod 600 $OUTPUT
hadoop fs -rm -r -skipTrash $TEMP1
echo "done!"
