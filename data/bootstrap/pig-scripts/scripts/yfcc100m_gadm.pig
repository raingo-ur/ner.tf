-- yfcc100m_gadm.pig
-- arguments: [job name] AS $name, [input data] AS $input, [gadm indexed data + symlink] AS $gadm, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_gadm.pig -param name=example -param input=/projects/example/input -param gadm=/projects/example/gadm#gadm
--            -param output=/projects/example/output -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- register the jar containing udfs
REGISTER boundaries.jar;
DEFINE Searcher com.yahoo.gadm.Searcher('$gadm');

-- add the index archive to the distributed cache
SET mapreduce.job.cache.archives '$gadm';

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
IMPORT 'macros/load_yfcc100m.pig';

-- load dataset
Y1 = loadYFCC100M('$input');
Y2 = FOREACH Y1 GENERATE photoid AS p, longitude AS x, latitude AS y;
Y3 = FILTER Y2 BY (p IS NOT NULL AND x IS NOT NULL AND x > -180.0 AND x < 180.0 AND x != 0.0 AND y IS NOT NULL AND y > -90.0 AND y < 90.0 AND y != 0.0);

-- force processing to take place during reduce stage
R1 = FOREACH Y3 GENERATE p, x, y, RANDOM() AS r:double;
R2 = GROUP R1 BY r PARALLEL 1000;
R3 = FOREACH R2 GENERATE FLATTEN($1.(p, x, y)) AS (p, x, y);

-- find matching gadm boundaries
B1 = FOREACH R3 GENERATE p, x, y, Searcher(x, y) AS t:tuple(rid:int, pid:int, name0:chararray, type0:chararray, name1:chararray, type1:chararray, name2:chararray, type2:chararray, name3:chararray, type3:chararray, name4:chararray, type4:chararray, name5:chararray, type5:chararray, xctr:double, yctr:double, xmin:double, ymin:double, xmax:double, ymax:double);
B2 = FILTER B1 BY (t IS NOT NULL);
B3 = FOREACH B2 GENERATE p, x, y, FLATTEN(t) AS (rid, pid, a0, t0, a1, t1, a2, t2, a3, t3, a4, t4, a5, t5, xctr, yctr, xmin, ymin, xmax, ymax);
B4 = ORDER B3 BY p ASC PARALLEL 1;

-- save output
STORE B4 INTO '$output' USING PigStorage('\t');