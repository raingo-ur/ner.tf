-- yfcc100m_autotags.pig
-- arguments: [job name] AS $name, [input data] AS $input, [autotags data] AS $autotags, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_autotags.pig -param name=example -param input=/projects/example/input
--            -param autotags=/projects/example/autotags -param output=/projects/example/output -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- register the jar containing udfs
REGISTER yfcc100m.jar;
DEFINE AutotagParser com.yahoo.yfcc100m.AutotagParser();
DEFINE NumberFormatter com.yahoo.util.pig.NumberFormatter('0.0000');
DEFINE Bag2SortedString com.yahoo.util.pig.Bag2SortedString();

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;
SET pig.maxCombinedSplitSize 200000000;
SET mapreduce.input.fileinputformat.split.minsize 200000000;
SET mapreduce.input.fileinputformat.split.maxsize 200000000;

-- import macros
IMPORT 'macros/load_yfcc100m.pig';

-- load dataset
Y1 = loadYFCC100M('$input');
Y2 = FILTER Y1 BY (photoid IS NOT NULL);
Y3 = FOREACH Y2 GENERATE photoid AS p;

-- load autotags
A1 = LOAD '$autotags' USING PigStorage('\t') AS (id:chararray, autotags:chararray);
A2 = FILTER A1 BY (id IS NOT NULL AND SIZE(id) > 0L AND INDEXOF(id, ',') > 0 AND autotags IS NOT NULL AND SIZE(autotags) > 0L);
A3 = FOREACH A2 GENERATE FLATTEN(STRSPLIT(id,',')) AS (userid:chararray, photoid:chararray), autotags AS a;
A4 = FILTER A3 BY (userid IS NOT NULL AND SIZE(userid) > 0L AND photoid IS NOT NULL AND SIZE(photoid) > 0L);
A5 = FOREACH A4 GENERATE (long)photoid AS p:long, a;
A6 = FILTER A5 BY (p IS NOT NULL);

-- join the two
J1 = JOIN Y3 BY p, A6 BY p;
J2 = FOREACH J1 GENERATE A6::p AS p, A6::a AS a;

-- parse autotags
P1 = FOREACH J2 GENERATE p, AutotagParser(p, a) AS b:bag{t:tuple(version:chararray,autotag:chararray,score:double)};
P2 = FILTER P1 BY (b IS NOT NULL);
P3 = FOREACH P2 GENERATE p, FLATTEN(b) AS (v, a, s);

-- group them
Z1 = FOREACH P3 GENERATE p, CONCAT(v, CONCAT(':', CONCAT(a, CONCAT(':', NumberFormatter(s))))) AS s:chararray;
Z2 = FILTER Z1 BY (s IS NOT NULL AND SIZE(s) > 0L);
Z3 = GROUP Z2 BY p PARALLEL 1;
Z4 = FOREACH Z3 GENERATE $0 AS p, Bag2SortedString($1.s, '@') AS s:chararray;

-- save output
STORE Z4 INTO '$output' USING PigStorage('\t');