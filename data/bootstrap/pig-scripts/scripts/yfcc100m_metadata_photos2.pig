-- yfcc100m_metadata_photos2.pig
-- arguments: [job name] AS $name, [input data] AS $input, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_metadata_photos2.pig -param name=example -param input=/projects/example/input -param output=/projects/example/output
--            -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- register the jar containing udfs
REGISTER yfcc100m.jar;
DEFINE Base64EncodeBytes com.yahoo.util.pig.Base64EncodeBytes();

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- load input
M1 = LOAD '$input' USING PigStorage('\t') AS (photoid:long, usernsid:chararray, metadata:chararray);
M2 = FILTER M1 BY (photoid IS NOT NULL AND usernsid IS NOT NULL AND SIZE(usernsid) > 0L AND metadata IS NOT NULL AND SIZE(metadata) > 0L);
M3 = FOREACH M2 GENERATE photoid AS p, usernsid AS u, metadata AS m;

-- process metadata
-- note: force the processing to take place during the reduce stage
P1 = ORDER M3 BY p PARALLEL 1000;
P2 = FOREACH P1 GENERATE p, u, Process(m) AS m:map[];
P3 = FILTER P2 BY (m IS NOT NULL AND SIZE(m) > 0L);

-- save output
STORE P3 INTO '$output' USING PigStorage('\t');