-- yfcc100m_stats_users.pig
-- arguments: [job name] AS $name, [input data] AS $input, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_stats_users.pig -param name=example -param input=/projects/example/input
--            -param output=/projects/example/output -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
IMPORT 'macros/load_yfcc100m.pig';

-- load dataset
Y1 = loadYFCC100M('$input');
Y2 = FILTER Y1 BY (isvideo IS NOT NULL AND usernsid IS NOT NULL AND SIZE(usernsid) > 0L);
Y3 = FOREACH Y2 GENERATE isvideo AS i, usernsid AS u;

-- count users
S1 = GROUP Y3 BY (i, u);
S2 = FOREACH S1 GENERATE FLATTEN($0) AS (i, u), COUNT($1) AS c:long;
-- order the data
S3 = ORDER S2 BY c DESC PARALLEL 1;
 
-- save output
STORE S3 INTO '$output' USING PigStorage('\t');