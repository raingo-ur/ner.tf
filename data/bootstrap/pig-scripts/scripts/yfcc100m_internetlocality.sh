#! /usr/bin/env bash

# yfcc100m_internetlocality.sh
# arguments: [input data] [output data]
#            e.g. /projects/example/input /projects/example/output

# parse arguments
NARGS=2;
if [ $# -ne $NARGS ]; then echo "ERROR: this script requires $NARGS arguments, please refer to script itself for usage information"; exit 1; fi
INPUT=$1
OUTPUT=$2

echo "INPUT:    $INPUT"
echo "OUTPUT:   $OUTPUT"

# determine which queue on the grid to use
if [[ `hostname` =~ ^gwta3[0-9]+\.tan\.ygrid\.yahoo\.com$ ]]; then
	kinit flickgr@YGRID.YAHOO.COM -k -t ~/flickgr.prod.headless.keytab; QUEUE=flickr; PROXY=httpproxy-res.tan.ygrid.yahoo.com:4080; # tiberium
elif [[ `hostname` =~ ^gwta7[0-9]+\.tan\.ygrid\.yahoo\.com$ ]]; then
	QUEUE=p5; PROXY=httpproxy-prod.tan.ygrid.yahoo.com:4080; # bassnium
elif [[ `hostname` =~ ^gwbl6[0-9]+\.blue\.ygrid\.yahoo\.com$ ]]; then
	if [ $RANDOM -lt 21500 ]; then QUEUE=search_fast_lane; elif [ $RANDOM -lt 10700 ]; then QUEUE=search_general; else QUEUE=default; fi; PROXY=httpproxy-res.blue.ygrid.yahoo.com:4080; # nitro
else
	QUEUE=default; PROXY=; # default
fi

echo "preprocessing data"
hadoop fs -rm -r $OUTPUT

# set internet locality variables
INTERNETLOCALITY_GRIDUDFVERSION=8.2.0.918.6
INTERNETLOCALITY_DATAPACKVERSION=8.11.2.72

echo "querying internet locality"
NAME=yfcc100m_internetlocality-data
pig -w -f yfcc100m_internetlocality.pig -param name=$NAME -param input=$INPUT -param output=$OUTPUT -Dmapreduce.job.queuename=$QUEUE -Dmapreduce.job.acl-view-job=* -Dmapreduce.job.acl-modify-job=* -Dpig.import.search.path=$PWD,$HOME -Dmapred.cache.archives=/shared/internetlocality_gridudf/$INTERNETLOCALITY_GRIDUDFVERSION/base.tar.gz#internetlocality,/shared/internetlocality_datapack_locationprobability_allzips/$INTERNETLOCALITY_DATAPACKVERSION/base.tar.gz#internetlocality_data -Dmapred.child.env='LD_LIBRARY_PATH=$LD_LIBRARY_PATH:./internetlocality/lib64,JAVA_HOME=/home/gs/java/jdk64/current' -Dmapreduce.map.memory.mb=4096 -param INTERNETLOCALITY_GRIDUDFVERSION=$INTERNETLOCALITY_GRIDUDFVERSION
RET=$?; echo "exit code is $RET"; if [ $RET -ne 0 ]; then exit $RET; fi

echo "postprocessing data"
hadoop fs -chmod 700 $OUTPUT
hadoop fs -chmod 600 $OUTPUT/*
echo "done!"