-- yfcc100m_dataset_geo.pig
-- arguments: [job name] AS $name, [input data] AS $input, [geofences data] AS $geofences, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_dataset_geo.pig -param name=example -param input=/projects/example/input -param geofences=/projects/example/geofences
--            -param output=/projects/example/output -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- register the jar containing udfs
REGISTER yfcc100m.jar;
DEFINE DistancePoint2Point com.yahoo.util.pig.DistancePoint2Point();

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
REGISTER '/homes/bthomee/macros/string.jar';
IMPORT 'macros/inc_read_dataset_geofences.pig';

-- load input data
I1 = LOAD '$input' USING PigStorage('\t') AS (photoid:long, userid:int, usernsid:chararray, userhandle:chararray, date_taken:chararray, date_imported:long,
                                              camera:chararray, name:chararray, description:chararray, tags:chararray, machinetags:chararray,
                                              longitude:double, latitude:double, accuracy:int,
                                              photopage:chararray, photopixels:chararray, licensename:chararray, licenseurl:chararray,
                                              server:int, farm:int, secret:chararray, secreto:chararray, extension:chararray);
-- remove all non-geo photos/videos
I2 = FILTER I1 BY (longitude IS NOT NULL OR latitude IS NOT NULL OR accuracy IS NOT NULL);
I3 = FOREACH I2 GENERATE photoid AS p, userid AS u, usernsid AS i, userhandle AS h, date_taken AS s, date_imported AS m,
                         camera AS c, name AS n, description AS d, tags AS t1, machinetags AS t2,
                         longitude AS x, latitude AS y, accuracy AS a,
                         photopage AS z1, photopixels AS z2, licensename AS l1, licenseurl AS l2,
                         server AS s1, farm AS s2, secret AS s3, secreto AS s4, extension AS s5;

-- load geofences data
F1 = readGeoFences('$geofences');
F2 = FOREACH F1 GENERATE user_id AS u, longitude AS x, latitude AS y, (double)radius AS r:double;
F3 = FILTER F2 BY (u IS NOT NULL AND x IS NOT NULL AND x > -180.0 AND x < 180.0 AND x != 0.0 AND
                   y IS NOT NULL AND y > -90.0 AND y < 90.0 AND y != 0.0 AND r IS NOT NULL AND r > 0.0);

-- join the two
J1 = JOIN I3 BY u LEFT, F3 BY u;
J2 = FOREACH J1 GENERATE I3::p AS p, I3::u AS u, I3::i AS i, I3::h AS h, I3::s AS s, I3::m AS m,
                         I3::c AS c, I3::n AS n, I3::d AS d, I3::t1 AS t1, I3::t2 AS t2,
                         I3::x AS x, I3::y AS y, I3::a AS a,
                         I3::z1 AS z1, I3::z2 AS z2, I3::l1 AS l1, I3::l2 AS l2,
                         I3::s1 AS s1, I3::s2 AS s2, I3::s3 AS s3, I3::s4 AS s4, I3::s5 AS s5,
                         F3::x AS fx, F3::y AS fy, F3::r AS fr;

-- check each photo against each geofence
-- note: geofence radius is in meters, whereas the point2point distance is in kilometers.
C1 = FOREACH J2 GENERATE p, u, i, h, s, m, c, n, d, t1, t2, x, y, a, z1, z2, l1, l2, s1, s2, s3, s4, s5, (fx IS NULL ? 0L : (DistancePoint2Point(x, y, fx, fy) * 1000.0 <= fr ? 1L : 0L)) AS b:long;
C2 = GROUP C1 BY (p, u, i, h, s, m, c, n, d, t1, t2, x, y, a, z1, z2, l1, l2, s1, s2, s3, s4, s5);
C3 = FOREACH C2 GENERATE FLATTEN($0) AS (p, u, i, h, s, m, c, n, d, t1, t2, x, y, a, z1, z2, l1, l2, s1, s2, s3, s4, s5), SUM($1.b) AS b:long;
C4 = FILTER C3 BY (b == 0L);
C5 = FOREACH C4 GENERATE p, u, i, h, s, m, c, n, d, t1, t2, x, y, a, z1, z2, l1, l2, s1, s2, s3, s4, s5;

-- save output
STORE C5 INTO '$output' USING PigStorage('\t');