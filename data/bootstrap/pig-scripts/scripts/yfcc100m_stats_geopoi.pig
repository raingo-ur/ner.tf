-- yfcc100m_stats_geopoi.pig
-- arguments: [job name] AS $name, [input data] AS $input, [geonames data] AS $geonames, [iso data] AS $iso, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_stats_geopoi.pig -param name=example -param input=/projects/example/input
--            -param geonames=/projects/example/geonames -param iso=/projects/example/iso -param output=/projects/example/output
--            -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- register the jar containing udfs
REGISTER yfcc100m.jar;
DEFINE URLDecode com.yahoo.util.pig.URLDecode();
DEFINE StringNormalize com.yahoo.util.pig.StringNormalize();
DEFINE StringAlphaOnly com.yahoo.util.pig.StringAlphaOnly();
DEFINE StringStrip com.yahoo.util.pig.StringStrip();
DEFINE ConvertPoint2BBox com.yahoo.util.pig.ConvertPoint2BBox();
DEFINE DistancePoint2Point com.yahoo.util.pig.DistancePoint2Point();

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
IMPORT 'macros/load_yfcc100m.pig';

-- load dataset
Y1 = loadYFCC100M('$input');
Y2 = FILTER Y1 BY (photoid IS NOT NULL AND usernsid IS NOT NULL AND SIZE(usernsid) > 0L AND longitude IS NOT NULL AND latitude IS NOT NULL AND tags IS NOT NULL AND SIZE(tags) > 0L);
Y3 = FOREACH Y2 GENERATE photoid AS p, usernsid AS u, longitude AS x, latitude AS y, tags AS t;
-- process the tags
-- note: tags are already lowercased
Y4 = FOREACH Y3 GENERATE p, x, y, FLATTEN(TOKENIZE(t, ',')) AS t:chararray;
Y5 = FOREACH Y4 GENERATE p, x, y, URLDecode(t) AS t:chararray;
Y6 = FOREACH Y4 GENERATE p, x, y, StringStrip(StringAlphaOnly(StringNormalize(t))) AS t:chararray;
Y7 = FILTER Y6 BY (t IS NOT NULL AND SIZE(t) > 0L);
Y8 = DISTINCT Y7;

-- load geonames data
-- note: fields are:
--    geonameid         : integer id of record in geonames database
--    name              : name of geographical point (utf8) varchar(200)
--    asciiname         : name of geographical point in plain ascii characters, varchar(200)
--    alternatenames    : alternatenames, comma separated varchar(5000), may be empty
--    latitude          : latitude in decimal degrees (wgs84)
--    longitude         : longitude in decimal degrees (wgs84)
--    feature class     : see http://www.geonames.org/export/codes.html, char(1)
--    feature code      : see http://www.geonames.org/export/codes.html, varchar(10)
--    country code      : ISO-3166 2-letter country code, 2 characters
--    cc2               : alternate country codes, comma separated, ISO-3166 2-letter country code, 60 characters
--    admin1 code       : fipscode (subject to change to iso code), see exceptions below, see file admin1Codes.txt for display names of this code; varchar(20)
--    admin2 code       : code for the second administrative division, a county in the US, see file admin2Codes.txt; varchar(80) 
--    admin3 code       : code for third level administrative division, varchar(20)
--    admin4 code       : code for fourth level administrative division, varchar(20)
--    population        : bigint (8 byte int) 
--    elevation         : in meters, integer
--    dem               : digital elevation model, srtm3 or gtopo30, average elevation of 3''x3'' (ca 90mx90m) or 30''x30'' (ca 900mx900m) area in meters, integer. srtm processed by cgiar/ciat.
--    timezone          : the timezone id (see file timeZone.txt) varchar(40)
--    modification date : date of last modification in yyyy-MM-dd format
G1 = LOAD '$geonames' USING PigStorage('\t');
G2 = FOREACH G1 GENERATE (int)$0 AS geoid:int, (chararray)$6 AS featureclass:chararray, (chararray)$7 AS featurecode:chararray, (chararray)$8 AS isocode:chararray, (chararray)$1 AS name:chararray, (chararray)$3 AS names:chararray, (double)$5 AS longitude:double, (double)$4 AS latitude:double;
G3 = FILTER G2 BY (geoid IS NOT NULL AND featureclass IS NOT NULL AND SIZE(featureclass) > 0L AND featurecode IS NOT NULL AND SIZE(featurecode) > 0L AND
                   (featureclass == 'H' OR featureclass == 'L' OR featureclass == 'S' OR featureclass == 'T') AND
                   isocode IS NOT NULL AND SIZE(isocode) > 0L AND name IS NOT NULL AND SIZE(name) > 0L AND
                   longitude IS NOT NULL AND longitude > -180.0 AND longitude < 180.0 AND longitude != 0.0 AND
                   latitude IS NOT NULL AND latitude > -90.0 AND latitude < 90.0 AND latitude != 0.0);
G4 = FOREACH G3 GENERATE geoid AS g, isocode AS i, featureclass AS fk, featurecode AS fc, name AS n, names AS nn, longitude AS x, latitude AS y;
-- convert the isocode to uppercase
G5 = FOREACH G4 GENERATE g, fk, fc, UPPER(i) AS i:chararray, n, nn, x, y;
G6 = FILTER G5 BY (i IS NOT NULL AND SIZE(i) > 0L);
-- separate the alternate names
N1 = FOREACH G6 GENERATE g, fk, fc, i, n, n AS nn, x, y;
N2 = FILTER G6 BY (nn IS NOT NULL AND SIZE(nn) > 0L);
N3 = FOREACH N2 GENERATE g, fk, fc, i, n, FLATTEN(TOKENIZE(nn, ',')) AS nn:chararray, x, y;
N4 = FILTER N3 BY (nn IS NOT NULL AND SIZE(nn) > 0L);
N5 = UNION N1, N4;
-- normalize the names
N6 = FOREACH N5 GENERATE g, fk, fc, i, n, StringStrip(StringAlphaOnly(StringNormalize(LOWER(nn)))) AS nn:chararray, x, y;
N7 = FILTER N6 BY (nn IS NOT NULL AND SIZE(nn) > 0L);
-- deduplicate the data
N8 = DISTINCT N7;
-- convert place to enlarged bounding box surrounding it
B1 = FOREACH N8 GENERATE g, fk, fc, i, n, nn, x AS xctr, y AS yctr, ConvertPoint2BBox(x, y, 2.5) AS box:tuple(xmin:double, ymin:double, xmax:double, ymax:double);
B2 = FILTER B1 BY (box IS NOT NULL);
B3 = FOREACH B2 GENERATE g, fk, fc, i, n, nn, xctr, yctr, FLATTEN(box) AS (xmin, ymin, xmax, ymax);

-- load iso data
I1 = LOAD '$iso' USING PigStorage('\t') AS (continent:chararray, iso2:chararray, iso3:chararray, code:int, name:chararray);
I2 = FILTER I1 BY (continent IS NOT NULL AND SIZE(continent) > 0L AND iso2 IS NOT NULL AND SIZE(iso2) > 0L AND
                   iso3 IS NOT NULL AND SIZE(iso3) > 0L AND code IS NOT NULL AND name IS NOT NULL AND SIZE(name) > 0L);
I3 = FOREACH I2 GENERATE continent AS t, iso2 AS i, name AS n;

-- join the two
J1 = JOIN B3 BY i, I3 BY i;
J2 = FOREACH J1 GENERATE B3::g AS g, B3::fk AS fk, B3::fc AS fc, I3::t AS t, B3::i AS i, I3::n AS z, B3::n AS n, B3::nn AS nn,
                         B3::xctr AS xctr, B3::yctr AS yctr, B3::xmin AS xmin, B3::ymin AS ymin, B3::xmax AS xmax, B3::ymax AS ymax;

-- filter out all photos that don't have any tag that matches the normalized name of the poi
F1 = JOIN J2 BY nn, Y8 BY t;
F2 = FOREACH F1 GENERATE J2::g AS g, J2::fk AS fk, J2::fc AS fc, J2::t AS t, J2::i AS i, J2::z AS z, J2::n AS n,
                         J2::xctr AS xctr, J2::yctr AS yctr, J2::xmin AS xmin, J2::ymin AS ymin, J2::xmax AS xmax, J2::ymax AS ymax,
                         Y8::p AS p, Y8::x AS x, Y8::y AS y;
-- filter out all photos not falling within the bounding box of a poi
F3 = FILTER F2 BY (x >= xmin AND x <= xmax AND y >= ymin AND y <= ymax);
F4 = FOREACH F3 GENERATE g, fk, fc, t, i, z, n, p;
-- deduplicate the data, in case a photo's tags matched more than one alternative spelling of the poi
F5 = DISTINCT F4;

-- determine top pois, ordered by continent and the number of unique users contributing photos
T1 = JOIN Y3 BY p, F5 BY p;
T2 = FOREACH T1 GENERATE Y3::p AS p, Y3::u AS u, F5::n AS n, F5::g AS g, F5::fk AS fk, F5::fc AS fc, F5::t AS t, F5::i AS i, F5::z AS z;
T3 = GROUP T2 BY (g, u);
T4 = FOREACH T3 {
       O1 = LIMIT T2 1L;
       GENERATE FLATTEN(O1.(g, n, fk, fc, t, i, z)) AS (g, n, fk, fc, t, i, z);
};
T5 = GROUP T4 BY (g, n, fk, fc, t, i, z);
T6 = FOREACH T5 GENERATE FLATTEN($0) AS (g, n, fk, fc, t, i, z), COUNT($1) AS c:long;
-- filter out all pois that don't have at least 5 unique users
T7 = FILTER T6 BY (c > 5L);
-- order the data
T8 = ORDER T7 BY t ASC, c DESC PARALLEL 1;

-- save output
STORE T8 INTO '$output' USING PigStorage('\t');