-- yfcc100m_stats_machinetags1.pig
-- arguments: [job name] AS $name, [input data] AS $input, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_stats_machinetags1.pig -param name=example -param input=/projects/example/input
--            -param output=/projects/example/output -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- register the jar containing udfs
REGISTER yfcc100m.jar;
DEFINE URLDecode com.yahoo.util.pig.URLDecode();

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- load dataset
A = LOAD '$input' USING PigStorage('\t') AS (photoid:chararray, tags:chararray);
A1 = FILTER A BY (tags IS NOT NULL AND SIZE(tags) > 0L);
A2 = FOREACH A1 GENERATE photoid, FLATTEN(TOKENIZE(tags, '@')) AS tag;
A3 = FOREACH A2 GENERATE photoid, FLATTEN(STRSPLIT(tag, ':')) AS (ver:chararray, tag:chararray, conf:chararray);
A4 = FOREACH A3 GENERATE photoid, tag, (double) conf;
A5 = FILTER A4 BY conf > 0.5;

-- targetting a specific tags
T1 = FILTER A5 BY tag == '$target';
T2 = LIMIT T1 10;
dump T2;

