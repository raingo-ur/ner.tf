-- yfcc100m_stats_resolution.pig
-- arguments: [job name] AS $name, [input data] AS $input, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_stats_resolution.pig -param name=example -param input=/projects/example/input
--            -param output=/projects/example/output -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- register the jar containing udfs
REGISTER yfcc100m.jar;
DEFINE Base64DecodeBytes com.yahoo.util.pig.Base64DecodeBytes();
DEFINE PhotoResolution com.yahoo.yfcc100m.PhotoResolution();

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;
SET pig.maxCombinedSplitSize 2000000000;
SET mapreduce.input.fileinputformat.split.minsize 2000000000;
SET mapreduce.input.fileinputformat.split.maxsize 2000000000;

-- load pixel data
P1 = LOAD '$input' USING PigStorage('\t') AS (photoid:long, image:chararray);
P2 = FILTER P1 BY (photoid IS NOT NULL AND image IS NOT NULL AND SIZE(image) > 0L);
P3 = FOREACH P2 GENERATE photoid AS p, image AS i;

-- base64 decode image
I1 = FOREACH P3 GENERATE p, Base64DecodeBytes(i) AS b:bytearray;
I2 = FILTER I1 BY (b IS NOT NULL AND SIZE(b) > 0L);

-- get image resolution
R1 = FOREACH I2 GENERATE p, PhotoResolution(p, b) AS t:tuple(xdim:int, ydim:int);
R2 = FILTER R1 BY (t IS NOT NULL);
R3 = FOREACH R2 GENERATE p, FLATTEN(t) AS (x, y);
R4 = FILTER R3 BY (x IS NOT NULL AND x > 0 AND y IS NOT NULL AND y > 0);
-- aggregate the data
R5 = FOREACH R4 GENERATE p, x, y, x*y AS r:int;
R6 = GROUP R5 BY r PARALLEL 1;
R7 = FOREACH R6 GENERATE FLATTEN($1.(p, x, y)) AS (p, x, y);

-- save output
STORE R7 INTO '$output' USING PigStorage('\t');