-- yfcc100m_flyer.pig
-- arguments: [job name] AS $name, [input data] AS $input, [photos data] AS $photos, [output data] AS $output
-- e.g.:      pig -w -f yfcc100m_flyer.pig -param name=example -param input=/projects/example/input -param photos=/projects/example/photos
--            -param output=/projects/example/output -Dmapreduce.job.queuename=default

-- set name of job
SET job.name '$name';

-- register the jar containing udfs
REGISTER yfcc100m.jar;
DEFINE NumberFormatter com.yahoo.util.pig.NumberFormatter('0.00000');

-- set optimizations
SET default_parallel 1000;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.tmpfilecompression true;
SET pig.tmpfilecompression.codec lzo;
SET pig.exec.mapPartAgg true;

-- import macros
REGISTER '/homes/bthomee/macros/string.jar';
IMPORT 'macros/inc_read_photo_tiberium.pig';
IMPORT 'macros/load_yfcc100m.pig';

-- load dataset
Y1 = loadYFCC100M('$input');
Y2 = FOREACH Y1 GENERATE photoid AS p, usernsid AS u1, userhandle AS u2, photopage AS z1, photopixels AS z2;
Y3 = FILTER Y2 BY (p IS NOT NULL AND u1 IS NOT NULL AND SIZE(u1) > 0L AND u2 IS NOT NULL AND SIZE(u2) > 0L AND z1 IS NOT NULL AND SIZE(z1) > 0L AND z2 IS NOT NULL AND SIZE(z2) > 0L);

-- load photos
P1 = readPhotos('$photos');
P2 = FOREACH P1 GENERATE id AS p, content_type AS t, is_video AS v, perms AS i, adultness AS a, license AS l, 
                         count_views AS c1, count_comments AS c2, count_faves AS c3, (int)(ROUND(score + score_offset/100.0) + score_offset_auto) AS c4:int;
-- retain only public photos (not screenshots, illustrations, etc.) that are not adult, have a suitable creative commons
-- license, and have at least one view, comment, fave and interest
P3 = FILTER P2 BY (p IS NOT NULL AND t IS NOT NULL AND t == 0 AND v IS NOT NULL AND v == 0 AND i IS NOT NULL AND i == 0 AND
                   a IS NOT NULL AND a == 0 AND l IS NOT NULL AND l >= 1 AND l <= 6 AND
                   c1 IS NOT NULL AND c1 > 0 AND c2 IS NOT NULL AND c2 > 0 AND c3 IS NOT NULL AND c3 > 0 AND c4 IS NOT NULL AND c4 > 0);
-- focus on interestingness * favorites as score
P4 = FOREACH P3 GENERATE p, c4 AS s:int; --c3 * c4 AS s:int;

-- join the two
S1 = JOIN Y3 BY p, P4 BY p;
S2 = FOREACH S1 GENERATE Y3::p AS p, Y3::u1 AS u1, Y3::u2 AS u2, Y3::z1 AS z1, Y3::z2 AS z2, P4::s AS s;
-- retain the highest scoring photo per user
S3 = GROUP S2 BY u1;
S4 = FOREACH S3 {
       O1 = ORDER S2 BY s DESC;
       O2 = LIMIT O1 1L;
       GENERATE FLATTEN(O2) AS (p, u1, u2, z1, z2, s);
};
-- order the data
S5 = ORDER S4 BY s DESC PARALLEL 1;

-- save output
STORE S5 INTO '$output' USING PigStorage('\t');