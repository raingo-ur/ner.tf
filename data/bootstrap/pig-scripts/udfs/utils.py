#/usr/bin/python

import urllib
#
#######################
# Data Type Functions #
#######################
#collectBag- collect elements of a bag into other bag
#This is useful UDF after group operation
@outputSchema("y:bag{t:tuple(word1:chararray,word2:chararray)}")
def genCoWords(bag):
    outBag = []
    bag_set = set(bag)

    bag_list = []
    for word in bag_set:
        word = urllib.unquote_plus(word[0])
        bag_list.append(word)

    n_words = len(bag_list)

    for i in range(n_words):
        word1 = bag_list[i]
        for j in range(i + 1, n_words):
            word2 = bag_list[j]
            outBag.append((word1, word2))

    return outBag

import re
pattern = re.compile('[^0-9a-zA-Z]+')
@outputSchema("schema:chararray")
def convertBagToStr(acctBag):
    return ' '.join([i[1] for i in acctBag])

@outputSchema("schema:chararray")
def tokenize(raw_text):
    text = urllib.unquote_plus(raw_text)
    clean = pattern.sub(' ', text).lower().strip()
    return clean

@outputSchema("y:bag{t:tuple(bigram:chararray)}")
def genBiGram(raw_text):
    if raw_text is None:
        return []
    text = urllib.unquote_plus(raw_text)
    clean = pattern.sub(' ', text).lower().strip()
    fields = clean.split(' ')
    outBag = []

    if len(fields) == 0:
        return outBag

    outBag.append((fields[0]))
    for i in range(1, len(fields)):
        outBag.append((fields[i]))
        outBag.append((fields[i - 1] + ' ' + fields[i]))

    return outBag

@outputSchema("y:bag{t:tuple(tag1:chararray,tag2:chararray)}")
def genCoTags(bag):
    bag_list = []

    for tag_info in bag:
        fields = tag_info[0].split(':')
        tag = fields[1]
        conf = float(fields[2])

        if conf > 0.5:
            bag_list.append(tag)

    bag_list.sort()

    outBag = []
    n_words = len(bag_list)

    for i in range(n_words):
        word1 = bag_list[i]
        for j in range(i + 1, n_words):
            word2 = bag_list[j]
            outBag.append((word1, word2))

    return outBag

# Few comments-
# Pig mandates that a bag should be a bag of tuples, Python UDFs should follow this pattern.
# Tuples in Python are immutable, appending to a tuple is not possible.

def main():
    # print genCoWords(['abc', 'cdf', 'axd'])
    # print genCoTags(['abc:asd:0.8', 'cdf:aasdx:0.4', 'axd:asdfas:0.7', 'axd:asfas:0.8'])
    print genBiGram('asdfa;43;.,.as*&^ aAd')
    print genBiGram('asdfa;(*^')
    pass

if __name__ == "__main__":
    main()

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
