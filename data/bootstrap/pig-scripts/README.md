yfcc100m
========

This repository contains code for processing the YFCC100M dataset. It contains the pig scripts and java code that were used to create the dataset, as well as to download the photos from cache servers and from the sequence files on the grid.

Overview
========

* `scripts` contains utility `pig` scripts for computing statistics and dataset processing.
* `src` is the main Eclipse project directory that contains the java code for building the `jar` file.
* `lib` contains the current output `jar` file.
* `ext` contains the dependencies for the output `jar`.

Description
===========
An overview of the dataset and its contents can be found at

    http://twiki.corp.yahoo.com/view/YResearch/YFCC100M

In short, the dataset contains approximately 99.3 million photos and 0.7 million videos, sampled from all those that were public, non-adult, and using one of the Creative Commons licenses.

Each photo/video is represented by a tab-separated line that contains metadata, e.g. identifier, title, tags, geo-coordinates, photo page URL, etc.

The dataset can be found on Tiberium Tan, at

    /projects/flickr/flabs/yfcc100m/yfcc100m_dataset.bz2

The subset of the dataset only pertaining to the videos is stored at 

    /projects/flickr/flabs/yfcc100m/yfcc100m_videos.bz2

Each photo/video is also represented by an MD5 hash, which can be found at

    /projects/flickr/flabs/yfcc100m/yfcc100m_hash.bz2
    
The content of the photos and videos are available at

    /projects/flickr/flabs/yfcc100m/photos/data
    /projects/flickr/flabs/yfcc100m/videos/data
    
In these directories the content is stored in 1000 part-files, totalling 14TB of photo data and 3TB of video data. The file format is simply a tab separated line that contains the photoid as the first field, and the Base64-encoded binary content as the second field. Please refer to the scripts and source code for insights on how to load and process the data.

Of the 99,206,564 photos in the dataset, the pixels of 99,206,412 photos have been successfully obtained. In terms of resolution, 21,192,304 photos have a largest dimension of 640px, 76,391,148 photos a largest dimension of 500px, and 98,651 photos a largest dimension of 240px; the remaining 1,524,309 photos have another value as largest dimension (e.g. some photos are only 1x1px). The resolution of the photos are available at

    /projects/flickr/flabs/yfcc100m/photos/resolution.bz2


Compile
=======
The code comes in the form of an Eclipse project, for easy development and debugging. To build the yfcc100m.jar file for deployment, an `ant` script is provided. To compile the code for use on your personal machine, please use the 'build.xml' file, whereas to compile the code for use on the grid, please use the 'grid.xml' file. The difference is simply that the former embeds the hadoop and pig libraries present in the ext directory, whereas the latter dynamically loads them from the grid; this avoids potential version mismatches.

For help on getting things working, please reach out to Bart Thomee (bthomee@).
