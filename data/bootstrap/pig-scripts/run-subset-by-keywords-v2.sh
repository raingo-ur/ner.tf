#!/bin/bash
# vim ft=sh


name=subsets/test-keywords.txt
input=/projects/flickr/flabs/yfcc100m/yfcc100m_dataset.bz2
max_per_class=1000
#input=subsets/yfcc100m_mscoco_intersection.txt.subset.bz2
#input=subsets/yfcc100m_mscoco_intersection_2014.txt.subset.bz2
./scripts2/subset-by-keywords-v2.sh $input $name $name.subset-v2.bz2 $max_per_class
