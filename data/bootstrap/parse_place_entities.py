#!/usr/bin/env python
"""
Code description
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp

def main():
  lines = sys.stdin.readlines()
  for i in range(0, len(lines), 2):
    line = lines[i].strip()
    fields = line.split()
    cid = int(fields[-1])
    entities = eval(lines[i+1].strip())
    if entities:
      for entity in entities:
        print(cid, entity[1])

  pass

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
