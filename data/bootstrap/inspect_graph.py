#!/usr/bin/env python
"""
inspect rtypes. sample up to 10 examples for a edge type
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp

from extract_relations import hex_pb2
import random

def main():
  with open(sys.argv[1]) as reader:
    graph = hex_pb2.Graph()
    data = reader.read()
    from google.protobuf import text_format
    text_format.Parse(data, graph)

    max_num = 10

    vocab = [""] * graph.num_labels

    for info in graph.label_infos:
      vocab[info.index] =  ""+info.str_id.encode('utf-8')

    while True:
      print("Showing examples of edge types .....")
      rtype = raw_input("rtype: ")
      rtype = rtype.strip()
      edges = [_edge for _edge in graph.subset_relations \
          if _edge.rtype == rtype]
      print("#edges:", len(edges))

      if len(sys.argv) > 2:
        with open('eg.' + rtype + '.txt', 'w') as writer:
          for edge in edges:
            print(vocab[edge.parent], vocab[edge.child], file=writer)

      if len(edges) > max_num:
        edges = random.sample(edges, max_num)

      for edge in edges:
        print("http://dbpedia.org/page/"+vocab[edge.parent], \
            "https://www.cs.rochester.edu/u/yli/ner.py/_query/entity2images?key="+vocab[edge.child], rtype)

  pass

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
