#!/bin/bash
# vim ft=sh

data=$1
cat $data | python entity_extract.py > $data.entities
python ~/email_notify.py "Done yfcc100m entity extraction"
