#!/usr/bin/env python
"""
python filter-in.py key-path field idx
read from standard in, and write the the line with key in key-path to stdout

if idx is negative, the key is not transformed, otherwise the key is converted to the uri form first
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp

def main():
  key_path = sys.argv[1]
  try:
    idx = int(sys.argv[2])
  except:
    idx = 0

  try:
    sep = sys.argv[3]
  except:
    sep = None

  fmt = '<http://dbpedia.org/resource/%s>'
  if idx < 0:
    fmt = '%s'
    idx = -idx
  keys = []
  with open(key_path) as reader:
    for line in reader:
      line = fmt % line.strip()
      keys.append(line)
  keys = set(keys)

  for line in sys.stdin:
    fields = line.strip().split(sep)
    if len(fields) > idx and fields[idx] in keys:
      print(line.strip())
  pass

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
