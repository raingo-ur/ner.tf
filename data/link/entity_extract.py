#!/usr/bin/env python
"""
Given stdin, extract the entities for the title field of yfcc100m
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
import json
import urllib

import requests

head = {'Accept': 'application/json'}
payload = {'text': "", 'confidence': '0.5', 'spotter': 'Default', \
    'disambiguator': 'Default', 'policy': 'whitelis', 'support': 0, 'types':''}
#api = 'http://spotlight.sztaki.hu:2222/rest/candidates'
api = 'http://localhost:2222/rest/candidates'

def query(text):
  text = text.strip()
  if len(text) > 0:
    payload['text'] = text
  else:
    return {}
  r = requests.get(api, params=payload, headers=head)
  #print(text, r.text, file=sys.stderr)
  return r.json()

def extract_type(text):
  A = query(text)

  # parse the json file
  if 'annotation' not in A or 'surfaceForm' not in A['annotation']:
    return []

  sur_form = A['annotation']['surfaceForm']

  if type(sur_form) is dict:
    sur_form = [sur_form]

  res = []

  term_num = len(sur_form)
  for i in range(term_num):
    term = sur_form[i]['@name']
    offset = sur_form[i]['@offset']
    entities = sur_form[i]['resource']

    if type(entities) is dict:
      entities = [entities]

    for j in range(len(entities)):
      entity = entities[j]
      types = entity['@types']
      res.append((term.encode('utf-8'), entity['@uri'].encode('utf-8'), entity['@support']))
  return res

def main():
  header = []
  with open('./fields') as reader:
    for line in reader:
      header.append(line.strip())
  header = {h:i for i, h in enumerate(header)}
  id_idx = header['pid']
  title= header['title']
  tags = header['u-tags']

  total = 100000000L
  import progressbar
  progress = progressbar.ProgressBar(max_value=total)
  for idx, line in enumerate(sys.stdin):
    fields = line.strip().split('\t')
    title_ = urllib.unquote_plus(fields[title])
    tags_ = urllib.unquote_plus(fields[tags])
    target = title_ + " | " + tags_
    target = target[:140]
    entities = extract_type(target)
    for entity in entities:
      print(fields[id_idx], *entity, sep='\t')
    progress.update(idx)

  pass

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
