#!/usr/bin/env python

"""
Python source code - replace this with a description of the code and write the code below this text.
"""

import networkx as nx
from networkx.algorithms.traversal.depth_first_search import dfs_successors

def main():

  import sys

  edgelist=sys.argv[1]
  G = nx.read_edgelist(edgelist, create_using=nx.DiGraph())
  fmt = "http://dbpedia.org/ontology/%s"

  node_set = set()

  for node in sys.stdin:
    node = fmt % node
    root = node.strip()
    node = node.strip()
    if G.has_node(node):
      succs = dfs_successors(G, node)
      for key, items in succs.items():
        node_set.add((key, root))
        node_set |= set([(i, root) for i in items])
    node_set.add((node, root))

  for node, root in node_set:
    print '<' + node + '>', '<' + root + '>'

  pass

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
