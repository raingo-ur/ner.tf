
## Extract entity for the YFCC100M dataset

4. `entity_extract.*` expect the [dbpedia-spotlight](https://github.com/dbpedia-spotlight/dbpedia-spotlight) running in the background. e.g.:
```
java -Xms10G -jar ./dbpedia-spotlight-latest.jar en http://0.0.0.0:2222/rest
```
The runnable java code `dbpedia-spotlight-latest.jar` and the model `en` can be downloaded from `http://spotlight.sztaki.hu/downloads/`

2. `entity_extract.*` is used to extract entities from the yfcc100m dataset, whose results are consumed by `process.sh`
1. `entity-blacklist` should be editted manually to exclude some obvious errors
3. `fields` is the header of the yfcc100m dataset

Following files are expected from the `./data/meta-data/` directory, which are downloaded from [dbpedia](http://wiki.dbpedia.org/Downloads2015-10)
```
instance_types_en.ttl.bz2
mappingbased_objects_en.tql.bz2 # used in ../bootstrap/extract-relations.sh
dbpedia_2015-10.owl
```

## Stats and processing

`./process.sh $PATH_TO_ENTITY_LINK_OUTPUT` generates the stats files and the entity to image table, which are consumed by `../vis/view.py` and `../bootstrap/pipeline.sh`

Two sets files with the same format will be generated
1. `$PREFIX.valid`: `pid \t term \t entity \t support`
2. `$PREFIX.valid.count`: `#photos \t entity`
3. `$PREFIX.valid.pid`: `pid`

where `$PREFIX` is either `yfcc100m_dataset.entities` or `yfcc100m_dataset.entities.valid.500`, and the latter means filtered by `#photos>500` threshold.
