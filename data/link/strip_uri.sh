#!/bin/bash
# vim ft=sh

# strip out entity from the uri
# e.g. <http://dbpedia.org/resource/Aldous_Huxley> ==> Aldous_Huxley
sed 's#<http://dbpedia.org/resource/##' | \
  sed 's/>$//'
