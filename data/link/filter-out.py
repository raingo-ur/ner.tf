#!/usr/bin/env python
"""
python filter-out.py key-path field idx
read from standard in, and write the the line with key not in key-path to stdout
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
def load_list(path):
  header = []
  with open(path) as reader:
    for line in reader:
      header.append(line.strip())
  return header


def main():
  keys = load_list(sys.argv[1])
  keys = set(keys)
  fid = int(sys.argv[2])

  for line in sys.stdin:
    fields = line.strip().split()
    if fields[fid] not in keys:
      print(line.strip())
  pass

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
