#!/bin/bash
# vim ft=sh

code_dir=`dirname $0`

python $code_dir/flatten_skos.py \
  <(python $code_dir/dump_owl_subclass.py \
  $code_dir/data/meta-data/dbpedia_2015-10.owl) | awk '{print $1}'
