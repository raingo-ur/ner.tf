#!/usr/bin/env python
"""
Code description
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
import urllib

def main():
  header = []
  with open('./fields') as reader:
    for line in reader:
      header.append(line.strip())
  header = {h:i for i, h in enumerate(header)}
  id_idx = header['pid']
  title= header['title']
  tags = header['u-tags']

  total = 100000000L
  import progressbar
  progress = progressbar.ProgressBar(max_value=total)
  for idx, line in enumerate(sys.stdin):
    fields = line.strip().split('\t')
    tags_ = urllib.unquote_plus(fields[tags])
    if len(tags_) > 0:
      print(tags_)
    progress.update(idx)


  pass

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
