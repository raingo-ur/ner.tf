#!/bin/bash
# vim ft=sh

# given a list of entitities,
# do some filtering, output the public url and photo id of the linked photos
# output entities.info

this_dir=`dirname $0`
vis_dir=$this_dir/../../vis/
entity_path=$1

vis_data_dir=$this_dir/data/wiki-category/

python $vis_dir/db.py $vis_data_dir entity2photo $entity_path \
  | python $this_dir/filter-out.py $this_dir/data/meta-data/domains/blacklist 0 \
  > $entity_path.pid

python $vis_dir/db.py $vis_data_dir pid2hash <(awk '{print $1}' $entity_path.pid) > $entity_path.hash

public_url_base="https://multimedia-commons.s3-us-west-2.amazonaws.com/data/images"
join <(sort $entity_path.pid) <(sort $entity_path.hash) \
  | awk -v base=$public_url_base '{print $0, base"/"substr($NF,1,3)"/"substr($NF,4,3)"/"$NF".jpg"}' > $entity_path.info


function stat {
  N=$1
  cat $entity_path.info | python $this_dir/../bootstrap/group_and_sample.py $N 3 > $entity_path.info.$N
  awk '{print $4}' $entity_path.info.$N | sort | uniq -c | sort -nr > $entity_path.info.$N.stat
}

stat 100
stat 1000
stat 500
