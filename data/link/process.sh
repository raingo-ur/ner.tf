#!/bin/bash
# vim ft=sh

## Given entity link raw results. do some statistics about the number of images per entity
# filter out some categories of entities
# ./dump_type_blacklist.sh shows the categories of entities will be removed
# output a prelimiary #images based filtering
# invoke:
# ./process.sh $PATH_TO_RAW_ENTITY_RESULT

#entities=yfcc100m_dataset.entities
entities=$1

# get uniq entities
awk -F'\t' '{print $3}' $entities | sort | uniq -c | sort -nr > $entities.count
awk '{print $2}' $entities.count > $entities.uniq

# dump the types of the entities
bzcat ./data/meta-data/instance_types_en.ttl.bz2 | python filter-in.py $entities.uniq > $entities.uniq.type

# dump valid types
cat $entities.uniq.type | \
  python filter-out.py <(./dump_type_blacklist.sh) 2 > $entities.uniq.type.valid
awk '{print $3}' $entities.uniq.type.valid | \
  sort | uniq -c | sort -nr > $entities.uniq.type.valid.count

# dump valid entities
awk '{print $1}' $entities.uniq.type.valid | \
  sort -u | ./strip_uri.sh | \
  grep -v -f entity-blacklist > $entities.uniq.type.valid.entities

cat $entities | \
  python filter-in.py $entities.uniq.type.valid.entities -2 $'\t' > $entities.valid

function stat_entities
{
  entity_file=$1
  awk -F'\t' '{print $1}' $entity_file | sort -u > $entity_file.pid
  awk -F'\t' '{print $3}' $entity_file | sort | uniq -c | sort -nr > $entity_file.count
}

# filter out entities with less than 500 images
stat_entities $entities.valid
threshold=500
awk -v threshold=$threshold '($1>threshold) && (FNR > 100) {print $2}' $entities.valid.count > $entities.valid.$threshold

# get all the pid-entity mapping for the valid entites
cat $entities.valid | python filter-out.py $entities.valid.$threshold 2 > $entities.valid.$threshold.valid

# statistics of the valid entities
stat_entities $entities.valid.$threshold.valid
