#!/usr/bin/env python

"""
Python source code - replace this with a description of the code and write the code below this text.
"""

from rdflib.namespace import RDFS
from rdflib import Graph

def main():

    import sys

    g = Graph()
    owl_path = sys.argv[1]
    g.parse(owl_path)

    for s,p,o in g.triples((None, RDFS.subClassOf, None)):
        print o, s, '{}'

if __name__ == "__main__":
    main()

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
