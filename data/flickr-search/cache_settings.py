#!/usr/bin/env python
"""
settings for django cache
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

CACHES = {
    'default': {
      'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
      'LOCATION': 'data/caches/',
      }
    }

SECRET_KEY = "raingo"
# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
