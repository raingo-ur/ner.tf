#!/usr/bin/env python
"""
given key words search flickr
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
import os
import json
import time

import os

os.environ['DJANGO_SETTINGS_MODULE'] = 'cache_settings'

key_word = sys.argv[1]
max_num_photos = int(sys.argv[2])

save_dir = osp.join('data', 'photos', key_word)
if not osp.exists(save_dir):
  os.makedirs(save_dir)

import warnings
warnings.filterwarnings("ignore")
import flickr_api
from django.core.cache import cache
flickr_api.enable_cache(cache)

api_key = '849efdca049bb9ce1eb67f10001ce034'
api_secret = 'df613d648dcc3e6b'
flickr_api.set_keys(api_key = api_key, api_secret = api_secret)

auth_path = 'flickr-auth'
if not osp.exists(auth_path):
  a = flickr_api.auth.AuthHandler()
  url = a.get_authorization_url('read')
  print("auth url:", url)
  code = raw_input("auth code from flickr:").strip()
  a.set_verifier(code)
  a.save(auth_path)
else:
  flickr_api.set_auth_handler('flickr-auth')

w = flickr_api.Walker(flickr_api.Photo.search,
    tags=key_word.replace('_', ' '),
    tag_mode="all",
    sort="relevance",
    safe_search=1,
    content_type=1,
    )

def get_size_label(photo):
  sizes = photo.getSizes()
  goal = 'Medium 640'
  if goal in sizes:
    return goal
  else:
    return None

for i, photo in zip(range(max_num_photos), w):
  try:
    save_path = osp.join(save_dir, photo.id + '.jpg')
    photo.save(save_path, size_label=get_size_label(photo))
    print(key_word, i, photo.title.encode('utf-8'), save_path)
  except Exception as ioe:
    print(ioe)
    time.sleep(100)
    print("restart")

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
