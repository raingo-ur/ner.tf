#!/bin/bash
# vim ft=sh

base_dir=$1

if [ ! -d "$base_dir/data" ]
then
    echo usage: $0 base_dir
    exit 1;
fi

base_dir=`readlink -f $base_dir`

bz2Dir=$base_dir/data
images_dir=$base_dir/pixels

mkdir -p $images_dir

while :
do
  find $bz2Dir -name '*.bz2' | xargs -L 1 -P 4 python dump-pixels.py $images_dir
done
