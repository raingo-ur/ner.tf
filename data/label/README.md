

1. `./dump-grid.sh` to download images from grid
1. `./dump-pixels.sh` to extract yfcc100m images from base64 to jpeg
1. `./download-imagenet.sh` download imagenet photos given the synset id
1. `./split.sh` to sample and split imagenet images and label yfcc100m images
