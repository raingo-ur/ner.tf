#!/bin/bash
# vim ft=sh

base_path=$1
save_dir=$2
id=$3
mkdir -p $save_dir

full_path=$base_path$id.bz2
name=`basename $full_path`

ssh tt "hdfs dfs -cat $full_path" > $save_dir/$name
echo $id `date`
