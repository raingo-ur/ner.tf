#!/usr/bin/env python
"""
Code description
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp

from label_imagenet import load_vocab, load_list, list2dict
from collections import defaultdict

def load_pid2entity(path='pid2entity.txt'):
  pid2entity = defaultdict(list)
  for line in load_list(path):
    fields = line.split()
    pid2entity[fields[0]].append(fields[1])

  res = {}
  for pid, entities in pid2entity.items():
    if len(set(entities)) == 1:
      res[pid] = entities[0]
  return res

def main():
  entities, _ = load_vocab()
  entity2id = list2dict(entities)
  pid2entity = load_pid2entity(sys.argv[1])

  for line in sys.stdin:
    pid = osp.splitext(osp.basename(line.strip()))[0]
    try:
      print(line.strip(), entity2id[pid2entity[pid]])
    except KeyError:
      pass

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
