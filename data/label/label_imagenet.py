#!/usr/bin/env python
"""
Code description
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp

def load_list(path):
  res = []
  with open(path) as reader:
    for line in reader:
      res.append(line.strip())
  return res

def write_list(path, data):
  with open(path, 'w') as writer:
    for d in data:
      print(d, file=writer)

def load_vocab():
  entity_labels = 'entities.txt'
  entities = load_list(entity_labels)

  entity2synset_path = 'synset.wnid.entity.txt'
  entity2synset = [line.split() for line in
      load_list(entity2synset_path)]
  entity2synset = {f[2]:f[0] for f in entity2synset}

  synsets = [entity2synset[e] for e in entities]
  return entities, synsets

def list2dict(synsets):
  return {synset:id for id, synset in enumerate(synsets)}

def main():
  _, synsets = load_vocab()
  synset2id = list2dict(synsets)
  labels = {}
  if osp.exists('labels.txt'):
    with open('labels.txt') as reader:
      for line in reader:
        fields = line.strip().split()
        labels[fields[0]] = fields[1]

  for line in sys.stdin:
    dirname = osp.dirname(line.strip())
    wnid = osp.basename(dirname)
    if wnid not in synset2id:
      image_name = osp.basename(line.strip())
      wnid = labels.get(image_name, wnid)

    if wnid in synset2id:
      print(line.strip(), synset2id[wnid])
    #else:
    #  print("warning:", line.strip(), wnid, file=sys.stderr)

  pass

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
