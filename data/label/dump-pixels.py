#!/usr/bin/env python
"""
Code description
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp

import base64, random, bz2
from collections import defaultdict

images_dir = sys.argv[1]
bz2path = sys.argv[2]

flag = bz2path + '.done'

if not osp.exists(flag):
  cnt = 0
  try:
    with bz2.BZ2File(bz2path) as reader:
      for line in reader:
        fields = line.strip().split('\t')

        with open(osp.join(images_dir, fields[0] + '.jpg'), 'w') as writer:
          writer.write(base64.b64decode(fields[1]))
          cnt += 1

          if not cnt % 10000:
            print(cnt)
  except Exception as ioe:
    print(ioe, file=sys.stderr)
    print("waiting for more bz files", file=sys.stderr)
    import time; time.sleep(100)
    pass
  else:
    with open(flag, 'w') as writer:
      print(bz2path, file=writer)
      print(bz2path)
# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
