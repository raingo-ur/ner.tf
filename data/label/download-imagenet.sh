#!/bin/bash
# vim ft=sh

save_base=$1
synset=$2
save_dir=$save_base/$synset

if [ -d $save_dir ]
then
  exit
fi

mkdir -p $save_dir

username=
accesskey=

wget "http://www.image-net.org/download/synset?wnid=$synset&username=$username&accesskey=$accesskey&release=latest&src=stanford" -O $save_dir/imgs.tar

cd $save_dir
tar xfa imgs.tar
rm imgs.tar
