#!/bin/bash
# vim ft=sh

target=$1
this_dir=`dirname $0`

sort $target > current.txt

function _split
{
  num_samples=$1
  save_name=$2
  cat current.txt | python $this_dir/../bootstrap/group_and_sample.py $num_samples | sort > $save_name.txt
  comm -13 $save_name.txt current.txt > remains.txt
  cp remains.txt current.txt
}

_split 300 train
_split 50 dev
_split 150 test
