#!/usr/bin/env python
"""
Code description
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os
import os.path as osp

from db import *
app = create_app(sys.argv[1])
app.test_request_context().push()
db.create_all()

from flask import render_template, request, jsonify, make_response
from collections import defaultdict

import networkx as nx
def load_owl(edge_path):
  print("loading owl", edge_path)
  #path = 'dbpedia_2015-10.owl.edgelist'
  #path = '../data/link/data/meta-data/skos_categories_en.tql.bz2.edges'
  graph = nx.DiGraph()
  with open(edge_path) as reader:
    for line in reader:
      fields = line.strip().split()
      graph.add_edge(fields[0], fields[1])
  roots = [node for node, ids in graph.in_degree().items() if ids == 0]
  assert len(roots) == 1, 'single root is allowed for now: len(roots): %d' % len(roots)
  return roots[0], graph

import simplejson
def build_taxonomy(root, graph):
  print('build_taxonomy')
  num_entity_, num_photos_ = {}, {}

  for item in Category.query.all():
    if item.num_photos is not None:
      num_photos_[item.name] = item.num_photos
    if item.num_entities is not None:
      num_entity_[item.name] = item.num_entities

  def is_valid(node):
    if node is None:
      return False
    info = node['info']
    return info['num_photos'] > 10 and info['num_entity'] > 1

  max_depth = 5
  def _build_node(name, depth):
    if depth > max_depth:
      return None
    text = name
    children = []
    for child in graph.successors(name):
      node = _build_node(child, depth+1)
      if is_valid(node):
        children.append(node)
    num_children = len(children)
    num_entity = num_entity_.get(name, 0)
    num_photos = num_photos_.get(name, 0)

    if (num_children == 1) and (num_photos == 0):
      # collapse
      res = children[0]
      res['text'] += '/' + text
      return res

    children.sort(key = lambda x:-x['info']['num_photos'])

    infos = ['num_entity', 'num_photos', 'num_children']
    info = {}
    for item in infos:
      value = eval(item)
      info[item + '_direct'] = value
      for child in children:
        value += child['info'][item]
      info[item] = value

    return {'text': text,
        'children':children,
        'info': info}

  res = _build_node(root, 0)
  return res

def get_tree_base(tree):
  if tree is None:
    tree = 'owl'
  data_dir = _DATA_DIR
  res = osp.join(data_dir, 'trees', tree)
  if not osp.exists(res+'.json'):
    res = osp.join(data_dir, 'trees', 'owl')
  return res

import json
def load_taxonomy(name):
  tree_base = get_tree_base(name)
  buffer_path = tree_base + '.json'
  with open(buffer_path) as reader:
    tree = json.load(reader)

  se = request.cookies.get('session')
  if len(se) > 0:
    label_path = osp.join(tree_base + '.selected', se)
    if osp.exists(label_path):
      selected = []
      with open(label_path) as reader:
        for line in reader:
          selected.append(line.strip())
      selected = set(selected)
      def _check_tree(root):
        if root['text'] in selected:
          root['checked'] = True
        for child in root['children']:
          _check_tree(child)
      _check_tree(tree)

  return tree

def init_database():
  global _DATA_DIR
  data_dir = sys.argv[1]
  _DATA_DIR = data_dir

  load_dataset(data_dir)

  import glob
  children = []
  tree_dir = '../data/link/data/meta-data/trees'
  save_dir = osp.join(data_dir, 'trees')
  if not osp.exists(save_dir):
    os.mkdir(save_dir)
  for tree in glob.glob(osp.join(tree_dir, '*.edges')):
    name = osp.splitext(osp.basename(tree))[0]
    buffer_path = osp.join(save_dir, name + '.json')
    if not osp.exists(buffer_path):
      root, graph = load_owl(tree)
      taxonomy = build_taxonomy(root, graph)
      with open(buffer_path, 'w') as writer:
        json.dump(taxonomy, writer)
      print('built', name)

MAX_NUM = 50
def _link_query(name, AnchorClass, LinkClass, target):
  res = []
  for link in (db.session
      .query(LinkClass)
      .join(AnchorClass)
      .filter(AnchorClass.name==name.decode('utf-8'))
      .order_by(func.random())
      .limit(MAX_NUM)):
    try:
      res.append(getattr(link, target))
    except AssertionError:
      db.session.rollback()
      continue
  return res

import random
from time import gmtime, strftime
def get_ts_str():
  return strftime("%Y-%m-%d-%H-%M-%S", gmtime())


def _handle_type_labels(data):
  label_dir = get_tree_base(data['taxonomy']) + '.selected'
  if not osp.exists(label_dir):
    os.mkdir(label_dir)
  se = request.cookies.get('session')
  if len(se) > 0:
    label_path = osp.join(label_dir, se)
    with open(label_path, 'w') as writer:
      for node in data['selected']:
        print(node.encode('utf-8'), file=writer)

def type2entity(category):
  if request.method == 'POST':
    _handle_type_labels(request.json)

  category = category.split('/')[0]
  res = []
  query = (db.session
      .query(Entity)
      .join(EntityCategory)
      .join(Category)
      .filter(Category.name==category.decode('utf-8'))
      .filter(Entity.num_photos>=ENTITY_NUM_PHOTO_THRE)
      .order_by(func.random())
      .limit(MAX_NUM))
  res = [(e.name, e.num_photos) for e in query]
  res.sort(key=lambda x:-x[1])
  return render_template('entities.html', data = res, category = category)

SERVER = "//www.cs.rochester.edu/u/yli/ner.php"
def entity2images(entity_name):
  res = []
  photos = _link_query(entity_name, Entity, EntityPhoto, 'photo')
  for photo in photos:
    res.append((photo.purl, photo.murl.replace('http:', '')))
  users = [f[0].split('/')[-3] for f in res]
  users = len(set(users))
  return render_template('images.html',
      entity_name=entity_name, images=res, users = users,
      root=SERVER)

def taxonomy(name):
  taxonomy = load_taxonomy(name)
  return jsonify(taxonomy=taxonomy, status='OK')

@app.route('/_query/<action>', methods=['GET', 'POST'])
def query(action):
  query = request.args.get('key', '').encode('utf-8')
  return eval(action)(query)

import uuid
@app.route('/')
def index():
  se = request.args.get('_se', '')
  if len(se) == 0:
    se = request.cookies.get('session', "")
    if len(se) == 0:
      se = uuid.uuid4().hex
  resp = make_response(render_template('index.html', session=se))
  resp.set_cookie('session', se)
  return resp

if __name__ == '__main__':
  init_database()
  app.run(host='0.0.0.0', debug=False, threaded=True, port=5001)
  #app.run(host='0.0.0.0', debug=True, port=5001)

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
