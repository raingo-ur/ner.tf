#!/usr/bin/env python
"""
database and loading facilities
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_compress import Compress
from sqlalchemy import func

import os.path as osp
import os
from time import gmtime, strftime
from collections import defaultdict

db = SQLAlchemy()
compress = Compress()

try:
  profile
except Exception as ioe:
  print("profiler not imported", file=sys.stderr)
  def profile(func):
    return func

def load_list(path):
  header = []
  with open(path) as reader:
    for line in reader:
      header.append(line.strip())
  return header

import re
patn = re.compile(r'.*\/([^>]*)>*$')
def extract(target):
  # extract dummy from <dummy>
  target = target.decode('utf-8')
  mm = patn.match(target)
  if mm is None:
    return target
  else:
    return mm.group(1)

def _reader_sep(sep):
  def _reader(gen):
    def wrap(path):
      with open(path) as reader:
        for line in reader:
          fields = line.strip().split(sep)
          res = gen(fields)
          if res is None:
            continue
          else:
            yield res
    return wrap
  return _reader

@_reader_sep(' ')
@profile
def gen_entities(fields):
  name = extract(fields[0])
  return Entity, dict(name=name)

import time
ENTITY_NUM_PHOTO_THRE = 50
def gen_category_stat():
  query = (db.session.query(Category.id,
    func.sum(Entity.num_photos))
    .join(EntityCategory)
    .join(Entity)
    .filter(Entity.num_photos >= ENTITY_NUM_PHOTO_THRE)
    .group_by(Category.id))
  print(query)

  print("num_photos")
  num_photos = {}
  start_time = time.time()
  for cid, cnt in query:
    num_photos[cid] = cnt
  print("Elapsed time:", time.time() - start_time)

  query = (db.session.query(Category.id,
    func.count(func.distinct(Entity.id)))
    .join(EntityCategory)
    .join(Entity)
    .filter(Entity.num_photos >= ENTITY_NUM_PHOTO_THRE)
    .group_by(Category.id))

  print(query)
  print("num_entities")
  num_entities = {}
  start_time = time.time()
  for cid, cnt in query:
    num_entities[cid] = cnt
  print("Elapsed time:", time.time() - start_time)

  for cid in num_entities:
    yield Category, dict(id=cid, num_photos=num_photos.get(cid, 0),
        num_entities=num_entities.get(cid, 0))

def reset_table(table):
  db.session.query(table).delete()
  db.session.commit()

def gen_entity_stat():
  query = (db.session.query(EntityPhoto.entity_id,
    func.count(EntityPhoto.photo_id))
    .group_by(EntityPhoto.entity_id))
  start_time = time.time()
  for eid, cnt in query:
    yield Entity, dict(id=eid, num_photos=cnt)
  print("Elapsed time:", time.time() - start_time)

def gen_categories(type_path):
  print("generating categories")
  num_lines = 4639526L

  reset_table(Category)
  reset_table(EntityCategory)

  seen = set()
  @_reader_sep(' ')
  @profile
  def _parse_category(fields):
    if fields[2] not in seen:
      seen.add(fields[2])
      return Category, dict(name=extract(fields[2]))
  add_data_from_gen(_parse_category(type_path), num_lines)

  entities = {}
  for entity in Entity.query.all():
    entities[entity.name] = entity

  categories = {}
  for entity in Category.query.all():
    categories[entity.name] = entity
  seen = set()
  @_reader_sep(' ')
  @profile
  def _parse_ec(fields):
    entity = entities[extract(fields[0])]
    category = categories[extract(fields[2])]
    key = (entity.id, category.id)
    if key not in seen:
      seen.add(key)
      return EntityCategory, dict(entity_id=entity.id,
        category_id=category.id)
    else:
      return None
  add_data_from_gen(_parse_ec(type_path), num_lines)

def gen_links(link_path):
  print("loading entities")
  entities = {}
  for entity in Entity.query.all():
    entities[entity.name] = entity

  buf = [None, None]

  print("generating")
  @_reader_sep('\t')
  @profile
  def _parse(fields):
    if fields[2] not in entities:
      return None
    entity = entities[fields[2]]
    photo_id = int(fields[0])
    if photo_id != buf[0]:
      buf[0] = photo_id
      buf[1] = set()

    if entity.id in buf[1]:
      # assuming sequential uniqness, sorted by photo_id
      return None
    else:
      buf[1].add(entity.id)
      return EntityPhoto, dict(entity_id=entity.id, photo_id=photo_id)

  return _parse(link_path)

def add_data(cls, data):
  db.session.bulk_insert_mappings(cls, data)

import subprocess
def add_data_from_gen_post(gen, max_num):
  pbar = progressbar.ProgressBar(max_value=max_num).start()
  first = True
  for cnt, (cls, data) in enumerate(gen):
    if first:
      keys = data.keys()
      db_name = osp.basename(str(db.engine.url))
      table_name = cls.__tablename__
      cfg = '%s(%s)' % (table_name, ','.join(keys))

      # cursor copy_from is not stable
      cmd = [
          'psql', db_name,
          '-c', r'\COPY %s FROM STDIN' % cfg,
          '--set=ON_ERROR_STOP=true'
          ]
      p = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=sys.stderr)
      first = False
    print(*[unicode(data[k]).encode('utf-8') for k in keys], sep='\t', file=p.stdin)
    pbar.update(cnt)
  if not first:
    p.stdin.close()
    p.communicate()

def update_data(cls, data):
  db.session.bulk_update_mappings(cls, data)

import progressbar
def add_data_from_gen(gen, max_num, op=add_data):
  if op == add_data \
      and max_num > 100000L \
      and db.engine.name == 'postgresql':
    add_data_from_gen_post(gen, max_num)
    return

  batch_size = 5000000L
  buf = []
  pbar = progressbar.ProgressBar(max_value=max_num).start()
  for cnt, (cls, data) in enumerate(gen):
    buf.append(data)
    if len(buf) >= batch_size:
      op(cls, buf)
      buf = []
      pbar.update(cnt+1)
  op(cls, buf)
  db.session.commit()

class PhotoHash(db.Model):
  id = db.Column(db.BigInteger, primary_key=True)
  hash = db.Column(db.String(32))

class Photo(db.Model):
  id = db.Column(db.BigInteger, primary_key=True)
  murl = db.Column(db.Text)
  purl = db.Column(db.Text)
  links = db.relationship('EntityPhoto', backref = 'photo')

class Entity(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.Text)
  num_photos = db.Column(db.Integer, default=0)

  links = db.relationship('EntityPhoto', backref = 'entity')
  cats = db.relationship('EntityCategory', backref = 'entity')

class EntityPhoto(db.Model):
  entity_id = db.Column(db.Integer, db.ForeignKey('entity.id'), primary_key=True)
  photo_id = db.Column(db.BigInteger, db.ForeignKey('photo.id'), primary_key=True)

class EntityCategory(db.Model):
  entity_id = db.Column(db.Integer, db.ForeignKey('entity.id'), primary_key=True)
  category_id = db.Column(db.Integer, db.ForeignKey('category.id'), primary_key=True)

class Category(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.Text)
  num_photos = db.Column(db.Integer, default=0)
  num_entities = db.Column(db.Integer, default=0)

def gen_photo_hash(tsv_path):
  @_reader_sep('\t')
  @profile
  def _parse(fields):
    return PhotoHash, dict(id=int(fields[0]), hash=fields[1])
  return _parse(tsv_path)

def gen_photos(txt_path):
  header = {h:i for i, h in enumerate(load_list('../data/link/fields'))}

  pid = header['pid']
  murl = header['murl']
  purl = header['purl']

  @_reader_sep('\t')
  @profile
  def _parse(fields):
    return Photo, dict(id=int(fields[pid]), murl=fields[murl], purl=fields[purl])

  return _parse(txt_path)

def load_dataset(data_dir):

  base_path = osp.join(data_dir, 'yfcc100m_dataset')
  updated = False

  if Photo.query.first() is None:
    updated = True
    print("photos")
    add_data_from_gen(gen_photos(base_path), 100000000L)

  if PhotoHash.query.first() is None:
    print('photo hash')
    add_data_from_gen(gen_photo_hash(osp.join(data_dir, 'pid-hash')), 100000000L)

  if Entity.query.first() is None:
    print("entities")
    updated = True
    add_data_from_gen(gen_entities(base_path+'.entities.uniq'), 738264L)

  if EntityCategory.query.first() is None:
    print('entity2category')
    updated = True
    gen_categories(base_path+'.entities.uniq.type')

  if EntityPhoto.query.first() is None:
    print('entity2pid')
    updated = True
    add_data_from_gen(gen_links(base_path+'.entities'), 149392974L)
    add_data_from_gen(gen_entity_stat(), 1000L, update_data)

  if updated:
    print("stat category")
    add_data_from_gen(gen_category_stat(), 1000L, update_data)

class TmpIds(db.Model):
  id = db.Column(db.BigInteger, primary_key=True)

class TmpStrs(db.Model):
  name = db.Column(db.Text, primary_key=True)

def get_sqlite3_uri(data_dir):
  db_path = osp.join(data_dir, 'vis.db')
  db_path = osp.realpath(db_path)
  return 'sqlite+pysqlite:///' + db_path

def get_postgres_uri(data_dir):
  """
  to start postgres
  pg_ctl -D /u/yli/data-bank/pb-dbs/ -l /u/yli/data-bank/pb-dbs/logfile restart
  support utf8 accoring to https://coderwall.com/p/j-_mia/make-postgres-default-to-utf8
  header = {h:i for i, h in enumerate(load_list('../data/link/fields'))}
  create database use psql
  """
  if not data_dir.endswith('/'):
    data_dir += '/'
  name = osp.basename(osp.dirname(data_dir))
  return 'postgresql://raingo:raingo@localhost:5432/' + name

def create_app(data_dir):
  app = Flask('YFCC100M Exploerer')
  # app.config['SQLALCHEMY_DATABASE_URI'] = get_sqlite3_uri(data_dir)
  app.config['SQLALCHEMY_DATABASE_URI'] = get_postgres_uri(data_dir)
  app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
  db.init_app(app)
  compress.init_app(app)
  return app

def pid2murl(lines):
  reset_table(TmpIds)
  lines = set(lines)

  def _gen_tmp():
    for line in lines:
      try:
        pid = int(line)
      except Exception as ioe:
        print(ioe, file=sys.stderr)
      else:
        yield TmpIds, dict(id=int(line))

  add_data_from_gen(_gen_tmp(), len(lines))

  query = (db.session.query(Photo)
      .join(TmpIds, TmpIds.id == Photo.id))
  print(query, file=sys.stderr)

  for res in query.all():
    yield res.id, res.murl

def pid2hash(lines):
  reset_table(TmpIds)
  lines = set(lines)

  def _gen_tmp():
    for line in lines:
      try:
        pid = int(line)
      except Exception as ioe:
        print(ioe, file=sys.stderr)
      else:
        yield TmpIds, dict(id=int(line))

  add_data_from_gen(_gen_tmp(), len(lines))

  query = (db.session.query(PhotoHash)
      .join(TmpIds, TmpIds.id == PhotoHash.id))
  print(query, file=sys.stderr)

  for res in query.all():
    yield res.id, res.hash

def _tmp_str(lines):
  reset_table(TmpStrs)
  def _gen_tmp():
    for line in lines:
      name = extract(line.strip())
      yield TmpStrs, dict(name=name)
  add_data_from_gen(_gen_tmp(), len(lines))

def entity2photo(lines):
  _tmp_str(lines)
  query = (db.session
      .query(Photo, Entity.name)
      .join(EntityPhoto)
      .join(Entity)
      .join(TmpStrs, TmpStrs.name == Entity.name))
  print(str(query), file=sys.stderr)
  for (photo, ename) in query.all():
    yield photo.id, photo.murl, photo.purl, ename

def entity2category(lines):
  _tmp_str(lines)
  query = (db.session
      .query(Category, Entity.name)
      .join(EntityCategory)
      .join(Entity)
      .join(TmpStrs, TmpStrs.name == Entity.name))
  print(str(query), file=sys.stderr)
  for (category, ename) in query.all():
    yield category.name.encode('utf-8'), ename.encode('utf-8')

def category2entity(lines):
  _tmp_str(lines)
  query = (db.session
      .query(Entity.name, Category.name)
      .join(EntityCategory)
      .join(Category)
      .join(TmpStrs, TmpStrs.name == Category.name))
  print(str(query), file=sys.stderr)
  for (ename, cname) in query.all():
    yield ename.encode('utf-8'), cname.encode('utf-8')

def main():
  app = create_app(sys.argv[1])
  app.test_request_context().push()
  db.create_all()
  action = eval(sys.argv[2])
  try:
    input_file = sys.argv[3]
    reader = open(input_file)
  except Exception as ioe:
    print(ioe, file=sys.stderr)
    reader = sys.stdin
  lines = [line.strip() for line in reader]
  for idx, res in enumerate(action(lines)):
    print(*res)
  pass

if __name__ == "__main__":
  main()


# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
