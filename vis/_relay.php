<?php
// vim: set et sw=4 ts=4 sts=4 fdm=marker ff=unix fenc=utf8
/**
 * ner.php
 *
 * @author
 * @date 2016/08/26
 * @link
 */

require('vendor/autoload.php');

use Proxy\Http\Request;
use Proxy\Proxy;

proxy_main();

function proxy_main() {

  $path = $_SERVER['PATH_INFO'];

  if ($path == "") {
		$url = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}" . "{$_SERVER['REQUEST_URI']}";
    header('Location: ' . $url . '/', true, 301);
    die();
  }

  $request = Request::createFromGlobals();

  $proxy = new Proxy();

  $proxy->getEventDispatcher()->addListener('request.before_send', function($event){
    $event['request']->headers->set('X-Forwarded-For', 'php-proxy');
  });

  $response = $proxy->forward($request, "http://node1x4c:5001" . $path);

  // send the response back to the client
  $response->send();
}

?>
