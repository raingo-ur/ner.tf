/**
 * @overview
 *
 * @author Yuncheng Li (raingomm at gmail.com)
 * @version 2016/04/30
 */


$(function() {
	var _error = function(elem, e, ex) {
		$(elem).html("Error: " + e.responseText + " " + e.status + " " + ex);
	}
	$("body").on('click', '.entity-name', function(e) {
		e.preventDefault();
		var text = this.text;
		var text_quote = encodeURIComponent(text);
		var url = '_query/entity2images?key='+text_quote;
		$('#imgs').html("<h1>Loading images ... </h1>");
		$.get(url, function(data) {
			$('#imgs').html(data);
		}).error(function(e, ex) {
			_error("#imgs", e, ex);
		});
	});
	$(".search-input").keypress(function(e) {
		if (e.which == 13) {
			var searchString = $(this).val();
			$('#jstree').jstree('search', searchString);
		}
	});

	var _build_node = function(root) {
		var i = 0;
		for(i = 0; i < root.children.length; ++i) {
			_build_node(root.children[i]);
		}
		var title = '<table border=1>';
		for (var k in root.info) {
			if (root.info.hasOwnProperty(k)) {
				title += '<tr><td>';
				title += k.replace('num_','');
				title += '</td><td>';
				title += root.info[k];
				title += '</td></tr>'
			}
		}
		title += '</table>';
		root.a_attr = {'title': title};
		var text = root.text;
		root.text = text.replace(/Category:/g, "");
		root.data = text;

		if (root.checked) {
			root.state = {'selected':true};
		}
	}

	function getParameterByName(name, url) {
		if (!url) url = window.location.href;
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}

	var _taxonomy = getParameterByName("q");

	$.getJSON('_query/taxonomy?key='+_taxonomy, function(data) {
		_build_node(data.taxonomy);
		$('#jstree').on('changed.jstree', function(e, data) {

			if (data.action != 'select_node') {
				return;
			}

			var i, j, r = [];
			for(i = 0, j = data.selected.length; i < j; ++i) {
				r.push(data.instance.get_node(data.selected[i]).data);
			}
			var _data = {};
			_data.selected = r;
			_data.taxonomy = _taxonomy;

			var _key = data.node.data;
			var url = "_query/type2entity?key=" + encodeURIComponent(_key);

			$("#imgs").html("");
			$("#entity").data('key',  _key);
			$("#entity").html("<h3>Loading entities of "+_key+"... </h3>");
			$.ajax({
				type: "POST",
				url: url,
				data: JSON.stringify(_data),
				contentType: 'application/json;charset=UTF-8',
			}).success(function(data) {
				if ($("#entity").data('key') == _key) {
					// maybe clicked during click
					$('#entity').html(data);
				}
			}).error(function(e, ex) {
				_error("#entity", e, ex);
			});
			//$('#event_result').attr('src', url);
		}).jstree({
			'core': {
				'data': data.taxonomy
			},
			"search": {
				"case_insensitive": true,
				"show_only_matches": true,
				"fuzzy": false,
			},
			"checkbox": {
				"whole_node": true,
				"cascade": "",
				"three_state": false,
			},
			"plugins": ["search", "checkbox", "themes", "ui"]
		});
	}).error(function(e, ex) {
		_error("#jstree", e, ex);
	});

	$('#upper').on('mouseover', 'a[title]', function() {
		$(this).qtip({
			overwrite: false,
			// Make sure another tooltip can't overwrite this one without it being explicitly destroyed
			// content: 'I\'m a live qTip', // comma was missing here
			show: {
				ready: true // Needed to make it show on first mouseover event
			}
		});
	});

});
