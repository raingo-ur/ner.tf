#!/usr/bin/env python
"""
Code description
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
import bz2

from db import extract, load_list
import networkx as nx

meta_dir = '../data/link/data/meta-data'

def load_graph():
  print("loading graph")
  path = osp.join(meta_dir, 'skos_categories_en.tql.bz2')
  graph = nx.DiGraph()
  with bz2.BZ2File(path) as reader:
    for line in reader:
      fields = line.strip().split()
      if '#broader' in fields[1]:
        graph.add_edge(extract(fields[2]), extract(fields[0]))
  return graph

def norm_graph(graph, root):
  #root = 'Category:Contents'
  #TREE = nx.dfs_tree(graph, root)
  TREE = nx.bfs_tree(graph, root)
  return TREE

def dump_roots(roots_path, trees_dir, graph):
  for rname in load_list(roots_path):
    edges_path = osp.join(trees_dir, rname+".edges")
    if osp.exists(edges_path):
      continue
    root = 'Category:' + rname
    tree = nx.bfs_tree(graph, root)
    print("saving graph", rname)
    nx.write_edgelist(tree, edges_path)

def dump_lists(lists_path, trees_dir, graph):
  for lname in load_list(lists_path):
    list_path = osp.join(trees_dir, lname)
    edges_path = list_path + '.edges'
    if osp.exists(edges_path):
      continue
    nodes = set(load_list(list_path))
    subg = nx.DiGraph()
    for edge in graph.edges_iter():
      if edge[0] in nodes and edge[1] in nodes:
        subg.add_edge(*edge)
    roots = [n for n, ideg in subg.in_degree().items() if ideg == 0]
    for root in roots:
      subg.add_edge("ROOT", root)
    tree = nx.bfs_tree(subg, "ROOT")
    nx.write_edgelist(tree, edges_path)

def main():
  graph = load_graph()
  trees_dir = osp.join(meta_dir, 'trees')
  roots_path = osp.join(trees_dir, 'roots')
  if osp.exists(roots_path):
    dump_roots(roots_path, trees_dir, graph)

  list_path = osp.join(trees_dir, 'lists')
  if osp.exists(list_path):
    dump_lists(list_path, trees_dir, graph)

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
