#!/usr/bin/env python
"""
image classification
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
import tensorflow as tf
FLAGS = tf.app.flags.FLAGS

import cnns
from cnns import runner
from cnns import ops

import inputs
import loss
import time
import numpy as np
import metrics

def get_iter(split, is_train, batch_size):
  CNN = cnns.load_cnn()
  LOSS = loss.load_loss()

  with tf.name_scope("inputs"):
    data, epoch_size = inputs.inputs(is_train, split, batch_size, _CNN=CNN)
    images = data['image']
    label = data['label']

  with tf.variable_scope("classifier"):
    cnn = CNN(images, is_train)
    features = cnn.features
    if is_train:
      features = tf.nn.dropout(features, 0.5)
    logits = ops.fc_layer(features, LOSS.logits_size, 'logits')

  tf.add_to_collection("INPUT_IMAGES", images)

  if is_train:
    with tf.name_scope("loss", values=[label, logits]):
      loss_list = LOSS.loss(logits, label)
  else:
    logits = LOSS.infer(logits)
    outputs_T = {'logits':logits, 'label':label, 'path':data['path']}

    if len(FLAGS.eval_outputs_path) > 0:
      outputs_T['features'] = features

  def _eval(outputs_cat):
    weights = tf.get_collection('rtype_weights')
    if len(weights) > 1:
      weights = weights[-1].eval()
      weights_ = {}
      for idx, rtype in enumerate(inputs.RTYPES()):
        weights_[rtype] = weights[:, idx].tolist()
      weights = weights_
    else:
      weights = []

    metric, score = metrics.evaluate(outputs_cat)
    metric['weights'] = weights
    metric['num_samples'] = outputs_cat.values()[0].shape[0]
    metric['timestamp'] = time.time()

    return metric, score

  if is_train:
    return epoch_size, loss_list, cnn
  else:
    return epoch_size, outputs_T, _eval

def main(_):
  runner.main('classifier', get_iter)

if __name__ == "__main__":
  tf.app.run()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
