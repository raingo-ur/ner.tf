#!/usr/bin/env python
"""
testing setup for loss module
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
this_dir = osp.dirname(osp.realpath(__file__))
sys.path.append(osp.join(this_dir, './hex/scripts/'))
import infer
import numpy as np
import time

import tensorflow as tf
FLAGS = tf.app.flags.FLAGS

import loss
import inputs
from inputs import SHARE_PARENT_TYPE_PREFIX

# common test for loss
for name in loss.configs.keys():
  test_cls = type('Test_Loss_Common_%s' % name,
      (loss.TestLoss, tf.test.TestCase), {'_CONFIG':loss.load_loss(name)})
  globals()[test_cls.__name__] = test_cls

class TestRelabel(tf.test.TestCase):

  def setUp(self):
    super(TestRelabel, self).setUp()
    self.labeler = inputs.hie_relabel()
    loss._cache.clear()
    inputs._cache.clear()

  def test_toy(self):
    gt_pairs = [(5, {5,28,23,29,26}),]
    for leaf, gt in gt_pairs:
      self.assertEqual(set(self.labeler(leaf, 'default')), gt)
    pass

  def test_tf_relabel(self):
    label = tf.placeholder(tf.int64, [1])
    tf_relabel = loss.setup_relabeler()
    labels = tf_relabel(label, 'default')
    with self.test_session():
      for leaf in range(inputs.NUM_L1_CLASSES()):
        label_map = labels.eval(feed_dict={label:[leaf]})
        label_set = set(self.labeler(leaf, 'default'))
        for idx, l in enumerate(label_map):
          self.assertEqual(idx in label_set, l[0] == 1)

  def test_tf_relabel_sp_type(self):
    FLAGS.use_sp_edge = True
    label = tf.placeholder(tf.int64, [1])
    etype = SHARE_PARENT_TYPE_PREFIX+'default'
    tf_relabel = loss.setup_relabeler()
    labels = tf_relabel(label, etype)
    with self.test_session():
      for leaf in range(inputs.NUM_L1_CLASSES()):
        label_map = labels.eval(feed_dict={label:[leaf]})
        label_set = set(self.labeler(leaf, etype))
        for idx, l in enumerate(label_map):
          self.assertEqual(idx in label_set, l[0] == 1)
    FLAGS.use_sp_edge = False

  def test_all_if_none(self):
    FLAGS.all_if_none = True
    label = tf.placeholder(tf.int64, [1])
    etype = 'r1'
    for leaf in range(inputs.NUM_L1_CLASSES()):
      label_set = set(self.labeler(leaf, etype))
      self.assertLess(1, len(label_set))
    FLAGS.all_if_none = False

class HexTest(tf.test.TestCase):
  test_graph = osp.join(this_dir, './hex/graphs/fig3.graph.pb')
  imagenet_graph = osp.join(this_dir, './hex/graphs/imagenet_1k_extra.graph.pb')

  def test_shape_eval(self):
    batch_size = 10
    num_nodes = infer.load_jtree(self.test_graph)[-1]

    with self.test_session():
      log_pots = tf.random_normal((batch_size, num_nodes), dtype=tf.float32)

      hex_output = infer.hex_infer(log_pots, self.test_graph)
      self.assertAllEqual(hex_output.eval().shape, (batch_size, num_nodes))

  def _test_marginal(self, graph_path):
    jtree, num_nodes = infer.load_jtree(graph_path)

    with self.test_session():
      log_pots = np.array(jtree.log_potentials,
          dtype=np.float64).reshape((1, num_nodes))

      hex_output = infer.hex_infer(log_pots, graph_path)
      probabilities = tf.exp(hex_output)
      probabilities_gt = np.array(jtree.probabilities, dtype = np.float64).reshape((1, num_nodes))
      self.assertAllClose(probabilities.eval(), probabilities_gt)

  def test_fig3_marginal(self):
    self._test_marginal(self.test_graph)

  def test_imagenet_marginal(self):
    self._test_marginal(self.imagenet_graph)

  def test_speed(self):
    _, num_nodes = infer.load_jtree(self.imagenet_graph)
    batch_size = 256
    with self.test_session():
      log_pots = tf.random_normal((batch_size, num_nodes), dtype=tf.float32)
      hex_output = infer.hex_infer(log_pots, self.imagenet_graph)
      t0 = time.time()
      self.assertAllEqual(hex_output.eval().shape, [batch_size, num_nodes])
      t1 = time.time()
      print("per instance inference cost:", (t1-t0)/batch_size, file=sys.stderr)
      self.assertLess((t1 - t0) / batch_size, 0.01)

  def test_shape(self):
    _, num_nodes = infer.load_jtree(self.imagenet_graph)
    batch_size = 10
    with self.test_session():
      log_pots = tf.random_normal((batch_size, num_nodes), dtype=tf.float32)
      hex_output = infer.hex_infer(log_pots, self.imagenet_graph)
      self.assertAllEqual(hex_output.get_shape().as_list(), [batch_size, num_nodes])

  def test_invalid_graph(self):
    invalid_path = "graph.pb"
    num_nodes = 10
    batch_size = 10
    with self.test_session():
      log_pots = tf.random_normal((batch_size, num_nodes), dtype=tf.float32)
      hex_output = infer.hex_infer(log_pots, invalid_path)
      with self.assertRaises(tf.errors.InvalidArgumentError):
        _ = hex_output.eval()

  def test_invalid_shape(self):
    batch_size = 10
    _, num_nodes = infer.load_jtree(self.test_graph)
    num_nodes -= 2
    with self.test_session():
      log_pots = tf.random_normal((batch_size, num_nodes), dtype=tf.float32)
      hex_output = infer.hex_infer(log_pots, self.test_graph)
      with self.assertRaises(tf.errors.InvalidArgumentError):
        _ = hex_output.eval()

  def test_label_types(self):
    batch_size = 10
    _, num_nodes = infer.load_jtree(self.test_graph)

    def test_itype(i_type):
      with self.test_session():
        log_pots = tf.random_normal((batch_size, num_nodes), dtype=tf.float32)
        labels = tf.random_uniform((batch_size,),
            dtype=i_type, minval=0, maxval=num_nodes)

        hex_output = infer.hex_loss(log_pots, labels, self.test_graph)
        try:
          hex_output.eval()
        except:
          self.fail("failed with itype: %s" % i_type)

    test_itype(tf.int32)
    test_itype(tf.int64)

  def test_gradients(self):
    _, num_nodes = infer.load_jtree(self.test_graph)
    batch_size = 10
    x_shape = [batch_size, num_nodes]
    labels_np = np.random.randint(0, num_nodes, size = [batch_size])
    log_pots_np = 1-np.random.rand(*x_shape)*2
    with self.test_session():
      log_pots = tf.constant(log_pots_np, dtype=tf.float64)
      labels = tf.constant(labels_np, dtype=tf.int64)
      loss = infer.hex_loss(log_pots, labels, self.test_graph)
      loss = tf.reduce_mean(loss)
      num_grad, ana_grad = tf.test.compute_gradient(log_pots, x_shape,
          loss, [1], delta=1e-2)
      self.assertAllClose(ana_grad, num_grad, 1e-4)

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
