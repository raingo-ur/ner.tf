#!/usr/bin/env python
"""
read the tf record, supply the images and labels
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
import tensorflow as tf
import cnns

this_dir = osp.dirname(__file__)
sys.path.append(osp.join(this_dir, './hex/scripts/'))
from infer import hex_pb2
import networkx as nx
from collections import OrderedDict, defaultdict

import tensorflow as tf
import os

FLAGS = tf.app.flags.FLAGS
# use_sp_edge
tf.app.flags.DEFINE_boolean('use_sp_edge', False,
    """whether to use the share parent edge type""")

# all_if_none
tf.app.flags.DEFINE_boolean('all_if_none', False,
    """set to all ones if there is not edge for the leaf""")

def graph_base():
  return os.environ.get("GRAPH_BASE", './jtree.pb/')

def _GRAPH_PATH():
  return osp.join(graph_base(), 'graph.pb')

def load_graph():
  graph = hex_pb2.Graph()
  with open(_GRAPH_PATH()) as reader:
    data = reader.read()
  graph.ParseFromString(data)
  return graph

SHARE_PARENT_TYPE_PREFIX = 'sp-'
def _num_classes():
  graph = load_graph()
  rtypes = []
  for info in graph.subset_relations:
    if info.rtype not in rtypes:
      rtypes.append(info.rtype)

  native_types = list(rtypes)
  if 'merged' not in native_types:
    rtypes.append('merged')

  if FLAGS.use_sp_edge:
    for etype in native_types:
      rtypes.append(SHARE_PARENT_TYPE_PREFIX+etype)

  return graph.num_labels, graph.num_leaves, rtypes

_cache = {}

def _setup():
  if len(_cache) > 0:
    return
  num_l2, num_l1, rtypes = _num_classes()
  _cache['num_l2'] = num_l2
  _cache['num_l1'] = num_l1
  _cache['rtypes'] = rtypes

def NUM_L1_CLASSES():
  _setup()
  return _cache['num_l1']

def NUM_L2_CLASSES():
  _setup()
  return _cache['num_l2']

def RTYPES():
  _setup()
  return _cache['rtypes']

def hie_relabel():
  graph = load_graph()

  relations = defaultdict(nx.DiGraph)
  for edge in graph.subset_relations:
    # inverse relations
    relations[edge.rtype].add_edge(edge.child, edge.parent)

  native_types = relations.keys()
  for rtype in native_types:
    relations['merged'].add_edges_from(relations[rtype].edges())

  for etype, _graph in relations.items():
    _graph.add_nodes_from(range(graph.num_labels))

  # fix the relations to disallow accidient empty graph
  relations = dict(relations.items())

  def _relabel(leaf_id, rtype):
    labels = [leaf_id]

    if rtype.startswith(SHARE_PARENT_TYPE_PREFIX):
      relation = relations[rtype[len(SHARE_PARENT_TYPE_PREFIX):]]
      for pred in relation.successors(leaf_id):
        labels.extend(relation.predecessors(pred))
    else:
      relation = relations[rtype]
      if leaf_id in relation:
        labels += list(nx.descendants(relation, leaf_id))

    if FLAGS.all_if_none:
      if len(labels) == 1:
        labels += range(NUM_L1_CLASSES(), NUM_L2_CLASSES())

    return list(set(labels))

  return _relabel

def _parse_example_proto(example_serialized):
  # parse record
  # decode jpeg
  # compute the length of the caption
  feature_map = {
      'image/encoded': tf.FixedLenFeature([], dtype=tf.string),
      'image/path': tf.FixedLenFeature([], dtype=tf.string),
      'label': tf.FixedLenFeature([], dtype=tf.int64),
  }
  features = tf.parse_single_example(example_serialized, feature_map)

  image_path = features['image/path']
  image = tf.image.decode_jpeg(
      features['image/encoded'],
      channels=3,
      try_recover_truncated=True)

  label = features['label']
  res = {}
  res['image'] = image
  res['label'] = label
  res['path'] = image_path

  #image_path = tf.expand_dims(image_path,0)
  #indicator=tf.sparse_tensor_to_dense(tf.string_split(image_path, '/'), "")[0,-2]
  #res['is-yfcc'] = tf.equal(indicator, 'pixels')
  return res

from cnns import utils
inputs = lambda is_train, split, batch_size, _CNN=None: \
    utils.inputs(is_train, split, _parse_example_proto, batch_size, _CNN)

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
