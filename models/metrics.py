#!/usr/bin/env python
"""
compute evaluation metric for the npz results, generated from classifier.py
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
import json

if __name__ != "__main__":
  from inputs import NUM_L1_CLASSES
  from inputs import hie_relabel
  labeler = hie_relabel()

  from collections import defaultdict
  from sklearn.metrics import average_precision_score
  import numpy as np

  from mll_tf.metrics import evaluate as mll_eval

def evaluate(outputs):
  logits = outputs['logits']
  labels = outputs['label']
  path = outputs['path']

  _, indices = np.unique(path, return_index=True)
  logits = logits[indices, :]
  labels = labels[indices]

  res = {}
  predicted = np.argmax(logits[:, :NUM_L1_CLASSES()], axis=1)
  res['top1-accuracy-l1'] = (predicted == labels).sum() / logits.shape[0]

  aps = []
  num_samples = []
  num_relabels = []
  RTYPE = 'merged'

  precisions = {10:[], 50:[], 100:[]}

  for i in range(NUM_L1_CLASSES()):
    this_labels = labels == i
    this_score = logits[:, i]

    # precision@K
    rank = np.argsort(-this_score)
    for K, lst in precisions.items():
      lst.append(this_labels[rank[:K]].mean())

    num_samples.append(sum(this_labels))
    num_relabels.append(len(labeler(i, RTYPE)))
    aps.append(average_precision_score(this_labels, this_score))

  # allow nan in aps
  # for the labels that are not well represented in testing
  # ignore it
  mAP = np.nanmean(aps)
  for K, lst in precisions.items():
    res['prec@%d' % K] = np.nanmean(lst)

  res['mean-ap-l1'] = mAP
  res['per-class'] = {}
  res['per-class']['AP'] = aps
  res['per-class']['#Samples'] = num_samples
  res['per-class']['#Relabel'] = num_relabels

  if logits.shape[1] > NUM_L1_CLASSES():
    label_map = np.zeros_like(logits, dtype=np.int64)
    for idx, label in enumerate(labels):
      for l in labeler(label, RTYPE):
        label_map[idx, l] = 1
    outputs['label_map'] = label_map
    for key, value in mll_eval(outputs)[0].items():
      res['l2-' + key] = value

  return res, res['mean-ap-l1']

def main():
  json_path = sys.argv[1]

  with open(json_path) as reader:
    res = json.load(reader)

  res = max(res.items(), key = lambda x:x[1]['score'])[1]['info']
  print(json_path, res['top1-accuracy-l1'], res['mean-ap-l1'])

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
