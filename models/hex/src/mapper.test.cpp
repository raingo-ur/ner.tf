/**
 * @author
 * @version 2016/06/06
 */

#include "hex/test_common.hpp"
#include <cmath>
#include "hex/mapper.hpp"

using namespace hex;
using namespace std;

template <typename T>
class MapperTest : public testing::Test {};

TYPED_TEST_CASE(MapperTest, TestDtypes);

TYPED_TEST(MapperTest, Inf) {
  EXPECT_NEAR(0, exp(NEG_INF), EPS);
  EXPECT_TRUE(0 > NEG_INF);
  EXPECT_TRUE(ISNAN(NEG_INF - NEG_INF));
  EXPECT_TRUE(ISINF(logsumexp2(NEG_INF, NEG_INF)));
  EXPECT_TRUE(ISINF(NEG_INF + NEG_INF));
  EXPECT_TRUE(ISINF(NEG_INF + 0));
  EXPECT_EQ(0, std::max(NEG_INF, TypeParam(0)));
  EXPECT_TRUE(ISINF(std::max(NEG_INF, NEG_INF)));

  // EXPECT_TRUE(ISINF(NEG_INF - NEG_INF)); // this is NaN
  EXPECT_TRUE(ISINF(0 - NEG_INF));
  EXPECT_TRUE(ISINF(NEG_INF - 0));
}

TYPED_TEST(MapperTest, Setup) {
  Mapper<TypeParam> mapper;
  mapper.resize(10);
  mapper.set(1, 0);
  mapper.set(2, 0);
  mapper.freeze();
}

TYPED_TEST(MapperTest, Compute) {
  Mapper<TypeParam> mapper;
  Belief<TypeParam> belief_from;
  belief_from.domain = {0, 1};
  belief_from.add_state({0});
  belief_from.add_state({1});
  belief_from.init_prob(BATCH_SIZE);

  belief_from[0].fill_rand();
  belief_from[1].fill_rand();

  Belief<TypeParam> belief_to;
  belief_to.domain = {0, 1};
  belief_to.add_state({0});
  belief_to.add_state({0, 1});
  belief_to.init_prob(BATCH_SIZE);

  mapper.resize(2);
  mapper.set(0, 0);
  mapper.set(1, 0);
  mapper.set(1, 1);
  mapper.freeze();

  mapper.map(belief_from, belief_to);
  Vec<TypeParam> res(BATCH_SIZE);
  res = belief_from[0];
  res.binary(belief_from[1], logsumexp2<TypeParam>);
  EXPECT_EQ(belief_to[0], belief_from[0]);
  EXPECT_EQ(belief_to[1], res);
}
