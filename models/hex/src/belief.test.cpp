/**
 * @author
 * @version 2016/06/06
 */

#include "hex/test_common.hpp"
#include <cmath>
#include "hex/belief.hpp"

using namespace hex;
using namespace std;

template <typename T>
class BeliefTest : public testing::Test {};

TYPED_TEST_CASE(BeliefTest, TestDtypes);

TYPED_TEST(BeliefTest, Setup) {
  Belief<TypeParam> belief;
  belief.domain = {0, 1};
  EXPECT_TRUE(belief.add_state({0}));
  belief.init_prob(BATCH_SIZE);

  EXPECT_TRUE(belief.has_state({0}));
  EXPECT_FALSE(belief.has_state({1}));

  Vec<TypeParam> probs(BATCH_SIZE);
  probs.fill_rand();
  belief.prob[0] = probs;

  clique_type key = {0};
  EXPECT_EQ(belief[key], probs);
}

TYPED_TEST(BeliefTest, DuplicateState) {
  Belief<TypeParam> belief;
  belief.domain = {0, 1};

  EXPECT_TRUE(belief.add_state({0}));
  EXPECT_FALSE(belief.add_state({0}));

  belief.init_prob(BATCH_SIZE);
  EXPECT_EQ(belief._num_states, 1);
}

TYPED_TEST(BeliefTest, Equality) {
  Belief<TypeParam> belief;
  belief.domain = {0, 1};
  EXPECT_TRUE(belief.add_state({0}));
  belief.init_prob(BATCH_SIZE);
  belief.prob[0].fill_rand();

  Belief<TypeParam> belief2;
  belief2.domain = {0, 1};
  EXPECT_TRUE(belief2.add_state({0}));
  belief2.init_prob(BATCH_SIZE);
  belief2.prob[0] = belief.prob[0];

  EXPECT_TRUE(belief == belief2);
}

TYPED_TEST(BeliefTest, InEqualityDomain) {
  Belief<TypeParam> belief;
  belief.domain = {0, 1};
  EXPECT_TRUE(belief.add_state({0}));
  belief.init_prob(BATCH_SIZE);
  belief.prob[0].fill_rand();

  Belief<TypeParam> belief2;
  belief2.domain = {0, 1, 2};
  EXPECT_TRUE(belief2.add_state({0}));
  belief2.init_prob(BATCH_SIZE);
  belief2.prob[0] = belief.prob[0];

  EXPECT_FALSE(belief == belief2);
}

TYPED_TEST(BeliefTest, InEqualityState) {
  Belief<TypeParam> belief;
  belief.domain = {0, 1};
  EXPECT_TRUE(belief.add_state({0}));
  belief.init_prob(BATCH_SIZE);
  belief.prob[0].fill_rand();

  Belief<TypeParam> belief2;
  belief2.domain = {0, 1};
  EXPECT_TRUE(belief2.add_state({1}));
  belief2.init_prob(BATCH_SIZE);
  belief2.prob[0] = belief.prob[0];

  EXPECT_FALSE(belief == belief2);
}

TYPED_TEST(BeliefTest, InEqualityPvalue) {
  Belief<TypeParam> belief;
  belief.domain = {0, 1};
  EXPECT_TRUE(belief.add_state({0}));
  belief.init_prob(BATCH_SIZE);
  belief.prob[0].fill_rand();

  Belief<TypeParam> belief2;
  belief2.domain = {0, 1};
  EXPECT_TRUE(belief2.add_state({0}));
  belief2.init_prob(BATCH_SIZE);
  belief2.prob[0].fill_rand();

  EXPECT_FALSE(belief == belief2);
}

TYPED_TEST(BeliefTest, Binary) {
  Belief<TypeParam> belief;
  belief.domain = {0, 1};
  EXPECT_TRUE(belief.add_state({0}));
  belief.init_prob(BATCH_SIZE);
  Vec<TypeParam> probs0(BATCH_SIZE);
  probs0.fill_rand();
  belief[0] = probs0;

  Belief<TypeParam> belief2;
  belief2.domain = {0, 1};
  EXPECT_TRUE(belief2.add_state({0}));
  belief2.init_prob(BATCH_SIZE);
  Vec<TypeParam> probs1(BATCH_SIZE);
  probs1.fill_rand();
  belief2[0] = probs1;

  belief.divide(belief2);

  Vec<TypeParam> res(BATCH_SIZE);
  res = probs0;
  res.binary(probs1, hex::minus<TypeParam>);

  EXPECT_EQ(belief[0], res);
  EXPECT_EQ(belief2[0], probs1);
}

TYPED_TEST(BeliefTest, InterState) {
  Belief<TypeParam> belief;
  belief.domain = {0, 1};
  EXPECT_TRUE(belief.add_state({0}));
  EXPECT_FALSE(belief.add_state({0, 2}));
  belief.init_prob(BATCH_SIZE);

  EXPECT_EQ(belief._num_states, 1);
}

TYPED_TEST(BeliefTest, Index) {
  Belief<TypeParam> belief;
  belief.domain = {0, 1};
  EXPECT_TRUE(belief.add_state({1}));
  EXPECT_TRUE(belief.add_state({0}));
  EXPECT_TRUE(belief.add_state({0, 1}));
  belief.init_prob(BATCH_SIZE);
  belief[0].fill_rand();
  belief[1].fill_rand();
  belief[2].fill_rand();
  assignment key;

  key = {1};
  EXPECT_EQ(belief[0], belief[key]);
  key = {0};
  EXPECT_EQ(belief[1], belief[key]);
  key = {1, 0};
  EXPECT_EQ(belief[2], belief[key]);
}

TYPED_TEST(BeliefTest, Assignment) {
  Belief<TypeParam> belief;
  belief.domain = {0, 1};
  EXPECT_TRUE(belief.add_state({0}));
  belief.init_prob(BATCH_SIZE);
  Vec<TypeParam> probs(BATCH_SIZE);
  probs.fill_rand();
  belief[0] = probs;

  Belief<TypeParam> copy = belief;
  Vec<TypeParam> probs2(BATCH_SIZE);
  copy.init_prob(BATCH_SIZE);
  probs2.fill_rand();
  copy[0] = probs2;

  EXPECT_EQ(belief[0], probs);
  EXPECT_EQ(copy[0], probs2);
}
