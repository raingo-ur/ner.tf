/**
 * @author
 * @version 2016/06/07
 */

#include <gtest/gtest.h>
#include <glog/logging.h>
#include <cstdlib>
#include <cmath>
#include <cassert>
#include <limits>

int main(int argc, char **argv) {
  // http://stackoverflow.com/a/20016972/2243015
  // make sure NEG_INF is valid
  static_assert(std::numeric_limits<float>::is_iec559, "IEEE 754 required");
  ::testing::InitGoogleTest(&argc, argv);
  google::InstallFailureSignalHandler();
  google::InitGoogleLogging(argv[0]);
  return RUN_ALL_TESTS();
}
