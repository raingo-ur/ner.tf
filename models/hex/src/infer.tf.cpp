/**
 * @author
 * @version 2016/07/11
 */

#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/register_types.h"
#include "tensorflow/core/framework/tensor.h"
#include "tensorflow/core/framework/tensor_shape.h"

#include "hex/infer.hpp"

REGISTER_OP("Hex")
    .Input("logits: T")
    .Input("labels: Tlabels")
    .Output("hex: T")
    .Output("multiplier: T")
    .Attr("T: {float, double}")
    .Attr("Tlabels: {int64, int32}")
    .Attr("graph_path: string")
    .Attr("batch_size: int")
    .Doc(R"doc(
Computes hex activations.

logits: 2-D with shape `[num_classes, batch_size]`.
labels: 1-D with shape `[batch_size]`, indicating clamping index for each instance
hex: Same shape as `logits`.
multiplier: 1-D with shape `[num_classes,1]` constant per graph. used to compute gradients

gradients will look like:

get_nnz entries from top (should be only one)
use that as labels to call this operator again

)doc");

namespace tensorflow {

template <typename T, typename Index>
class HexOp : public OpKernel {
 public:
  explicit HexOp(OpKernelConstruction* ctx)
      : OpKernel(ctx), multiplier_(ctx->output_type(0)) {
    int batch_size;
    OP_REQUIRES_OK(ctx, ctx->GetAttr("batch_size", &batch_size));
    engine_.reset(new hex::infer<T, Index>(batch_size));

    OP_REQUIRES_OK(ctx, ctx->GetAttr("graph_path", &graph_path_));
    OP_REQUIRES(ctx, engine_->init(graph_path_.c_str()),
                errors::InvalidArgument(
                    "invalid graph (open failed) "));

    num_nodes_ = engine_->num_nodes();
    Tensor tmp(ctx->output_type(0), {num_nodes_});
    auto flat = tmp.vec<T>();
    engine_->get_multiplier(flat.data());
    multiplier_ = tmp;
  }

  void Compute(OpKernelContext* ctx) override {
    const Tensor* log_pots;
    OP_REQUIRES_OK(ctx, ctx->input("logits", &log_pots));
    OP_REQUIRES(ctx, TensorShapeUtils::IsMatrix(log_pots->shape()),
                errors::InvalidArgument("logits must be 2-dimensional"));
    OP_REQUIRES(ctx, log_pots->dim_size(0) == num_nodes_,
                errors::InvalidArgument("num_classes (dim0) in logits mismatch "
                                        "with the num_nodes in the graph"));

    const Tensor* labels;
    OP_REQUIRES_OK(ctx, ctx->input("labels", &labels));
    if (!TensorShapeUtils::IsVector(labels->shape())) {
      labels = NULL;
    }

    // constant multiplier
    ctx->set_output("multiplier", multiplier_);

    Tensor* hex_output = nullptr;
    OP_REQUIRES_OK(ctx,
                   ctx->allocate_output("hex", log_pots->shape(), &hex_output));

    if (hex_output->NumElements()) {
      T* hex_output_raw = hex_output->matrix<T>().data();
      const T* log_pots_raw = log_pots->matrix<T>().data();
      const Index* labels_raw = NULL;
      if (NULL != labels) {
        labels_raw = labels->vec<Index>().data();
      }
      engine_->inference(log_pots_raw, hex_output_raw, labels_raw);
    }
  }

 private:
  Tensor multiplier_;
  string graph_path_;
  std::shared_ptr<hex::infer<T, Index>> engine_;
  int num_nodes_;
};

#define REGISTER_CPU(T, Index)                                   \
  REGISTER_KERNEL_BUILDER(Name("Hex")                            \
                              .Device(DEVICE_CPU)                \
                              .TypeConstraint<T>("T")            \
                              .TypeConstraint<Index>("Tlabels"), \
                          HexOp<T, Index>);

REGISTER_CPU(float, int32);
REGISTER_CPU(float, int64);
REGISTER_CPU(double, int32);
REGISTER_CPU(double, int64);

}  // namespace tensorflow
