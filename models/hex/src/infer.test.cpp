/**
 * @author
 * @version 2016/06/07
 */

#include "hex/infer.hpp"
#include "hex/test_common.hpp"

using namespace std;
using namespace hex;

namespace {

template <typename TypeParam>
infer<TypeParam> load_test_infer() {
  infer<TypeParam> I(BATCH_SIZE);
  I.init(EXAMPLE_TREE);
  return I;
}
}

template <typename T>
class InferTest : public testing::Test {};

TYPED_TEST_CASE(InferTest, TestDtypes);

TYPED_TEST(InferTest, AssignBelief) {
  auto I = load_test_infer<TypeParam>();
  TypeParam log_pots[BATCH_SIZE * 6];
  Vec<TypeParam> log_pots_vec(log_pots, BATCH_SIZE * 6);
  log_pots_vec.fill_rand();

  I.assign_belief(log_pots);
  // test all unary and binary
  for (node_type i = 0; i < 6; i++) {
    clique_type key = {i};
    if (I.has_clique(key) && I[key].has_state(key)) {
      Vec<TypeParam> this_state(&log_pots[i * BATCH_SIZE], BATCH_SIZE);
      EXPECT_EQ(this_state, I[key][key]);
    }

    for (node_type j = 0; j < 6; j++) {
      clique_type key = {i, j};
      if (i == j) {
        continue;
      }
      if (I.has_clique(key)) {
        assignment key0 = {i};
        if (I[key].has_state(key0)) {
          Vec<TypeParam> this_state(&log_pots[i * BATCH_SIZE], BATCH_SIZE);
          EXPECT_EQ(this_state, I[key][key0]);
        }
        key0 = {j};
        if (I[key].has_state(key0)) {
          Vec<TypeParam> this_state(&log_pots[j * BATCH_SIZE], BATCH_SIZE);
          EXPECT_EQ(this_state, I[key][key0]);
        }
        if (I[key].has_state(key)) {
          Vec<TypeParam> res(BATCH_SIZE);
          res.fill(0);
          Vec<TypeParam> state_i(&log_pots[i * BATCH_SIZE], BATCH_SIZE);
          Vec<TypeParam> state_j(&log_pots[j * BATCH_SIZE], BATCH_SIZE);

          res.binary(state_i, add<TypeParam>);
          res.binary(state_j, add<TypeParam>);
          EXPECT_EQ(res, I[key][key]);
        }
      }
    }
  }
}

TYPED_TEST(InferTest, AssignBeliefClamp) {
  auto I = load_test_infer<TypeParam>();
  TypeParam log_pots[BATCH_SIZE * 6];
  Vec<TypeParam> log_pots_vec(log_pots, BATCH_SIZE * 6);
  log_pots_vec.fill_rand();

  I.assign_belief(log_pots);

  node_type labels[BATCH_SIZE];
  size_t ii = 0;
  for (ii = 0; ii < BATCH_SIZE; ii++) {
    labels[ii] = int(rand01() * 1000000) % 6;
  }
  I.clamp_belief(labels);

  for (ii = 0; ii < BATCH_SIZE; ii++) {
    // test all unary and binary
    size_t i, j, k;
    for (i = 0; i < 6; i++) {
      for (j = 0; j < 6; j++) {
        clique_type key = {i, j};
        if (I.has_clique(key)) {
          for (k = 0; k < I[key].states.size(); k++) {
            if (is_contain(I[key].states[k], labels[ii])) {
              EXPECT_FALSE(ISINF(I[key].prob[k].data[ii]));
            } else {
              EXPECT_TRUE(ISINF(I[key].prob[k].data[ii]) ||
                          !is_contain(I[key].domain, labels[ii]));
            }
          }
        }
      }
    }
  }
}

TYPED_TEST(InferTest, CalibrateSingle) {
  auto I = load_test_infer<TypeParam>();
  TypeParam log_pots[BATCH_SIZE * 6];
  Vec<TypeParam> log_pots_vec(log_pots, BATCH_SIZE * 6);
  log_pots_vec.fill_rand();

  for (int i = 0; i < I.num_edges(); i++) {
    I.assign_belief(log_pots);
    I.pass_message_iter(i, true);
    I.pass_message_iter(i, false);
    ASSERT_TRUE(I.edge_caliberated(i));
  }
}

TYPED_TEST(InferTest, CalibrateTwoEdge) {
  auto I = load_test_infer<TypeParam>();
  TypeParam log_pots[BATCH_SIZE * 6];
  Vec<TypeParam> log_pots_vec(log_pots, BATCH_SIZE * 6);
  log_pots_vec.fill_rand();

  for (int i = 1; i < I.num_edges(); i++) {
    I.assign_belief(log_pots);
    I.pass_message_iter(i, true);
    I.pass_message_iter(i - 1, true);
    I.pass_message_iter(i - 1, false);
    I.pass_message_iter(i, false);
    ASSERT_TRUE(I.edge_caliberated(i));
    ASSERT_TRUE(I.edge_caliberated(i - 1));
  }
}

TYPED_TEST(InferTest, CalibrateFull) {
  auto I = load_test_infer<TypeParam>();
  TypeParam log_pots[BATCH_SIZE * 6];
  Vec<TypeParam> log_pots_vec(log_pots, BATCH_SIZE * 6);
  log_pots_vec.fill_rand();

  I.assign_belief(log_pots);
  I.pass_message();
  EXPECT_TRUE(I.calibrated());

  node_type labels[BATCH_SIZE];
  size_t ii = 0;
  for (ii = 0; ii < BATCH_SIZE; ii++) {
    labels[ii] = int(rand01() * 1000000) % 6;
  }
  I.assign_belief(log_pots);
  I.clamp_belief(labels);
  EXPECT_FALSE(I.calibrated());
  I.pass_message();
  EXPECT_TRUE(I.calibrated());
}

namespace {

template <typename TypeParam>
void test_marginals(const string& graph_path) {
  JTree jtree;
  ReadProtoFromBinaryFile(graph_path.c_str(), &jtree);

  infer<TypeParam> I(BATCH_SIZE);
  ASSERT_TRUE(I.init(graph_path.c_str())) << graph_path << " not found";

  size_t num_nodes = I.num_nodes();
  TypeParam log_pots[BATCH_SIZE * num_nodes];
  size_t j;
  for (j = 0; j < BATCH_SIZE; j++) {
    for (size_t i = 0; i < num_nodes; i++) {
      log_pots[i * BATCH_SIZE + j] = jtree.log_potentials(i);
    }
  }
  TypeParam outputs[I.num_nodes() * BATCH_SIZE];
  I.inference(log_pots, outputs);

  for (j = 0; j < BATCH_SIZE; j++) {
    for (size_t i = 0; i < num_nodes; i++) {
      TypeParam res = std::exp(outputs[i * BATCH_SIZE + j]);
      EXPECT_NEAR(jtree.probabilities(i), res, EPS);
    }
  }
}
}

TYPED_TEST(InferTest, Marginals) { test_marginals<TypeParam>(EXAMPLE_TREE); }

TYPED_TEST(InferTest, MarginalsImageNet) {
  test_marginals<TypeParam>(IMAGENET_GRAPH);
}

#define PERF_GRAPH IMAGENET_GRAPH

TYPED_TEST(InferTest, PerfTestImageNet) {
  const int total = 2560;
  const int batch_size = 256;
  infer<TypeParam> I(batch_size);
  ASSERT_TRUE(I.init(PERF_GRAPH)) << PERF_GRAPH << " not found";

  TypeParam log_pots[batch_size * I.num_nodes()];
  Vec<TypeParam> log_pots_vec(log_pots, batch_size * I.num_nodes());
  log_pots_vec.fill_rand();

  int num_tests = total / batch_size;

  TypeParam prob[I.num_nodes() * batch_size];
  node_type labels[batch_size];
  size_t ii = 0;
  for (ii = 0; ii < batch_size; ii++) {
    labels[ii] = int(rand01() * 1000000) % I.num_nodes();
  }

  const clock_t t0 = clock();
  for (int i = 0; i < num_tests; i++) {
    I.inference(log_pots, prob, labels);
  }
  const clock_t t1 = clock();
  const double elapsedSec =
      (t1 - t0) / (double)CLOCKS_PER_SEC / (num_tests * batch_size);
  LOG(INFO) << elapsedSec << " seconds per iteration";
  EXPECT_LE(elapsedSec, 1.);
}

#if 0
template <typename Dtype>
void compute_softmax(const Dtype *src, Dtype *dst, size_t num, Dtype *grad,
                     size_t label) {
  Dtype _max = *std::max_element(src, src + num);

  _max = std::max(_max, Dtype(0));

  size_t i = 0;
  Dtype _sum = Dtype(1);  // for the all zero state
  for (i = 0; i < num; i++) {
    dst[i] = std::exp(src[i] - _max);
    _sum += dst[i];
  }
  for (i = 0; i < num; i++) {
    dst[i] /= _sum;
    grad[i] = dst[i];
  }

  grad[label] -= 1;
}

template <typename Dtype>
inline Dtype sigmoid(Dtype x) {
  return 1. / (1. + exp(-x));
}

template <typename Dtype>
void compute_ce(const Dtype *src, Dtype *dst, size_t num, Dtype *grad,
                size_t label) {
  size_t i = 0;
  for (i = 0; i < num; i++) {
    dst[i] = sigmoid(src[i]);
    grad[i] = dst[i];
  }

  grad[label] -= 1;
}

namespace {
template <typename TypeParam, typename Op>
void test_special_case(const string &graph_path, const Op &op) {
  infer<TypeParam> I;
  I.init(graph_path.c_str());
  TypeParam log_pots[I.num_nodes()];

  TypeParam prob[I.num_nodes()];
  TypeParam gt_prob[I.num_nodes()];

  TypeParam grads[I.num_nodes()];
  TypeParam gt_grads[I.num_nodes()];

  TypeParam multipler[I.num_nodes()];
  I.get_multiplier(multipler);

  int num_tests = 10;
  for (int i = 0; i < num_tests; i++) {
    for (size_t i = 0; i < I.num_nodes(); i++) {
      log_pots[i] = 10 * rand01() - 20;
    }

    // check forward
    I.inference(log_pots, prob);
    op(log_pots, gt_prob, I.num_nodes(), gt_grads, 0);
    for (size_t j = 0; j < I.num_nodes(); j++) {
      ASSERT_NEAR(std::exp(prob[j]), gt_prob[j], EPS);
    }

    // check backward
    for (size_t k = 0; k < I.num_nodes(); k++) {
      I.inference(log_pots, prob);
      I.inference(log_pots, grads, k);
      op(log_pots, gt_prob, I.num_nodes(), gt_grads, k);
      for (size_t j = 0; j < I.num_nodes(); j++) {
        ASSERT_NEAR((std::exp(prob[j]) - std::exp(grads[j])) * multipler[j],
                    gt_grads[j], EPS);
      }
    }
    // I.inference(log_pots, prob, 0, grads);
  }
}
}

#define EXCLUSION_GRAPH "./graphs/100-complete.graph.pb"
TYPED_TEST(InferTest, TestSoftMax) {
  test_special_case<TypeParam>(EXCLUSION_GRAPH, compute_softmax<TypeParam>);
}

#endif

#if 0
// HEX can handle disconnected cases, but it will requires significant effort to make it happen in my inference code
#define DISJOINT_GRAPH "./graphs/100-disjoint.graph.pb"
TYPED_TEST(InferTest, TestDisjoint) {
  test_special_case<TypeParam>(DISJOINT_GRAPH, compute_ce<TypeParam>);
}
#endif
