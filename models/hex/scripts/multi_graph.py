#!/usr/bin/env python
"""
Code description
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os
import os.path as osp
import gc

from graph2jtree import graph2jtree, hex_pb2
from google.protobuf import text_format
from collections import defaultdict

def load_graph(graph_path):
  graph = hex_pb2.Graph()
  with open(graph_path) as reader:
    data = reader.read()
    graph = text_format.Parse(data, graph)
  return graph

def main():
  graph_path = sys.argv[1]
  graph = load_graph(graph_path)

  graph = hex_pb2.Graph()
  with open(graph_path) as reader:
    data = reader.read()
    graph = text_format.Parse(data, graph)

  save_dir = graph_path + '.pbs'
  try:
    os.mkdir(save_dir)
  except:
    pass

  def _save(msg, name):
    with open(osp.join(save_dir, name + '.pbtxt'), 'w') as writer:
      print(str(msg), file=writer)

    with open(osp.join(save_dir, name + '.pb'), 'wb') as writer:
      writer.write(msg.SerializeToString())

  rtypes = defaultdict(list)
  for relation in graph.subset_relations:
    rtypes[relation.rtype].append((relation.parent, relation.child))

  rtypes = sorted(rtypes.items(), key = lambda x:-len(x[1]))
  num_edges = sum([len(edges) for _, edges in rtypes])
  rtypes_ = []
  so_far = 0
  for rtype in rtypes:
    if so_far/num_edges > .95:
      break
    rtypes_.append(rtype)
    so_far += len(rtype[1])
  rtypes = rtypes_

  rts = set([r[0] for r in rtypes])

  graph_filtered = hex_pb2.Graph()
  graph_filtered.CopyFrom(graph)
  del graph_filtered.subset_relations[:]
  for subset in graph.subset_relations:
    if subset.rtype in rts:
      graph_filtered.subset_relations.add().CopyFrom(subset)
  _save(graph_filtered, 'graph')

  if 'merged' not in rts:
    EDGES = sum([edges for _, edges in rtypes], [])
    rtypes.insert(0, ('merged', EDGES))

  print([(rtype, len(edges)) for rtype, edges in rtypes])
  #rtypes.reverse()
  for rtype, edges in rtypes:
    jtree = graph2jtree(graph.num_labels, edges)
    jtree_pb = hex_pb2.JTree()
    if isinstance(jtree, str):
      print(graph_path, 'invalid rtype:', rtype, 'reason', jtree)
    else:
      jtree_pb.CopyFrom(jtree)
      print(graph_path, rtype, len(jtree.bfs_edges))

    _save(jtree_pb, 'jtree_' + rtype)

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
