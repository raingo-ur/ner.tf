#!/usr/bin/env python
"""
convert the pbtxt graph to parent, child, rtype txt format
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp

from multi_graph import load_graph

def main():
  graph = load_graph(sys.argv[1])
  for edge in graph.subset_relations:
    print(edge.parent, edge.child, edge.rtype)
  pass

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
