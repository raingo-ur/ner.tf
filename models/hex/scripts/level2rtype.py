#!/usr/bin/env python
"""
convert the non rtype graph to graph with rtype, where rtype is the level in the hierachy
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp

from multi_graph import load_graph, hex_pb2
import networkx as nx

def main():
  graph_path = sys.argv[1]
  graph = load_graph(graph_path)

  new_graph = hex_pb2.Graph()
  new_graph.CopyFrom(graph)
  del new_graph.exclusive_cliques[:]
  del new_graph.subset_relations[:]

  nxg = nx.DiGraph()
  for edge in graph.subset_relations:
    nxg.add_edge(edge.child, edge.parent)

  roots = []
  for node, idgree in nxg.out_degree_iter():
    if idgree == 0:
      roots.append(node)
  print('#roots:', len(roots))

  levels = {}
  for leaf in range(graph.num_leaves):
    for node in nx.descendants(nxg, leaf):
      edge = new_graph.subset_relations.add()
      edge.parent = node
      edge.child = leaf
      edge.rtype = 'path-%03d' % nx.shortest_path_length(nxg, leaf, node)

      edge = new_graph.subset_relations.add()
      edge.parent = node
      edge.child = leaf
      if node not in levels:
        for root in roots:
          try:
            level = nx.shortest_path_length(nxg, node, root)
          except:
            continue
          else:
            levels[node] = level
            break
      edge.rtype = 'level-%03d' % levels[node]

  for nxe in nxg.edges():
    edge = new_graph.subset_relations.add()
    edge.parent = nxe[1]
    edge.child = nxe[0]
    edge.rtype = 'merged'

  with open(graph_path + '.level-graph', 'w') as writer:
    print(str(new_graph), file=writer)

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
