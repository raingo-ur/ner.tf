#!/usr/bin/env python
"""
utility functions for graph manipulation
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
this_dir=osp.dirname(__file__)
sys.path.append(osp.join(this_dir, '../build/caffe/proto'))
import hex_pb2

import numpy as np
import random

import networkx as nx
import itertools
HIE_ETYPE = 1 #etype id for hiearchy edges
ECLU_ETYPE = 0 #etype id for exclusive edges

from collections import defaultdict
from pgmpy.models import BayesianModel, JunctionTree
from pgmpy.models import MarkovModel
from pgmpy.factors import Factor
from pgmpy.inference import  BeliefPropagation
from pgmpy.factors.Factor import factor_product
from Ndsparse import Ndsparse
from pgmpy.factors import TabularCPD

class HexGraph(object):
  """docstring for HexGraph"""
  def __init__(self):
    self.hie = nx.DiGraph()
    self.excl = nx.Graph()

  def add_edge(self, edge, etype):
    if etype == HIE_ETYPE:
      self.hie.add_edge(*edge)
    else:
      self.excl.add_edge(*edge)

  def add_nodes_from(self, nodes):
    self.hie.add_nodes_from(nodes)
    self.excl.add_nodes_from(nodes)

  def subgraph(self, nodes):
    res = HexGraph()
    res.hie = self.hie.subgraph(nodes)
    res.excl = self.excl.subgraph(nodes)
    return res

  def nodes(self):
    res0 = self.hie.nodes()
    res1 = self.excl.nodes()
    return set(res0 + res1)

  def number_of_edges(self):
    return self.hie.number_of_edges() + self.excl.number_of_edges()

  def empty(self):
    return len(self.nodes()) == 0

  def pick_node(self):
    if self.hie.number_of_edges() > 0:
      edges = self.hie.edges()
      edge = random.choice(edges)
      return random.choice(edge)
    return self.excl.nodes()[0]

  def copy(self):
    res = HexGraph()
    res.hie = self.hie.copy()
    res.excl = self.excl.copy()
    return res

def _draw_node(graph, node):
  directed = True
  if isinstance(graph, nx.Graph):
    directed = False

  if directed:
    nodes = nx.descendants(graph, node)
  else:
    nodes = set(graph.neighbors(node))
  if len(nodes) < 2:
    return

  nodes.add(node)
  node_graph = graph.subgraph(nodes)
  if directed:
    depths = nx.shortest_path_length(node_graph, node)
    depth = max(depths.items(), key=lambda x:x[1])[1]
    if depth < 2:
      return
  _draw(node_graph, node)

def draw(graph):
  for node in graph:
    _draw_node(graph, node)

def _draw(node_graph, node=None):
  from networkx.drawing.nx_agraph import graphviz_layout
  import matplotlib.pyplot as plt
  if node is None:
    node = max(node_graph.nodes())
  pos = graphviz_layout(node_graph,prog='twopi',root=node)
  nx.draw(node_graph, pos, with_labels=True, arrows=True,node_size=20, alpha=0.5, node_color="blue")
  plt.waitforbuttonpress()

def break_circles(graph):
  circles = list(nx.simple_cycles(graph))
  edges = defaultdict(list)

  for idx, circle in enumerate(circles):
    circle.append(circle[0])
    for i in range(len(circle)-1):
      edges[(circle[i], circle[i+1])].append(idx)

  if len(edges) == 0:
    return

  print('break_circles:', graph.number_of_edges(), len(edges))

  removed = set()
  key = lambda x: -len([i for i in x[1] if i not in removed])
  edges = sorted(edges.items(), key = key)
  while key(edges[0]) < 0:
    graph.remove_edge(*edges[0][0])
    removed |= set(edges[0][1])
    edges = sorted(edges, key = key)

  print('break_circles done:', graph.number_of_edges())

def same_edge(lhs, rhs):
  if lhs[0] == rhs[0] and lhs[1] == rhs[1]:
    return True
  if lhs[0] == rhs[1] and lhs[1] == rhs[0]:
    return True
  return False

def norm_undirected(graph, op):
  for edge in graph.excl.edges():
    descendants = []
    for v in edge:
      if v in graph.hie:
        alpha_v = nx.descendants(graph.hie, v)
      else:
        alpha_v = set()
      alpha_v.add(v)
      descendants.append(alpha_v)

    for new_edge in itertools.product(*descendants):
      if not same_edge(new_edge, edge):
        op(new_edge, ECLU_ETYPE)

def norm_directed(hie, op):
  cache = set()

  roots = []
  leaves = []
  for node, odgree in hie.out_degree_iter():
    if odgree == 0:
      leaves.append(node)

  for node, idgree in hie.in_degree_iter():
    if idgree == 0:
      roots.append(node)

  for root, leave in itertools.product(roots, leaves):
    for path in nx.all_simple_paths(hie, root, leave):
      for i in range(len(path)-1):
        for j in range(i+2,len(path)):
          edge = (path[i], path[j])
          if edge in cache:
            continue
          op(edge, HIE_ETYPE)
          cache.add(edge)
def _check_and_remove(edge, G):
  if G.has_edge(*edge):
    G.remove_edge(*edge)
    return True
  return False

def normalize_graph(graph, ntype):
  """
  normalize graph by sparsity or densify
  ntype = 'sparse' or 'dense'
  return the normalized graph
  """
  print('norm graph:', ntype)
  res = graph.copy()

  def remove_edge(edge):
    if not _check_and_remove(edge, res_):
      edge = (edge[1], edge[0])
      _check_and_remove(edge, res_)

  if ntype == 'sparse':
    op = lambda edge, etype: remove_edge(edge)
  else:
    op = lambda edge, etype: res_.add_edge(*edge, etype=etype)

  res_ = res.excl
  norm_undirected(graph, op)

  res_ = res.hie
  norm_directed(graph.hie, op)

  print('number_of_edges', res.number_of_edges(), ntype)

  return res

def assign_potentials(shape, pot_vec):
  """
  assign potentials according to coordinates
  e.g.

  f(a, b, c):

    | 00   | 01       | 10       | 11
  0 | f    | f(a)     | f(b)     | f(a)f(b)
  1 | f(c) | f(a)f(c) | f(b)f(c) | f(a)f(b)f(c)

  returns the table the last assignment
  the last assignment needs special treatment for different kinds of potentials
  """
  if len(shape) == 1:
    res = np.zeros((2, 1))
    res[0] = 1
    res[1] = pot_vec[0]
    return res, (1,)

  res = np.zeros(shape)
  for i in range(res.size):
    assignment = np.unravel_index(i, shape)
    pots = [pot_vec[ii] for ii, assign_
        in enumerate(assignment) if assign_ == 1]
    res[assignment] = np.prod(pots)
  return res, assignment

def sub_pot_vec(potentials, nodes):
  return [potentials[i] for i in nodes]

def add_potential_hie(hie_mn, potentials, hie_org):
  for edge_ in hie_org.edges():
    #the first element should be the child
    edge = []
    edge.append(edge_[1])
    edge.append(edge_[0])

    pots = sub_pot_vec(potentials, edge)
    values, assignment = assign_potentials((2,2), pots)

    # except the last assignment
    # all last row has to be zero
    if len(assignment) > 1:
      last_pot = values[assignment]
      values[1,] = 0
      values[assignment] = last_pot

    factor = Factor(edge, cardinality=[2,2], values=values)
    hie_mn.add_factors(factor)

def add_potential_excl(excl_mn, potentials):
  for edge in excl_mn.edges():
    pots = sub_pot_vec(potentials, edge)
    value, assignment = assign_potentials((2,2), pots)

    # the all 1 case is zero
    value[assignment] = 0
    value[1,1]=0

    factor = Factor(edge, cardinality=[2,2], values=value)
    excl_mn.add_factors(factor)

def hex2jtree(graph, potentials=None):
  """
  a more efficent algorithm:
  find out the exclusive clique. for each connected components, do the standard hex2jtree, but making sure the root nodes are in the single nodes, and then connect the clique with all roots with the large exclusive_cliques
  """

  cliques = []
  jtrees = []
  for cc in nx.weakly_connected_component_subgraphs(graph.hie):
    if cc.number_of_nodes() == 1:
      cliques.append(cc.nodes()[0])
    else:
      roots = [n for n, degree in cc.in_degree().items() if degree == 0]
      jtree = _hex2jtree(graph.subgraph(cc.nodes()), roots=roots)
      jtrees.append((jtree, roots))

  cliques = tuple(sorted(cliques))
  JTREE = nx.Graph()
  JTREE.add_node(cliques)

  for jtree, roots in jtrees:
    JTREE.add_edges_from(jtree.edges())
    roots = set(roots)
    found = False
    for node in jtree.nodes():
      if len(roots-set(node)) == 0:
        found = True
        break
    if found:
      JTREE.add_edge(node, cliques)
    else:
      raise Exception("roots are not in the same node")
  return JTREE

def _hex2jtree(graph, potentials=None, roots = []):
  """
  convert the graph into junction tree
  the junction tree has the set of nodes as node, and sep node as edge

  pot_in_jtree: whether we should infer the potentials in the junction tree
  if so, means brutal force

  the hiearchical graph is directed and should use BayesianModel,
  because the CPD is not seperable:

  For example,
  f(Q|A,B):

     | 00 | 01   | 10   | 11
   0 | 1  | f(a) | f(b) | f(a)f(b)
   1 | 0  | 0    | 0    | f(a)f(b)f(q)

  f(Q|A)f(Q|B):

      | 00 | 01   | 10   | 11
    0 | 1  | f(a) | f(b) | f(a)f(b)
    1 | 0  | 0    | 0    | f(a)f(b)f(q)f(q)

  Note that the last value is different

  However, because the fact the the paper define the equavlence as the state space, which means iff the non zero positives are the same, they are equivilent.
  not adding the moralize edges to reduce junction tree width

  """

  hie_mn = MarkovModel()
  hie_mn.add_edges_from(graph.hie.edges())
  hie_mn.add_nodes_from(graph.hie.nodes())

  if potentials is not None:
    add_potential_hie(hie_mn, potentials, graph.hie)

  excl_mn = MarkovModel()
  excl_mn.add_edges_from(graph.excl.edges())
  excl_mn.add_nodes_from(graph.excl.nodes())

  if potentials is not None:
    add_potential_excl(excl_mn, potentials)

  merged = excl_mn
  merged.add_edges_from(hie_mn.edges())
  merged.add_nodes_from(hie_mn.nodes())
  merged.add_factors(*hie_mn.factors)

  # make sure the roots are in the same clique
  for edge in itertools.combinations(roots, 2):
    merged.add_edge(*edge)

  order = []
  reduce_G = merged.copy()
  while len(reduce_G) > 0:
    t =  min(reduce_G.degree().items(), key=lambda x:x[1])[0]
    for lhs, rhs in itertools.combinations(nx.neighbors(reduce_G, t), 2):
      reduce_G.add_edge(lhs, rhs)
    reduce_G.remove_node(t)
    order.append(t)

  J1 = merged.to_junction_tree(order=order,
      with_pot=potentials is not None)

  order = sorted(merged.degree().items(), key=lambda x:x[1])
  order = [o[0] for o in order]

  J2 = merged.to_junction_tree(order=order,
      with_pot=potentials is not None)

  def _max_node_size(jtree):
    return len(max(jtree.nodes(), key = lambda x:len(x)))

  return min([J1, J2], key = _max_node_size)


from networkx.algorithms.approximation import ramsey
def clique_removal(G):
  """fixed version of the networkx library"""
  graph = G.copy()
  c_i, i_i = ramsey.ramsey_R2(graph)
  cliques = [c_i]
  isets = [i_i]
  while graph:
    graph.remove_nodes_from(c_i)
    c_i, i_i = ramsey.ramsey_R2(graph)
    if c_i:
      cliques.append(c_i)
    if i_i:
      isets.append(i_i)

  def _max(target):
    return max(target, key=lambda x:len(x))
  return _max(isets), _max(cliques)

def max_clique(graph):
  cnt = 0
  res = []
  max_checks = 10000
  for clique in nx.find_cliques(graph):
    if len(res) < len(clique):
      res = clique
    cnt += 1
    if cnt > max_checks:
      break

  if cnt <= max_checks:
    return res

  try:
    _, clique = clique_removal(graph)
    if len(res) < len(clique):
      res = clique

    cgraph = nx.complement(graph)
    clique, _ = clique_removal(graph)
    if len(res) < len(clique):
      res = clique
  except RuntimeError:
    pass

  return res

def excl_only_states(graph):
  clique = max_clique(graph)

  others = set(graph.nodes()) - set(clique)

  states = []
  states.append(frozenset())
  for node in clique:
    states.append(frozenset([node]))

  num_nodes = graph.number_of_nodes()

  for idx, other in enumerate(others):
    new_states = []
    for state in states:
      excl = False
      for node in state:
        if graph.has_edge(other, node):
          excl = True
      new_states.append(state)
      if not excl:
        state = state | frozenset([other])
        new_states.append(state)
    states = set(new_states)
    num_states = len(states)
    assert num_states / num_nodes < 5000, \
        "the state size is too much %d %d %d" % (
            num_states, num_nodes, len(others)-idx)


  res = []
  for state in states:
    this_state = {}
    for n in graph.nodes():
      this_state[n] = 0

    for n in state:
      this_state[n] = 1

    res.append(this_state)
  return res

_cache = {}
def list_states(graph):
  """
  list all feasible states for the graph
  algorithm 1
  """
  if graph.empty():
    return [{}]

  nodes_ = graph.nodes()
  nodes = tuple(sorted(nodes_))
  if nodes in _cache:
    return _cache[nodes]

  if graph.hie.number_of_edges() == 0:
    return excl_only_states(graph.excl)

  # because graph is densified, it should be safe to just direct predecessors, not ancestors
  # so does with the descendants
  v = graph.pick_node()
  if v in graph.hie:
    alpha_v = graph.hie.predecessors(v)
    delta_v = graph.hie.successors(v)
  else:
    alpha_v = []
    delta_v = []

  if v in graph.excl:
    e_v = graph.excl.neighbors(v)
  else:
    e_v = []

  O_v = set(delta_v + alpha_v + e_v + [v])
  o_v = list(nodes_ - O_v)

  def _combine(s_new, S):
    res = []
    for s in S:
      # if not copy, the cache will scrum
      s = s.copy()
      s.update(s_new)
      res.append(s)
    return res

  V0 = alpha_v + e_v + o_v
  G0 = graph.subgraph(V0)
  S_G0 = list_states(G0)
  s0 = {d:0 for d in delta_v}
  s0[v] = 0
  S_G0 = _combine(s0, S_G0)

  V1 = delta_v + o_v
  G1 = graph.subgraph(V1)
  S_G1 = list_states(G1)
  s1 = {d:0 for d in e_v}
  s1.update({a:1 for a in alpha_v})
  s1[v] = 1
  S_G1 = _combine(s1, S_G1)

  states = S_G0 + S_G1

  # verify
  for state in states:
    assert tuple(sorted(state.keys())) == nodes, \
        'invalid state'

  _cache[nodes] = states
  return states

def dump_graph(jtree, states, log_potentials=None, res=None):

  nodes = jtree.nodes()
  root = nodes[random.choice(range(len(nodes)))]
  bfs_edges = nx.bfs_edges(jtree, root)
  edges = list(bfs_edges)

  def _sep(u, v):
    return set(u) & set(v)

  edges_ = edges
  edges = [(u, v,_sep(u, v)) for u, v in edges]
  nodes = [(f, states[f]) for f in jtree.nodes()]

  variables = set(sum(jtree.nodes(), ()))
  num_cliques = [0]*len(variables)
  for clique in jtree.nodes():
    for var in clique:
      num_cliques[var] += 1

  if res is None:
    res = hex_pb2.Jtree()

  def _assign_nodes(pb_nodes, int_nodes):
    for n in int_nodes:
      pb_nodes.append(n)

  res.num_cliques.extend(num_cliques)

  for edge in edges:
    _edge = res.bfs_edges.add()
    _assign_nodes(_edge.source_nodes, edge[0])
    _assign_nodes(_edge.dst_nodes, edge[1])
    _assign_nodes(_edge.sepset , edge[2])

  for clique, _states in nodes:
    _clique = res.cliques.add()
    _assign_nodes(_clique.nodes, clique)
    for state in _states:
      _state = _clique.states.add()
      _nodes = []
      for n, v in state.items():
        if v == 1:
          _nodes.append(n)
      _assign_nodes(_state.nodes, _nodes)

  # info to build marginals for each node
  # use the smallest clique if possible
  # idealy, use sepset is even more efficent
  # but not all variables in sepset
  marginal_table_ = defaultdict(list)
  for clique in jtree.nodes():
    for node in clique:
      others = set(clique)
      others.remove(node)
      others = tuple(sorted(others))
      marginal_table_[node].append((clique, others, len(clique)))

  marginal_table = [None]*len(marginal_table_)
  for node, cliques in marginal_table_.items():
    clique = sorted(cliques, key=lambda x:x[-1])[0]
    marginal_table[node] = (clique[0], clique[1])

  for node, (clique, others) in enumerate(marginal_table):
    _mt = res.marginal_info.add()
    _mt.node = node;
    _assign_nodes(_mt.clique, clique)
    _assign_nodes(_mt.others, others)

    # dump marginals to file to compare against dlib results
  if log_potentials is not None and len(sys.argv) > 2:
    print("log_potentials")
    res.log_potentials.extend(log_potentials.tolist())

    import time
    start_time = time.time()
    clique_beliefs = hex_inference(jtree, states,
        log_potentials)
    # dump individual marginals to compare with dlib
    marginals_gt = [None]*len(marginal_table)
    for node, (clique, others) in enumerate(marginal_table):
      belief = clique_beliefs[clique]
      factor = belief.marginalize(others,
          inplace=False).normalize(inplace=False)
      marginals_gt[node] = factor.values.to_numpy()[1]
    print("elapsed_time", time.time() - start_time)

    res.probabilities.extend(marginals_gt)

  return res

def build_jtree(graph, potentials=None):
  # build junction tree
  jtree = hex2jtree(graph, potentials)

  if potentials is not None:
    inference = BeliefPropagation(jtree)
    inference.calibrate()
    joint_table = factor_product(*inference.clique_beliefs.values())
    return joint_table
  else:
    return jtree

def list_all_states(jtree, graph):
  STATES = {}
  for node in jtree.nodes():
    subg = graph.subgraph(node)
    states = list_states(subg)
    STATES[node] = states
  _cache.clear()

  return STATES

def transform_graph(graph):
  # sparsify and densify
  sparse_graph = normalize_graph(graph, 'sparse')
  densify_graph = normalize_graph(sparse_graph, 'dense')

  return sparse_graph, densify_graph

def hex_inference(jtree, STATES, log_potentials, with_np = 0):
  """
  online message passing, should have counterpart in dlib

  if with_np == 0:
    use Ndsparse and return clique_beliefs in log space
  elif with_np == 1:
    use numpy and return inference.query and joint_table
  elif with_np == 2:
    use Ndsparse and return inference.query and joint_table
  """
  jtree = JunctionTree(jtree.edges())
  # jtree.remove_factors(*jtree.get_factors())

  for clique in jtree.nodes():
    states = STATES[clique]
    var_card = [2 for _ in clique]
    n2i = {n:i for i, n in enumerate(clique)}
    values = {}
    for state in states:
      key = [0]*len(clique)
      value = 0
      for n, s in state.items():
        key[n2i[n]] = s
        if s == 1:
          value += log_potentials[n]
      values[tuple(key)] = value
    values = Ndsparse(values, var_card)
    if with_np == 1:
      # use numpy in the message passing
      values = values.to_numpy()
    jtree.add_factors(Factor(clique, var_card, values=values))
  inference = BeliefPropagation(jtree)
  inference.calibrate()
  if with_np == 0:
    return inference.clique_beliefs

  res = {}
  nodes = [jtree.nodes()[0][0]]
  for factor in inference.query(nodes).values():
    values = factor.values
    if with_np == 2:
      values = values.to_numpy()
    res[tuple(factor.variables)] = values

  joint_table = factor_product(*inference.clique_beliefs.values())
  if with_np == 2:
    joint_table = Factor(joint_table.variables,
        joint_table.cardinality, joint_table.values.to_numpy())
  return res, joint_table

def compare_nnz(lhs, rhs):
  lhs.reorder_axes(rhs.variables)
  try:
    return np.allclose(np.nonzero(lhs.values), np.nonzero(rhs.values))
  except:
    # share mismatch in the nnz
    return False

def sanity_check(graphs, jtree, STATES, log_potentials):

  # ndspace should be equal to numpy
  marginals_numpy, jt_numpy = hex_inference(jtree, STATES,
      log_potentials, with_np = 1)
  marginals_ndsparse, jt_ndsparse = hex_inference(jtree, STATES,
      log_potentials, with_np = 2)

  assert all([np.allclose(marginals_numpy[k], marginals_ndsparse[k])
    for k in marginals_numpy])
  assert np.allclose(jt_numpy.values, jt_ndsparse.values)

  # check equavlent in terms of state space
  joint_tables = [jt_numpy, jt_ndsparse]
  for graph in graphs:
    joint_tables.append(build_jtree(graph, np.exp(log_potentials)))

  equals = []
  for idx, (lhs, rhs) in enumerate(itertools.combinations(joint_tables, 2)):
    equals.append(compare_nnz(lhs, rhs))
    if not equals[-1]:
      print(idx)
  assert(all(equals))

def dump_hex_graph(G):
  graph = hex_pb2.Graph()
  graph.num_labels = len(G.nodes())

  for edge in G.hie.edges_iter():
    edge_ = graph.subset_relations.add()
    edge_.parent = edge[0]
    edge_.child = edge[1]

  for edge in G.excl.edges_iter():
    edge_ = graph.exclusive_cliques.add()
    edge_.labels.append(edge[0])
    edge_.labels.append(edge[1])

  return graph

def load_graph(num_nodes, edges):

  nxg = nx.DiGraph()
  nxg.add_nodes_from(range(num_nodes))
  nxg.add_edges_from(edges)
  break_circles(nxg)

  G = HexGraph()
  G.add_nodes_from(nxg.nodes())

  for edge in nxg.edges():
    G.add_edge(edge, HIE_ETYPE)

  import itertools
  for edge in itertools.combinations(nxg.nodes(), 2):
    lhs = nx.descendants(nxg, edge[0])
    lhs.add(edge[0])
    rhs = nx.descendants(nxg, edge[1])
    rhs.add(edge[1])
    if len(lhs & rhs) == 0:
      G.add_edge(edge, ECLU_ETYPE)

  print("load_graph:", G.number_of_edges())

  return G

def graph2jtree(num_nodes, edges):
  graph = load_graph(num_nodes, edges)
  res = hex_pb2.JTree()

  # initialize individual belief
  small_tree = len(graph.nodes()) < 10
  log_potentials = np.random.randn(len(graph.nodes()))

  # sparsify and densify
  sparse_graph, densify_graph = transform_graph(graph)

  def _add_graph_info(graph, name):
    graph_info = res.graph_infos.add()
    graph_info.name = name
    graph_info.num_nodes = len(graph.nodes())
    graph_info.num_edges = graph.number_of_edges()

    nodes = list(graph.nodes())

    if hasattr(nodes[0], '__len__'):
      node_sizes = [len(n) for n in graph.nodes()]
      graph_info.max_node_size = max(node_sizes)
      graph_info.mean_node_size = sum(node_sizes)/len(node_sizes)

    return graph_info

  _add_graph_info(graph, 'original')
  _add_graph_info(sparse_graph, 'sparse')
  _add_graph_info(densify_graph, 'dense')

  # build jtree
  print("build_jtree")
  jtree = build_jtree(sparse_graph)

  info = _add_graph_info(jtree, 'jtree')

  print("list_all_states: ", info.max_node_size)
  try:
    STATES = list_all_states(jtree, densify_graph)
  except AssertionError as ioe:
    return str(ioe)

  _SG = nx.Graph()
  for _, states in STATES.items():
    states = tuple([tuple(f.items()) for f in states])
    _SG.add_node(states)
  print(len(STATES), len(_SG.nodes()))
  info = _add_graph_info(_SG, 'states')
  complexity = info.mean_node_size*info.num_nodes/num_nodes
  print("complexity: %.2f" % complexity)
  if complexity > 500:
    # the average complexity compared with softmax
    return 'intractable complexity: %.2f' % complexity

  if small_tree and False:
    graphs = [sparse_graph, densify_graph, graph]
    sanity_check(graphs, jtree, STATES, log_potentials)

  print("dump_graph")
  dump_graph(jtree, STATES, log_potentials, res = res)
  return res

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
