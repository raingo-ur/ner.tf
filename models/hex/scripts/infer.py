#!/usr/bin/env python
"""
interface for the hex layer, including tests

@@hex_infer
@@hex_loss
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp

this_dir = osp.dirname(osp.realpath(__file__))
import tensorflow as tf

#_hex_module = tf.load_op_library(osp.join(this_dir, '../build/ops.tf.so'))

sys.path.append(osp.join(this_dir, '../build/caffe/proto'))
import hex_pb2
from tensorflow.python.framework import tensor_shape

def hex_infer(logits, graph_path, name=None):
  """
  logits: 2D float32 or float64 `batch_size x num_labels`
  graph_path: path to the graph.pb file

  `num_labels` should be consistent the number of nodes in the graph file

  returns the marginal probabilities
  """
  with tf.name_scope(name, "hex", [logits]) as name:
    logits = tf.convert_to_tensor(logits, name="logits")
    batch_size = logits.get_shape()[0]
    labels = tf.constant(-1, dtype=tf.int64, name="fake_labels")
    logits = tf.transpose(logits);
    res, unused = _hex_module.hex(logits, labels, graph_path, batch_size)
    res = tf.transpose(res)
  return res

def hex_loss(logits, labels, graph_path, name=None):
  """
  logits: 2D float32 or float64 `batch_size x num_labels`
  labels: 1D int32 or int64 `batch_size`
  graph_path: path to the graph.pb file

  returns the loss value (negative log liklihood)
  """
  _, num_nodes = load_jtree(graph_path)
  with tf.name_scope(name, "hex_loss", [logits]) as name:
    labels = tf.one_hot(labels, depth=num_nodes, dtype=logits.dtype)

    log_prob = hex_infer(logits, graph_path=graph_path)
    neg_log_prob = -1 * log_prob
    loss = tf.reduce_sum(labels * neg_log_prob, 1)
    return loss

def _tell(name):
  return tf.get_default_graph().get_tensor_by_name(name).eval()

@tf.RegisterShape("Hex")
def _hex_shape(op):
  """Shape function for the Hex op.
  """

  logits_shape = op.inputs[0].get_shape()
  input_shape = logits_shape.with_rank(2)
  #batch_size = input_shape[1]
  num_labels = input_shape[0]

  # labels_shape
  # op.inputs[1].get_shape().merge_with(tensor_shape.vector(batch_size))
  return [input_shape, tensor_shape.vector(num_labels.value)]

@tf.RegisterGradient("Hex")
def _hex_grad(op, grad, _):
  """The gradients for `hex_infer`.

  Returns
    Gradients with respect to the input of `hex`.
  """
  logits = op.inputs[0]
  num_labels = logits.get_shape()[0]
  log_prob = op.outputs[0]
  graph_path = op.get_attr('graph_path')
  batch_size = op.get_attr('batch_size')

  # use one element from grad for each instance
  # dim x batch_size
  grad_abs = tf.abs(grad)
  labels = tf.argmax(grad_abs, 0)
  labels_onehot = tf.one_hot(labels, depth=num_labels, dtype=logits.dtype)
  grad_masked = grad * tf.transpose(labels_onehot)
  grad_0 = tf.expand_dims(tf.reduce_sum(grad_masked, 0), 0)

  clamped, multiplier = _hex_module.hex(logits, labels, graph_path, batch_size)
  multiplier = tf.expand_dims(multiplier, 1)
  grads = tf.exp(clamped) - tf.exp(log_prob)
  grads *= multiplier
  grads *= grad_0
  return [grads, None]

def load_jtree(graph_path):
  jtree = hex_pb2.JTree()
  with open(graph_path) as reader:
    jtree.ParseFromString(reader.read())
  return jtree, len(jtree.marginal_info)

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
