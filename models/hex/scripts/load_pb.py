#!/usr/bin/env python
"""
Code description
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp

import sys
from utils import hex_pb2, _draw, nx

def pbg2nxg(graph):
  res = nx.DiGraph()
  edges = [(info.parent,info.child) for info in graph.subset_relations]
  res.add_edges_from(edges)
  return res

def load_graph(path):
  with open(path) as reader:
    data = reader.read()
    graph = hex_pb2.JTree()
    graph.ParseFromString(data)
  return graph

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
