#ifndef HEADER_MAPPER
#define HEADER_MAPPER

#include "common.hpp"
#include "belief.hpp"

using std::vector;

namespace hex {

template <typename TypeParam>
TypeParam add_exp_minus(TypeParam x0, TypeParam x1, TypeParam x2) {
  x1 = minus(x1, x2);
  return x0 + exp(x1);
}

template <typename TypeParam>
TypeParam log_add(TypeParam x1, TypeParam x2) {
  INF_GUARD(x1);
  return log(x1) + x2;
}

// logsumexp of container
template <typename TypeParam>
void logsumexp(const vector<const Vec<TypeParam>*>& src, Vec<TypeParam>& dst,
               Vec<TypeParam>& max_) {
  size_t j;
  max_ = *src[0];
  for (j = 1; j < src.size(); j++) {
    max_.binary(*src[j], max<TypeParam>);
  }
  dst.fill(0);
  for (j = 0; j < src.size(); j++) {
    dst.trinary(*src[j], max_, add_exp_minus<TypeParam>);
  }
  dst.binary(max_, log_add<TypeParam>);
}

template <typename Dtype>
class Mapper {
  vector<vector<size_t>> _map;
  vector<vector<const Vec<Dtype>*>> _tmp;

 public:
  void resize(size_t s) { _map.resize(s); }
  void set(size_t i, size_t j) { _map[i].push_back(j); }

  void freeze() {
    _tmp.resize(_map.size());
    for (size_t i = 0; i < _map.size(); i++) {
      _tmp[i].resize(_map[i].size());
    }
  }

  friend std::ostream& operator<<(std::ostream& o, const Mapper& a) {
    size_t i, j;
    o << " --mapper-- " << std::endl;
    for (i = 0; i < a._map.size(); i++) {
      o << i << ":";
      for (j = 0; j < a._map[i].size(); j++) {
        o << a._map[i][j] << " ";
      }
      o << std::endl;
    }
    o << " --mapper end-- ";
    return o;
  }

  void map(const Belief<Dtype>& from, Belief<Dtype>& to) {
    typedef Dtype TypeParam;
    size_t i, j;

    for (i = 0; i < _map.size(); i++) {
      if (_map[i].size() == 0) {
        to[i].fill(NEG_INF);
      } else {
        for (j = 0; j < _map[i].size(); j++) {
          _tmp[i][j] = &from[_map[i][j]];
        }
        logsumexp(_tmp[i], to[i], *to._tmp);
      }
    }
  }
};
}

#endif
