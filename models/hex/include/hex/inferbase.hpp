#ifndef HEADER_INFERBASE
#define HEADER_INFERBASE

#include "common.hpp"

namespace hex {
template <typename Dtype>
class InferBase {
 public:
  virtual ~InferBase() {}
  virtual void inference(const Dtype* log_pots, Dtype* prob,
                         node_type label = 0) = 0;
  virtual void get_multiplier(Dtype*) = 0;
  virtual size_t num_nodes() const = 0;

 protected:
  virtual void init_from_pb(const JTree& _htree) = 0;
};
}

#endif
