#ifndef HEADER_TEST_COMMON
#define HEADER_TEST_COMMON

#include <gtest/gtest.h>
#include "hex/common.hpp"
typedef ::testing::Types<float, double> TestDtypes;

// http://stackoverflow.com/a/37731351/2243015
#define ASSERT_FLOAT_NE(val1, val2)                                          \
  ASSERT_PRED_FORMAT2(!::testing::internal::CmpHelperFloatingPointEQ<float>, \
                      val1, val2)

#define ASSERT_DOUBLE_NE(val1, val2)                                          \
  ASSERT_PRED_FORMAT2(!::testing::internal::CmpHelperFloatingPointEQ<double>, \
                      val1, val2)

#define BATCH_SIZE 10

#endif
