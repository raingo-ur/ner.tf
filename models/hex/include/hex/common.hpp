#ifndef HEADER_COMMON
#define HEADER_COMMON

#include "caffe/proto/hex.pb.h"
#include <boost/unordered_map.hpp>
#include <cmath>
#include <cstdlib>
#include <cstdlib>
#include <fstream>
#include <glog/logging.h>
#include <map>
#include <numeric>
#include <set>

// using namespace std;
// using namespace google::protobuf;

namespace hex {
#define NEG_INF -std::numeric_limits<TypeParam>::infinity()
#define POS_INF std::numeric_limits<TypeParam>::infinity()
#define EPS (1e-4)
template <typename T>
inline bool ISINF(const T& x) {
  return std::isinf(x);
}
template <typename T1, typename T2>
inline bool ISINF(const T1& x, const T2& y) {
  return std::isinf(x) || std::isinf(y);
}
#define ISNAN(x) std::isnan(x)
#define EXAMPLE_TREE "./graphs/fig3.graph.pb"
#define IMAGENET_GRAPH "./graphs/imagenet_1k_extra.graph.pb"

#define DEFINE_SET_OP(op)                                   \
  template <typename T>                                     \
  T _##op(const T& lhs, const T& rhs) {                     \
    T res;                                                  \
    std::op(lhs.begin(), lhs.end(), rhs.begin(), rhs.end(), \
            std::inserter(res, res.begin()));               \
    return res;                                             \
  }

#define INF_GUARD(x)  \
  do {                \
    if (ISINF(x)) {   \
      return NEG_INF; \
    }                 \
  } while (0)

#define INF_GUARD2(x1, x2)        \
  do {                            \
    if (ISINF(x1) && ISINF(x2)) { \
      return NEG_INF;             \
    }                             \
  } while (0)

DEFINE_SET_OP(set_intersection);
DEFINE_SET_OP(set_union);

typedef size_t node_type;

// this is to make the ostream work
// comment of http://stackoverflow.com/a/18817428/2243015
class clique_type : public std::set<node_type> {
  // inherit the ctors
  using std::set<node_type>::set;
};
// typedef std::set<node_type> clique_type;
typedef clique_type assignment;

template <typename D>
using table_type = std::map<clique_type, D>;
// using table_type = boost::unordered_map<clique_type, D>;

template <typename C, typename V>
inline bool is_contain(const C& i, const V& t) {
  return i.find(t) != i.end();
}

// name copy from caffe
// in order to make this library independent from caffe
inline bool ReadProtoFromBinaryFile(const char* filename,
                                    google::protobuf::Message* proto) {
  std::ifstream in(filename);
  if (!in.is_open()) {
    // LOG(WARNING) << "Graph: " << filename << " does not exisits";
    return false;
  }
  return proto->ParseFromIstream(&in);
}

template <typename Container>
void assign_clique(const Container& c, clique_type& clique) {
  for (typename Container::const_iterator it = c.begin(); it < c.end(); it++) {
    clique.insert(*it);
  }
}

template <typename TypeParam>
TypeParam max(TypeParam x1, TypeParam x2) {
  return std::max(x1, x2);
}

template <typename TypeParam>
TypeParam logsumexp2(TypeParam a, TypeParam b) {
  TypeParam m = max(a, b);
  INF_GUARD(m);
  return log(exp(a - m) + exp(b - m)) + m;
}

inline double rand01() { return (double)std::rand() / RAND_MAX; }

inline bool is_subset(const clique_type& lhs, const clique_type& rhs) {
  // check if lhs \subset rhs
  return _set_intersection(lhs, rhs).size() == lhs.size();
}

inline std::ostream& operator<<(std::ostream& o, const clique_type& a) {
  o << "{";
  bool first = true;
  for (const auto& elem : a) {
    if (!first) {
      o << " ";
    }
    first = false;
    o << elem;
  }
  o << "}";
  return o;
}

}

#endif
