#ifndef HEADER_BELIEF
#define HEADER_BELIEF

#include "common.hpp"
using std::vector;

namespace hex {

template <typename TypeParam>
TypeParam add(TypeParam x1, TypeParam x2) {
  return x1 + x2;
}
template <typename TypeParam>
TypeParam minus(TypeParam x1, TypeParam x2) {
  INF_GUARD2(x1, x2);
  return x1 - x2;
}

template <typename TypeParam>
TypeParam iminus(TypeParam x1, TypeParam x2) {
  INF_GUARD2(x1, x2);
  return x2 - x1;
}

template <typename Dtype>
struct Vec {
  typedef Dtype TypeParam;
  Dtype* data;
  size_t _len;
  bool _owner;

  Vec(size_t len) : _len(len) {
    data = new Dtype[_len];
    memset(data, 0, _len * sizeof(Dtype));
    _owner = true;
  }

  Vec(Dtype* data_, size_t len) {
    data = data_;
    _len = len;
    _owner = false;
  }

  Vec(const Vec& src) {
    _len = src._len;
    data = new Dtype[_len];
    memcpy(data, src.data, _len * sizeof(Dtype));
  }

  Vec& operator=(const Vec& src) {
    DCHECK_EQ(_len, src._len) << "can only assignment with same length";
    size_t i;
    for (i = 0; i < _len; i++) {
      data[i] = src.data[i];
    }
    return (*this);
  }

  void fill_rand() {
    size_t i;
    for (i = 0; i < _len; i++) {
      data[i] = rand01();
    }
  }

  void fill(Dtype val) { std::fill(data, data + _len, val); }

  template <typename T, typename Op>
  void binary(const T& target, const Op& op) {
    // T maybe Vec<const TypeParam>
    DCHECK_EQ(target._len, _len) << "length mismatch";
    size_t i;
    for (i = 0; i < _len; i++) {
      data[i] = op(data[i], target.data[i]);
    }
  }

  template <typename Op>
  void trinary(const Vec& opd0, const Vec& opd1, const Op& op) {
    DCHECK_EQ(opd0._len, _len) << "length mismatch";
    DCHECK_EQ(opd1._len, _len) << "length mismatch";
    size_t i;
    for (i = 0; i < _len; i++) {
      data[i] = op(data[i], opd0.data[i], opd1.data[i]);
    }
  }

  friend std::ostream& operator<<(std::ostream& o, const Vec& a) {
    size_t i;
    for (i = 0; i < a._len; i++) {
      o << a.data[i] << " ";
    }
    return o;
  }

  friend bool operator==(const Vec& lhs, const Vec& rhs) {
    if (lhs._len != rhs._len) {
      return false;
    }
    size_t i;
    for (i = 0; i < lhs._len; i++) {
      if (fabs(lhs.data[i] - rhs.data[i]) > EPS) {
        return false;
      }
    }
    return true;
  }
  friend bool operator!=(const Vec& lhs, const Vec& rhs) {
    return !(lhs == rhs);
  }
  void swap(Vec& target) {
    DCHECK(_owner) << "can't swap out delegate vec";
    std::swap(target._len, _len);
    std::swap(target.data, data);
  }

  ~Vec() {
    if (_owner) {
      delete[] data;
    }
  }
};

// BeliefType contains infor about a probability table
template <typename Dtype>
struct Belief {
  typedef Dtype TypeParam;

  vector<Vec<Dtype>> prob;
  std::shared_ptr<Vec<Dtype>> _tmp;
  // the same state is ordered continously
  clique_type domain;
  vector<assignment> states;

  size_t _num_states;

  Belief() {}

  void init_prob(size_t batch_size) {
    _num_states = states.size();
    prob.clear();
    for (size_t i = 0; i < _num_states; i++) {
      prob.push_back(Vec<Dtype>(batch_size));
    }
    _tmp.reset(new Vec<Dtype>(batch_size));
  }

  int _query_state(const assignment& target) const {
    for (size_t i = 0; i < states.size(); i++) {
      if (states[i] == target) {
        return i;
      }
    }
    return -1;
  }

  bool has_state(const assignment& target) const {
    return _query_state(target) >= 0;
  }

  Vec<Dtype>& operator[](int sid) { return prob[sid]; }
  const Vec<Dtype>& operator[](int sid) const { return prob[sid]; }

  Vec<Dtype>& operator[](const assignment& target) {
    DCHECK_GE(_query_state(target), 0)
        << "invalid state, check by has_state first";
    return (*this)[_query_state(target)];
  }

  const Vec<Dtype>& operator[](const assignment& target) const {
    DCHECK_GE(_query_state(target), 0)
        << "invalid state, check by has_state first";
    return (*this)[_query_state(target)];
  }

  friend std::ostream& operator<<(std::ostream& o, const Belief& a) {
    o << " --belief -- " << std::endl;
    o << a.domain << std::endl;
    for (size_t i = 0; i < a.states.size(); i++) {
      o << a.states[i] << ":" << a.prob[i] << std::endl;
    }
    o << " --belief end-- ";
    return o;
  }

  friend bool operator==(const Belief& lhs, const Belief& rhs) {
    if (lhs.domain != rhs.domain) {
      return false;
    }

    if (lhs.states.size() != rhs.states.size()) {
      return false;
    }

    for (size_t j = 0; j < rhs.states.size(); j++) {
      if (lhs.states[j] != rhs.states[j] || lhs.prob[j] != rhs.prob[j]) {
        return false;
      }
    }
    return true;
  }

  template <typename Op>
  void binary(const Belief& target, const Op& op) {
    DCHECK_EQ(target.domain, domain) << "Domain mismatch";
    size_t i;
    for (i = 0; i < states.size(); i++) {
      DCHECK_EQ(states[i], target.states[i]) << "State mismatch";
      prob[i].binary(target.prob[i], op);
    }
  }

  void product(const Belief& target) { binary(target, add<Dtype>); }
  void divide(const Belief& target) { binary(target, minus<Dtype>); }
  void idivide(const Belief& target) { binary(target, iminus<Dtype>); }

  void swap(Belief& target) {
    DCHECK_EQ(target.domain, domain) << "only swap with same domain belief";
    states.swap(target.states);
    prob.swap(target.prob);
    _tmp.swap(target._tmp);
  }

  // try to add state,
  // ignore, if already exist
  // return if actually added
  bool add_state(const assignment& target) {
    assignment target_ = _set_intersection(target, domain);
    if (has_state(target_)) {
      return false;
    } else {
      states.push_back(target_);
      return true;
    }
  }
};
}
#endif
