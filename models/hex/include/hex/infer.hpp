#ifndef HEADER_INFER
#define HEADER_INFER

#include "belief.hpp"
#include "common.hpp"
#include "inferbase.hpp"
#include "mapper.hpp"

using std::vector;

namespace hex {

template <typename Dtype, typename LType = node_type>
class infer {
  typedef Dtype TypeParam;
  typedef Belief<Dtype> Belief_;
  typedef Mapper<Dtype> Mapper_;

 protected:
  void init_from_pb(const JTree& _htree) { _init_belief(_htree); }

 public:
  infer(size_t batch_size) : _batch_size(batch_size), Z(_batch_size) {}

  void assign_belief(const Dtype* log_pots) {
    size_t i;
    for (auto& elem : clique_belief) {
      // first pass for the actual values
      for (i = 0; i < elem.states.size(); i++) {
        // for each state in the clique
        elem[i].fill(0);
        for (const node_type& node : elem.states[i]) {
          // log_pots is of shape dim x batch
          Vec<const Dtype> _pots_vec(&log_pots[node * _batch_size],
                                     _batch_size);
          elem[i].binary(_pots_vec, add<Dtype>);
        }
      }
    }
  }

  // clamp means any state with state[nodeid]==0 are removed
  // removed means log-pot = -inf
  void clamp_belief(const LType* nodeid) {
    size_t i, j;
    for (auto& elem : clique_belief) {
      for (j = 0; j < _batch_size; j++) {
        if (!is_contain(elem.domain, nodeid[j])) {
          continue;
        }
        for (i = 0; i < elem.states.size(); i++) {
          if (!is_contain(elem.states[i], nodeid[j])) {
            elem[i].data[j] = NEG_INF;
          }
        }
      }
    }
  }

  inline void pass_message_iter(int edgeid, bool reverse) {
    const auto& edge = edges[edgeid];

    node_type from = edge.first;
    node_type to = edge.second;

    size_t m_id_from = 0;
    size_t m_id_to = 2;

    if (reverse) {
      std::swap(from, to);
      std::swap(m_id_from, m_id_to);
    }

    auto& c2e = mappers[edgeid][m_id_from + 1];
    auto& e2c = mappers[edgeid][m_id_to];

    c2e.map(clique_belief[from], sepset_belief_tmp[edgeid]);
    // sepset_belief: Old
    // tmp: New

    sepset_belief_tmp[edgeid].swap(sepset_belief[edgeid]);
    // sepset_belief: New
    // tmp: Old

    if (!reverse) {
      sepset_belief_tmp[edgeid].idivide(sepset_belief[edgeid]);
      // tmp = New - Old
      e2c.map(sepset_belief_tmp[edgeid], clique_belief_tmp[to]);
      // C = C * (New - Old)
    } else {
      e2c.map(sepset_belief[edgeid], clique_belief_tmp[to]);
      // clique_belief[to].product();
      // C = C * New
    }
    clique_belief[to].product(clique_belief_tmp[to]);
  }

  bool edge_caliberated(int edgeid) {
    const auto& edge = edges[edgeid];
    const auto& from = edge.first;
    const auto& to = edge.second;

    Belief_ M1 = sepset_belief[edgeid];
    Belief_ M2 = sepset_belief[edgeid];

    mappers[edgeid][1].map(clique_belief[from], M1);
    mappers[edgeid][3].map(clique_belief[to], M2);

    return M1 == M2 && M1 == sepset_belief[edgeid];
  }

  // message passing
  void pass_message() {
    int num_edges = edges.size();

    // upward
    for (int i = 0; i < num_edges; i++) {
      pass_message_iter(num_edges - i - 1, true);
    }

    // download
    for (int i = 0; i < num_edges; i++) {
      pass_message_iter(i, false);
    }
  }

  bool calibrated() {
    int num_edges = edges.size();
    for (int i = 0; i < num_edges; i++) {
      if (!edge_caliberated(i)) {
        return false;
      }
    }
    return true;
  }

  // marginalize
  // return Z value
  // to unnormalized marginalizes sits in prob
  // assume clique_belief and sepset_belief are calibrated
  void marginalize(Dtype* prob) {
    bool first = true;

    for (size_t i = 0; i < num_nodes(); i++) {
      const Belief_& belief = clique_belief[margin_table[i]];
      Vec<Dtype> res(&prob[i * _batch_size], _batch_size);

      // marginalize to the variable
      margin_mapper[i].map(belief, margin_res[i]);

      // fetch unnormalized p[i]
      res = margin_res[i][1];

      if (first) {
        // everyone share the Z value, because the graph is connected
        // if not connected, it's not possible to calibrate
        Z = margin_res[i][0];
        Z.binary(margin_res[i][1], logsumexp2<Dtype>);
        first = false;
      }

      // normalize p[i]
      res.binary(Z, hex::minus<Dtype>);
    }
  }

 private:
  void _init_clique(const JTree& htree) {
    int i, j;
    // step 1: assign clique_belief and clique2idx
    for (i = 0; i < htree.cliques_size(); i++) {
      Clique clique = htree.cliques(i);
      Belief_ belief;
      assign_clique(clique.nodes(), belief.domain);

      for (j = 0; j < clique.states_size(); j++) {
        assignment a;
        assign_clique(clique.states(j).nodes(), a);
        belief.states.push_back(a);
      }
      belief.init_prob(_batch_size);

      clique_belief.push_back(belief);
      clique_belief_tmp.push_back(belief);
      clique2idx[belief.domain] = i;
    }
  }

  void _add_edge_states(Belief_& edge_belief, const clique_type& clique) {
    for (const auto& elem : clique_belief[clique2idx[clique]].states) {
      edge_belief.add_state(elem);
    }
  }

  void _init_edges(const JTree& htree) {
    // step 2: initialize edge information
    // initialize sepset table
    for (int i = 0; i < htree.bfs_edges_size(); i++) {
      // S1: retrieve basic information. cliques and sepset
      Edge edge = htree.bfs_edges(i);
      clique_type forward_c, backward_c;

      Belief_ edge_belief;

      assign_clique(edge.sepset(), edge_belief.domain);
      assign_clique(edge.source_nodes(), forward_c);
      assign_clique(edge.dst_nodes(), backward_c);

      // S2: compute states of the edge
      // which is the union of both sides
      _add_edge_states(edge_belief, forward_c);
      _add_edge_states(edge_belief, backward_c);

      edge_belief.init_prob(_batch_size);
      sepset_belief.push_back(edge_belief);
      sepset_belief_tmp.push_back(edge_belief);
      edges.push_back(
          std::make_pair(clique2idx[forward_c], clique2idx[backward_c]));
    }
  }

  std::pair<Mapper_, Mapper_> _assign_mapper(const Belief_& edge_belief,
                                             const clique_type& clique) {
    size_t esize = edge_belief.states.size();
    size_t i, j;

    std::pair<Mapper_, Mapper_> res;
    const Belief_& c_b = clique_belief[clique2idx[clique]];
    size_t csize = c_b.states.size();

    res.first.resize(csize);
    res.second.resize(esize);

    for (i = 0; i < csize; i++) {
      for (j = 0; j < esize; j++) {
        // S_e == S_c \cap D_e
        clique_type c_b_j =
            _set_intersection(c_b.states[i], edge_belief.domain);
        if (c_b_j == edge_belief.states[j]) {
          res.first.set(i, j);
          res.second.set(j, i);
        }
      }
    }
    res.first.freeze();
    res.second.freeze();
    return res;
  }

  void _init_mappers(const JTree& htree) {
    // step 2: initialize edge information
    // initialize sepset table
    // initialize edge list
    for (int i = 0; i < htree.bfs_edges_size(); i++) {
      // S1: retrieve basic information. cliques and sepset
      Edge edge = htree.bfs_edges(i);
      clique_type forward_c, backward_c;
      const Belief_& edge_belief = sepset_belief[i];

      assign_clique(edge.source_nodes(), forward_c);
      assign_clique(edge.dst_nodes(), backward_c);

      auto forward_mapper = _assign_mapper(edge_belief, forward_c);
      auto backward_mapper = _assign_mapper(edge_belief, backward_c);
      vector<Mapper_> edge_mapper(4);
      edge_mapper[0] = forward_mapper.first;
      edge_mapper[1] = forward_mapper.second;
      edge_mapper[2] = backward_mapper.first;
      edge_mapper[3] = backward_mapper.second;

      mappers.push_back(edge_mapper);
    }
  }

  void _init_marginal_table(const JTree& htree) {
    // initialize margin_table
    for (int i = 0; i < htree.marginal_info_size(); i++) {
      clique_type clique;
      assign_clique(htree.marginal_info(i).clique(), clique);
      margin_table.push_back(clique2idx[clique]);

      Belief_ belief;
      node_type node_i = i;
      belief.domain = {node_i};
      belief.add_state({});
      belief.add_state({node_i});
      belief.init_prob(_batch_size);

      margin_res.push_back(belief);

      Mapper_ mapper;
      mapper.resize(2);
      const auto& cb_s = clique_belief[clique2idx[clique]].states;
      for (size_t j = 0; j < cb_s.size(); j++) {
        if (is_contain(cb_s[j], i)) {
          mapper.set(1, j);
        } else {
          mapper.set(0, j);
        }
      }
      mapper.freeze();

      margin_mapper.push_back(mapper);

      assert(htree.marginal_info(i).node() == i);
    }
  }

  void _init_normalier(const JTree& htree) {
    // copy node_normalize_factor
    for (int i = 0; i < htree.num_cliques_size(); i++) {
      Dtype factor = (Dtype)htree.num_cliques(i);
      node_normalize_factor.push_back(factor);
    }
    _num_nodes = node_normalize_factor.size();
  }

  void _init_belief(const JTree& htree) {
    _init_clique(htree);
    _init_edges(htree);
    _init_mappers(htree);
    _init_marginal_table(htree);
    _init_normalier(htree);
  }

 public:
  // log_pots: potentials for each variable in log space
  // label: the label data point,
  //
  // outputs:
  //  prob: calibrate probability in real space
  //  gradients: loss w.r.t. log_pots
  //    if (gradients == NULL) gradients computation is avoided
  //    and the label is not used
  //
  // return loss (log liklihood of the label)
  inline void inference(const Dtype* log_pots, Dtype* prob,
                        const LType* label = NULL) {
    // initialize belief (clique)
    assign_belief(log_pots);

    if (label != NULL) {
      clamp_belief(label);
    }

    // message passing (two pass)
    pass_message();

    // marginalize
    marginalize(prob);
  }

  inline void get_multiplier(Dtype* res) {
    for (size_t i = 0; i < num_nodes(); i++) {
      res[i] = node_normalize_factor[i];
    }
  }

  const Belief_& operator[](const clique_type& key) const {
    return clique_belief[clique2idx.at(key)];
  }

  bool has_clique(const clique_type& key) const {
    return clique2idx.find(key) != clique2idx.end();
  }

  size_t num_nodes() const { return _num_nodes; }
  size_t num_edges() const { return sepset_belief.size(); }
  size_t num_cliques() const { return clique_belief.size(); }

  bool init(const std::string& tree_path) {
    JTree _jtree;
    if (!ReadProtoFromBinaryFile(tree_path.c_str(), &_jtree)) {
      return false;
    }
    init_from_pb(_jtree);
    return true;
  }

 private:
  // clique info
  std::map<clique_type, int> clique2idx;
  vector<Belief_> clique_belief;
  vector<Belief_> clique_belief_tmp;

  // edge info
  vector<Belief_> sepset_belief;
  vector<Belief_> sepset_belief_tmp;

  // left to sep
  // sep to left
  // right to sep
  // sep to right
  vector<vector<Mapper_>> mappers;
  vector<std::pair<node_type, node_type>> edges;

  // marginal info
  size_t _num_nodes;
  vector<int> margin_table;
  vector<Mapper_> margin_mapper;
  vector<Dtype> node_normalize_factor;
  vector<Belief_> margin_res;

  // only fixed batch size can be used
  size_t _batch_size;

  // temporary variable for margin_res
  Vec<Dtype> Z;
};
}

#endif
