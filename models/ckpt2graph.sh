#!/bin/bash
# vim ft=sh

ckpt_path=$1

this_dir=`dirname $0`
$this_dir/cnns/ckpt2graph.sh $ckpt_path eval/classifier/inputs/batch_join eval/classifier/classifier/logits/add
