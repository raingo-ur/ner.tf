#!/usr/bin/env python
"""
convert txt file of the format:
  image-path label-id
into tfrecords

python compile_data.py txt-path

encoded image. regular jpg file
assume images are properly encoded jpg, although if otherwise it will be converted to jpeg

the directory data/txt-path-name.tf will be created
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp

import random
import tensorflow as tf
from time import gmtime, strftime

from cnns import utils
def _convert_to_example(data):
  path = data[0]
  label = data[1]

  image_buffer = utils.ensure_jpeg(path)
  features = {}
  features['image/path'] = utils.bytes_feature(path)
  features['image/encoded'] = utils.bytes_feature(image_buffer)
  features['label'] = utils.int64_feature(label)

  example = tf.train.Example(
      features=tf.train.Features(feature=features))

  return example

def main(_):
  txt_path = sys.argv[1]

  def parse(fields):
    path = fields[0]
    label = int(fields[1])
    if not osp.exists(path):
      return None
    return (path, label)

  utils._compile_main(txt_path, _convert_to_example, parse)

if __name__ == "__main__":
  tf.app.run()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
