#!/usr/bin/env python
"""
Code description
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
import os

from classifier import get_iter
import cnns
import tensorflow as tf
FLAGS = tf.app.flags.FLAGS

class MainTest(tf.test.TestCase):

  def setUp(self):
    super(MainTest, self).setUp()
    FLAGS.data_dir = osp.join(os.environ.get("DATA_DIR"), 'data')
    FLAGS.ckpt_path = osp.join(os.environ.get("DATA_DIR"), 'data', 'model.ckpt')

  def test_num_outputs_train(self):
    ep_size, loss_list, cnn = get_iter("train.tf", True, 10)
    self.assertEqual(ep_size, 1600)
    self.assertTrue(isinstance(loss_list, list))

    first_loss = loss_list[0]
    self.assertEqual(len(first_loss), 3)
    self.assertTrue(isinstance(first_loss[0], tf.Tensor))
    self.assertTrue(isinstance(first_loss[1], float))
    self.assertTrue(isinstance(first_loss[2], list))

    self.assertTrue(isinstance(cnn, cnns.CNN))

  def test_num_outputs_eval(self):
    ep_size, outputs_T, _eval = get_iter("dev.tf", False, 10)
    self.assertEqual(ep_size, 800)
    self.assertTrue(isinstance(outputs_T, dict))

    for k, v in outputs_T.items():
      self.assertTrue(isinstance(k, str))
      self.assertTrue(isinstance(v, tf.Tensor))

    self.assertTrue(callable(_eval))

  def test_full_run_single_iter(self):
    FLAGS.num_epochs = .01
    FLAGS.num_eval_epochs = 1./160
    FLAGS.eval_split = 'test.tf'
    with self.test_session() as sess:
      cnns.runner._main(sess, "test", get_iter)

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
