#!/bin/bash
# vim ft=sh

gpu=$1

exps="sports-b2 animal-b2 species-b2 artifacts"

source ~/.virtualenvs/tf/bin/activate
for exp in `echo $exps | tr ' ' '\n' | sort -R`
do
  pushd $exp
  #../../cnns/gcs-download.sh data/exps-subsets-0.8/
  #../../cnns/gcs-download.sh data/exps-ratios-0.8/
  #../../cnns/gcs-download.sh data/exps-ratios-0.8-clean/
  ../../trains.sh data/ $gpu
  popd
done
