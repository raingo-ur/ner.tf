#!/bin/bash
# vim ft=sh

export SUBSET=train
ratio=$1

if [ -z "$ratio" ]
then
  ratio=0.2
fi

SCRIPT=../animal-b2/

export EXP_SUFFIX=$ratio
export BASELINE_ONLY=1
subset_dir=`$SCRIPT/_setup.sh subsets $ratio`

export VARY=train
export FIX=yfcc100m
export BASELINE_ONLY=0
ratios_dir=`$SCRIPT/_setup.sh ratios $ratio`
$SCRIPT/_link_subset.sh $subset_dir $ratios_dir
echo $subset_dir $ratios_dir

# upper bound
export FIX=yfcc100m-clean
export BASELINE_ONLY=1
export EXP_SUFFIX=$EXP_SUFFIX-clean
$SCRIPT/_setup.sh ratios $ratio
