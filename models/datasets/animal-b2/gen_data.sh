#!/bin/bash
# vim ft=sh

raw_dir=data/raw
data_dir=data/data-v3
mkdir -p $data_dir

this_dir=`dirname $0`
this_dir=`readlink -f $this_dir`

raw_dir=`readlink -f $raw_dir`
data_dir=`readlink -f $data_dir`

label_imagenet_path=$this_dir/../../../data/label/label_imagenet.py
label_yfcc_path=$this_dir/../../../data/label/label_yfcc100m.py
group_sample_path=$this_dir/../../../data/bootstrap/group_and_sample.py

label_imagenet_path=`readlink -f $label_imagenet_path`
label_yfcc_path=`readlink -f $label_yfcc_path`

function split_ratio {
  src=$1
  ratio=$2
  total=`cat $src | wc -l`
  num_sample=`echo "$ratio * $total/1" | bc`
  sort -R $src | split -l $num_sample
}

cd $raw_dir
# label imagenet images
cat $raw_dir/images | python $label_imagenet_path | python $group_sample_path 500 > $data_dir/imagenet.txt

#split imagenet images
split_ratio $data_dir/imagenet.txt 0.6
mv xaa $data_dir/train.txt
mv xab devtest.txt
split_ratio devtest.txt 0.75
mv xaa $data_dir/test.txt
mv xab $data_dir/dev.txt
rm devtest.txt
rm $data_dir/imagenet.txt

# label yfcc images
function gen_pid2entity {
  flag=$1
  cat $raw_dir/all.csv.result.txt | python $this_dir/hash2pid.py | grep " $flag$"
}

cat $raw_dir/images | python $label_yfcc_path <(gen_pid2entity yes) > $data_dir/yfcc-clean.txt
cat $raw_dir/images | python $label_yfcc_path <(gen_pid2entity no) | sort -R > $data_dir/yfcc-noise.txt

cd $data_dir
# mix the same number of background images into evaluation
function mix_eval {
  split=$1
  noise_level=$2
  num_org=`cat $split.txt | wc -l`
  num_org=`echo $noise_level*$num_org/1 | bc`

  # the first num-org lines
  head -$num_org yfcc-noise.txt > tmp.txt

  # the rest
  ((num_org++))
  tail -n +$num_org yfcc-noise.txt > tmp2.txt

  mv tmp2.txt yfcc-noise.txt

  # mix noise with clean
  cat tmp.txt | awk '{print $1,"-1"}' | cat - $split.txt | sort -R > tmp2.txt
  mv tmp2.txt $split.txt
  rm tmp.txt
}
mix_eval test 0.2
mix_eval dev 0.2

# mix clean and noise for training
num_train=`cat train.txt | wc -l`
cat yfcc-clean.txt yfcc-noise.txt | sort -R | head -$num_train > yfcc100m.txt
rm yfcc-noise.txt
mv yfcc-clean.txt yfcc100m-clean.txt

function _split {
  echo train.txt
  echo dev.txt
  echo test.txt
  echo yfcc100m.txt
  echo yfcc100m-clean.txt
}

_split | xargs -L 1 python $this_dir/../../compile_data.py
