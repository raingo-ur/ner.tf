#!/bin/bash
# vim ft=sh

#####
## link all sub directory of $src into $dst

src=$1
dst=$2

this_dir=`dirname $0`
this_dir=`readlink -f $this_dir`
RELPATH=$this_dir/_relpath.py

for subdir in `find $src/ -mindepth 1 -maxdepth 1 -type d`
do
  subdir=`readlink -f $subdir`
  name=`basename $subdir`
  pushd $dst/$name
  rm -f subset_dir
  ln -s `$RELPATH $subdir` subset_dir
  popd
done
