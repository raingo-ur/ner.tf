#!/bin/bash
# vim ft=sh

pushd () {
  command pushd "$@" > /dev/null
}

popd () {
  command popd "$@" > /dev/null
}

if [ $# -lt 2 ]
then
  echo $0 data-name params
  echo "example: $0 ratios 0.2"
  echo "example: $0 subsets 0.2"
  exit
fi

this_dir=`dirname $0`
this_dir=`readlink -f $this_dir`
RELPATH=$this_dir/_relpath.py
GS=$this_dir/_group_symlinks.sh

_GS=`readlink -f $GS`

real_data_dir=`readlink -f data/data-v3/`

function join_by {
  local IFS="$1"; shift; echo "$*";
}

function _main {
  name=$1

  exp_suffix=
  if [ ! -z "$EXP_SUFFIX" ]
  then
    exp_suffix="-$EXP_SUFFIX"
  fi

  exp_dir=data/exps-$name$exp_suffix

  mkdir -p $exp_dir
  rsync -arL ../hex-exps/base/ $exp_dir


  rm -Rf $exp_dir/data
  mkdir -p $exp_dir/data/

  pushd $exp_dir/data/
  ln -sf `$RELPATH $real_data_dir/test.tf` .
  ln -sf `$RELPATH $real_data_dir/dev.tf` .
  popd


  train_data_dir=$exp_dir/data/train.tf
  mkdir -p $train_data_dir
  pushd $train_data_dir
  data_func=$1
  _debug $data_func ${@:2}
  $data_func ${@:2}
  popd

  ac=`readlink -f ../../cnns/add_config.sh`
  pushd $exp_dir
  $ac "--data_dir $exp_dir/data/" .
  popd

  if [ "$BASELINE_ONLY" == "1" ]
  then
    find $exp_dir/ -name config | grep -v mll-ce-l1 | xargs rm
  else
    python ../../kb_guided.py $exp_dir/data/ > /dev/null
  fi
  echo $exp_dir
}

function _debug {
  #>&2 echo $@
  :
}

function _data_dir {
  base=$1
  gname=$2
  if [ ! -z "$gname" ]
  then
    gname="-$gname"
  fi
  echo $real_data_dir/$base$gname.tf/
  _debug $real_data_dir/$base$gname.tf
}

function subsets {
  subset=$1
  subset_base=$SUBSET
  if [ -z "$subset_base" ]
  then
    _debug "invalid subset:$subset_base"
    exit
  fi
  group_dir=`_data_dir $subset_base $group`
  num_files=`find $group_dir -type f | wc -l`
  limit=`echo "$num_files*$subset/1" | bc`
  $_GS $group_dir $limit
}

function ratios {
  fix=$FIX
  vary=$VARY
  if [ -z "$fix" -o -z "$vary" ]
  then
    _debug "invalid fix:$fix, vary:$vary"
    exit
  fi

  ratio=$1
  vary_dir=`_data_dir $vary`
  num_files=`find $vary_dir -type f | wc -l`
  limit=`echo "$num_files*$ratio/1" | bc`
  $_GS $vary_dir $limit

  fix_dir=`_data_dir $fix $group`
  $_GS $fix_dir
}

_main $@
