#!/usr/bin/env python
"""
map two vocabulary
python _map.py src-vocab dst-vocab fields
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp

def main():

  def _load(path):
    res = []
    with open(path) as reader:
      for line in reader:
        res.append(line.strip())
    return res

  src = _load(sys.argv[1])
  dst = _load(sys.argv[2])
  dst = {w:i for i, w in enumerate(dst)}

  wl = set([int(f) for f in sys.argv[3].split(',')])

  for line in sys.stdin:
    fields = line.strip().split()
    for idx in range(len(fields)):
      if idx in wl:
        f = int(fields[idx])
        f = src[f]
        if f in dst:
          fields[idx] = dst[f]
        else:
          break
    if f in dst:
      print(*fields)

  pass

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
