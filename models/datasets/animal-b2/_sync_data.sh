#!/bin/bash
# vim ft=sh

remote=gpu-cs3:/u/yli/data-bank/ner-tf-data/data-v3/

rsync -rv --include '*/' --include 'tf-*' --exclude '*' --update --progress $remote .
find . -type d -empty -delete
