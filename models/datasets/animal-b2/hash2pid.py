#!/usr/bin/env python
"""
given table of entities.hash with "pid hash"
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp

def main():
  hash2pid = {}
  with open('./entities.hash') as reader:
    for line in reader:
      fields = line.strip().split()
      hash2pid[fields[1]] = fields[0]
  for line in sys.stdin:
    fields = line.strip().split()
    hh = osp.splitext(osp.basename(fields[0]))[0]
    if hh in hash2pid:
      pid = hash2pid[hh]
      print(pid, *fields[1:])
  pass

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
