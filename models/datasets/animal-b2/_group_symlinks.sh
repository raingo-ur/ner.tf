#!/bin/bash
# vim ft=sh

# group symlink
target=$1
limit=$2

this_dir=`dirname $0`
RELPATH=$this_dir/_relpath.py

filename=$(basename "$target")
name="${filename%.*}"

if [ -z "$limit" ]
then
  limit=`find $target/ -type f | wc -l`
fi

export LC_ALL=C
for tt in `find $target/ -type f | sort | head -$limit`
do
  filename=`basename $tt`
  tg=$filename.$name
  rm -f $tg
  ln -s `$RELPATH $tt` $tg
done
