#!/bin/bash
# vim ft=sh

ratio=$1

if [ -z "$ratio" ]
then
  ratio=0.2
fi

noise_ratio=`echo "1-$ratio" | bc`

echo "clean ratio: $ratio; noise ratio: $noise_ratio"

export SUBSET=train-clean
export EXP_SUFFIX=$ratio
export BASELINE_ONLY=1
subset_dir=`../animal-b2/_setup.sh subsets $ratio`

export VARY=train-noise
export FIX=train-clean
export BASELINE_ONLY=0
ratios_dir=`../animal-b2/_setup.sh ratios $noise_ratio`
../animal-b2/_link_subset.sh $subset_dir $ratios_dir
echo $subset_dir $ratios_dir
