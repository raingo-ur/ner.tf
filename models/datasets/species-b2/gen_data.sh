#!/bin/bash
# vim ft=sh

raw_dir=data/raw
data_dir=data/data-v3
mkdir -p $data_dir

this_dir=`dirname $0`
this_dir=`readlink -f $this_dir`
hash2pid=`readlink -f $this_dir/../animal-b2/hash2pid.py`

raw_dir=`readlink -f $raw_dir`
data_dir=`readlink -f $data_dir`

label_yfcc_path=$this_dir/../../../data/label/label_yfcc100m.py

label_yfcc_path=`readlink -f $label_yfcc_path`

function split_ratio {
  src=$1
  ratio=$2
  total=`cat $src | wc -l`
  num_sample=`echo "$ratio * $total/1" | bc`
  sort -R $src | split -l $num_sample
}

cd $raw_dir

# label yfcc images
function gen_pid2entity {
  flag=$1
  cat $raw_dir/all.csv.result.txt | python $hash2pid | grep " $flag$"
}

cat $raw_dir/images | python $label_yfcc_path <(gen_pid2entity yes) > $data_dir/yfcc-clean.txt
cat $raw_dir/images | python $label_yfcc_path <(gen_pid2entity no) | sort -R > $data_dir/yfcc-noise.txt

cd $data_dir

# split both noise and clean into 6:3:1
function _split {
  suffix=$1
  #split imagenet images
  split_ratio yfcc-$suffix.txt 0.6
  mv xaa train-$suffix.txt
  mv xab devtest.txt
  split_ratio devtest.txt 0.75
  mv xaa test-$suffix.txt
  mv xab dev-$suffix.txt
  rm devtest.txt
  rm yfcc-$suffix.txt
}
_split noise
_split clean

# merge clean and noise eval samples, -1 means none of the above
function mix_eval {
  split=$1
  cat "$split-noise.txt" | awk '{print $1,"-1"}' | cat - "$split-clean".txt | sort -R > $split.txt
}
mix_eval dev
mix_eval test

# generate tf files
function _split {
  echo train-clean.txt
  echo train-noise.txt

  echo dev.txt
  echo test.txt
}

_split | xargs -L 1 python $this_dir/../../compile_data.py
