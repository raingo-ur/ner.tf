#!/bin/bash
# vim ft=sh

# ls data/*.txt | xargs -L 1 python ../../compile_data.py

rm -Rf data/exps
cp ../hex-exps/base data/exps -R
../../cnns/add_config.sh "--cnn_class slim.lenet"
