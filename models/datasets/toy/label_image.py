#!/usr/bin/env python
"""
Code description
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp

sys.path.append('../../../data/label/')
from label_imagenet import load_list
from gen_graph import toy_meta_dir

def main():
  leaves = load_list(osp.join(toy_meta_dir, 'leaves.txt'))
  synset2id = {leaf:idx for idx, leaf in enumerate(leaves)}

  for line in sys.stdin:
    dirname = osp.dirname(line.strip())
    wnid = osp.basename(dirname)
    if wnid in synset2id:
      print(line.strip(), synset2id[wnid])

  pass

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
