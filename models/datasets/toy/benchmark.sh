#!/bin/bash
# vim ft=sh

ben_dir=_benchmark
echo "single gpu"
time ../../train.sh $ben_dir 0
rm -f $ben_dir/FINISHED
rm -f $ben_dir/model.*

echo "double gpu"
time ../../train.sh $ben_dir 0,1
rm -f $ben_dir/FINISHED
rm -f $ben_dir/model.*
