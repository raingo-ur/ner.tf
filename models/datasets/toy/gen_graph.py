#!/usr/bin/env python
"""
Code description
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp

this_dir = osp.dirname(__file__)
sys.path.append(osp.join(this_dir, '../../hex/build/caffe/proto/'))
import hex_pb2

sys.path.append('../../../data/label/')
from label_imagenet import load_list
import random

def random_ralations(graph, rtype, num_edges):
  nodes = range(graph.num_labels)
  steps = (num_edges+3-1)//3
  for _ in range(steps):
    if len(nodes) < 3:
      break
    sub = random.sample(nodes, 3)
    _edge = graph.subset_relations.add()
    _edge.parent = sub[0]
    _edge.child = sub[1]
    _edge.rtype = rtype

    _edge = graph.subset_relations.add()
    _edge.parent = sub[0]
    _edge.child = sub[2]
    _edge.rtype = rtype

    nodes.remove(sub[1])
    nodes.remove(sub[2])

toy_meta_dir = '../../cnns/toy/'

def main():
  leaves = load_list(osp.join(toy_meta_dir, 'leaves.txt'))
  relations = [line.split() for line in load_list(osp.join(toy_meta_dir, 'relations.txt'))]
  all_nodes = set(sum(relations, []))
  extras = list(all_nodes - set(leaves))
  vocab = leaves + extras
  w2i = {w:i for i, w in enumerate(vocab)}

  graph = hex_pb2.Graph()
  graph.num_labels = len(vocab)
  graph.num_leaves = len(leaves)
  for idx, entity in enumerate(vocab):
    info = graph.label_infos.add()
    info.index = idx
    info.str_id = entity.decode('utf-8')

  for edge in relations:
    _edge = graph.subset_relations.add()
    _edge.parent = w2i[edge[1]]
    _edge.child = w2i[edge[0]]

  random_ralations(graph, 'r1', len(relations))
  random_ralations(graph, 'r2', len(relations))

  save_path = 'graph.pbtxt'
  with open(save_path, 'w') as writer:
    print(str(graph), file=writer)

  pass

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
