#!/usr/bin/env python
"""
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
import cPickle

this_dir = osp.dirname(__file__)
sys.path.append(osp.join(this_dir, '../../hex/build/caffe/proto/'))
import hex_pb2

def main():
  meta_path = sys.argv[1]

  meta = cPickle.load(open(meta_path))

  leaves = meta['fine_label_names']
  vocab = leaves + meta['coarse_label_names']

  graph = hex_pb2.Graph()
  graph.num_labels = len(vocab)
  graph.num_leaves = len(leaves)

  for line in sys.stdin:
    fields = line.strip().split()
    fine = int(fields[0])
    coarse = int(fields[1])
    _edge = graph.subset_relations.add()
    _edge.parent = coarse + len(leaves)
    _edge.child = fine

  for idx, entity in enumerate(vocab):
    info = graph.label_infos.add()
    info.index = idx
    info.str_id = entity

  with open(meta_path + '.pbtxt', 'w') as writer:
    print(str(graph), file=writer)

  with open(meta_path + '.pb', 'wb') as writer:
    writer.write(graph.SerializeToString())
  pass

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
