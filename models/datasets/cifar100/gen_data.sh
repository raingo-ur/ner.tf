#!/bin/bash
# vim ft=sh

raw_dir=data/raw/cifar-100-python
data_dir=data/data-v3

mkdir -p $data_dir

this_dir=`dirname $0`
this_dir=`readlink -f $this_dir`

raw_dir=`readlink -f $raw_dir`
data_dir=`readlink -f $data_dir`

sort -R data/raw/noise.txt > $data_dir/yfcc-noise.txt
cd $raw_dir
sort -R train.txt | awk '{print $1,$2}' | split -l 40000

mv xaa $data_dir/train.txt
mv xab $data_dir/dev.txt
cat test.txt | awk '{print $1,$2}' > $data_dir/test.txt

cd $data_dir

# mix the same number of background images into evaluation
function mix_eval {
  split=$1
  noise_level=$2
  num_org=`cat $split.txt | wc -l`
  num_org=`echo $noise_level*$num_org/1 | bc`

  # the first num-org lines
  head -$num_org yfcc-noise.txt > tmp.txt

  # the rest
  ((num_org++))
  tail -n +$num_org yfcc-noise.txt > tmp2.txt

  mv tmp2.txt yfcc-noise.txt

  # mix noise with clean
  if [ $split == 'train' ]
  then
    cat tmp.txt $split.txt | sort -R > tmp2.txt
  else
    cat tmp.txt | awk '{print $1,"-1"}' | cat - $split.txt | sort -R > tmp2.txt
  fi
  mv tmp2.txt $split.txt
  rm tmp.txt
}
mix_eval test 0.2
mix_eval dev 0.2

num_train=`cat train.txt | wc -l`
head -$num_train yfcc-noise.txt > noise.txt
rm yfcc-noise.txt

mv noise.txt train-noise.txt
mv train.txt train-clean.txt

function _split {
  echo train-noise.txt
  echo train-clean.txt
  echo dev.txt
  echo test.txt
}

_split | xargs -L 1 python $this_dir/../../compile_data.py
