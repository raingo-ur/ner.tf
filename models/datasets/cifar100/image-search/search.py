import json
import os
import sys
import time
import hashlib

import requests
import requests_cache

requests_cache.install_cache('demo_cache')

# Given a list of queries from stdin, output the top image search results to stdout

# parameters
max_retries = 10

Engine_ID = '009365802921528089378:l1mi9fcov30'

base_url = "https://www.googleapis.com/customsearch/v1"
# query parameters:
# https://developers.google.com/custom-search/json-api/v1/reference/cse/list#parameters
params = {
    'key':'AIzaSyB9-x6d3TcYWAAA6TzJ_CAaaLpPWhVl3eI',
    'cx': Engine_ID,
    'searchType':'image',
    'num':10,
    'imgType':'photo',
    'imgColorType':'color',
    'start':-1,
    'q': ""
    }

num_pages = 1

for idx, line in enumerate(sys.stdin):
  # obtain the search result
  query = line.strip()
  print >> sys.stderr, 'Searching for the query %s:%d' % (query, idx+1)
  params['q'] = query
  for j in range(num_pages):
    for i in range(max_retries):
      params['start'] = 1 + j * params['num']
      try:
        req = requests.get(base_url, params=params)
        A = req.json()
      except Exception as ioe:
        print >> sys.stderr, ioe
        time.sleep(10)
        continue
      else:
        break

    if 'items' in A:
      for item in A['items']:
        link = item['link']
        hash_object = hashlib.sha1(link)
        hex_dig = str(hash_object.hexdigest())
        print '\t'.join([query, link, hex_dig])

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
