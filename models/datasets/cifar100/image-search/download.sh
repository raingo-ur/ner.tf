#!/bin/bash
# vim ft=sh

base_dir=$1
image_dir=$base_dir/imgs
mkdir -p $image_dir
cmd='--dry-run'
cmd='--progress'
awk -F'\t' 'BEGIN{OFS=FS} {print $2,$3}' $base_dir/search-results.tsv | parallel --eta --col-sep '\t' -P4 $cmd "wget {1} -O - -q | convert - $image_dir/{2}.jpg"
