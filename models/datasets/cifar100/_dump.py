#!/usr/bin/env python
"""
dump cifar100 numpy files into png files
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
import os
import cPickle
import skimage.io as sio

def main():
  target = sys.argv[1]
  data = cPickle.load(open(target))

  img_dir = target + '.pixels'
  if not osp.exists(img_dir):
    os.mkdir(img_dir)
  label_file = target + '.txt'
  with open(label_file, 'w') as writer:
    for img, fine, coarse, fn in zip(
        data['data'], data['fine_labels'],
        data['coarse_labels'], data['filenames']):
      image_path = osp.join(img_dir, fn)
      print(image_path, fine, coarse, file=writer)
      img = img.reshape((3, 32, 32)).transpose((1,2,0))
      sio.imsave(image_path, img)
  pass

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
