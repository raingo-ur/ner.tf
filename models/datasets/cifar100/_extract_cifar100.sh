#!/bin/bash
# vim ft=sh
url=http://www.cs.toronto.edu/~kriz/cifar-100-python.tar.gz
hash=eb9058c3a382ffc7106e4002c42a8d85
raw_dir=data/raw
name=`basename $url`
raw_tgz_path=$raw_dir/$name

raw_path="${raw_tgz_path%.tar.gz}"
raw_path=`readlink -f $raw_path`

if [ ! -f $raw_tgz_path ]
then
  wget $url -P $raw_dir
fi

if [ ! -d $raw_path ]
then
  pushd $raw_dir
  verify=`echo $hash $name | md5sum -c - | awk '{print $2}'`
  if [ "$verify" == "OK" ]
  then
    echo "extracting"
    tar xfa $name
  else
    rm $name
  fi
  popd
fi

if [ ! -d $raw_path/train.pixels ]
then
  python _dump.py $raw_path/train
fi

if [ ! -d $raw_path/test.pixels ]
then
  python _dump.py $raw_path/test
fi

if [ ! -f $raw_path/meta.pb ]
then
  cat $raw_path/train.txt | awk '{print $2,$3}' | sort -u | python _gen_graph.py $raw_path/meta
fi

mkdir -p jtree.pb
cp $raw_path/meta.pb jtree.pb/graph.pb
cp $raw_path/meta.pbtxt jtree.pb/graph.pbtxt
