#!/usr/bin/env python
"""
given debug npz file, compute guide matrix based on confusion matrix
optionally, load a predefined guide matrix to mask out
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
import numpy as np

def main():
  npz = sys.argv[1]

  top_num = 5
  data = np.load(npz)
  labels = data['label']
  logits = data['logits']
  ranks = np.argsort(-logits, axis=1)[:, :top_num]
  dim = logits.shape[1]

  confusion = np.zeros((dim, dim))
  for label, rank in zip(labels, ranks):
    for r in rank:
      confusion[label,r] += 1

  # keep only top_num non zero entries for each confusion matrix
  ranks = np.argsort(-confusion, axis=0)
  for col in range(ranks.shape[0]):
    for l in ranks[top_num:, col]:
      confusion[l, col] = 0

  # avoid all zero columns
  confusion += np.eye(dim) * 0.01

  res = res / res.sum(axis=0, keepdims=True)
  save_path = npz + '.conf.npy'
  np.save(save_path, res)

  pass

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
