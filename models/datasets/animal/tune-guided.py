#!/usr/bin/env python
"""
given debug.npz from mll-ce-l1, tune the kb-guided schema
the debug.npz should be from dev set
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
import os

import numpy as np

sys.path.append('../../')
from loss import __get_kb_guide, FLAGS
from metrics import evaluate

from scipy.special import expit
from multiprocessing import Pool
import json

def _ben(config, data):
  d0 = {}
  d0['label'] = data['label']
  guide = __get_kb_guide([config['coeff0'], config['coeff1']], [config['rtype']], config['damp'])
  logits = expit(data['logits'] / config['temp'])
  d0['logits'] = np.dot(logits, guide)

  res = {}
  res['config'] = config
  res['score'] = evaluate(d0)[1]
  return res

def main():
  npz_path = sys.argv[1]
  FLAGS.data_dir = sys.argv[2]
  data = np.load(npz_path)
  data = dict(data.items())

  coeffs = [0.1, 0.4, 0.5, 1.0]
  rtypes = ['class','division','family','genus','kingdom','order','phylum']
  temps = [1, 0.5, 10]

  perfs = []
  configs = []
  config = {}
  for coeff0 in coeffs:
    config['coeff0'] = coeff0
    for coeff1 in coeffs:
      config['coeff1'] = coeff1
      for damp in [True, False]:
        config['damp'] = damp
        for rtype in rtypes:
          config['rtype'] = rtype
          for temp in temps:
            config['temp'] = temp
            configs.append(config.copy())

  results = {}
  p = Pool(8)
  from functools import partial
  results['tune'] = p.map(partial(_ben, data=data), configs)
  results['baseline'] = evaluate(data)[1]
  json.dump(results, open('tune.json', 'w'), indent=4)
  pass

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
