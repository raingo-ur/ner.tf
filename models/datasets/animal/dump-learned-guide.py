#!/usr/bin/env python
"""
given ckpt and masking guide, output the learned guide
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp

import numpy as np
import tensorflow as tf

def softmax(x, axis):
  """Compute softmax values for each sets of scores in x."""
  e_x = np.exp(x - np.max(x, axis=axis, keepdims=True))
  return e_x / e_x.sum(axis=axis, keepdims=True)

def main():
  ckpt_path = sys.argv[1]
  mask_npy = sys.argv[2]
  name = sys.argv[3]

  mask = np.load(mask_npy) > 0

  if 'off-diag' in ckpt_path:
    np.fill_diagonal(mask, 0)
    all_zero_cols = np.where(mask.sum(axis=0)==0)[0]
    mask[all_zero_cols, all_zero_cols] = 1

  mask_inf = -np.inf * np.ones_like(mask)
  mask_inf[mask] = 1
  reader = tf.train.NewCheckpointReader(ckpt_path)

  tensor_name = 'kb-guide-learn'
  tensor_name = [a for a in reader.get_variable_to_shape_map()
      if tensor_name in a]
  tensor_name = tensor_name[0]
  weights = reader.get_tensor(tensor_name).T
  weights *= mask_inf
  weights = softmax(weights, 0)
  save_path = '_guides/%s.npy' % name
  np.save(save_path, weights)
  print('saved to', save_path)

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
