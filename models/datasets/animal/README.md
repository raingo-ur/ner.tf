
== For animal dataset

=== Expected files

1. `entities.txt` contains the list of entities
2. `pid2entity.txt` contains the pid to entity mapping
3. `synset.wnid.entity.txt` synset wnid entity mapping
4. `entities.graph`

=== label images

1. `cd data`
2. `find dir-to-yfcc-pixels -type f | python $PATH_TO_LABEL_YFCC100M.PY > yfcc100m-all.txt`
3. `find dir-to-imagenet-images -type f | python $PATH_TO_LABEL_IMAGENET.PY > all.txt`
4. split `all.txt` so that (use `data/label/split.sh`)
  1. `train.txt` 300 images per class
  2. `test.txt` 150 images per class
  3. `dev.txt` 50 images per class
5. use `data/bootstrap/group_and_sample.py` to sample `yfcc100m-all.txt` into `yfcc100m.txt`, so that 300 images per class

=== generate graph files

1. `python ../../hex/scripts/augment_exclusion.py ./data/entities.graph`
2. `python ../../hex/scripts/utils.py ./data/entities.graph.excl`
3. move `./data/entities.graph.excl` to `./graph.pbtxt`
4. move `./data/entities.graph.excl.pb` to `./jtree.pb`
