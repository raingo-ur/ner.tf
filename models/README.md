

# normalize data format
tsv files with `image-path label`

# compile data
Given the tsv files, generating tf records

python compile_data.py `txt-path`


# train
`CUDA_VISIBLE_DEVICES=$gpu python classifier.py --data_path /path/to/train.tf/ --ckpt_path /path/to/ckpt`

# eval
`./eval.sh` for the reference
