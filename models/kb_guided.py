#!/usr/bin/env python
"""
generate kb_guided npy files
given a data directory
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os
import os.path as osp
import numpy as np
import math

from inputs import load_graph, NUM_L1_CLASSES, hie_relabel, \
    SHARE_PARENT_TYPE_PREFIX

def _get_clean_labels(data_dir):
  """
  example of data_dir data/exps-mixed-G0-0.2/data/
  """
  # infer the set of clean labels from data_dir
  num_groups = 4

  keys = []
  key_order_path = 'data/data-v2/key-order'
  if not osp.exists(key_order_path):
    return range(NUM_L1_CLASSES())
  with open(key_order_path) as reader:
    for line in reader:
      keys.append(int(line.strip()))
  num_keys = len(keys)
  num_key_per_group = int(math.ceil(num_keys / num_groups))

  res = []
  for i in range(num_groups):
    if 'G%d' % i in data_dir:
      res.extend(keys[i*num_key_per_group:(i+1)*num_key_per_group])

  if len(res) == 0:
    res = range(NUM_L1_CLASSES())

  print('clean labels:', res, file=sys.stderr)
  return res

def dump_kb_guide(kb_guide, save_path):
  graph = load_graph()
  entities = [''] * graph.num_labels
  for info in graph.label_infos:
    entities[info.index] = info.str_id.encode('utf-8')

  name = osp.basename(save_path)

  with open(save_path, 'w') as writer:
    for i in range(graph.num_leaves):
      for j in range(graph.num_leaves):
        print(entities[i], entities[j], kb_guide[i, j], name, file=writer)

def get_kb_guide(coeffs, rtypes, damp_num_siblings, clean_labels):
  res = np.eye(NUM_L1_CLASSES(), dtype=np.float32) * coeffs[0]

  labeler = hie_relabel()
  prefix = SHARE_PARENT_TYPE_PREFIX
  for label in clean_labels:
    for rtype in rtypes:
      sibs = labeler(label, prefix+rtype)
      sibs = [sib for sib in sibs if sib < NUM_L1_CLASSES() and sib != label]
      for sib in sibs:
        coeff = coeffs[1]
        if damp_num_siblings:
          coeff /= math.sqrt(len(sibs))
        res[label, sib] = coeff
  return res

def _LP_PATH():
  return "_label_pairs.txt"

def get_hand_guide(coeffs, clean_labels, with_negative=False):
  res = np.eye(NUM_L1_CLASSES(), dtype=np.float32) * coeffs[0]
  with open(_LP_PATH()) as reader:
    for line in reader:
      fields = line.strip().split()
      lhs = int(fields[0])
      rhs = int(fields[1])

      sign = int(fields[2])

      if not with_negative and sign < 0:
        continue

      value = coeffs[1]/2*sign

      if lhs in clean_labels:
        res[lhs, rhs] = value
      if rhs in clean_labels:
        res[rhs, lhs] = value

  return res

def main():
  data_dir = sys.argv[1]
  clean_labels = _get_clean_labels(data_dir)
  clean_labels = set(clean_labels)

  def _save(name, res):
    res = res / np.abs(res).sum(axis=0, keepdims=True)
    save_dir = osp.join(data_dir, '..', 'guides')
    save_dir = osp.realpath(save_dir)
    if not osp.exists(save_dir):
      os.mkdir(save_dir)
    save_path = osp.join(save_dir, name + '.npy')
    np.save(save_path, res)
    dump_kb_guide(res, osp.join(save_dir, name + '.txt'))
    print(save_path)

  coeffs = [1,1]
  rtypes = 'class,division,family,genus,kingdom,order,phylum'.split(',')
  try:
    rtypes = open('_rtypes').read().strip().split(',')
  except:
    pass
  print(rtypes)
  _save('sym', get_kb_guide(coeffs, rtypes, False, clean_labels))

  coeffs = [1,0.2]
  _save('asym', get_kb_guide(coeffs, rtypes, False, clean_labels))

  coeffs = [1,0.8]
  _save('damp_num_siblings', get_kb_guide(coeffs, rtypes, True, clean_labels))

  coeffs = [1, 0.5]
  _save('hand_graph', get_hand_guide(coeffs, clean_labels))
  _save('hand_graph2', get_hand_guide(coeffs, clean_labels, True))

  coeffs = [1, 0.2]
  _save('hand_graph-0.2', get_hand_guide(coeffs, clean_labels))

  coeffs = [1, 0.8]
  _save('hand_graph-0.8', get_hand_guide(coeffs, clean_labels))

  pre_guides_dir = '_guides'
  if osp.exists(pre_guides_dir):
    for guide_name in os.listdir(pre_guides_dir):
      guide = np.load(osp.join(pre_guides_dir, guide_name))
      _save(osp.splitext(guide_name)[0], guide)

  pass

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
