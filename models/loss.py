#!/usr/bin/env python
"""
definition of the loss functions
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
import os
import math

import tensorflow as tf
from cnns import ops
from cnns import optim
FLAGS = tf.app.flags.FLAGS

from inputs import NUM_L1_CLASSES, NUM_L2_CLASSES, \
    graph_base, hie_relabel, RTYPES, SHARE_PARENT_TYPE_PREFIX
from collections import OrderedDict
import numpy as np

# loss
tf.app.flags.DEFINE_string('loss', 'softmax',
    """specifiy the loss function to use, softmax, mll_ce""")

# loss_param
tf.app.flags.DEFINE_string('loss_param', '',
    """additional parameters passing to the loss function""")

# rtype
tf.app.flags.DEFINE_string('rtype', 'default',
    """specify the edge type to use""")

# argmin
tf.app.flags.DEFINE_boolean('argmin', True,
    """whether to use argmin inference""")

# argmin_distill
tf.app.flags.DEFINE_boolean('argmin_distill', False,
    """for distillation, use the argmin inference""")

# kb_guided_npy
tf.app.flags.DEFINE_string('kb_guided_npy', "",
    """the guided npy file to use""")

# trainable_kb_guide
tf.app.flags.DEFINE_boolean('trainable_kb_guide', False,
    """make the kb guide trainable""")

# temperature
tf.app.flags.DEFINE_float('temperature', 1.,
    """the temperature used for distill""")

# label_smooth_unknown_ratio
tf.app.flags.DEFINE_float('label_smooth_unknown_ratio', 0.,
    """larger than zero value turns on the unknown elements with the specicied weight for label smooth""")

# off_diag_only
tf.app.flags.DEFINE_boolean('off_diag_only', False,
    """for trainable_kb_guide, use only off diag elements""")

# bootstrap_guide_z
tf.app.flags.DEFINE_boolean('bootstrap_guide_z', False,
    """for guided bootstrap, guiding the z, not the logits""")

def setup_relabeler():

  # setup tf relabeler
  labeler = hie_relabel()
  label_maps = OrderedDict()
  with tf.variable_scope("relabeler"):
    for rtype in RTYPES():
      emb_np = np.zeros((NUM_L1_CLASSES(), NUM_L2_CLASSES()), dtype=np.float32)
      for i in range(NUM_L1_CLASSES()):
        for l in labeler(i, rtype):
          emb_np[i, l] = 1
      label_maps[rtype] = tf.constant(emb_np)

  def _relabel(label, rtype):
    # batch size x 1, labels between [0, NUM_L1_CLASSES())
    # return batch_size x NUM_L2_CLASSES()
    return tf.nn.embedding_lookup(label_maps[rtype], label)

  return _relabel

_cache = {}

def _setup():
  if len(_cache) > 0:
    return
  _cache['relabel'] = setup_relabeler()

def _RELABEL():
  _setup()
  return _cache['relabel']

def _argmin_infer(logits, loss_func):
  """
  y_hat == loss(y, logits)
  """
  if not FLAGS.argmin:
    return tf.slice(logits, [0,0], [-1,NUM_L1_CLASSES()])

  label = tf.range(NUM_L1_CLASSES())

  # logits: batch_size x NUM_L2_CLASSES() ==> batch_size*NUM_L1_CLASSES() x 1
  batch_size = logits.get_shape()[0].value
  logits = tf.expand_dims(logits, 1)
  logits = tf.tile(logits, [1, NUM_L1_CLASSES(), 1])
  logits = tf.reshape(logits, [batch_size*NUM_L1_CLASSES(), -1])

  # label: NUM_L1_CLASSES() ==> batch_size*NUM_L1_CLASSES() x 1
  label = tf.expand_dims(label, 0)
  label = tf.tile(label, [batch_size, 1])
  label = tf.reshape(label, [batch_size*NUM_L1_CLASSES()])

  # loss: batch_size*NUM_L1_CLASSES() x 1
  conf = -loss_func(logits, label)
  conf = tf.reshape(conf, [batch_size, NUM_L1_CLASSES()])
  return conf

class LossConfig(object):
  def __init__(self, name, loss, logits_size, infer='equal'):
    self.name = name
    self._loss_func = loss

    def _loss(x, y):
      # always output list of
      # loss_tensor, mix_rate, target variables
      loss_list = loss(x, y)
      if not isinstance(loss_list, list):
        loss_list = [(loss_list, )]

      res = []
      for loss_cfg in loss_list:
        loss_t = loss_cfg[0]
        if loss_t.get_shape().num_elements() > 1:
          loss_t = tf.reduce_mean(loss_t)

        if len(loss_cfg) < 2:
          mix_rate = 1.
        else:
          mix_rate = loss_cfg[1]

        if len(loss_cfg) < 3:
          tvars = tf.trainable_variables()
        else:
          tvars = loss_cfg[2]

        res.append((loss_t, mix_rate, tvars))
      return res

    self.loss = _loss

    def argmin(x):
      return _argmin_infer(x, loss)

    def equal(x):
      return x

    if isinstance(infer, str):
      self.infer = eval(infer)
    else:
      self.infer = infer

    self.logits_size = logits_size

configs = {}
def loss(loss_func):
  res = loss_func()
  config = LossConfig(loss_func.func_name, *res)
  assert config.name not in configs, config.name + " defined multiple times"
  configs[config.name] = config
  return loss_func

@loss
def softmax():
  def loss(logits, label):
    xent = tf.nn.sparse_softmax_cross_entropy_with_logits(
        logits, label)
    return xent
  return loss, NUM_L1_CLASSES()

@loss
def mll_ce():
  def loss(logits, label):
    label_map = _RELABEL()(label, FLAGS.rtype)
    return ops.cross_entropy(logits, label_map)

  def infer(logits):
    return tf.slice(logits, [0, 0], [-1, NUM_L1_CLASSES()])

  return loss, NUM_L2_CLASSES(), infer

@loss
def mll_ce_l1():
  def loss(logits, label):
    label_map_l1 = tf.one_hot(label, NUM_L1_CLASSES())
    return ops.cross_entropy(logits, label_map_l1)
  return loss, NUM_L1_CLASSES()

@loss
def mll_noise_layer():
  """
  reference:
  Sukhbaatar, Sainbayar, and Rob Fergus. "Learning from noisy labels with deep neural networks."arXiv preprint arXiv:1406.2080, no. 3 (2014): 4.k
  "has to be pretrained"
  """
  def loss(logits, label):
    new_logits = ops.fc_layer(logits, NUM_L1_CLASSES(),
        'noise_layer', bias=False, decay=2.)
    label_map_l1 = tf.one_hot(label, NUM_L1_CLASSES())
    return ops.cross_entropy(new_logits, label_map_l1)
  return loss, NUM_L1_CLASSES()


def _load_graph(graph_path, input_tensor,
    name='imported',
    input_name='eval/classifier/inputs/batch_join',
    output_name='eval/classifier/classifier/logits/add:0'):

  graph_def = tf.GraphDef()
  graph_def.ParseFromString(open(graph_path).read())
  def _load(_name):
    return tf.import_graph_def(graph_def,
        input_map={_name: input_tensor},
        name=name,
        return_elements=[output_name])
  try:
    output = _load(input_name)
  except Exception as ioe:
    print("loading error:", str(ioe), "trying new version", file=sys.stderr)
    output = _load(input_name+':1')

  return output[0]

def _distill_targets():
  images = tf.get_collection("INPUT_IMAGES")[-1]
  graph_path = FLAGS.loss_param.split(':')
  if len(graph_path) > 2:
    # the first two are consumed by resolve_path
    image_size = int(graph_path[2])
    images = tf.image.resize_images(images, image_size, image_size)
  graph_path = ops.resolve_path(FLAGS.loss_param)
  privileged_logits = _load_graph(graph_path, images)

  if FLAGS.temperature > 1.:
    privileged_logits /= FLAGS.temperature

  privileged_targets = tf.sigmoid(tf.stop_gradient(privileged_logits))
  return privileged_targets

def _get_kb_guide(guided_npy_path):
  """
  res = L1 x L1
  1. res[i, j] = 1 if (i, j) share parent and i is a clean label, or i = j
  2. normalize res, such that sum(res[:, j]) = 1
  """
  res = np.load(guided_npy_path)
  if FLAGS.trainable_kb_guide:
    with tf.variable_scope("kb-guide-learn"):
      w = tf.get_variable('weights', res.shape,
          initializer=tf.constant_initializer(1))
      optim.set_var_lr(w, 0.01)

      # softmax will make sure w is in the range of [0,1]
      # mask after the exp
      mask = (res > 0).astype(np.float32)

      if FLAGS.off_diag_only:
        np.fill_diagonal(mask, 0)
        all_zero_cols = np.where(mask.sum(axis=0)==0)[0]
        mask[all_zero_cols, all_zero_cols] = 1

      w_exp = tf.exp(w)
      w_exp *= mask.astype(np.float32)

      w = w_exp / tf.reduce_sum(w_exp, 1, keep_dims=True)
      return w
  else:
    return tf.constant(res, dtype=tf.float32)


@loss
def label_smooth():
  """
  https://arxiv.org/pdf/1512.00567v3.pdf
  section 7
  """

  epsilon = .1
  def loss(logits, label):
    label_map_l1 = tf.one_hot(label, NUM_L1_CLASSES())

    if len(FLAGS.kb_guided_npy) > 0:
      assert not FLAGS.trainable_kb_guide, 'label smooth does not take trainable guides at this point'
      kb_guided_npy = ops.resolve_path(FLAGS.kb_guided_npy)
      res = np.load(kb_guided_npy) > 0
      res = res.astype(np.float32)

      if FLAGS.label_smooth_unknown_ratio > 0:
        unknown = np.load(kb_guided_npy) == 0
        unknown = unknown.astype(np.float32)
        res += FLAGS.label_smooth_unknown_ratio * unknown

      kb_guide = res / res.sum(axis=0, keepdims=True)
      delta = tf.matmul(label_map_l1, kb_guide.T)
    else:
      delta = tf.ones_like(label_map_l1)/NUM_L1_CLASSES()

    soft_target = (1-epsilon) * label_map_l1 + epsilon * delta
    return ops.cross_entropy(logits, soft_target)

  return loss, NUM_L1_CLASSES()

def _get_M():
  M = np.ones((NUM_L1_CLASSES(), NUM_L1_CLASSES()), dtype=np.float32)
  np.fill_diagonal(M, 0)
  return M

@loss
def mll_ce_l1_distill():
  """
  Reference: http://arxiv.org/abs/1511.03643
  """

  beta = .2 # the lambda in (4)

  def _privileged_targets():
    privileged_targets = _distill_targets()

    if len(FLAGS.kb_guided_npy) > 0:
      kb_guided_npy = ops.resolve_path(FLAGS.kb_guided_npy)
      kb_guide = _get_kb_guide(kb_guided_npy)
      privileged_targets = tf.matmul(privileged_targets, kb_guide)

    return privileged_targets

  def loss(logits, label):
    label_map_l1 = tf.one_hot(label, NUM_L1_CLASSES())
    primary_loss = ops.cross_entropy(logits, label_map_l1)

    privileged_targets = _privileged_targets()
    privileged_loss = ops.cross_entropy(logits, privileged_targets)
    return (1-beta)*primary_loss + beta*privileged_loss

  def infer(logits):
    privileged_targets = _privileged_targets()
    xent = tf.nn.sigmoid_cross_entropy_with_logits(logits,
         privileged_targets)

    # log(sigmoid(x)) = log(1/(1+exp(-x))) = -log(1+exp(-x))
    # log(1-sigmoid(x)) = log(exp(-x)/(1+exp(-x))) = -x-log(1+exp(-x))
    # _inner = log(1+exp(-x)), if x < 0, use, -x + log(1+exp(x))
    _inner = 1+tf.exp(-logits)
    _inner = 1+tf.exp(-tf.abs(logits))
    _inner = tf.log(_inner) + tf.nn.relu(-logits)

    log_probs = -_inner
    log_neg_prob = -logits-_inner

    M = _get_M()
    primary = log_probs + tf.matmul(log_neg_prob, M)

    # primary = -loss_primay
    # infer should output score, i.e., the larger, the better
    # xent = loss
    # should primary - xent is the -loss total,
    return (1-beta)*primary - beta*xent

  return loss, NUM_L1_CLASSES(), infer if FLAGS.argmin_distill else "equal"

this_dir = osp.dirname(__file__)
sys.path.append(osp.join(this_dir, './hex/scripts/'))
from infer import hex_infer, hex_loss
def _jtree_path():
  return osp.join(graph_base(), 'jtree_%s.pb' % FLAGS.rtype)

@loss
def hex():
  _graph_path = _jtree_path()
  def loss(logits, label):
    return hex_loss(logits, label,
        graph_path=_graph_path)

  def infer(logits):
    res = hex_infer(logits,
        graph_path=_graph_path)
    return tf.slice(res, [0,0],[-1,NUM_L1_CLASSES()])

  return loss, NUM_L2_CLASSES(), infer

def PRL(label, hie_labels, HIDDEN_SIZE=100, normalize=True):
  label_combined = tf.pack(hie_labels, 2)

  label_embs = tf.get_variable("label_embs", [NUM_L1_CLASSES(), HIDDEN_SIZE])
  label_h1 = tf.nn.embedding_lookup(label_embs, label)

  h2 = ops.fc_layer(label_h1, HIDDEN_SIZE//2, 'proj_h1')
  h2 = tf.nn.relu(h2)
  alpha = ops.fc_layer(h2, len(hie_labels), 'proj_h2')
  alpha = tf.nn.relu(alpha)

  if normalize:
    weights = tf.nn.softmax(alpha)
  else:
    weights = alpha

  # update to NUM_L1_CLASSES() weights is saved
  # the other dimensions are just copies
  tf.add_to_collection("rtype_weights", weights[:NUM_L1_CLASSES(), :])
  weights = tf.reshape(weights, [-1, 1, len(hie_labels)])
  targets = tf.reduce_sum(weights * label_combined, 2)
  return targets

def prl(label_func):

  """
  label_func: returns the list of label maps to be weighted

  'secret sauce':
  if 'single' in label_func.func_name, the loss depends on rtype
  if 'resnet' in label_func.func_name, shorted connection is used, also depends on rtype
  """

  name = label_func.func_name
  def loss(logits, label):
    hie_labels = label_func(label)
    with tf.variable_scope("prl") as scope:
      targets = PRL(label, hie_labels)
      tvars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES,
          scope=scope.name)
      optim.set_var_lr(tvars, 0.01)

      if 'resnet' in name:
        res_label = _RELABEL()(label, FLAGS.rtype)
        targets += res_label
        targets /= 2
    return ops.cross_entropy(logits, targets)

  def wrap():
    return loss, NUM_L2_CLASSES(), 'argmin'

  wrap.__name__ = 'prl_' + name
  return wrap

@loss
@prl
def single(label):
  return [_RELABEL()(label, FLAGS.rtype), tf.one_hot(label, NUM_L2_CLASSES())]

def _get_full_relabels(label):
  res = []
  for rtype in RTYPES():
    res.append(_RELABEL()(label, rtype))
  return res

@loss
@prl
def full(label):
  return _get_full_relabels(label)

@loss
@prl
def full_bias(label):
  res = []
  for rtype in RTYPES():
    res.append(_RELABEL()(label, rtype))
  res.append(tf.one_hot(label, NUM_L2_CLASSES()))
  return res

@loss
@prl
def resnet_full(label):
  res = []
  for rtype in RTYPES():
    res.append(_RELABEL()(label, rtype))
  return res

def get_loss_list(typ):
  loss_names = FLAGS.loss_param.split(',')
  LOSS = []
  for idx in range(0, len(loss_names), 2):
    LOSS.append((configs[loss_names[idx]], typ(loss_names[idx+1])))

  assert len(set([f[0].logits_size for f in LOSS])) == 1, "only one logits is allowed with " + FLAGS.loss_param
  assert LOSS[0][0].logits_size == NUM_L2_CLASSES(), "only L2 logits supported with loss_param"

  return LOSS

def _fc_dis(real, fake):
  def _discriminator(z):
    def _layer(bottom, nouts, name):
      return tf.nn.relu(ops.fc_layer(bottom, nouts, name))
    h0 = _layer(z, 100, 'h0')
    h1 = _layer(h0, 50, 'h1')
    h2 = _layer(h1, 20, 'h2')
    pred = _layer(h2, 1, 'h3')
    return pred
  with tf.variable_scope("discriminator") as scope:
    real_pred = _discriminator(real)
    tvars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES,
        scope=scope.name)

  with tf.variable_scope("discriminator", reuse=True):
    fake_pred = _discriminator(fake)

  d_loss_real = ops.cross_entropy(real_pred, tf.ones_like(real_pred))
  d_loss_fake = ops.cross_entropy(fake_pred, tf.zeros_like(fake_pred))
  g_loss = ops.cross_entropy(fake_pred, tf.ones_like(fake_pred))
  d_loss = d_loss_real + d_loss_fake
  return g_loss, d_loss, tvars

@loss
def label_gan():

  def loss(logits, label):
    label_map = _RELABEL()(label, FLAGS.rtype)
    loss_mll = ops.cross_entropy(logits, label_map)

    hie_labels = _get_full_relabels(label)
    primary_vars = tf.trainable_variables()

    with tf.variable_scope("prl") as scope:
      targets = PRL(label, hie_labels, normalize=False)
      prl_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES,
        scope=scope.name)

    g_loss, d_loss, d_vars = _fc_dis(logits, targets)
    disjoint_loss_list = [
        (loss_mll, 1, primary_vars),
        (g_loss, 1, prl_vars),
        (g_loss, .1, primary_vars),
        (d_loss, 1, d_vars),
        ]
    return disjoint_loss_list

  def infer(logits):
    return tf.slice(logits, [0, 0], [-1, NUM_L1_CLASSES()])

  return loss, NUM_L2_CLASSES(), infer

@loss
def mll_ce_l1_bootstrap():
  beta = .8

  def loss(logits, label):

    def _threshold(scores):
      return tf.cast(scores > .5, dtype=tf.float32)

    guided_logits = tf.stop_gradient(tf.sigmoid(logits))
    if FLAGS.kb_guided_npy:
      kb_guided_npy = ops.resolve_path(FLAGS.kb_guided_npy)
      kb_guide = _get_kb_guide(kb_guided_npy)

      if FLAGS.bootstrap_guide_z:
        z = _threshold(guided_logits)
        z = tf.matmul(z, kb_guide)
      else:
        guided_logits = tf.matmul(guided_logits, kb_guide)
        z = _threshold(guided_logits)
    else:
      z = tf.cast(guided_logits > .5, dtype=tf.float32)
    label_map_l1 = tf.one_hot(label, NUM_L1_CLASSES())
    lm_combine = label_map_l1 * beta + z * (1-beta)
    return ops.cross_entropy(logits, lm_combine)

  return loss, NUM_L1_CLASSES()

def _reweight(Dp, label_map):
  batch_size = Dp.get_shape()[0].value

  # \rho_{+1}
  Dp_0 = tf.reduce_min(Dp, 0, keep_dims=True)
  Dp_0 = tf.tile(Dp_0, [batch_size, 1])

  # \rho_{-1}
  Dp_1 = tf.reduce_min(1-Dp, 0, keep_dims=True)
  Dp_1 = tf.tile(Dp_1, [batch_size, 1])

  cond = label_map>.5

  # P_{D_\rho}(\hat{Y}|X)
  Dp_hat = tf.select(cond, Dp, 1-Dp)
  Dp_hat += 1e-6

  # \rho_{-\hat{Y}}
  rho_hat_inv = tf.select(cond, Dp_0, Dp_1) #

  # \beta
  beta_numerator = (Dp_hat - rho_hat_inv)
  beta_denom = (1-Dp_1-Dp_0) * Dp_hat
  beta = beta_numerator / beta_denom
  return beta

@loss
def distill_reweight():

  def loss(logits, label):
    label_map = tf.one_hot(label, NUM_L1_CLASSES())
    Dp = _distill_targets()
    beta = _reweight(Dp, label_map)
    return ops.cross_entropy(logits, label_map, beta)

  return loss, NUM_L1_CLASSES()

@loss
def mll_ce_l1_reweight():

  def loss(logits, label):
    label_map = tf.one_hot(label, NUM_L1_CLASSES())
    primary_vars = tf.trainable_variables()

    with tf.variable_scope('noise') as scope:
      Dp_logits = ops.fc_layer(tf.nn.relu(logits),
          NUM_L1_CLASSES(), "noise_logitis")
      Dp_loss = ops.cross_entropy(Dp_logits, label_map)
      Dp_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES,
          scope=scope.name)

    with tf.name_scope("reweight",
        values=[logits, label_map, Dp_logits]):
      Dp = tf.sigmoid(tf.stop_gradient(Dp_logits))
      beta = _reweight(Dp, label_map)

    primary_loss = ops.cross_entropy(logits, label_map, beta)
    disjoint_loss_list = [
        (primary_loss, 1, primary_vars),
        (Dp_loss, 1, Dp_vars),]
    return disjoint_loss_list

  return loss, NUM_L1_CLASSES()

@loss
def label_l2():

  def loss(logits, label):
    label_map = _RELABEL()(label, FLAGS.rtype)
    loss_mll = ops.cross_entropy(logits, label_map)

    hie_labels = _get_full_relabels(label)
    primary_vars = tf.trainable_variables()

    with tf.variable_scope("prl") as scope:
      targets = PRL(label, hie_labels, normalize=False)
      prl_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES,
        scope=scope.name)

    prl_loss = tf.nn.l2_loss(targets-logits)
    disjoint_loss_list = [
        (loss_mll, 1, primary_vars),
        (prl_loss, 1, prl_vars),
        (prl_loss, .1, primary_vars),
        ]
    return disjoint_loss_list

  def infer(logits):
    return tf.slice(logits, [0, 0], [-1, NUM_L1_CLASSES()])

  return loss, NUM_L2_CLASSES(), infer

@loss
def hybrid():

  # loss_name,mix_rate
  def loss(logits, label):
    LOSS = get_loss_list(float)
    config, weight = LOSS[0]
    res = config._loss_func(logits, label) * weight
    for config, weight in LOSS[1:]:
      res += config._loss_func(logits, label) * weight
    return res

  return loss, NUM_L2_CLASSES(), 'argmin'

@loss
def mix():
  """
  multi task loss, loss_param should specify the actual losses and the mixing rate
  the first loss is used in inference time

  the loss should share the same logits_size

  e.g.:
  prl_full,100,mll_ce,1
  """

  # deferred eval
  def loss(logits, label):
    LOSS = get_loss_list(int)
    res = []
    for config, mix in LOSS:
      res.append((config.loss(logits, label)[0][0], mix))
    return res

  def infer(logits):
    LOSS = get_loss_list(int)
    return LOSS[0][0].infer(logits)

  return loss, NUM_L2_CLASSES(), infer

def load_loss(loss_name=None):
  if loss_name is None:
    loss_name = FLAGS.loss
  return configs[loss_name]

class TestLoss(object):
  batch_size = 10

  _CONFIG = None

  def setUp(self):
    tf.reset_default_graph()
    FLAGS.loss_param = 'prl_full,50,mll_ce,1'
    if 'distill' in self._CONFIG.name.lower():
      FLAGS.loss_param = 'abs:' + osp.join(os.environ.get("DATA_DIR"),
          'data/mll_ce_l1.distill.pb')
      images = tf.random_normal([self.batch_size, 28, 28, 3])
      tf.add_to_collection("INPUT_IMAGES", images)
    _cache.clear()

  def _gen_data(self):
    logits = tf.random_normal([self.batch_size, self._CONFIG.logits_size])
    label = tf.random_uniform([self.batch_size],
        maxval=NUM_L1_CLASSES(), dtype=tf.int64)
    return logits, label

  def test_loss_shape(self):
    logits, label = self._gen_data()
    loss = self._CONFIG.loss(logits, label)
    with self.test_session() as sess:
      sess.run(tf.initialize_all_variables())
      for l in loss:
        l = l[0]
        loss_ = l.eval()
        self.assertEqual(loss_.shape, ())
        self.assertLess(0, loss_)

  def test_infer_shape(self):
    logits, _ = self._gen_data()
    probs = self._CONFIG.infer(logits)

    with self.test_session() as sess:
      sess.run(tf.initialize_all_variables())
      probs_ = probs.eval()
      self.assertEqual(probs_.shape, (self.batch_size, NUM_L1_CLASSES()))

  def test_consistency(self):
    """
    loss(x_gt, y_gt) < loss(x, y_gt)
    """
    blacklist = ['label', 'reweight', 'distill', 'noise']
    for term in blacklist:
      if term in self._CONFIG.name.lower():
        return
    logits, label = self._gen_data()
    label_map = tf.one_hot(label, self._CONFIG.logits_size)
    logits_better = logits + label_map*5

    with tf.variable_scope("loss"):
      loss = self._CONFIG.loss(logits, label)

    with tf.variable_scope("loss", reuse=True):
      loss_gt = self._CONFIG.loss(logits_better, label)

    with self.test_session() as sess:
      sess.run(tf.initialize_all_variables())
      for l, l_gt in zip(loss, loss_gt):
        l = l[0]
        l_gt = l_gt[0]
        loss_, loss_gt_ = sess.run([l, l_gt])
        self.assertLess(loss_gt_, loss_)


# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
