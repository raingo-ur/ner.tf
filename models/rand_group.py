#!/usr/bin/env python
"""
split input file image-path label
into groups according to the labels
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
from collections import defaultdict
import random
import math

def main():
  label_file = sys.argv[1]
  label_dir = osp.dirname(label_file)
  label_file_name = osp.splitext(label_file)[0]

  group = defaultdict(list)
  with open(label_file) as reader:
    for line in reader:
      fields = line.strip().split()
      group[fields[1]].append(fields[0])

  key_order_path = osp.join(label_dir, 'key-order')
  if not osp.exists(key_order_path):
    keys = group.keys()
    random.shuffle(keys)
    with open(key_order_path, 'w') as writer:
      for key in keys:
        print(key, file=writer)
  else:
    keys = []
    with open(key_order_path) as reader:
      for line in reader:
        keys.append(line.strip())

  num_keys = len(keys)
  num_groups = 4
  num_key_per_group = int(math.ceil(num_keys / num_groups))

  for i in range(num_groups):
    images = []
    for key in keys[i*num_key_per_group:(i+1)*num_key_per_group]:
      for image in group[key]:
        images.append((image, key))

    random.shuffle(images)
    save_path = label_file_name + "-G%d.txt" % i
    with open(save_path, 'w') as writer:
      for image, key in images:
        print(image, key, file=writer)

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
